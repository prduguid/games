

## Install Netcool

## Install apache httpd

### Install gcc

    sudo yum install gcc

## Install Perl

### Install cpan

* install

        sudo yum install cpan

* run 

        sudo cpan

* configure 
    accept defaults

* update

        install CPAN

* reload

        reload cpan

* quit

        quit

## Install Perl Modules

    sudo cpan -f install Log::Log4perl          ## this takes a long time
    sudo cpan -f install Mail::Sendmail 
    sudo cpan -f install HTML::Entities 
    sudo cpan -f install HTML::Element
    sudo cpan -f install IO::Prompt 
    sudo cpan -f install Net::Syslog 
    sudo cpan -f install Net::SNMP 
    sudo cpan -f install JSON

## stage PRD:: Perl modules
  
    sudo cp -pr PRD/ /usr/share/perl5/.
    sudo chown --recursive root:root /usr/share/perl5/PRD