-- c log file
CREATE OR REPLACE file monopoly '/opt/netcool/omnibus/log/monopoly.log' maxfiles 3 maxsize 20971520 BYTES;

-- cdb monopoly
create database monopoly;
go

-- cv monopoly.users view
create view monopoly.users persistent as 
   select UserID, UserName, FullName from security.users where UserID in ( 
      select UserID from security.group_members where GroupID in ( 
         select GroupID from security.groups where GroupName in ( 'monopolyUser') 
      ) 
   );
go

-- g security
grant select on table alerts.status to role 'monopolyUser';
grant select on table security.users to role 'monopolyUser';
grant select on table security.group_members to role 'monopolyUser';
grant select on table security.groups to role 'monopolyUser';
grant select on view monopoly.users to role 'monopolyUser';
go

-- ct inventory
create table monopoly.inventory persistent (
   item        varchar(24) primary key,
   type        varchar(24),
   qty         integer,
   json        varchar(4096), 
   text        varchar(128)
);
go
-- g inventory
grant select on table monopoly.inventory to role 'monopolyUser';
go


-- ct inventory_deeds
create table monopoly.inventory_deeds persistent (
   title             varchar(24) primary key,
   subtitle          varchar(24),
   sets              varchar(24),
   colourSet         Boolean,
   setOrder          integer,
   setSize           integer,
   boardPlace        integer,
   htmlColour        varchar(24),
   icon              varchar(24),
   purchase          integer,
   mortgage          integer,
   unmortgage        integer,    -- 110% of mortgage
   mortgagedTxFee    integer,    -- 10% of mortgage, charged when transfering a mortgaged deed to another player
   rent_0            integer,
   rent_1            integer,
   rent_2            integer,
   rent_3            integer,
   rent_4            integer,
   rent_5            integer,
   cost_building     integer,
   refund_building   integer    -- 50% of cost_building
);
go

-- g inventory_deeds
grant select on table monopoly.inventory_deeds to role 'monopolyUser';
go

-- ct board
create table monopoly.board persistent (
   place    integer primary key,
   title    varchar(24),
   subtitle varchar(64),
   rule     varchar(24),
   sets     varchar(24),
   icon     varchar(64)
);
go

-- g board
grant select on table monopoly.board to role 'monopolyUser';
go

-- ct games
create table monopoly.games persistent (
   gameId                  incr primary key,
   start                   time,
   moves                   integer,
   state                   integer,    -- 0 inactive, 1 active, 2 blocked (manual intervention required), 3 completed
   activePlayer            integer,    -- the userid of the active player
   activeMoveDice0         integer,    -- the last dice thrown value
   activeMoveDice1         integer,    -- the last dice thrown value
   action                  char(20),   -- chance, communityChest, superTax, incomeTax, rentDemanded, paymentDemanded, forSale, auction, etc...
   banker                  integer,
   cardsChance             char(64),   -- CH12CH00CH04.... etc
   cardsCChest             char(64),
   cardsChancePointer      integer,    -- 0 .. 15
   cardsCChestPointer      integer,
   cardsChance4Move        integer,    -- the move that the point was set for
   cardsCChest4Move        integer,
   cardsChanceGoojfOwner   integer,    -- the owner of the get out of jail free card (if any)
   cardsCChestGoojfOwner   integer,
   touch                   integer     -- increments on row update by db row trigger
);
go

-- g games
grant update on Table monopoly.games to role 'monopolyCardRandomiser';
grant select on Table monopoly.games to role 'monopolyCardRandomiser', 'monopolyUser';
go


-- ct players
create table monopoly.players persistent (
   identifer varchar(11) primary key,         -- concatenation of gameId-uid (5-5)
   gameId    integer,
   uid       integer,
   avatar    varchar(12),
   sequence  integer,
   cash      integer,
   isActive  boolean,
   jailTime  integer,
   position  integer,
   doubleRun integer
);
go

-- g players
grant select on table monopoly.players to role 'monopolyUser';
go

-- ct deeds
create table monopoly.deeds persistent (
   identifer  varchar(30) primary key,      -- concatenation of gameId-title (5-24)
   gameId     integer,
   title      varchar(24),
   owner      integer,
   state      integer,  -- -1 = mortgaged, 0 = no buildings, 1 = 1 house, 2 = 2 houses, 3 = 3 houses, 4 = 4 houses, 5 = 1 hotel 
   sets       varchar(9),
   boardPlace integer
);
go

-- g deeds
grant select on table monopoly.deeds to role 'monopolyUser';
go

