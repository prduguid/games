-- e schema
call procedure monopoly_event( 'schema.sql', -1, -1, -1, '', 5, 'the monopoly schema is being removed.', false, '', '' );
go

-- d monopoly_auctionDeedBid
drop procedure monopoly_auctionDeedBid;
go

-- d monopoly_auctionDeedStart
drop procedure monopoly_auctionDeedStart;
go

-- d monopoly_enactCard
drop procedure monopoly_enactCard;
go

-- d monopoly_playerAdvanceOrRetreat
drop procedure monopoly_playerAdvanceOrRetreat;
go

-- d monopoly_playerMove
drop procedure monopoly_playerMove;
go 

-- d monopoly_drawCard
drop procedure monopoly_drawCard;
go

-- d monopoly_tradeGOOJFC
drop procedure monopoly_tradeGOOJFC;
go

-- d monopoly_deedSetLiquidate
drop procedure monopoly_deedSetLiquidate;
go

-- d monopoly_deedManageState
drop procedure monopoly_deedManageState;
go

-- d monopoly_houses
drop procedure monopoly_houses;
go

-- d monopoly_hotels
drop procedure monopoly_hotels;
go

-- d monopoly_sellDeedToPlayer
drop procedure monopoly_sellDeedToPlayer;
go

-- d monopoly_player_jailTime
drop procedure monopoly_player_jailTime;
go

-- d monopoly_player_setAvatar
drop procedure monopoly_player_setAvatar;
go

-- d monopoly_creditToPlayer
drop procedure monopoly_creditToPlayer;
go

-- d monopoly_removePlayer
drop procedure monopoly_removePlayer;
go

-- d monopoly_setBanker
drop procedure monopoly_setBanker;
go 

-- d monopoly_newPlayer
drop procedure monopoly_newPlayer
go

-- d monopoly_setGameState
drop procedure monopoly_setGameState;
go

-- d monopoly_demandPayment
drop procedure monopoly_demandPayment;
go

-- d monopoly_payDemand
drop procedure monopoly_payDemand;
go

-- d monopoly_demandRent
drop procedure monopoly_demandRent;
go

-- d monopoly_payRent
drop procedure monopoly_payRent;
go


-- d monopoly_nextPlayersTurn
drop procedure monopoly_nextPlayersTurn;
go

-- d monopoly_newGame
drop procedure monopoly_newGame;
go 

-- d monopoly_deedsRowUpdate
drop trigger monopoly_deedsRowUpdate;
go

-- d monopoly_playerRowUpdate
drop trigger monopoly_playerRowUpdate;
go

-- d monopoly_gameRowUpdate
drop trigger monopoly_gameRowUpdate;
go

-- d monopoly_newGameRow
drop trigger monopoly_newGameRow;
go

-- d trigger group
drop trigger group monopoly;
go

-- d monopoly_cardRansomiser
drop procedure monopoly_cardRansomiser;
go

-- dp monopoly_event
drop procedure monopoly_event;
go

-- dp monopoly_eventSerial
drop procedure monopoly_eventSerial;
go

