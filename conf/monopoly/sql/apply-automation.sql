-- monopoly_eventSerial
create or replace procedure monopoly_eventSerial(in out serial integer) begin
   for each row eventSerial in monopoly.inventory where eventSerial.type = 'serial' and eventSerial.item = 'eventSerial' begin
      set eventSerial.qty = eventSerial.qty + 1;
      set serial = eventSerial.qty;
   end;
   if( serial = -1 ) then 
      insert into monopoly.inventory (item, type, qty) values ('eventSerial','serial',1);
      set serial = 1;
   end if;
end;
go


-- monopoly_event
create or replace procedure monopoly_event(in component Char(32), in gameId Integer, in moveSerial Integer, in userId Integer, in deed Char(32), in severity Integer, in txt Char(256), in doToast Boolean, in toastHeader Char(64), in toastBody Char(64)) 
-- SQL procedure to create an event
declare node Char(64); suppressescl Integer; serial Integer;
begin
   set serial     = -1;
   call procedure monopoly_eventSerial(serial);
      
   if( gameId >= 0 ) then
      set node = to_char(gameId);
   else
      set node = 'unspecificed';
   end if;

   if( doToast = true ) then 
      set suppressescl = 1;      -- Escalated
   else
      set suppressescl = 4;      -- Suppressed
   end if;

   insert into alerts.status ( Identifier, Type, Class, Manager, Agent, ExpireTime, Node, Grade, OwnerUID, AlertGroup, AlertKey, Severity, Summary, Poll, SuppressEscl, ProbeSubSecondId, TEXT01, TEXT02 ) 
   values ( 
      'monopoly_event: '+to_char(serial)+' (gameId=' + to_char(gameId) + ', moveSerial=' + to_char(moveSerial) + ', userId=' + to_char(userId) +  ', deed="' + deed + '", component="' + component, 
      1, 1500, 'games', 'Monopoly', 60*60*24*12, node,  gameId, userId, component, deed, severity, txt, moveSerial, suppressescl, serial, toastHeader, toastBody
   );
   write into monopoly values ( '[' + to_char(getdate) + ']['+component + ' (via monopoly_event)][gameId=' + to_char(gameId) + ', moveSerial=' + to_char(moveSerial) + ', userId=' + to_char(userId) +  ', deed="' + deed + '", doToast=' + to_char(doToast) + '] ' + txt);
end;
call procedure monopoly_event( 'schema-apply.sql', -1, -1, -1, '', 4, 'Applying the Monopoly schema.', false, '','' );
go

-- g monopoly_event
grant execute on sql procedure monopoly_event to role 'monopolyCardRandomiser', 'monopolyUser';
go

-- c trigger group
create or replace trigger group monopoly;
go
-- c monopoly_cardRansomiser
create or replace procedure monopoly_cardRansomiser (in gameId Integer) executable '/usr/share/nginx/html/games/monopoly/monopolyCardRandomiser.pl' 
   host  'localhost' 
   user 20 
   group 20 
   arguments '--game='+to_char(gameId)+' --host=localhost --username=monopolyCardRandomiser --pass=monopolyCardRandomiser';
go

-- ct monopoly_newGameRow
create or replace trigger monopoly_newGameRow
   group monopoly
   priority 1
   comment 'When a new game record is inserted, new records in the deeds and cards table must also be inserted'
   before insert on monopoly.games for each row 
   declare tag char(128); i integer;
   begin
      set tag = '[' + to_char(getdate) + '][monopoly_newGameRow trigger] ';
      set i = 0;
      for each row deeds in monopoly.inventory_deeds begin
         insert into monopoly.deeds ( identifer, gameId, title, owner, state, sets, boardPlace ) values ( to_char(new.gameId) + '-' + deeds.title, new.gameId, deeds.title,-1,0,deeds.sets, deeds.boardPlace );
         set i = i + 1;
      end;
      call procedure monopoly_cardRansomiser( new.gameId );
      call procedure monopoly_event( 'monopoly_newGameRow', new.gameId, -1, 0, '', 1, 'new game created, title deeds, chance and community chest cards populated for game.', false, '', '' );
   end;
go

-- ct monopoly_gameRowUpdate
create or replace trigger monopoly_gameRowUpdate group monopoly priority 1 comment 'increments touch for every update to the row' before update on monopoly.games for each row 
begin
   if( new.touch < 1 ) then set new.touch = 1; end if;
   if( new.touch = old.touch ) then set new.touch = new.touch + 1; end if; 
end;
go
-- ct monopoly_playerRowUpdate
create or replace trigger monopoly_playerRowUpdate group monopoly priority 1 comment 'increments touch for every update to the row' after update on monopoly.players for each row 
   begin update monopoly.games via new.gameId set touch = touch + 1; end
go
-- ct monopoly_deedsRowUpdate
create or replace trigger monopoly_deedsRowUpdate group monopoly priority 1 comment 'increments touch for every update to the row' after update on monopoly.deeds for each row 
   begin update monopoly.games via new.gameId set touch = touch + 1; end
go

-- cp monopoly_newGame
create or replace procedure monopoly_newGame(in foo Integer) 
-- creates a new record repesenting a game into monopoly.game
-- populates records repesenting the deeds into monopoly.deeds
-- sql procedures can't have zero parameters, foo is ignored
declare tag char(128);
begin
   set tag = '[' + to_char(getdate) + '][monopoly_newGame()] ';
   write into monopoly values (tag, 'Creating a new game' );
   insert into monopoly.games ( start, moves, state, activePlayer, banker, cardsChanceGoojfOwner, cardsCChestGoojfOwner, touch ) values ( 0, 0, 0, -1, -1, -1, -1, 0 );
   -- the monopoly_newGameRow will then create the records in the monopoly.deeds table with the gameId.
end;
go

-- g monopoly_newGame
grant execute on sql procedure monopoly_newGame to role 'monopolyUser';
go


-- cp monopoly_nextPlayersTurn
create or replace procedure monopoly_nextPlayersTurn(in gameId Integer)
declare tag char(128); found Integer; nextPlayerUid Integer; currentPlayerSequence Integer; counter Integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_nextPlayersTurn()] ';
   write into monopoly values (tag, 'gameId: ' + to_char(gameId) );
   set nextPlayerUid = -1;
   for each row game in monopoly.games where game.gameId = gameId and game.state in (1,2) begin
      set found = found + 1;
      write into monopoly values (tag,' GAME: gameId: ' + to_char(game.gameId) + ', action: "' + game.action + '", state: ' + to_char(game.state) + ', Dice: ' + to_char(game.activeMoveDice0) + ', ' + to_char(game.activeMoveDice1) );

      if( game.activeMoveDice0=game.activeMoveDice1 and game.action != 'jailed' ) then
         -- player threw a double, they get another go
         -- NB: if the player has thrown 2 doubles, then on this next throw if they throw a double again, the monopoly_playerMove procedure will send them to jail and will have set game.action to "jailed"
         call procedure monopoly_event( 'monopoly_nextPlayersTurn', gameId, game.moves, game.activePlayer, '', 3, 'Player threw a double ('+to_char(game.activeMoveDice0)+' and '+to_char(game.activeMoveDice1)+') and still has another go.', false, '','' );
         set game.state    = 1;
         set game.action   = '';
      else
         -- player did not throw a double their turn is over, find the next player in sequence

         set currentPlayerSequence = -1;
         set nextPlayerUid = -1;

         for each row player in monopoly.players where player.gameId = gameId and player.uid = game.activePlayer begin
            set currentPlayerSequence = player.sequence;
         end;

         if( currentPlayerSequence = -1 ) then 
            write into monopoly values (tag, 'failed determine the seq of the current player' );
            cancel;
         end if;
         
         -- attmpt 1 to select next player
         for counter = currentPlayerSequence to 5 do begin
            for each row nextplayer in monopoly.players where nextplayer.gameId = gameId and nextplayer.uid <> game.activePlayer and nextplayer.isActive = true and nextplayer.sequence = counter begin
               set nextPlayerUid = nextplayer.uid;
            end;
            if( nextPlayerUid > -1 ) then
               break;
            end if;
         end;

         if( nextPlayerUid > -1 ) then
            -- found on attempt 1
            set game.activePlayer = nextPlayerUid
         else 
            -- not found on attempt 1, try attmpt 2 to select next player
            for counter = 0 to currentPlayerSequence do begin
               for each row nextplayer in monopoly.players where nextplayer.gameId = gameId and nextplayer.uid <> game.activePlayer and nextplayer.isActive = true and nextplayer.sequence = counter begin
                  set nextPlayerUid = nextplayer.uid;
               end;
               if( nextPlayerUid > -1 ) then
                  break;
               end if;
            end;
            
            if( nextPlayerUid > -1 ) then
               set game.activePlayer = nextPlayerUid
            else 
               write into monopoly values (tag, 'failed to find the next player in sequence' );
               cancel;
            end if;
         end if;

         if( game.state = 2 ) then 
            call procedure monopoly_event( 'monopoly_nextPlayersTurn', gameId, game.moves, -1, '', 1, 'game resumed.', false, '','' );
         end if;
         set game.state    = 1;
         set game.action   = '';

         -- finally decrement the age for all active entries in the rent ledger (0=missed, 1=due(last chance), 2=due)
         -- finally decrement the "age" for all active rent due entries in the rent ledger INT03 [0=missed, 1=due(last chance), 2=due]
         update alerts.status set INT03 = 0, Severity = 2 where Class = 1501 and INT03 = 1 and Severity = 3 and AlertGroup = 'rent';
         update alerts.status set INT03 = 1, Summary = 'The player landed on a property but the owner missed their chance to demand rent' where Class = 1501 and INT03 = 2 and Severity = 3 and AlertGroup = 'rent';
   
      end if;
   end;


   if( found < 1 ) then 
      write into monopoly values (tag, 'game not found' );
      cancel;
   end if;   
end;

-- g monopoly_nextPlayersTurn
grant execute on sql procedure monopoly_nextPlayersTurn to role 'monopolyUser';
go



-- cp monopoly_setGameState
create or replace procedure monopoly_setGameState(in gameId Integer, in state char(10)) 
-- states
-- 0        inactive
-- 1        active
-- 2        blocked (manual intervention required)
-- 3        complete
--
-- legal state changes 
-- activate    0 -> 1      (only if 2+ players have been added)
-- deactivate  1 -> 0
-- complete    x -> 3
-- block       1 -> 2
-- resume      2 -> 1
--
declare tag char(128); found Integer; players Integer; nextPlayerUid Integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_setGameState()] ';
   write into monopoly values (tag, 'gameId: ' + to_char(gameId) + ', state: "'+state+'"' );
   set players = 0;
   set nextPlayerUid = -1;
   for each row game in monopoly.games where game.gameId = gameId begin
      set found = found + 1;
      if( ( state = 'activate' or state = 'active' ) and game.state = 0 ) then
         for each row player in monopoly.players where game.gameId = player.gameId begin
            set players = players + 1;
            if( player.sequence = 0 ) then 
               set nextPlayerUid = player.uid;
            end if;
         end;
         if( players >= 2 ) then
            set game.state = 1; -- active
            set game.action = '';
            call procedure monopoly_event( 'monopoly_setGameState', gameId, game.moves, -1, '', 4, 'game activated (player count: '+to_char(players)+').', false, '', '' );
         else
            write into monopoly values (tag, 'GAME does not have sufficient players to activate (' + to_char(players) + ').' );
            cancel;
         end if;
         if( game.activePlayer = -1 ) then
            set game.activePlayer = nextPlayerUid;
         end if;
      elseif( ( state = 'deactivate' or state = 'deactive' or state = 'inactive' ) and game.state = 1   ) then
         set game.state = 0;
         call procedure monopoly_event( 'monopoly_setGameState', gameId, game.moves, -1, '', 4, 'game deactivated.', false, '','' );
      elseif( state = 'block'       and game.state = 1   ) then
         set game.state = 2;
         call procedure monopoly_event( 'monopoly_setGameState', gameId, game.moves, -1, '', 2, 'game blocked (manual intervention required).', false, '','' );
      elseif( state = 'resume'      and game.state = 2 and game.activeMoveDice0 = game.activeMoveDice1 ) then
         -- player throwed a double, and can have another throw
         set game.state = 1;
         call procedure monopoly_event( 'monopoly_setGameState', gameId, game.moves, -1, '', 1, 'game resumed.', false, '','' );

      elseif( state = 'resume'      and game.state = 2 and game.activeMoveDice0 <> game.activeMoveDice1 ) then
         -- player did not throw a double, next players turn
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif( state = 'complete'                         ) then
         set game.state = 3;
         call procedure monopoly_event( 'monopoly_setGameState', gameId, game.moves, -1, '', 3, 'game completed.', false, '','' );
      else
         write into monopoly values (tag, 'illegal state change requested "'+state+'", current state: ' + to_char(game.state) );
         cancel;
      end if;
   end;
   if( found < 1 ) then 
      write into monopoly values (tag, 'game not found' );
      cancel;
   end if;   
end;
go

-- g monopoly_setGameState
grant execute on sql procedure monopoly_setGameState to role 'monopolyUser';
go

-- cp monopoly_newPlayer
create or replace procedure monopoly_newPlayer(in gameId Integer, in userId Integer, in userAvatar Char(24)) 
-- cannot add players to active or complete games
-- first player to be added will be set as the default banker
-- player sequence will be set in order of adding (zero index)
declare tag char(128); found Integer; players Integer;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_newPlayer()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', userId: ', to_char(userId), ', userAvatar: "', userAvatar ,'"' );

   for each row game in monopoly.games where game.gameId = gameId begin
      write into monopoly values (tag, 'GAME start: ' + to_char(game.start) + ', state: ' + to_char(game.state) +  ', moves: ' + to_char(game.moves) +  ', cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) +  ', cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) );
      set found = found + 1;
      if( game.state = 0) then
         set players = 0;
         for each row player in monopoly.players where player.gameId = game.gameId begin
            if( player.uid = userId ) then 
               write into monopoly values (tag, 'gameId: ' + to_char(gameId) + ', userId: ' + to_char(userId) + ' a record already exist in this game for this uid.' );
               cancel;
            end if;
            set players = players + 1;
         end;

         insert into monopoly.players (
            identifer, gameId, uid, avatar, sequence, cash, isActive, jailTime, position, doubleRun
         ) values (
            to_char(gameId) + '-' + to_char(userId), gameId, userId, userAvatar, players, 1500, true, 0, 0, 0
         );
         call procedure monopoly_event( 'monopoly_newPlayer', gameId, game.moves, userId, '', 2, 'A player has been added to the game.', false, '','' );
         if( game.banker = -1 ) then 
            set game.banker = userId;
            call procedure monopoly_event( 'monopoly_newPlayer', gameId, game.moves, userId, '', 2, 'A player has been set as the banker.', false, '','' );
         end if;
      else 
         write into monopoly values (tag, 'GAME is either active or completed, can not add player');
         cancel;
      end if;
   end;
   if( found < 1 ) then 
      write into monopoly values (tag, 'The game not found' );
      cancel;
   end if;         
end;
go

-- g monopoly_newPlayer
grant execute on sql procedure monopoly_newPlayer to role 'monopolyUser';
go

-- cp monopoly_setBanker
create or replace procedure monopoly_setBanker(in gameId Integer, in userId Integer ) 
declare tag char(128); found Integer; players Integer;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_setBanker()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', userId: ', to_char(userId) );

   for each row game in monopoly.games where game.gameId = gameId begin
      write into monopoly values (tag, 'GAME start: ' + to_char(game.start) + ', state: ' + to_char(game.state) +  ', moves: ' + to_char(game.moves) +  ', banker ' + to_char(game.banker) );
      set found = found + 1;
      if( game.state in ( 0, 1, 2 ) ) then
         if( game.banker <> userId ) then
            set players = 0;
            for each row player in monopoly.players where player.gameId = game.gameId and player.uid = userId begin
               set players = players + 1;
            end;
            if( players = 1 ) then 
               set game.banker = userId;
               call procedure monopoly_event( 'monopoly_setBanker', gameId, game.moves, userId, '', 2, 'A player has been set as the banker.', true, 'Banker role', 'A different player has been nominated as the banker' );
            else 
               write into monopoly values (tag, 'Specified user is not a player in this game');
               cancel;
            end if;
         else 
            write into monopoly values (tag, 'Specified player is already set as the banker for this game');
            cancel;
         end if;
      else 
         write into monopoly values (tag, 'GAME is completed, can not change the banker player');
         cancel;
      end if;
   end;
   if( found < 1 ) then 
      write into monopoly values (tag, 'The game not found' );
      cancel;
   end if;         
end;
go

-- g monopoly_setBanker
grant execute on sql procedure monopoly_setBanker to role 'monopolyUser';
go

-- cp monopoly_removePlayer
create or replace procedure monopoly_removePlayer(in gameId Integer, in userId Integer) 
-- If the game has not yet started the player record is deleted
-- otherwise the players assets are returned and they are marked as inactive
-- similar to bankruptcy

declare tag char(128); found integer;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_removePlayer()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', userId: ', to_char(userId) );
   set found = 0;

   for each row game in monopoly.games where game.gameId = gameId begin
      write into monopoly values (tag, 'GAME start: ' + to_char(game.start) + ', state: ' + to_char(game.state) +  ', moves: ' + to_char(game.moves) +  ', cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) +  ', cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) );
      set found = found + 1;

      if( game.moves = 0 ) then
         -- no moves, therefore no historic interest, delete row
         -- this also allows the player to be re-added
         delete from monopoly.players where gameId = game.gameId and uid = userId;
         set found = found + 1;
         write into monopoly values (tag, 'PLAYER has been deleted (moves=0)');
         call procedure monopoly_event( 'monopoly_removePlayer', gameId, game.moves, userId, '', 4, 'A player was deleted from the game.', false, '','' );
      else
         -- the game has started, therefore, instead of deleting the record, set the player as not active, return any assets etc
         -- NB: they cannot re-join the game

         for each row player in monopoly.players where player.gameId = game.gameId and player.uid = userId begin
            set found = found + 1;
            write into monopoly values (tag, 'PLAYER cash: £' + to_char(player.cash) + ', isActive: ' + to_char(player.isActive) + 'jailTime: ' + to_char(player.jailTime) + 'position: ' + to_char(player.position) );
            if( player.isActive = true ) then 
               for each row deed in monopoly.deeds where deed.gameId = gameId and deed.owner = userId begin
                  write into monopoly values (tag, 'DEED was owned by player title: "' + deed.title + '", state: ' + to_char(deed.state));
                  set deed.state = 0;
                  set deed.owner = -1;
                  call procedure monopoly_event( 'monopoly_removePlayer', gameId, game.moves, userId, deed.title, 2, 'The ownership of the deed was reset.', false, '','' );
               end;
               if( game.cardsChanceGoojfOwner = userId ) then
                  write into monopoly values (tag, 'The Chance Get out of Jail Free card was owned by player');
                  set game.cardsChanceGoojfOwner = -1;
                  call procedure monopoly_event( 'monopoly_removePlayer', gameId, game.moves, userId, 'Chance Get Out of Jail Free Card', 2, 'The ownership of the Get out of Jail Free card was reset.', false, '','' );
               end if;
               if( game.cardsCChestGoojfOwner = userId ) then
                  write into monopoly values (tag, 'The Community Chest Get out of Jail Free card was owned by player');
                  set game.cardsCChestGoojfOwner = -1;
                  call procedure monopoly_event( 'monopoly_removePlayer', gameId, game.moves, userId, 'Community Chest Get Out of Jail Free Card', 2, 'The ownership of the Get out of Jail Free card was reset.', false, '','' );
               end if;
               if( game.banker = userId )then
                  write into monopoly values (tag, 'The player was set as the banker, a new banker will need to be set before the game can continue');
                  set game.state = 2;
                  call procedure monopoly_event( 'monopoly_removePlayer', gameId, game.moves, userId, 'banker', 4, 'The player was set as the banker, a new banker will need to be set before the game can continue', false, '','' );
               end if;

               for each row playerNext in monopoly.players where playerNext.gameId = game.gameId and playerNext.uid <> player.uid and playerNext.sequence > player.sequence begin
                  set playerNext.sequence = playerNext.sequence - 1;
               end;

               -- deactivate player
               set player.isActive  = false;

               write into monopoly values (tag, 'PLAYER has been deactivated');
               call procedure monopoly_event( 'monopoly_removePlayer', gameId, game.moves, userId, '', 3, 'A player was removed (deactivated) from the game.', false, '','' );
            else
               write into monopoly values (tag, 'Player was already not active');
               cancel;
            end if;
         end;
      end if;

   end;

   if( found < 2 ) then 
      write into monopoly values (tag, 'The game or user not found' );
      cancel;
   end if;
end;
go

-- g monopoly_removePlayer
grant execute on sql procedure monopoly_removePlayer to role 'monopolyUser';
go

-- proc monopoly_creditToPlayer
create or replace procedure monopoly_creditToPlayer(in gameId Integer, in userId Integer, in credit Integer) 
-- transfer cash from the bank to the player
declare tag char(128); found Integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_creditToPlayer()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', userId: ', to_char(userId), ', credit: £', to_char(credit) );
   for each row game in monopoly.games where game.gameId = gameId and game.state in( 1, 2 ) begin
      set found = found + 1;
      for each row p in monopoly.players where p.gameId = game.gameId and p.uid = userId and p.isActive = true begin
         set found = found + 1;
         write into monopoly values (tag,' PLAYER: cash before: £'+ to_char(p.cash) +', cash after: £'+ to_char(p.cash + credit) );
         set p.cash = p.cash + credit;
         call procedure monopoly_event( 'monopoly_creditToPlayer', gameId, game.moves, userId, '', 2, 'the player has been credited a sum of £'+to_char(credit)+' (updated funds: £'+to_char(p.cash)+').', false, '','' );
         set game.state = 1;
      end;
   end;
   if( found < 2 ) then 
      write into monopoly values (tag, 'The game not found (or inactive or complete) or user not found (or inactive)' );
      cancel;
   end if;   
end;
go

-- g monopoly_creditToPlayer
grant execute on sql procedure monopoly_creditToPlayer to role 'monopolyUser';
go


-- proc monopoly_player_setAvatar
create or replace procedure monopoly_player_setAvatar(in gameId Integer, in userId Integer, in avatar Char(12) ) 
-- set the player avatar
declare tag char(128); found integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_player_setAvatar()] ';
   write into monopoly values (tag, 'gameId: ', gameId, ', userId: ', userId, ', avatar: ', avatar );

   for each row game in monopoly.games where game.gameId = gameId begin
      set found = found + 1;
      for each row player in monopoly.players where player.gameId = game.gameId and player.uid = userId begin
         write into monopoly values (tag, 'players avatar was : "' + player.avatar + '".' );
         set found = found + 1;
         set player.avatar = avatar;
      end;
   end;
   if( found < 2 ) then 
      write into monopoly values (tag, 'The game or user not found' );
      cancel;
   end if;
end;
go

-- g monopoly_player_setAvatar
grant execute on sql procedure monopoly_player_setAvatar to role 'monopolyUser';
go

-- proc monopoly_player_jailTime
create or replace procedure monopoly_player_jailTime(in gameId Integer, in userId Integer, in action Char(15) )
-- action         | conditions   | outcome      | position |
-- sentence 30    | jailTime = 0 | incarcerate  |       40 | player landed on the Go To Jail square
-- sentence dice  | jailTime = 0 | incarcerate  |       40 | player threw three doubles
-- sentence CC03  | jailTime = 0 | incarcerate  |       40 | player drew the Community Chest go to jail card
-- sentence CH02  | jailTime = 0 | incarcerate  |       40 | player drew the Chance go to jail card
-- chance         | jailTime > 0 | release      |       10 | chance goojf card returned
-- communityChest | jailTime > 0 | release      |       10 | community chest goojf card returned
-- doubledice     | jailTime > 0 | release      |       10 |
-- fine           | jailTime > 0 | release      |       10 | player has choosen to pay their £50 fine this turn
-- turn           | jailTime > 1 | serve time   |       40 | jailTime--
-- "              | jailTime = 1 | serve time   |       10 | jailTime-- and player fined £50

declare tag char(128); found integer; turnOver Boolean; fine Boolean;
begin
   set tag        =  '[' + to_char(getdate) + '][monopoly_player_jailTime()] ';
   set found      =  0;
   set turnOver   =  false;
   set fine       =  false;
   write into monopoly values (tag, 'gameId: ', gameId, ', userId: ', userId, ', action: "', action ,'"');

   for each row game in monopoly.games where game.gameId = gameId and game.state in ( 1, 2 ) begin
      write into monopoly values (tag,' GAME: gameId: ' + to_char(game.gameId) + ', cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) + ', cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) );
      set found = found + 1;
      for each row player in monopoly.players where player.gameId = game.gameId and player.uid = userId and player.isActive = true begin
         set found = found + 1;
         write into monopoly values (tag,' PLAYER | userId='+to_char(player.uid)+', jailTime='+to_char(player.jailTime)+', position='+to_char(player.position)+', cash=£'+to_char(player.cash) );

         if( action = 'sentence 30' and player.jailTime = 0 ) then
            -- incarcerate
            set player.jailTime  =  3;
            set player.position  =  40;
            set player.doubleRun =  0;
            set game.state       =  1;
            set game.action      =  'jailed';   -- indication for monopoly_nextPlayersTurn to suppress double-dice-another-turn
            set turnOver         =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, 'jail', 4, 'The player landed on the Go to Jail square and has been incarcerated.', true, 'The player has been sent to jail', 'The player landed on the Go to Jail square' );

         elseif( action = 'sentence dice' and player.jailTime = 0 ) then
            -- incarcerate
            set player.jailTime  =  3;
            set player.position  =  40;
            set player.doubleRun =  0;
            set game.state       =  1;
            set game.action      =  'jailed';   -- indication for monopoly_nextPlayersTurn to suppress double-dice-another-turn
            set turnOver         =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, 'jail', 4, 'The player threw three sets of double dice and has been incarcerated .', true, 'The player has been sent to jail', 'The player threw three sets of double dice in a row' );

         elseif( action = 'sentence CC03' and player.jailTime = 0 ) then
            -- incarcerate
            set player.jailTime  =  3;
            set player.position  =  40;
            set player.doubleRun =  0;
            set game.state       =  1;
            set game.action      =  'jailed';   -- indication for monopoly_nextPlayersTurn to suppress double-dice-another-turn
            set turnOver         =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, 'jail', 4, 'The player drew the Community Chest "Go to jail ..." card and has been incarcerated .', true, 'The player has been sent to jail', 'The player drew the Community Chest "Go to jail ..." card' );

         elseif( action = 'sentence CH02' and player.jailTime = 0 ) then
            -- incarcerate
            set player.jailTime  =  3;
            set player.position  =  40;
            set player.doubleRun =  0;
            set game.state       =  1;
            set game.action      =  'jailed';   -- indication for monopoly_nextPlayersTurn to suppress double-dice-another-turn
            set turnOver         =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, 'jail', 4, 'The player drew the Chance "Go to jail ..." card and has been incarcerated .', true, 'The player has been sent to jail', 'The player drew the Chance "Go to jail ..." card' );

         elseif( action = 'chance' and player.jailTime > 0 ) then
            if( game.cardsChanceGoojfOwner = userId ) then 
               set game.cardsChanceGoojfOwner   =  -1;
               set player.jailTime              =  0;
               set player.position              =  10;
               set game.state                   =  1;
               call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, 'Chance Get Out of Jail Free Card', 3, 'The player used their Get Out of Jail Free Card and has been released, the card has been returned.', true, 'The player has been released from jail', 'The player has used their Chance Get out of jail free card' );
            else
               write into monopoly values (tag,' PLAYER attempted to use a get out of jail free card, but they did not have one');
               cancel;
            end if;

         elseif( action = 'communityChest' and player.jailTime > 0 ) then
            if( game.cardsCChestGoojfOwner = userId ) then 
               set game.cardsCChestGoojfOwner   =  -1;
               set player.jailTime              =  0;
               set player.position              =  10;
               set game.state                   =  1;
               call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, 'Community Chest Get Out of Jail Free Card', 3, 'The player used their Get Out of Jail Free Card and has been released, the card has been returned.', true, 'The player has been released from jail', 'The player has used their Community Chest Get out of jail free card' );
            else
               write into monopoly values (tag,' PLAYER attempted to use a get out of jail free card, but they did not have one');
               cancel;
            end if;

         elseif( action = 'doubledice' and player.jailTime > 0 ) then
            set player.jailTime  =  0;
            set player.position  =  10;
            set game.state       =  1;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, '', 3, 'The player threw a double and has been released.', true, 'The player has been released from jail', 'The player threw a double' );

         elseif( action = 'fine' and player.jailTime > 0 ) then
            set player.jailTime  =  0;
            set player.position  =  10;
            set game.state       =  1;
            set fine             =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, '', 3, 'The player has choosen to pay their £50 incarceration fine this turn and has been released', true, 'The player has been released from jail', 'The player has choosen to pay their £50 incarceration fine this turn');

         elseif( action = 'turn' and player.jailTime = 3 ) then
            set player.jailTime  =  2;
            set game.state       =  1;
            set turnOver         =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, '', 1, 'The player has spent 1 turn in jail.', false, '','' );

         elseif( action = 'turn' and player.jailTime = 2 ) then
            set player.jailTime  =  1;
            set game.state       =  1;
            set turnOver         =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, '', 1, 'The player has spent 2 turns in jail.', false, '','' );

         elseif( action = 'turn' and player.jailTime = 1 ) then
            set player.jailTime  =  0;
            set player.position  =  10;
            set turnOver         =  true;
            set fine             =  true;
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, '', 1, 'The player has served their 3-turn sentence in jail and will be released but owes a £50 incarceration fine', true, 'The player has been released from jail', 'The player has served their sentence but owes a £50 fine');

         else
            write into monopoly values (tag,'UNEXPECTED combination of jailTime ('+to_char(player.jailTime)+') and action ("'+action+'")');
            call procedure monopoly_event( 'monopoly_player_jailTime', gameId, game.moves, userId, '', 5, 'UNEXPECTED combination of jailTime ('+to_char(player.jailTime)+') and action ("'+action+'")', false, '','' );
            cancel;
         end if;
      end;
      if( turnOver=true ) then
         call procedure monopoly_nextPlayersTurn(gameId);
      end if;
      if( fine=true ) then
         call procedure monopoly_demandPayment(gameId,userId,50,-1,'Incarceration fine, pay £50');
      end if;

   end;
   if( found < 2 ) then 
      write into monopoly values (tag, 'The game or player record not found' );
      cancel;
   end if;
end;
go

-- g monopoly_player_jailTime
grant execute on sql procedure monopoly_player_jailTime to role 'monopolyUser';
go

-- re-create procedure monopoly_sellDeedToPlayer
create or replace procedure monopoly_sellDeedToPlayer(in gameId Integer, in buyerId Integer, in deedTitle Char(24), in cash Integer) 
declare tag char(128); found int; vendorFound int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_sellDeedToPlayer()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', deedTitle: "', deedTitle, '", buyerId: ', to_char(buyerId), ', cash: ', to_char(cash));
   set found = 0;
   set vendorFound = 0;

   for each row game in monopoly.games where game.gameId = gameId  begin
      set found = found + 1;
      write into monopoly values (tag, ' GAME gameId: ',game.gameId,', state: ',game.state);
      if( game.state in ( 1, 2 ) ) then
         for each row deed in monopoly.deeds where deed.gameId = game.gameId and deed.title = deedTitle begin
            set found = found + 1;
            write into monopoly values (tag,' DEED: owner: ' + to_char(deed.owner) + ', state: ' + to_char(deed.state) + ', sets: "' + deed.sets + '"' );
            if ( deed.state = 0 ) then
               -- deed can be sold (no mortgage, no buildings)
               if ( deed.owner = buyerId ) then
                  write into monopoly values (tag,' DEED: is already owned by the buyer!');
                  cancel;
               else 
                  for each row buyer in monopoly.players where buyer.gameId = game.gameId and buyer.uid = buyerId begin
                     set found = found + 1;
                     write into monopoly values (tag,' BUYER: uid: ', buyer.uid, ', avatar: "',buyer.avatar,'", isActive: ',to_char(buyer.isActive),', jailTime: ',to_char(buyer.jailTime),', position: ',to_char(buyer.position),', cash: ',to_char(buyer.cash) );
                     if( buyer.isActive = true and buyer.cash >= cash ) then
                        write into monopoly values (tag,' BUYER: vetted ok.');

                        if( deed.owner < 0 ) then
                           write into monopoly values (tag,' DEED: deed is "owned" by the bank');
                           -- assume the bank has infinate funds, therefore no need to update it's balance
                           write into monopoly values (tag, ' transfering deed' );
                           write into monopoly values (tag, ' BEFORE buyer.cash: £' + to_char(buyer.cash) + ', deed.owner: '+ to_char(deed.owner) );

                           set deed.owner = buyerId;
                           set buyer.cash = buyer.cash - cash;
                           write into monopoly values (tag, ' AFTER  buyer.cash: £' + to_char(buyer.cash) + ', deed.owner: '+ to_char(deed.owner) );
                           write into monopoly values (tag,' -> transaction completed');
                           call procedure monopoly_event( 'monopoly_sellDeedToPlayer', gameId, game.moves, buyerId, deedTitle, 3, 'The deed has been puchased from the bank (£'+to_char(cash)+').', true, deedTitle, 'Deed purchased from the bank for £'+to_char(cash) );

                        else
                           write into monopoly values (tag,' DEED: deed is owned by another player');

                           for each row vendor in monopoly.players where vendor.gameId = gameId and vendor.uid = deed.owner begin
                              set vendorFound = 1;
                              write into monopoly values (tag,' VENDOR: uid: ', vendor.uid, ', avatar: "',vendor.avatar,'", isActive: ',to_char(vendor.isActive),', jailTime: ',to_char(vendor.jailTime),', position: ',to_char(vendor.position),', cash: ',to_char(vendor.cash) );
                              if( vendor.isActive = true ) then
                                 write into monopoly values (tag, ' transfering deed' );
                                 write into monopoly values (tag, ' BEFORE buyer.cash: £' + to_char(buyer.cash) + ', vendor.cash: £' + to_char(vendor.cash) + ', deed.owner: '+ to_char(deed.owner) );
                                 set deed.owner = buyerId;
                                 set buyer.cash = buyer.cash - cash;
                                 set vendor.cash = vendor.cash + cash;
                                 write into monopoly values (tag, ' AFTER  buyer.cash: £' + to_char(buyer.cash) + ', vendor.cash: £' + to_char(vendor.cash) + ', deed.owner: '+ to_char(deed.owner) );
                                 write into monopoly values (tag,' -> transaction completed');
                                 call procedure monopoly_event( 'monopoly_sellDeedToPlayer', gameId, game.moves, buyerId, deedTitle, 3, 'The deed has been puchased from another player (£'+to_char(cash)+').', true, deedTitle, 'Deed purchased from another player for £'+to_char(cash) );
                              else 
                                 write into monopoly values (tag,' BUYER: inactive user.');
                                 cancel;
                              end if;
                           end;
                           if( vendorFound < 1 ) then 
                              write into monopoly values (tag, 'The vendor (current owner) of the title deed was not found' );
                              cancel;
                           end if;
                        end if;
                     else
                        write into monopoly values (tag,' BUYER: inactive user or insufficent funds.');
                        cancel;
                     end if;
                  end;
               end if;
            else
               write into monopoly values (tag,' DEED: cannot be sold, either is mortgaged or has buildings.');
               cancel;
            end if;
         end;
      else
         write into monopoly values (tag, 'GAME either not active or is complete' );
         cancel;
      end if;
   end;
   if( found < 3 ) then 
      write into monopoly values (tag, 'The game, deed or buyer not found' );
      cancel;
   end if;
end;
go

-- g monopoly_sellDeedToPlayer
grant execute on sql procedure monopoly_sellDeedToPlayer to role 'monopolyUser';
go

-- monopoly_hotels procedure
create or replace procedure monopoly_hotels(in gameId Integer, in out stock integer)
-- returns the remaining hotel stock for the specified game
declare tag char(128); found Integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_hotels()] ';
   write into monopoly values (tag, 'gameId: ', gameId);
   for each row inventory in monopoly.inventory where inventory.type = 'stock' and inventory.item = 'hotel' begin
      set found = found + 1;
      set stock = inventory.qty;
      write into monopoly values (tag, 'inventory stock: ', stock);
      for each row gameBuildings in monopoly.deeds where gameBuildings.gameId = gameId and gameBuildings.state = 5 begin
         set found = found + 1;
         set stock = stock - 1;
      end;
      write into monopoly values (tag, ' INVENTORY Hotel Stock Total: ' + to_char(inventory.qty) + ', unallocated stock: ' +to_char(stock));
      call procedure monopoly_event( 'monopoly_hotels', gameId, -1, -1, '', 1, 'current hotel stock: '+to_char(stock)+' (inventory: '+to_char(inventory.qty)+').', false, '','' );
      break;
   end;
   if( found < 2 ) then
      write into monopoly values (tag, 'inventory or deeds records not found');
      call procedure monopoly_event( 'monopoly_hotels', gameId, -1, -1, '', 5, 'inventory or deeds records not found', false, '','' );
   end if;
end;
go

-- monopoly_houses procedure
create or replace procedure monopoly_houses(in gameId Integer, in out stock integer)
-- returns the remaining house stock for the specified game
declare tag char(128); found Integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_houses()] ';
   write into monopoly values (tag, 'gameId: ', gameId);
   for each row inventory in monopoly.inventory where inventory.type = 'stock' and inventory.item = 'house' begin
      set found = found + 1;
      set stock = inventory.qty;
      write into monopoly values (tag, 'inventory stock: ', stock);
      for each row gameBuildings in monopoly.deeds where gameBuildings.gameId = gameId and gameBuildings.state >= 1 and gameBuildings.state <= 4 begin
         set found = found + 1;
         set stock = stock - gameBuildings.state;
      end;
      write into monopoly values (tag, ' INVENTORY House Stock Total: ' + to_char(inventory.qty) + ', unallocated stock: ' +to_char(stock));
      call procedure monopoly_event( 'monopoly_houses', gameId, -1, -1, '', 1, 'current housing stock: '+to_char(stock)+' (inventory: '+to_char(inventory.qty)+').', false, '','' );
      break;
   end;
   if( found < 2 ) then
      write into monopoly values (tag, 'inventory or deeds records not found');
      call procedure monopoly_event( 'monopoly_houses', gameId, -1, -1, '', 5, 'inventory or deeds records not found', false, '','' );
   end if;
end;
go

-- monopoly_deedManageState procedure
create or replace procedure monopoly_deedManageState(in gameId Integer, in deedTitle Char(24), in action Char(10)) 
-- action can be "buy", "sell", "mortgage" or "unmortgage"
-- buy and sale will trade up/down one building
declare tag char(128); stock int; found int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_deedManageState()] ';
   write into monopoly values (tag, 'gameId: ', gameId, ', deedTitle: "', deedTitle, '", action: "', action, '"');
   set found = 0;
   set stock = 0;
   if( action in ( 'buy', 'sell', 'mortgage', 'unmortgage' ) ) then
      -- access the inventory_deeds, game, deeds and player tables
      for each row deedInventory in monopoly.inventory_deeds where deedInventory.title = deedTitle begin
         set found = found + 1;
         write into monopoly values (tag, ' INVENTORY DEED title: "'+deedInventory.title + '", sets: "'+deedInventory.sets + '", cost_building: £',deedInventory.cost_building,', mortgage : £',deedInventory.mortgage);
         for each row game in monopoly.games where game.gameId = gameId  begin
            set found = found + 1;
            write into monopoly values (tag, ' GAME gameId: ',game.gameId,', state: ',to_char(game.state));
            if( game.state in ( 1,2 ) ) then
               for each row deed in monopoly.deeds where deed.gameId = gameId and deed.title = deedTitle begin
                  set found = found + 1;
                  write into monopoly values (tag, ' DEED: state: ',deed.state,', owner: ',deed.owner);
                  if( deed.owner >= 0 ) then
                     -- DEED is owned
                     for each row owner in monopoly.players where owner.gameId = gameId and owner.uid = deed.owner begin
                        write into monopoly values (tag,' OWNER: uid: ', owner.uid, ', isActive: ',owner.isActive,', jailTime: ',owner.jailTime,', position: ',owner.position,', cash: ',owner.cash);
                        if( owner.isActive = true ) then

                           if(      action = 'buy'         and deed.state >= 0 and deed.state < 5 ) then
                              -- verify cash
                              if( owner.cash >= deedInventory.cost_building ) then
                                 -- house or hotel
                                 if( deed.state < 4 ) then
                                    -- buy one house, verify housing stock
                                    call procedure monopoly_houses(gameId, stock);
                                    if( stock >=  1 ) then
                                       set deed.state = deed.state + 1;
                                       set owner.cash = owner.cash - deedInventory.cost_building;
                                       write into monopoly values (tag,'-> HOUSE purchased.');
                                       call procedure monopoly_event( 'monopoly_deedManageState', gameId, game.moves, owner.uid, deedTitle, 2, 'A house has been purchased for site (now: ' + to_char(deed.state) + ').', false, '','' );
                                    else
                                       write into monopoly values (tag,'XX insufficient stock of houses.');
                                       cancel;
                                    end if;

                                 else
                                    -- buy one hotel, verify hotel stock
                                    call procedure monopoly_hotels(gameId, stock);
                                    if( stock >= 1 ) then
                                       set deed.state = deed.state + 1;
                                       set owner.cash = owner.cash - deedInventory.cost_building;
                                       write into monopoly values (tag,'-> Hotel purchased.');
                                       call procedure monopoly_event( 'monopoly_deedManageState', gameId, game.moves, owner.uid, deedTitle, 2, 'A hotel has been purchased for site.', false, '','' );
                                    else
                                       write into monopoly values (tag,'XX insufficient stock of hotels.');
                                       cancel;
                                    end if

                                 end if;
                              else
                                 write into monopoly values (tag,'XX insufficient funds to purchase building.');
                                 cancel;
                              end if

                           elseif ( action = 'sell'        and deed.state >= 1 and deed.state <= 5 ) then
                              if( deed.state < 5 ) then
                                 -- sell one house (at 50%)
                                 set deed.state = deed.state - 1;
                                 set owner.cash = owner.cash + ( deedInventory.cost_building / 2 );
                                 call procedure monopoly_event( 'monopoly_deedManageState', gameId, game.moves, owner.uid, deedTitle, 2, 'A house has been sold from the site (now: ' + to_char(deed.state) + ').', false, '','' );
                              else 
                                 -- downgrade from one hotel to 4 houses. NOTE: there must be 4 houses availaible in stock. refund 50% of one building
                                 call procedure monopoly_houses(gameId, stock);
                                 if( stock >= 4 ) then
                                    set deed.state = 4;
                                    set owner.cash = owner.cash + ( deedInventory.cost_building / 2 );
                                    call procedure monopoly_event( 'monopoly_deedManageState', gameId, game.moves, owner.uid, deedTitle, 2, 'A hotel has been downgraded to a house for site.', false, '','' );
                                 else
                                    write into monopoly values (tag,'XX insufficient stock of houses (4 are required to downgrade from 1 hotel to 4 houses).');
                                    cancel;
                                 end if;
                              end if;


                           elseif ( action = 'mortgage'    and deed.state =  0 ) then
                              set owner.cash = owner.cash + deedInventory.mortgage;
                              write into monopoly values (tag,' OWNER: credited with £',deedInventory.mortgage,', updated cash: £',owner.cash);
                              set deed.state = -1;
                              call procedure monopoly_event( 'monopoly_deedManageState', gameId, game.moves, owner.uid, deedTitle, 3, 'The site has been mortgaged.', false, '','' );


                           elseif ( action = 'unmortgage'  and deed.state = -1 ) then
                              -- release mortgage, NB: this encours a 10% premium, owner must have funds
                              if( owner.cash >= (deedInventory.mortgage * 1.1) ) then
                                 write into monopoly values (tag, '-> releaseing mortgage; cost: £',(deedInventory.mortgage * 1.1),' (including 10% interest)');
                                 set owner.cash = owner.cash - deedInventory.mortgage * 1.1;
                                 write into monopoly values (tag,' OWNER: debited with £',(deedInventory.mortgage * 1.1),', updated cash: £',owner.cash);
                                 set deed.state = 0;
                                 call procedure monopoly_event( 'monopoly_deedManageState', gameId, game.moves, owner.uid, deedTitle, 3, 'The site has been unmortgaged.', false, '','' );

                              else
                                 write into monopoly values (tag,'XX insufficient funds to release mortgage.');
                                 cancel;
                              end if;
                              
                           else 
                              write into monopoly values (tag, 'XX invalid instruction ("'+action+'") for state (',deed.state,').');
                              cancel;
                           end if;
                        else
                           write into monopoly values (tag, 'XX OWNER: is not active');
                           cancel;
                        end if;
                     end;
                  else
                     write into monopoly values (tag, 'XX DEED: is not owned by the current player');
                     cancel;
                  end if;
               end;
            else
               write into monopoly values (tag, 'XX  GAME: is either not started or is marked as complete');
               cancel;
            end if; 
         end;
      end;
   else 
      write into monopoly values (tag, 'XX invalid instruction ("'+action+'").');
      cancel;
   end if;
   if( found < 3 ) then 
      write into monopoly values (tag, 'The game, buyer or deed not found' );
      cancel;
   end if;
end;
go

-- g monopoly_deedManageState
grant execute on sql procedure monopoly_deedManageState to role 'monopolyUser';
go

create or replace procedure monopoly_demandRent(in servername char(64), in serverserial Integer)
   -- The purpose of this procedure is to a) set the game state to blocked and b) register the demand request against the class 1501 alerts.status record
   --    theoretically there could be rent due on multiple deeds.  Since can still be demanded after a players second dice throw and 
   --    also after the subsequents players second dice throw there could be up to four rent demands pending.
   --    https://boardgames.stackexchange.com/questions/39267/how-long-do-i-have-to-collect-rent-in-monopoly
declare tag char(128); found int; serial int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_demandRent()] ';
   write into monopoly values (tag, 'servername: "' + servername + '", serverserial: ' + to_char(serverserial));
   set found = 0;

   for each row rent in alerts.status where rent.ServerName = servername and rent.ServerSerial = serverserial and rent.Class = 1501 and rent.AlertGroup = 'rent' and rent.Severity = 3 and rent.INT03 > 0 begin
      set found = found + 1;
      write into monopoly values (tag, 'rentledger | gameId: '+to_char(rent.Grade)+', owner (uid): '+to_char(rent.INT01)+', tenant (uid): '+to_char(rent.OwnerUID)+', deed (squareid): '+to_char(rent.PhysicalPort)+', Severity: '+to_char(rent.Severity)+', Age: '+to_char(rent.INT03)+', rent: '+to_char(rent.INT02));

      for each row game in monopoly.games where game.gameId = rent.Grade and game.state in (1,2) begin
         set found = found + 1;
         for each row deed in monopoly.inventory_deeds where deed.boardPlace = rent.PhysicalPort begin
            for each row owner in security.users where owner.UserID = rent.INT01 begin
               for each row tenant in security.users where tenant.UserID = rent.OwnerUID begin
                  -- pause game
                  set game.state             =  2;
                  set game.action            =  'rentDemanded';
                  set rent.Severity          =  4;
                  set rent.Summary           =  'The owner ('+owner.FullName+') of the '+deed.title+' property has demanded that tenant '+tenant.FullName+' play rent of £' + to_char(rent.INT02);
                  set rent.LastOccurrence    =  getdate;
                  set rent.TEXT02            =  'The owner ('+owner.FullName+') has now demaded the tenant pay rent of £'+to_char(rent.INT02)+' this property';
                  set serial                 =  -1;
                  call procedure monopoly_eventSerial(serial);
                  set rent.ProbeSubSecondId  = serial;
                  write into monopoly values (tag, 'pausing game and registering the legitimate rent demand');
               end;
            end;
         end;
      end;
   end;
   if( found < 2 ) then 
      write into monopoly values (tag, 'The game or rentLedger record not found' );
      cancel;
   end if;
end;
go

-- g monopoly_demandRent
grant execute on sql procedure monopoly_demandRent to role 'monopolyUser';
go

create or replace procedure monopoly_payRent(in servername char(64), in serverserial Integer)
   -- The purpose of this procedure is to 
   -- a) allow a player to pay a rent due (if still in turn and regardless of owner demanded)
   -- b) reduce there bank balance or fail
   -- c) register successful rent payment against the class 1501 alerts.status rent record
   -- d) determine if the game is paused, determine if it can be resumed
declare tag char(128); found int; demandCount int; serial int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_payRent()] ';
   write into monopoly values (tag, 'servername: "' + servername + '", serverserial: ' + to_char(serverserial));
   set found = 0;

   for each row rent in alerts.status where rent.ServerName = servername and rent.ServerSerial = serverserial and rent.Class = 1501 and rent.AlertGroup = 'rent' begin
      set found = found + 1;
      write into monopoly values (tag, 'rentledger | gameId: '+to_char(rent.Grade)+', owner (uid): '+to_char(rent.INT01)+', tenant (uid): '+to_char(rent.OwnerUID)+', deed (squareid): '+to_char(rent.PhysicalPort)+', Severity: '+to_char(rent.Severity)+', Age: '+to_char(rent.INT03)+', rent: '+to_char(rent.INT02));

      if ( rent.Severity in ( 3,4) ) then
         -- due or demanded
         for each row game in monopoly.games where game.gameId = rent.Grade and game.state in (1,2) begin
            write into monopoly values (tag, 'game         | state: '+to_char(game.state)+', action: '+game.action);
            set found = found + 1;
            for each row deed in monopoly.inventory_deeds where deed.boardPlace = rent.PhysicalPort begin
               set found = found + 1;
               for each row owner in security.users where owner.UserID = rent.INT01 begin
                  set found = found + 1;
                  for each row tenant in security.users where tenant.UserID = rent.OwnerUID begin
                     set found = found + 1;
                     for each row tenantPlayer in monopoly.players where tenantPlayer.gameId = game.gameId and tenantPlayer.uid = tenant.UserID begin
                        set found = found + 1;
                        write into monopoly values (tag, 'tenantPlayer | isActive: ' + to_char(tenantPlayer.isActive) + ', cash: £'+to_char(tenantPlayer.cash) );
                        for each row ownerPlayer in monopoly.players where ownerPlayer.gameId = game.gameId and ownerPlayer.uid = owner.UserID begin
                           set found = found + 1;
                           write into monopoly values (tag, 'ownerPlayer  | isActive: ' + to_char(ownerPlayer.isActive) + ', cash: £'+to_char(ownerPlayer.cash) );
                           
                           if( rent.INT03 > 0  ) then
                              -- legimate payment
                              if( tenantPlayer.cash >= rent.INT02 ) then
                                 -- sufficent cash
                                 set tenantPlayer.cash = tenantPlayer.cash - rent.INT02;
                                 set ownerPlayer.cash  = ownerPlayer.cash  + rent.INT02;
                                 set rent.Severity          = 1;
                                 set rent.Summary           =  'The tenant ('+tenant.FullName+') of the '+deed.title+' property has paid rent due of £' + to_char(rent.INT02) + ' to the owner ('+owner.FullName+')';
                                 set rent.LastOccurrence    =  getdate;
                                 set rent.TEXT02            =  'a rent has now been paid against this property';
                                 set rent.SuppressEscl      =  4; --suppressed
                                 set serial                 =  -1;
                                 call procedure monopoly_eventSerial(serial);
                                 set rent.ProbeSubSecondId  = serial;

                                 if( game.state=2 and game.action = 'rentDemanded') then
                                    -- game blocked,
                                    set demandCount = 0;
                                    for each row rentledger in alerts.status where rentledger.Class = rent.Class and rentledger.AlertGroup = rent.AlertGroup and rentledger.Grade = rent.Grade and rentledger.Severity = 4 and rentledger.INT03 > 0 and rentledger.Identifier <> rent.Identifier begin
                                       set demandCount = demandCount + 1;
                                       write into monopoly values (tag, 'rentledger | outstanding | ServerName: '+rentledger.ServerName+', ServerSerial ' + to_char(rentledger.ServerSerial) + ', owner (uid): '+to_char(rentledger.INT01)+', tenant (uid): '+to_char(rentledger.OwnerUID)+', deed (squareid): '+to_char(rentledger.PhysicalPort)+', Severity: '+to_char(rentledger.Severity)+', Age: '+to_char(rentledger.INT03)+', rent: '+to_char(rentledger.INT02));
                                    end;
                                    if( demandCount = 0 ) then
                                       -- no pending demands
                                       set game.state    =  1;
                                       set game.action   =  '';
                                       write into monopoly values (tag, 'no more rent demands, unpausing game');
                                    end if;
                                 end if;
                              
                              else
                                 call procedure monopoly_event( 'monopoly_payRent', rent.Grade, game.moves, rent.INT01, deed.title, 4, 'The tenant ('+tenant.FullName+') of the '+deed.title+' property has insufficient funds to paid rent due of £' + to_char(rent.INT02) + ' to the owner ('+owner.FullName+')', false, '', '' );
                              end if;
                           end if;
                        end;
                     end;
                  end;
               end;
            end;
         end;
      end if;
   end;
   if( found not in ( 1,6,7 )) then 
      write into monopoly values (tag, 'The one or more records not found (rentLedger, game, owner, tenant or player)' );
      cancel;
   end if;
end;
go

-- g monopoly_payRent
grant execute on sql procedure monopoly_payRent to role 'monopolyUser';
go

-- monopoly_demandPayment
create or replace procedure monopoly_demandPayment(in gameId Integer, in debtorUID Integer, in value Integer, in creditorUID Integer, in description char(128))
declare tag char(128); serial int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_demandPayment()] ';
   set serial = -1;
   write into monopoly values (tag, 'gameId='+to_char(gameId)+', debtorUID='+to_char(debtorUID)+', value=£'+to_char(value)+', creditorUID='+to_char(creditorUID)+', description="'+description+'"');
   if( debtorUID=creditorUID ) then
      write into monopoly values (tag, 'debtor and creditor cannot be the same person');
      cancel;
   end if;

   for each row game in monopoly.games where game.gameId = gameId and game.state in (1,2) begin
      write into monopoly values (tag, 'GAME:  gameId='+to_char(game.gameId)+', state='+to_char(game.state)+', activePlayer='+to_char(game.activePlayer));

      for each row debtor in monopoly.players where debtor.gameId = gameId and debtor.uid = debtorUID begin
         write into monopoly values (tag,'DEBTOR: uid='+to_char(debtor.uid)+', isActive='+to_char(debtor.isActive)+', jailTime='+to_char(debtor.jailTime)+', cash=£'+to_char(debtor.cash));
         for each row debtoruser in security.users where debtoruser.UserID = debtor.uid begin
            write into monopoly values (tag, 'DEBTOR: "'+debtoruser.FullName+'" ('+to_char(debtoruser.UserID)+' / '+debtoruser.UserName+')');

            if( creditorUID < 101 ) then
               -- payment is to be made into the bank
               set game.state = 2;
               set game.action = 'paymentDemanded';
               -- insert payment demand event
               call procedure monopoly_eventSerial(serial);
               insert into alerts.status ( Type, Class, Manager, Agent, ExpireTime,  Grade, OwnerUID, AlertGroup, AlertKey, Severity, Poll, SuppressEscl, ProbeSubSecondId, INT01, INT02, TEXT01, TEXT02, Identifier, Summary ) 
               values ( 
                  1, 1501, 'games', 'Monopoly', 60*60*24*12, game.gameId, debtor.uid, 'payment demand', description, 4, game.moves, 1, serial, 0, value, 
                  'Payment demand', 
                  description,
                  'monopoly_event (1501 payment demand): '+to_char(serial)+' (gameId=' + to_char(game.gameId) + ', moveSerial=' + to_char(game.moves) + ', component="monopoly_demandPayment", debtorUID='+to_char(debtorUID)+', creditorUID=bank, value=£'+to_char(value),
                  description
               );
            else 
               -- payment is to another player
               for each row creditor in monopoly.players where creditor.gameId = gameId and creditor.uid = creditorUID begin
                  write into monopoly values (tag,'CREDITOR: uid='+to_char(creditor.uid)+', isActive='+to_char(creditor.isActive)+', jailTime='+to_char(creditor.jailTime)+', cash=£'+to_char(creditor.cash));
                  for each row creditoruser in security.users where creditoruser.UserID = creditor.uid begin
                     write into monopoly values (tag, 'CREDITOR: "'+creditoruser.FullName+'" ('+to_char(creditoruser.UserID)+' / '+creditoruser.UserName+')');
                     -- payment is to be made to another (validated) player
                     set game.state = 2;
                     set game.action = 'paymentDemanded';
                     -- insert payment demand event
                     call procedure monopoly_eventSerial(serial);
                     insert into alerts.status ( Type, Class, Manager, Agent, ExpireTime, Grade, OwnerUID, AlertGroup, AlertKey, Severity, Poll, SuppressEscl, ProbeSubSecondId, INT01, INT02, TEXT01, TEXT02, Identifier, Summary ) 
                     values ( 
                        1, 1501, 'games', 'Monopoly', 60*60*24*12, game.gameId, debtor.uid, 'payment demand', description, 4, game.moves, 1, serial, creditor.uid, value, 
                        'Payment demand', 
                        description,
                        'monopoly_event (1501 payment demand): '+to_char(serial)+' (gameId=' + to_char(game.gameId) + ', moveSerial=' + to_char(game.moves) + ', component="monopoly_demandPayment", debtorUID='+to_char(debtorUID)+', creditorUID='+to_char(creditor.uid)+', value=£'+to_char(value),
                        description
                     );
                  end;
               end;
            end if;
         end;
      end;
   end;
   if( serial < 0 ) then 
      write into monopoly values (tag, 'The one or more records not found (game, players or users)' );
      cancel;
   end if;
end;
go

-- g monopoly_demandPayment
grant execute on sql procedure monopoly_demandPayment to role 'monopolyUser';
go


create or replace procedure monopoly_payDemand(in servername char(64), in serverserial Integer)
   -- The purpose of this procedure is to 
   -- a) allow a player to pay off an outstanding demanded
   -- b) reduce their bank balance or fail
   -- c) if the demand creditor is another player increase their bank balance
   -- d) register successful demand payment against the class 1501 alerts.status demand record
   -- e) determine if the game is paused, determine if it can be resumed
declare tag char(128); found int; successful boolean; demandCount int; serial int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_payDemand()] ';
   write into monopoly values (tag, 'servername: "' + servername + '", serverserial: ' + to_char(serverserial));
   set found = 0;

   for each row demand in alerts.status where demand.ServerName = servername and demand.ServerSerial = serverserial and demand.Class = 1501 and demand.AlertGroup = 'payment demand' begin
      set found = found + 1;
      write into monopoly values (tag, 'demand | gameId: '+to_char(demand.Grade)+', creditor (uid): '+to_char(demand.INT01)+', debtor (uid): '+to_char(demand.OwnerUID)+', deed (squareid): '+to_char(demand.PhysicalPort)+', Severity: '+to_char(demand.Severity)+', Age: '+to_char(demand.INT03)+', demand value: '+to_char(demand.INT02));

      if ( demand.Severity in (3,4) ) then
         -- due or demanded
         for each row game in monopoly.games where game.gameId = demand.Grade and game.state in (1,2) begin
            write into monopoly values (tag, 'game         | state: '+to_char(game.state)+', action: '+game.action);
            set found = found + 1;
            for each row debtor in security.users where debtor.UserID = demand.OwnerUID begin
               set found = found + 1;
               for each row debtorPlayer in monopoly.players where debtorPlayer.gameId = game.gameId and debtorPlayer.uid = debtor.UserID begin
                  set found = found + 1;
                  write into monopoly values (tag, 'debtorPlayer | isActive: ' + to_char(debtorPlayer.isActive) + ', cash: £'+to_char(debtorPlayer.cash) );

                  if( debtorPlayer.cash >= demand.INT02 ) then
                     set successful = false;
                     if( demand.INT01 > 0 ) then
                        -- debt to another player
                        for each row creditor in security.users where creditor.UserID = demand.INT01 begin
                           for each row creditorPlayer in monopoly.players where creditorPlayer.gameId = game.gameId and creditorPlayer.uid = creditor.UserID begin
                              write into monopoly values (tag, 'creditorPlayer  | isActive: ' + to_char(creditorPlayer.isActive) + ', cash: £'+to_char(creditorPlayer.cash) );
                              set debtorPlayer.cash      =  debtorPlayer.cash - demand.INT02;
                              set creditorPlayer.cash    =  creditorPlayer.cash  + demand.INT02;
                              set demand.Summary         =  'The debtor ('+debtor.FullName+') of the demand has paid of £' + to_char(demand.INT02) + ' to the creditor ('+creditor.FullName+')';
                              set successful = true;
                              write into monopoly values (tag, 'payment successful (debtor -> creditor)' );
                           end;
                        end;

                     else 
                        -- debt to the bank
                        set debtorPlayer.cash      =  debtorPlayer.cash - demand.INT02;
                        set demand.Summary         =  'The debtor ('+debtor.FullName+') of the demand has paid of £' + to_char(demand.INT02) + ' to the bank';
                        set successful = true;
                        write into monopoly values (tag, 'payment successful (debtor -> bank)' );

                     end if;

                     if( successful = true ) then
                        set demand.Severity           =  1;
                        set demand.LastOccurrence     =  getdate;
                        set demand.TEXT02             =  'a rent has now been paid against this property';
                        set demand.SuppressEscl       =  4; --suppressed
                        set serial                    =  -1;
                        call procedure monopoly_eventSerial(serial);
                        set demand.ProbeSubSecondId   = serial;

                        set demandCount = 0;
                        for each row otherDemands in alerts.status where otherDemands.Class = demand.Class and otherDemands.Grade = demand.Grade and otherDemands.Severity = 4 and otherDemands.Identifier <> demand.Identifier begin
                           set demandCount = demandCount + 1;
                           write into monopoly values (tag, 'otherDemands | outstanding | ServerName='+otherDemands.ServerName+', ServerSerial=' + to_char(otherDemands.ServerSerial) + ', creditor (uid)='+to_char(otherDemands.INT01)+', debtor (uid)='+to_char(otherDemands.OwnerUID)+', Summary="'+otherDemands.Summary+'", Severity='+to_char(otherDemands.Severity)+', Value=£'+to_char(otherDemands.INT02));
                        end;
                        if( demandCount = 0 ) then
                           -- no pending demands
                           set game.state    =  1;
                           set game.action   =  '';
                           write into monopoly values (tag, 'no more rent demands, unpausing game');
                        end if;

                     else 
                        write into monopoly values (tag, 'ERROR: failed to transfer payment' );
                        cancel;
                     end if;

                  else 
                     -- insufficent funds
                     call procedure monopoly_event( 'monopoly_payDemand', demand.Grade, game.moves, demand.INT01, '', 4, 'The debtor ('+debtor.FullName+') insufficient funds to paid off a demand of £' + to_char(demand.INT02) + ' ('+demand.Summary+')', false, '', '' );
                  end if;
               end;
            end;
         end;
      end if;
   end;
   if( found not in ( 4 )) then 
      write into monopoly values (tag, 'The one or more records not found (demand, game, debtor or player) (found='+to_char(found)+')' );
      cancel;
   end if;
end;
go

-- g monopoly_payDemand
grant execute on sql procedure monopoly_payDemand to role 'monopolyUser';
go

-- monopoly_deedSetLiquidate procedure
create or replace procedure monopoly_deedSetLiquidate(in gameId Integer, in colourGroup Char(6) ) 
-- liquidate will sell all buildings on all site of the colour group
declare 
   tag char(128);
   deedsInSet int;
   minDeedState int;
   refund_building int;
   found int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_deedSetLiquidate()] ';
   write into monopoly values (tag, 'gameId: ', gameId, ', colourGroup: "', colourGroup, '"');
   set found = 0;

   -- verify that the specified colourGroup is valid and determine how many deeds are in the set
   for each row deedInventory in monopoly.inventory_deeds where deedInventory.colourSet = true and deedInventory.sets = colourGroup begin
      set deedsInSet = deedsInSet + 1;
      set refund_building = deedInventory.refund_building;
   end;
   if( deedsInSet in ( 2,3 ) ) then 
      write into monopoly values (tag, 'the specified colour group contains ', deedsInSet, ' deeds' );
      -- access the inventory_deeds, game, deeds and player tables
      for each row game in monopoly.games where game.gameId = gameId  begin
         set found = found + 1;
         write into monopoly values (tag, ' GAME gameId: ',game.gameId,', state: ',to_char(game.state));
         if( game.state in ( 1,2 ) ) then
            -- verify all deeds in the set (in this game) all owned by the same player
            set minDeedState = 6;
            for each row outer in monopoly.deeds where outer.gameId = gameId and outer.sets = colourGroup begin
               if( minDeedState = 6 ) then
                  for each row inner in monopoly.deeds where inner.gameId = outer.gameId and inner.sets = outer.sets and inner.owner <> outer.owner begin
                     write into monopoly values (tag, 'not all deeds in the specified colour group are owned by the same player' );
                     cancel;
                  end;
               end if;
               if( outer.state < minDeedState )then
                  set minDeedState = outer.state;
               end if;
            end;
            if( minDeedState = 6 )then
               write into monopoly values (tag, 'error: no deeds found' );
               cancel;
            elseif( minDeedState < 0 ) then
               write into monopoly values (tag, 'some deeds in the specified colour group are mortgaged' );
               cancel;
            else
               -- ok to proceed
               for each row deedToLiquidate in monopoly.deeds where deedToLiquidate.gameId = gameId and deedToLiquidate.sets = colourGroup begin
                  write into monopoly values (tag, ' DEED: title: ',deedToLiquidate.title,' state: ',deedToLiquidate.state,', owner: ',deedToLiquidate.owner);
                  for each row owner in monopoly.players where owner.gameId = gameId and owner.uid = deedToLiquidate.owner begin
                     write into monopoly values (tag,' OWNER: uid: ', owner.uid, ', isActive: ',owner.isActive,', jailTime: ',owner.jailTime,',cash: ',owner.cash);
                     if( owner.isActive = true ) then
                        -- sell all buildings on the site (at 50% each)
                        set owner.cash = owner.cash + ( deedToLiquidate.state * refund_building );
                        set deedToLiquidate.state = 0;
                        call procedure monopoly_event( 'monopoly_deedSetLiquidate', gameId, game.moves, owner.uid, deedToLiquidate.title, 3, 'All buildings have been liquidated from site.', false, '', '' );
                     else
                        write into monopoly values (tag, 'XX OWNER: is not active');
                        cancel;                  
                     end if;
                  end;
               end;
               write into monopoly values (tag, 'All buildings have been liquidated from all sites in the ',colourGroup,' colour group.');
            end if;
         else
            write into monopoly values (tag, 'XX  GAME: is either not started or is marked as complete');
            cancel;
         end if; 
      end;
   else
      write into monopoly values (tag, 'the specified colour group is not valid' );
      cancel;
   end if;
end;
go

-- g monopoly_deedSetLiquidate
grant execute on sql procedure monopoly_deedSetLiquidate to role 'monopolyUser';
go


-- monopoly_tradeGOOJFC procedure
create or replace procedure monopoly_tradeGOOJFC(in gameId Integer, in buyerId Integer, in goojfFromSet Char(14), in cash Integer) 
declare tag char(128); goojfOwnerId int; found Integer
begin
   set tag = '[' + to_char(getdate) + '][monopoly_tradeGOOJFC()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', goojfFromSet: "', goojfFromSet, '", buyerId: ', to_char(buyerId), ', cash: ', to_char(cash));
   set found = 0;

   if( goojfFromSet in ( 'chance', 'communityChest' ) ) then
      for each row game in monopoly.games where game.gameId = gameId begin
         set found = found + 1;
         write into monopoly values (tag,' GAME: gameId: ' + to_char(game.gameId) + ', start: ' + to_char(game.start) + ', state: ' + to_char(game.state) +  ', moves: ' + to_char(game.moves) + ', cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) + ', cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) );
         if( game.state in ( 1, 2 ) ) then
            if( goojfFromSet = 'chance' ) then set goojfOwnerId = game.cardsChanceGoojfOwner; else set goojfOwnerId = game.cardsCChestGoojfOwner; end if;
            write into monopoly values (tag, ' GOOJF card from set: ' + goojfFromSet + ', is owned by ' + to_char(goojfOwnerId) );

            for each row buyer in monopoly.players where buyer.gameId = gameId and buyer.uid = buyerId begin
               set found = found + 1;
               write into monopoly values (tag,' BUYER: uid: ', buyer.uid, ', avatar: "',buyer.avatar,'", isActive: ',to_char(buyer.isActive),', jailTime: ',to_char(buyer.jailTime),', position: ',to_char(buyer.position),', cash: ',to_char(buyer.cash) );
               if( buyer.isActive = true and buyer.cash >= cash ) then
                  if( goojfOwnerId = -1 and cash = 0 ) then
                     write into monopoly values (tag,' GOOJF card is held by the bank, transfering Get out of Jail Free card' );
                     write into monopoly values (tag, ' BEFORE game.cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) + ', game.cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner)  );
                     if( goojfFromSet = 'chance' ) then set game.cardsChanceGoojfOwner = buyer.uid; else set game.cardsCChestGoojfOwner = buyer.uid; end if;

                     write into monopoly values (tag, ' AFTER  game.cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) + ', game.cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) );
                     call procedure monopoly_event( 'monopoly_tradeGOOJFC', gameId, game.moves, buyerId, goojfFromSet + 'Get Out of Jail Free Card', 2, 'The ' + goojfFromSet + 'Get Out of Jail Free Card has been transfered from the bank.', true, 'Get Out of Jail Free Card ('+goojfFromSet+')', 'has been aquired' );
                     call procedure monopoly_nextPlayersTurn(gameId);

                  elseif( goojfOwnerId = -1  and cash > 0) then
                     write into monopoly values (tag, 'an attempt was made to buy a goojf card from the bank' );
                     cancel;

                  else 

                     for each row vendor in monopoly.players where vendor.gameId = gameId and vendor.uid = goojfOwnerId begin
                        write into monopoly values (tag,' VENDOR: uid: ', vendor.uid, ', avatar: "',vendor.avatar,'", isActive: ',to_char(vendor.isActive),', jailTime: ',to_char(vendor.jailTime),', position: ',to_char(vendor.position),', cash: ',to_char(vendor.cash) );
                        if( vendor.isActive = true ) then

                           write into monopoly values (tag, ' transfering Get out of Jail Free card' );
                           write into monopoly values (tag, ' BEFORE game.cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) + ', game.cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) + ', buyer.cash: £'+ to_char(buyer.cash) + ', vendor.cash: £'+ to_char(vendor.cash) );
                           if( goojfFromSet ='chance' ) then set game.cardsChanceGoojfOwner = buyer.uid; else set game.cardsCChestGoojfOwner = buyer.uid; end if;

                           set buyer.cash = buyer.cash - cash;
                           set vendor.cash = vendor.cash + cash;
                           write into monopoly values (tag, ' AFTER  game.cardsChanceGoojfOwner: ' + to_char(game.cardsChanceGoojfOwner) + ', game.cardsCChestGoojfOwner: ' + to_char(game.cardsCChestGoojfOwner) + ', buyer.cash: £'+ to_char(buyer.cash) + ', vendor.cash: £'+ to_char(vendor.cash) );
                           call procedure monopoly_event( 'monopoly_tradeGOOJFC', gameId, game.moves, buyerId, goojfFromSet + 'Get Out of Jail Free Card', 2, 'The ' + goojfFromSet + 'Get Out of Jail Free Card has been purchased from player '+to_char(vendor.uid)+' for £'+to_char(cash)+'.', false, '', '' );
                        else
                           write into monopoly values (tag,' vendor is an inactive user.');
                           cancel;
                        end if;
                     end;
                  end if;
               else
                  write into monopoly values (tag,' buyer is an inactive user or has insufficent funds.');
                  cancel;
               end if;
            end;
         else
            write into monopoly values (tag, 'GAME either not active or is complete' );
            cancel;
         end if;
      end;
   else
      write into monopoly values (tag, 'invalid value of goojfFromSet' );
      cancel;
   end if;
   if( found < 2 ) then 
      write into monopoly values (tag, 'The game or user not found' );
      cancel;
   end if;
end;
go

-- g monopoly_tradeGOOJFC
grant execute on sql procedure monopoly_tradeGOOJFC to role 'monopolyUser';
go


-- monopoly_drawCard procedure
create or replace procedure monopoly_drawCard(in gameId Integer ) 
-- called via JS when a Chance/Community Chest card is to be drawn
-- cards can only be drawn in game state 2 (blocked)
-- game.action will determine the card to draw
-- NB: if card CC15 (pay a £10 fine or take a chance) __HAS_ already been drawn in this move, then a second call to this procedure will cause a chance card to then be drawn
-- NB: if card CH07 (go back 3 places) they could end up on a Community Chest square
-- card sets are held as a list of randomised cardId.  Each cardId is four characters, e.g. Cxnn, thus CxnnCxnnCxnnCxnnCxnnCxnn etc
-- a pointer indicates the current card index, where 0=1, 1=5, 2=9 etc

declare tag char(128); cardId char(4);
begin
   set tag = '[' + to_char(getdate) + '][monopoly_drawCard()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId) );

   set cardId = '';
   for each row game in monopoly.games where game.gameId = gameId and game.state = 2 and game.action in ( 'communityChest', 'chance' ) begin
      write into monopoly values(tag, 'gameId: ' + to_char(game.gameId),', moves: ' + to_char(game.moves),', state: ' + to_char(game.state),', activePlayer: ' + to_char(game.activePlayer),', action: "'+game.action+'".');
      write into monopoly values(tag, 'CH pointer: ' + to_char(game.cardsChancePointer) + ', forMove: ' + to_char(game.cardsChance4Move) + ', GOOJF Owner: ' + to_char(game.cardsChanceGoojfOwner) );
      write into monopoly values(tag, 'CC pointer: ' + to_char(game.cardsCChestPointer) + ', forMove: ' + to_char(game.cardsCChest4Move) + ', GOOJF Owner: ' + to_char(game.cardsCChestGoojfOwner) );

      -- two cards allow the player to draw a second card from the other deck
      if( game.action = 'communityChest' and game.moves = game.cardsCChest4Move and substr( game.cardsChance, 4*game.cardsChancePointer+1, 4 ) = 'CC15' ) then
         write into monopoly values(tag, ' The Community Chest card "Pay a £10 fine or take a chance" (CC15) was previously drawn on this move, the player has opted to a chance card');
         set game.action = 'chance';
      elseif( game.action = 'chance'      and game.moves = game.cardsChance4Move and substr( game.cardsCChest, 4*game.cardsCChestPointer+1, 4 ) = 'CH07' ) then
         write into monopoly values(tag, ' The Chance card "Go back three squares" (CH07) was previously drawn on this move, and the player moved back to a Community Chest square');
         set game.action = 'communityChest';
      end if;

      if( game.action = 'chance') then
         write into monopoly values (tag, 'Chance          | pointer: ' + to_char(game.cardsChancePointer) + ', forMove: ' + to_char(game.cardsChance4Move) + ', GOOJF Owner: ' + to_char(game.cardsChanceGoojfOwner) );
         if( game.cardsChance4Move = game.moves ) then
            write into monopoly values (tag, 'Chance card already drawn for this turn' );
            cancel;
         end if;

         if( game.cardsChancePointer = 15 ) then
            set game.cardsChancePointer = 0;
         else
            set game.cardsChancePointer = game.cardsChancePointer + 1;
         end if;

         set cardId = substr( game.cardsChance, 4*game.cardsChancePointer+1, 4 );
         write into monopoly values (tag, 'Chance          | checking card '+to_char(game.cardsChancePointer)+'; cardId: ' + cardId );
         if( cardId = 'CH16' and game.cardsChanceGoojfOwner > -1  ) then
            write into monopoly values (tag, 'Chance          | drawn card was the get out of jail free card and this was already owned, selecting the next card' );
            if( game.cardsChancePointer = 15 ) then
               set game.cardsChancePointer = 0;
            else
               set game.cardsChancePointer = game.cardsChancePointer + 1;
            end if;
         end if;
         set game.cardsChance4Move = game.moves;

         set cardId = substr( game.cardsChance, 4*game.cardsChancePointer+1, 4 );
         
      elseif( game.action = 'communityChest' ) then 
         write into monopoly values (tag, 'Community Chest | pointer: ' + to_char(game.cardsCChestPointer) + ', forMove: ' + to_char(game.cardsCChest4Move) + ', GOOJF Owner: ' + to_char(game.cardsCChestGoojfOwner) );
         if( game.cardsCChest4Move = game.moves ) then
            write into monopoly values (tag, 'card already drawn for this turn' );
            cancel;
         end if;

         if( game.cardsCChestPointer = 15 ) then
            set game.cardsCChestPointer = 0;
         else
            set game.cardsCChestPointer = game.cardsCChestPointer + 1;
         end if;

         set cardId = substr( game.cardsCChest, 4*game.cardsCChestPointer+1, 4 );
         write into monopoly values (tag, 'Community Chest | checking card '+to_char(game.cardsCChestPointer)+'; cardId: ' + cardId );
         if( cardId = 'CH16' and game.cardsCChestGoojfOwner > -1  ) then
            write into monopoly values (tag, 'Community Chest | drawn card was the get out of jail free card and this was already owned, selecting the next card' );
            if( game.cardsCChestPointer = 15 ) then
               set game.cardsCChestPointer = 0;
            else
               set game.cardsCChestPointer = game.cardsCChestPointer + 1;
            end if;
         end if;

         set game.cardsCChest4Move = game.moves;
         set cardId = substr( game.cardsCChest, 4*game.cardsCChestPointer+1, 4 );

      end if;
      for each row card in monopoly.inventory where card.type='card' and card.item = cardId begin
         if( game.action = 'chance' ) then
            call procedure monopoly_event( 'monopoly_drawCard', gameId, game.moves, game.activePlayer, cardId, 2, 'Card Drawn: "'+card.text+'"', true, 'Chance card drawn', card.text );
         else
            call procedure monopoly_event( 'monopoly_drawCard', gameId, game.moves, game.activePlayer, cardId, 2, 'Card Drawn: "'+card.text+'"', true, 'Community Chest card drawn', card.text );
         end if;
      end

   end;
   if( cardId = '' ) then 
      write into monopoly values (tag, 'The game was not found, in the wrong state or its action was not to take a card' );
      cancel;
   end if;

end;
go

-- g monopoly_drawCard
grant execute on sql procedure monopoly_drawCard to role 'monopolyUser';
go




-- monopoly_playerMove procedure
create or replace procedure monopoly_playerMove(in gameId Integer, in userId Integer, in dice0 Integer, in dice1 Integer) 
-- move the given players counter
--  0 is go
-- 40 is in jail, 10 is just visiting, 30 is go to jail
--  1 is Old Kent Road, 39 is Mayfair etc
-- player could be in jail [and get a double and thus be released]
-- player could hit a run of three doubles and end up in jail
declare tag char(128); found Integer; jailAction Char(15); rentDue integer; setOwnership integer; serial integer; toastHeader Char(64);
begin
   set tag = '[' + to_char(getdate) + '][monopoly_playerMove()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId), ', userId: ', to_char(userId), ', dice0: ', to_char(dice0), ', dice1: ', to_char(dice1));
   set found      = 0;
   set jailAction = '';

   if( dice0 > 0 and dice0 <= 6 and dice1 > 0 and dice1 <= 6 ) then
      -- valid throw from a pair of dice
      set toastHeader = 'Dice thrown: ' + to_char(dice0) + ' and ' + to_char(dice1);
      if( dice0=dice1 ) then
         set toastHeader = toastHeader + ' Double!';
      end if;

      for each row game in monopoly.games where game.gameId = gameId begin
         set found = found + 1;
         -- increment the game moves counter
         set game.moves = game.moves + 1;
         -- record the dice
         set game.activeMoveDice0 = dice0;
         set game.activeMoveDice1 = dice1;
         write into monopoly values (tag,' GAME: gameId=' + to_char(game.gameId) + ', state=' + to_char(game.state) +  ', moves=' + to_char(game.moves) + ', activePlayer=' + to_char(game.activePlayer) + ', action="'+game.action+'", dice0='+to_char(game.activeMoveDice0)+', dice1='+to_char(game.activeMoveDice1));
         if( game.state = 1 ) then
            if( game.activePlayer = userId ) then
               -- always reset the doubleRun counter for all *other* players in this game
               update monopoly.players set doubleRun = 0 where gameId = game.gameId and uid <> userId;

               -- phase one get currentPosition / check doubleRun / check jailTime
               for each row player in monopoly.players where player.gameId = game.gameId and player.uid = userId begin
                  set found = found + 1;
                  write into monopoly values (tag,' PLAYER: uid: ', player.uid, ', isActive: ',to_char(player.isActive),', jailTime: ',to_char(player.jailTime),', position: ',to_char(player.position),', doubleRun: ',to_char(player.doubleRun),', cash: ',to_char(player.cash) );
                  if( player.isActive = true ) then
                     call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, '', 2, 'Dice thrown: ' + to_char(dice0) + ' and ' + to_char(dice1) + ' (' + to_char(dice0+dice1) + ')', true, toastHeader, '');

                     if( player.jailTime >= 1 ) then
                        -- in jail
                        if( dice0=dice1 ) then  
                           -- has thrown a double and will be released
                           set jailAction       = 'doubledice';
                           set player.doubleRun = player.doubleRun + 1;

                        elseif( player.jailTime = 1 ) then
                           -- in jail, but has now served 3 turns and will be released
                           set jailAction = 'lastturn';
                        else 
                           -- mark another turn in jail
                           set jailAction = 'turn';
                        end if;

                     else
                        -- not in jail

                        if( dice0=dice1 ) then
                           if( player.doubleRun = 2 ) then
                              -- player has throw 3 doubles in a row and will go to jail
                              set jailAction = 'sentence dice';
                           else
                              -- threw a double but not third in a row
                              set player.doubleRun = player.doubleRun + 1;
                           end if;
                        else 
                           -- not a double dice
                           set player.doubleRun = 0;
                        end if;

                        -- if( player.position + dice0 + dice1 = 30 ) then
                        --    -- landed on the GoToJail square
                        --    set jailAction = 'sentence 40';
                        --    call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, 'Go To Jail', 2, 'player has been moved ' + to_char(dice0 + dice1) + ' places to position '+ to_char(player.position+dice0+dice1), true );
                        -- end if;

                     end if;
  
                  else
                     write into monopoly values (tag, 'Player not active' );
                     cancel;
                  end if;
               end;



               -- can not call monopoly_player_jailTime() whilst in the for each row as it holds a lock on the players table

               if( jailAction = 'lastturn' ) then
                  call procedure monopoly_player_jailTime( gameId, userId, 'turn' );
                  set jailAction = '';
               end if;

               if( jailAction <> '' ) then
                  call procedure monopoly_player_jailTime( gameId, userId, jailAction );
               else
                  -- just a normal turn
                  -- phase 2
                  for each row player in monopoly.players where player.gameId = game.gameId and player.uid = userId begin
                     -- no need to verify active etc

                     if( player.position + dice0 + dice1 > 39 ) then
                        -- will pass go on this turn
                        set player.position = player.position + dice0 + dice1 - 40;
                        call procedure monopoly_creditToPlayer(gameId, userId,200);
                        write into monopoly values (tag, ' player has passed go and has received £200');
                        call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, 'Go', 2, 'player has just passed go and has received £200', false, '', '');

                     else 
                        set player.position = player.position + dice0 + dice1;
                     end if;

                     for each row place in monopoly.board where place.place = player.position begin
                        write into monopoly values (tag, ' player has moved to position ' + to_char(player.position) + ': "'+place.title+'" (set: "'+place.sets+'", rule: "'+place.rule+'")');
                        write into monopoly values (tag,' Updated Place: place: '+to_char(place.place)+', title: "' + place.title + '", set: "' + place.sets+'", rule: "'+place.rule+'"');
                        call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, place.title, 2, 'player has been moved ' + to_char(dice0 + dice1) + ' places to position '+ to_char(player.position), false, '', '');

                        if( place.rule = 'go' ) then
                           call procedure monopoly_creditToPlayer(gameId, userId,200);
                           write into monopoly values (tag, ' player has landed on go and has received an additional £200');
                           call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, 'Go', 2, 'player has landed on go and has received an additional £200', false, '', '');
                           call procedure monopoly_nextPlayersTurn(gameId);

                        elseif( place.rule = 'incomeTax' ) then
                           write into monopoly values (tag, 'incomeTax rule');
                           -- pay £200 
                           call procedure monopoly_demandPayment(gameId,userId,200,-1,'The player must pay their Income tax (£200)');

                        elseif( place.rule = 'superTax' ) then
                           write into monopoly values (tag, 'superTax rule');
                           -- pay £100
                           call procedure monopoly_demandPayment(gameId,userId,200,-1,'The player must pay their Super tax (£100)');

                        elseif( place.rule = 'communityChest' ) then
                           write into monopoly values (tag, 'communityChest rule');
                           set game.state = 2;
                           set game.action = 'communityChest';
                           call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, place.title, 2, 'The player needs to draw a Community Chest card', false, '', '');

                        elseif( place.rule = 'chance' ) then
                           write into monopoly values (tag, 'chance rule');
                           set game.state = 2;
                           set game.action = 'chance';
                           call procedure monopoly_event( 'monopoly_playerMove', gameId, game.moves, userId, place.title, 2, 'The player needs to draw a Chance card', false, '', '');

                        elseif( place.rule = 'goToJail' ) then
                           write into monopoly values (tag, 'goToJail rule');
                           call procedure monopoly_player_jailTime(gameId, userId, 'sentence 30');

                        elseif( place.rule in ('utilities', 'stations', 'deed' ) ) then 
                           -- if the square is a deed/station/utility, then is rent due or purchase possible?
                           -- select the corresponding monopoly.deeds (in game data) and inventory_deeds table rows

                           for each row deed in monopoly.deeds where deed.gameId = game.gameId and deed.boardPlace = place.place begin
                              for each row deedInventory in monopoly.inventory_deeds where deedInventory.boardPlace = place.place begin
                                 write into monopoly values (tag, 'rule: "'+ place.rule+'", deed: "'+deed.title+'", owner: ' + to_char(deed.owner) + ', state: ' + to_char(deed.state) );

                                 if( deed.owner > -1 and deed.owner <> userId and deed.state > -1 ) then
                                    -- rent due, calculate and inserted a new rent (class 1501 alerts.status) record
                                    write into monopoly values (tag, 'Rent due, calculating ...');
                                    set setOwnership = 0;
                                    for each row deedset in monopoly.deeds where deedset.gameId = game.gameId and deedset.sets = deed.sets and deedset.owner=deed.owner begin
                                       set setOwnership = setOwnership + 1;
                                    end;

                                    if( place.rule = 'utilities' ) then
                                       if( setOwnership = 2 ) then 
                                          set rentDue = 10 * ( game.activeMoveDice0 + game.activeMoveDice1);
                                       else
                                          set rentDue = 4 * ( game.activeMoveDice0 + game.activeMoveDice1);
                                       end if;

                                    elseif( place.rule = 'stations' ) then
                                       if( setOwnership = 1 ) then 
                                          set rentDue = deedInventory.rent_0;
                                       elseif( setOwnership = 2 ) then 
                                          set rentDue = deedInventory.rent_1;
                                       elseif( setOwnership = 3 ) then 
                                          set rentDue = deedInventory.rent_2;
                                       else
                                          set rentDue = deedInventory.rent_3;
                                       end if;

                                    else -- deed
                                       if( setOwnership < deedInventory.setSize ) then
                                          set rentDue = deedInventory.rent_0;
                                       else 
                                          -- owns all deeds
                                          if( deed.state = 0 ) then
                                             set rentDue = deedInventory.rent_0 * 2;
                                          elseif( deed.state = 1 ) then
                                             set rentDue = deedInventory.rent_1;
                                          elseif( deed.state = 2 ) then
                                             set rentDue = deedInventory.rent_2;
                                          elseif( deed.state = 3 ) then
                                             set rentDue = deedInventory.rent_3;
                                          elseif( deed.state = 4 ) then
                                             set rentDue = deedInventory.rent_4;
                                          elseif( deed.state = 5 ) then
                                             set rentDue = deedInventory.rent_5;
                                          end if;
                                       end if;
                                    end if;
                                    write into monopoly values (tag, place.rule + ' rent due on '+place.title+' (setOwnership: ' + to_char(setOwnership) + '/' + to_char(deedInventory.setSize) + '): £' + to_char(rentDue));

                                    set found = 0;

                                    for each row owner in security.users where owner.UserID = deed.owner begin
                                       set found = found + 1;
                                       write into monopoly values (tag, 'deed owner: "'+owner.FullName+'" ('+to_char(owner.UserID)+' / '+owner.UserName+')');

                                       for each row tenant in security.users where tenant.UserID = userId begin
                                          set found = found + 1;
                                          write into monopoly values (tag, 'deed tenant: "'+tenant.FullName+'" ('+to_char(tenant.UserID)+' / '+tenant.UserName+')');

                                          -- insert rent demand into the rent ledger table
                                          set serial = -1;
                                          call procedure monopoly_eventSerial(serial);
                                          insert into alerts.status ( Type, Class, Manager, Agent, ExpireTime, Node, Grade, OwnerUID, AlertGroup, AlertKey, Severity, Poll, SuppressEscl, ProbeSubSecondId, INT01, INT02, INT03, PhysicalPort, TEXT01, TEXT02, Identifier, Summary ) 
                                          values ( 
                                             1, 1501, 'games', 'Monopoly', 60*60*24*12, place.title, game.gameId, userId, 'rent', 'deed: ' + place.title, 3, game.moves, 1, serial, deed.owner, rentDue, 2, deed.boardPlace, 
                                             place.title, 
                                             'A rent of £'+to_char(rentDue)+' is due on this property from ' + tenant.FullName, 
                                             'monopoly_event (1501 rent): '+to_char(serial)+' (gameId=' + to_char(game.gameId) + ', moveSerial=' + to_char(game.moves) + ', component="monopoly_playerMove", squareid='+to_char(deed.boardPlace)+', owner='+to_char(deed.owner)+', tenant='+to_char(userId),
                                             'The player has landed on a property and owes rent to the owner ('+owner.FullName+') of £'+to_char(rentDue)
                                          );
                                       end;
                                    end;

                                    -- next players turn
                                    call procedure monopoly_nextPlayersTurn(gameId);

                                 elseif ( deed.owner = -1 ) then
                                    write into monopoly values (tag, 'property available for purchase');
                                    set game.state = 2;
                                    set game.action = 'forSale';
                                    call procedure monopoly_event( 'monopoly_playerMove-purchase', gameId, game.moves, userId, place.title, 2, 'The player has the opportunity to purchase this property', false, '', '');
                                 end if;
                              end;
                           end;
                        else
                           call procedure monopoly_nextPlayersTurn(gameId);
                        end if;
                     end;
                  end;
               end if;
            else
               write into monopoly values (tag, 'passed userId ('+to_char(userId)+') is not the current active user for the game ('+to_char(game.activePlayer)+')' );
               cancel;
            end if;
         else
            write into monopoly values (tag, 'game not active or is completed' );
            cancel;
         end if;

         -- set the start time if not already set
         if( game.start = 0 ) then
            set game.start = getdate();
         end if;
      end;
   else
      write into monopoly values (tag, 'Invalid throw from a pair of dice' );
      cancel;
   end if;
   if( found < 2 ) then 
      write into monopoly values (tag, 'The game or user not found' );
      cancel;
   end if;
end;
go

-- g monopoly_playerMove
grant execute on sql procedure monopoly_playerMove to role 'monopolyUser';
go


-- monopoly_playerAdvanceOrRetreat procedure
create or replace procedure monopoly_playerAdvanceOrRetreat(in gameId Integer) 
-- move the given players counter to a given location as indicated by a chance or community chest card
-- affects the active player
declare tag char(128); found integer; cardId char(4); position integer;
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_playerAdvanceOrRetreat()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId));
   for each row game in monopoly.games where game.gameId = gameId and game.state = 2 and game.action in ( 'chance', 'communityChest' ) begin
      set found = found + 1;
      write into monopoly values(tag, 'gameId: ' + to_char(game.gameId),', moves: ' + to_char(game.moves),', state: ' + to_char(game.state),', activePlayer: ' + to_char(game.activePlayer),', action: "'+game.action+'".');
      write into monopoly values(tag, 'CH pointer: ' + to_char(game.cardsChancePointer) + ', forMove: ' + to_char(game.cardsChance4Move) + ', GOOJF Owner: ' + to_char(game.cardsChanceGoojfOwner) );
      write into monopoly values(tag, 'CC pointer: ' + to_char(game.cardsCChestPointer) + ', forMove: ' + to_char(game.cardsCChest4Move) + ', GOOJF Owner: ' + to_char(game.cardsCChestGoojfOwner) );

      if( game.action = 'communityChest' and game.moves = game.cardsCChest4Move ) then
         set cardId = substr( game.cardsCChest, 4*game.cardsCChestPointer+1, 4 );
      elseif( game.action = 'chance' and game.moves = game.cardsChance4Move ) then 
         set cardId = substr( game.cardsChance, 4*game.cardsChancePointer+1, 4 );
      else 
         write into monopoly values (tag, 'The active player has not drawn a card' );
         cancel;
      end if;

      write into monopoly values(tag, 'the player had drawn the "' + cardId + '" card');

      for each row player in monopoly.players where player.gameId = game.gameId and player.uid = game.activePlayer begin
         set found = found + 1;
         write into monopoly values (tag,' PLAYER: uid: ', player.uid, ', position: ',to_char(player.position),', doubleRun: ',to_char(player.doubleRun),', cash: ',to_char(player.cash) );

         if( cardId in ( 'CC01', 'CH01') ) then
            -- advance to Go = 0
            set position = 0;

         elseif( cardId = 'CH03' ) then
            -- advance to Pall Mall - 11
            set position = 11;

         elseif( cardId = 'CH04' ) then
            -- advance to Marylebone Station - 15
            set position = 15;

         elseif( cardId = 'CH05' ) then
            -- advance to Trafalgar Square - 24
            set position = 24;

         elseif( cardId = 'CH06' ) then
            -- advance to Mayfair - 39
            set position = 39;

         elseif( cardId = 'CC02' ) then
            -- Go Back to Old Kent Road - 1
            set position = 1;

         elseif( cardId = 'CH07' ) then
            -- Go back 3 spaces
            -- NOTE the chance squares are at position 7, 22 and 36, so a player won't "unpass" go
            -- They will end up at 4 (Income Tax), 19 (Vine Street) or 33 (Community Chest)
            set position = player.position - 3;

         else 
            write into monopoly values (tag, 'The drawn card is not an Advance or Reteat card' );
            cancel;
         end if;

         if( position > player.position ) then
            write into monopoly values (tag, ' Player to move from position '+to_char(player.position)+' to '+to_char(position)+', the player will not pass Go' );
            set player.position = position;
            set game.state  = 1; 
            set game.action = '';
         else 
            write into monopoly values (tag, ' Player to move from position '+to_char(player.position)+' to '+to_char(position)+', the player will pass Go and will collect £200' );
            set player.position = position;
            --set player.cash     = player.cash + 200;
            call procedure monopoly_creditToPlayer(gameId, player.uid, 200);
            set game.state  = 1; 
            set game.action = '';
         end if;
         
         -- is there a further action due where the player has ended up?
         for each row place in monopoly.board where place.place = player.position begin
            set found = found + 1;
            write into monopoly values (tag, ' player has moved to position ' + to_char(player.position) + ': "'+place.title+'" (set: "'+place.sets+'", rule: "'+place.rule+'")');
            write into monopoly values (tag,' Updated Place: place: '+to_char(place.place)+', title: "' + place.title + '", set: "' + place.sets+'", rule: "'+place.rule+'"');
            call procedure monopoly_event( 'monopoly_playerAdvanceOrRetreat', gameId, game.moves, player.uid, place.title, 2, 'Player to move from position '+to_char(player.position)+' to '+to_char(position), false, '', '');

            if( place.rule = 'go' ) then
               call procedure monopoly_creditToPlayer(gameId, player.uid, 200);
               write into monopoly values (tag, ' player has landed on go and has received an additional £200');
               call procedure monopoly_nextPlayersTurn(gameId);

            elseif( place.rule = 'incomeTax' ) then
               write into monopoly values (tag, 'incomeTax rule');
               -- pay £200 
               set game.state = 2;
               set game.action = place.rule;
               call procedure monopoly_event( 'monopoly_playerAdvanceOrRetreat', gameId, game.moves, player.uid, place.title, 2, 'The player needs to pay their Income tax (£200)', false, '', '');

            elseif( place.rule = 'communityChest' ) then
               write into monopoly values (tag, 'communityChest rule');
               set game.state = 2;
               set game.action = place.rule;
               call procedure monopoly_event( 'monopoly_playerAdvanceOrRetreat', gameId, game.moves, player.uid, place.title, 2, 'The player needs to draw a Community Chest card', false, '', '');

            elseif( place.rule = 'deed' ) then
               -- is rent due or purchase possible
               for each row deed in monopoly.deeds where deed.gameId = game.gameId and deed.title = place.title begin
                  write into monopoly values (tag, 'rule: "'+ place.rule+'", deed: "'+deed.title+'", owner: ' + to_char(deed.owner) + ', state: ' + to_char(deed.state) );
                  if( deed.owner > -1 and deed.owner <> player.uid and deed.state > -1 ) then
                     -- rent due
                     write into monopoly values (tag, 'Rent due');
                     -- continue players turns, the owner has untill the second person in turn takes their turn to spot that rent is due
                     call procedure monopoly_event( 'monopoly_playerAdvanceOrRetreat', gameId, game.moves, player.uid, place.title, 2, 'The player owes rent on this property', false, '', '');
                     call procedure monopoly_nextPlayersTurn(gameId);

                  elseif ( deed.owner = -1 ) then
                     write into monopoly values (tag, 'property available for purchase');
                     set game.state = 2;
                     set game.action = 'forSale';
                     call procedure monopoly_event( 'monopoly_playerAdvanceOrRetreat', gameId, game.moves, player.uid, place.title, 2, 'The player has the opportunity to purchase this property', false, '', '');

                  end if;
               end;
            else
               call procedure monopoly_nextPlayersTurn(gameId);

            end if;
         end;
      end;
   end;
   if( found < 3 ) then 
      write into monopoly values (tag, 'The game, user or board record not found' );
      cancel;
   end if;
end;
go

-- g monopoly_playerAdvanceOrRetreat
grant execute on sql procedure monopoly_playerAdvanceOrRetreat to role 'monopolyUser';
go



-- monopoly_enactCard procedure
create or replace procedure monopoly_enactCard(in gameId Integer, in alternativeAction boolean ) 
-- called via JS when a Chance/Community Chest card has been drawn and its instruction requires enactment
-- the game should still be in state 2 (blocked) after the monopoly_drawCard
-- card CC15 (pay a £10 fine or take a chance) allows the player to select two actions:  alternativeAction=false or alternativeAction=true

declare tag char(128); found int; cardId char(4); activePlayer int; fine int;
begin
   set tag = '[' + to_char(getdate) + '][monopoly_enactCard()] ';
   write into monopoly values (tag, 'gameId: ', to_char(gameId) );

   set cardId = '';
   for each row game in monopoly.games where game.gameId = gameId and game.state = 2 and (game.action='communityChest' and game.moves=game.cardsCChest4Move) or (game.action='chance' and game.moves=game.cardsChance4Move) begin
      set found = found + 1;
      write into monopoly values(tag, 'gameId: ' + to_char(game.gameId),', moves: ' + to_char(game.moves),', state: ' + to_char(game.state),', activePlayer: ' + to_char(game.activePlayer),', action: "'+game.action+'".');
      write into monopoly values(tag, 'CH pointer: ' + to_char(game.cardsChancePointer) + ', forMove: ' + to_char(game.cardsChance4Move) + ', GOOJF Owner: ' + to_char(game.cardsChanceGoojfOwner) );
      write into monopoly values(tag, 'CC pointer: ' + to_char(game.cardsCChestPointer) + ', forMove: ' + to_char(game.cardsCChest4Move) + ', GOOJF Owner: ' + to_char(game.cardsCChestGoojfOwner) );

      if( game.action = 'chance' ) then
         set cardId = substr( game.cardsChance, 4*game.cardsChancePointer+1, 4 );
      else
         set cardId = substr( game.cardsCChest, 4*game.cardsCChestPointer+1, 4 );
      end if;
      set activePlayer = game.activePlayer;
   end;

   if( cardId = '' ) then 
      write into monopoly values (tag, 'The game was not found, in the wrong state or no card has been drawn' );
      cancel;
   end if;

   set found = 0;
   for each row card in monopoly.inventory where card.type='card' and card.item=cardId begin
      set found = found + 1;
      write into monopoly values(tag, 'cardId="' +cardId+ '", text="'+card.text+'"' );

      if(card.item in ('CC01', 'CH01', 'CH03', 'CH04', 'CH05', 'CH06', 'CC02', 'CH07')) then
         -- Advance to X If you pass "Go" collection £200 | Go back 3 squares | Go Back to old Kent Road
         call procedure monopoly_playerAdvanceOrRetreat(gameId);

      elseif(card.item in ('CC03')) then
         -- Go to jail. Move directly to jail. Do not pass "Go". Do not collect £200 (Community Chest)
         call procedure monopoly_player_jailTime(gameId, activePlayer, 'sentence CC03');

      elseif(card.item in ('CH02')) then
         -- Go to jail. Move directly to jail. Do not pass "Go". Do not collect £200 (Chance)
         call procedure monopoly_player_jailTime(gameId, activePlayer, 'sentence CH02');

      elseif(card.item in ('CC16')) then
         -- Get out of jail free. This card may be kept until needed or sold
         call procedure monopoly_tradeGOOJFC(gameId, activePlayer, 'communityChest', 0);

      elseif(card.item in ('CH16')) then
         -- Get out of jail free. This card may be kept until needed or sold
         call procedure monopoly_tradeGOOJFC(gameId, activePlayer, 'chance', 0);

      elseif(card.item in ('CH08')) then
         -- Make general repairs on all of your houses. For each house pay £25. For each hotel pay £100 Pay Fees
         set fine = 0;
         for each row deed in monopoly.deeds where deed.gameId=gameId and deed.owner=activePlayer and deed.state > 0 begin
                if( deed.state = 5 ) then set fine = fine + 100;
            elseif( deed.state = 4 ) then set fine = fine + (25*4);
            elseif( deed.state = 3 ) then set fine = fine + (25*3);
            elseif( deed.state = 2 ) then set fine = fine + (25*2);
            elseif( deed.state = 1 ) then set fine = fine + (25*1);
            end if;

         end;

         write into monopoly values (tag,'CH08 | calculated a general repair fee: £'+to_char(fine));
         if( fine=0 ) then
            call procedure monopoly_nextPlayersTurn(gameId);
         else 
            call procedure monopoly_demandPayment(gameId,activePlayer,fine,-1,card.text);
         end if;

      elseif(card.item in ('CC09')) then
         -- You are assessed for street repairs: £40 per house, £115 per hotel Pay Fees
         set fine = 0;
         for each row deed in monopoly.deeds where deed.gameId=gameId and deed.owner=activePlayer and deed.state > 0 begin
                if( deed.state = 5 ) then set fine = fine + 115;
            elseif( deed.state = 4 ) then set fine = fine + (40*4);
            elseif( deed.state = 3 ) then set fine = fine + (40*3);
            elseif( deed.state = 2 ) then set fine = fine + (40*2);
            elseif( deed.state = 1 ) then set fine = fine + (40*1);
            end if;
         end;
         write into monopoly values (tag,'CC09 | calculated a street repair fee: £'+to_char(fine));
         if( fine=0 ) then
            call procedure monopoly_nextPlayersTurn(gameId);
         else 
            call procedure monopoly_demandPayment(gameId,activePlayer,fine,-1,card.text);
         end if;

      elseif(card.item in ('CH15') and alternativeAction=true ) then
         -- Pay a £10 fine or take a "Chance" -> pay fine
         call procedure monopoly_demandPayment(gameId,activePlayer,10,-1,card.text);

      elseif(card.item in ('CH15') and alternativeAction=true ) then
         -- Pay a £10 fine or take a "Chance" -> take chance
         call procedure monopoly_drawCard(gameId);

      elseif(card.item in ('CC15')) then
         -- You have won second prize in a beauty contest. Collect £10
         call procedure monopoly_creditToPlayer(gameId,activePlayer,10);
         call procedure monopoly_nextPlayersTurn(gameId);


      elseif(card.item in ('CC12')) then
         -- Income tax refund. Collect £20
         call procedure monopoly_creditToPlayer(gameId,activePlayer,20);
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif(card.item in ('CC11')) then
         -- Receive interest on 7% preference shares: £25
         call procedure monopoly_creditToPlayer(gameId,activePlayer,25);
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif(card.item in ('CH15','CC10')) then
         -- CH15 Bank pays you dividend of £50
         -- CC10 From sale of stock you get £50
         call procedure monopoly_creditToPlayer(gameId,activePlayer,50);
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif(card.item in ('CC08', 'CH14', 'CC09')) then
         -- CC08 Annuity matures. Collect £100
         -- CH14 You have won a crossword competition. Collect £100
         -- CC09 You inherit £100
         call procedure monopoly_creditToPlayer(gameId,activePlayer,100);
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif(card.item in ('CH13')) then
         -- Your building loan matures. Receive £150
         call procedure monopoly_creditToPlayer(gameId,activePlayer,150);
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif(card.item in ('CC07')) then
         -- Bank error in your favour. Collect £200
         call procedure monopoly_creditToPlayer(gameId,activePlayer,200);
         call procedure monopoly_nextPlayersTurn(gameId);

      elseif(card.item in ('CC14')) then
         -- It is your birthday. Collect £10 from each player

         -- create a fine demand for each other player
         for each row otherPlayers in monopoly.players where otherPlayers.gameId=gameId and otherPlayers.uid <> activePlayer begin
            write into monopoly values (tag,'CC14 | demainding £10 from player ' + to_char(otherPlayers.uid));
            call procedure monopoly_demandPayment(gameId,otherPlayers.uid,10,-1,card.text);
         end;

      elseif(card.item in ('CH12')) then
         --  Speeding fine £15
         call procedure monopoly_demandPayment(gameId,activePlayer,15,-1,card.text);

      elseif(card.item in ('CH11')) then
         --  Drunk in charge fine £20
         call procedure monopoly_demandPayment(gameId,activePlayer,20,-1,card.text);

      elseif(card.item in ('CC05', 'CC06')) then
         -- CC05 Doctors fee. Pay £50
         -- CC06 Pay your insurance premium £50
         call procedure monopoly_demandPayment(gameId,activePlayer,50,-1,card.text);

      elseif(card.item in ('CC04')) then
         -- Pay hospital £100
         call procedure monopoly_demandPayment(gameId,activePlayer,100,-1,card.text);

      elseif(card.item in ('CH10')) then
         -- Pay school fees of £150
         call procedure monopoly_demandPayment(gameId,activePlayer,150,-1,card.text);

      end if;
   end;

   if( found < 1 ) then 
      write into monopoly values (tag, 'The card was not found in the inventory' );
      cancel;
   end if;

end;
go

-- g monopoly_enactCard
grant execute on sql procedure monopoly_enactCard to role 'monopolyUser';
go



-- monopoly_auctionDeedStart
create or replace procedure monopoly_auctionDeedStart(in gameId Integer) 
   -- start an auction on the un-owned deed the active player is placed at
   -- signals to all other players that they can submit bids
   -- bids are placed via monopoly_auctionDeedBid, which handles the logic of wining bid 
declare tag char(128); found integer; cardId char(4)
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_auctionDeedStart()] ';
   write into monopoly values (tag, 'gameId=', to_char(gameId));
   for each row game in monopoly.games where game.gameId = gameId and game.state = 2 and game.action = 'forSale' begin
      set found = found + 1;
      write into monopoly values(tag, 'gameId='+to_char(game.gameId)+', moves='+to_char(game.moves)+', state: '+to_char(game.state)+', activePlayer='+to_char(game.activePlayer)+', action: "'+game.action+'".');
      for each row activeplayer in monopoly.players where activeplayer.gameId = game.gameId and activeplayer.uid = game.activePlayer and activeplayer.isActive = true begin
         set found = found + 1;
         write into monopoly values(tag, 'activeplayer | position=' + to_char(activeplayer.position));
         for each deed in monopoly.deeds where deed.gameId = game.gameId and deed.boardPlace = activeplayer.position and deed.owner < 0 begin
            set found = found + 1;
            write into monopoly values(tag, 'deed | title="'+deed.title+'", sets="'+deed.sets+'", boardPlace='+to_char(deed.boardPlace));
            -- 1) insert alerts.status class 1502 events for each player

            for each row player in monopoly.players where player.gameId = game.gameId and player.isActive = true begin
               insert into alerts.status ( Type, Class, Manager, Agent, ExpireTime, Grade, OwnerUID, AlertGroup, Severity, Poll, ProbeSubSecondId, SuppressEscl, PhysicalPort, INT02, Tally, Identifier, Summary ) 
               values ( 
                  1, 1502, 'games', 'Monopoly', 60*60*24*12, game.gameId, player.uid, 'Title Deed Auction Bid',  1, game.moves, game.touch, 1, deed.boardPlace, 0, 0,
                  'monopoly_event (1502 Title Deed Auction Bid): (gameId=' + to_char(game.gameId) + ', moveSerial=' + to_char(game.moves) + ', component="monopoly_auctionDeedStart", deed='+to_char(deed.boardPlace)+', playerUID='+to_char(player.uid),
                  'no bid submitted'
               );
            end;

            -- 2) set the game state to auction
            set game.action = 'auction';
            call procedure monopoly_event('monopoly_auctionDeedStart', gameId, game.moves, activeplayer.uid, deed.title, 3, 'Player has offered a title deed to auction', true, deed.title + ' ('+deed.sets+')', 'Player has offered a title deed to auction' );
         end;
      end;
   end;
   if( found < 3 ) then 
      write into monopoly values (tag, 'The game, player, user or deed record not found' );
      cancel;
   end if;
end;
go

-- g monopoly_auctionDeedStart
grant execute on sql procedure monopoly_auctionDeedStart to role 'monopolyUser';
go

-- monopoly_auctionDeedBid
create or replace procedure monopoly_auctionDeedBid(in gameId Integer, in uid Integer, in bid Integer) 
-- allows a player to submit a bid in an auction for a title deed
-- multiple bids can be submitted
-- a player can opt to withdraw from the auction by submitting a negative value bid
-- the auction is won when all other players have withdrawn
-- bids are recorded via alerts.status rows
-- Sev 1 : no bids placed
-- Sev 2 : bid placed
-- Sev 3 : withdrawn
-- Sev 4 : bid error
declare tag char(128); found integer; serial integer; activeBidders integer; higestBid integer; higestBidder integer; deedTitle Char(24);
begin
   set found = 0;
   set tag = '[' + to_char(getdate) + '][monopoly_auctionDeedBid()] ';
   write into monopoly values (tag, 'gameId=', to_char(gameId)+', uid='+to_char(uid)+', bid=£'+to_char(bid));
   for each row game in monopoly.games where game.gameId = gameId and game.state = 2 and game.action = 'auction' begin
      set found = found + 1;
      write into monopoly values(tag, 'gameId='+to_char(game.gameId)+', moves='+to_char(game.moves)+', state: '+to_char(game.state)+', activePlayer='+to_char(game.activePlayer)+', action: "'+game.action+'".');
      for each row bidder in monopoly.players where bidder.gameId = game.gameId and bidder.uid = uid and bidder.isActive = true begin
         set found = found + 1;
         write into monopoly values(tag, 'bidder | cash=£' + to_char(bidder.cash));
         for each row bids in alerts.status where bids.Class=1502 and bids.Grade=game.gameId and bids.Poll=game.moves and bids.OwnerUID=bidder.uid begin
            set found = found + 1;
            write into monopoly values(tag, 'bids | Severity='+to_char(bids.Severity)+', INT02=£'+to_char(bids.INT02)+' (current bid)');
            if( bid < 0 ) then
               set bids.Severity       =  3;  -- withdrawn
               set bids.Summary        =  'The player has withdrawn from the auction';
               set bids.LastOccurrence =  getdate();
               set game.touch          =  game.touch + 1;
            elseif( bid > bidder.cash ) then
               set bids.Severity       =  2;  -- bid placed
               set bids.Summary        =  'A bid was rejected due to the player having insufficent funds to cover the bid';
               set bids.LastOccurrence =  getdate();
               set game.touch          =  game.touch + 1;
            elseif( bid = bids.INT02 ) then
               -- same bid re-submitted, ignore quietly
               write into monopoly values(tag, 'bids | player placed the same bid');
            else
               if( bids.Severity = 1 ) then
                  set bids.Summary        =  'The player has placed their first bid';
               elseif( bid < bids.INT02 ) then
                  set bids.Summary        =  'The player has reduced their bid';
               else
                  set bids.Summary        =  'The player has raised their bid';
               end if;
               set bids.Severity       =  2;  -- bid placed
               set bids.INT02          =  bid;
               set bids.Tally          =  bids.Tally + 1;
               set bids.BSM_Identity   =  bids.BSM_Identity + to_char(to_int(getdate())) + ',' + to_char(bid) + ';';
               set bids.LastOccurrence =  getdate();
               set game.touch          =  game.touch + 1;
            end if;
         end;
      end;

      if( found < 3 ) then 
         write into monopoly values (tag, 'The bidder or bid record not found' );
         cancel;
      end if;

      -- re-verify all active bids
      for each row bids in alerts.status where bids.Class=1502 and bids.Grade=game.gameId and bids.Poll=game.moves begin
         for each row overBidder in monopoly.players where overBidder.gameId = game.gameId and overBidder.uid = bids.OwnerUID and overBidder.cash < bids.INT02 begin
            write into monopoly values(tag, 'bids: OwnerUID='+to_char(bids.OwnerUID)+', Severity='+to_char(bids.Severity)+', INT02=£'+to_char(bids.INT02)+', Tally='+to_char(bids.Tally)+' | overBidder: cash=£' + to_char(overBidder.cash)+', isActive='+to_char(overBidder.isActive));
            set bids.Severity = 4;
            set bids.Summary = 'The player no longer has sufficent funds to cover their bid';
         end;
      end;

      -- scan the other bids to see if the auction is over
      set activeBidders = 0;
      set higestBid = 0;
      set higestBidder = 0;
      for each row bids in alerts.status where bids.Class=1502 and bids.Grade=game.gameId and bids.Poll=game.moves begin
         write into monopoly values(tag, 'bids: OwnerUID='+to_char(bids.OwnerUID)+', Severity='+to_char(bids.Severity)+', INT02=£'+to_char(bids.INT02)+', Tally='+to_char(bids.Tally));
         if( bids.Severity in (1,2,5) ) then
            set activeBidders = activeBidders + 1;
         end if;
         if( bids.Severity = 2 and bids.INT02 > higestBid ) then
            set higestBid = bids.INT02;
            set higestBidder = bids.OwnerUID;
         end if;
      end;
      write into monopoly values(tag, 'bid scan | activeBidders='+to_char(activeBidders)+', higestBid=£'+to_char(higestBid)+', higestBidder='+to_char(higestBidder));

      if( activeBidders = 1 ) then
         -- auction over
         set deedTitle = '';
         for each row bids in alerts.status where bids.Class=1502 and bids.Grade=game.gameId and bids.Poll=game.moves and bids.OwnerUID=higestBidder begin
            for each row winningPlayer in monopoly.players where winningPlayer.gameId = game.gameId and winningPlayer.uid = higestBidder and winningPlayer.isActive = true begin
               for each row deed in monopoly.deeds where deed.gameId = game.gameId and deed.boardPlace = bids.PhysicalPort begin
                  for each row winningUser in security.users where winningUser.UserID = winningPlayer.uid begin
                     set bids.Severity = 5;
                     set bids.Summary  = 'The player has won the auction';
                     set deedTitle = deed.title;
                     call procedure monopoly_event('monopoly_auctionDeedBid', gameId, game.moves, winningPlayer.uid, deed.title, 3, 'Player has won the auction with bid £'+to_char(higestBid)+', the auction is over', true, deed.title + ' ('+deed.sets+')', winningUser.FullName + ' has won the auction with a bid of £'+to_char(higestBid) );
                  end;
               end;
            end;
         end;

         if( deedTitle = '') then
            write into monopoly values (tag, 'The winning bid, player, deed or user record not found' );
            cancel;
         end if;

         call procedure monopoly_sellDeedToPlayer( gameId, higestBidder, deedTitle, higestBid);
         set game.state    =  1;
         set game.action   =  '';
         call procedure monopoly_nextPlayersTurn(gameId);
      end if;
   end;
   
   if( found < 3 ) then 
      write into monopoly values (tag, 'The game, bidder or bid record not found' );
      cancel;
   end if;
end;
go

-- g monopoly_auctionDeedBid
grant execute on sql procedure monopoly_auctionDeedBid to role 'monopolyUser';
go




-- e schema
call procedure monopoly_event( 'schema-apply.sql', -1, -1, -1, '', 2, 'The Monopoly schema has been re-deployed.', false, '', '');
go

-- fin
