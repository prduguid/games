
-- t/dt deeds
delete from monopoly.deeds;
drop table monopoly.deeds;
go

-- t/dt players
delete from monopoly.players;
drop table monopoly.players;
go

-- t/dt games
delete from monopoly.games;
drop table monopoly.games;
go

-- t/dt monopoly.board
delete from monopoly.board;
drop table monopoly.board;
go

-- t/dt inventory
delete from monopoly.inventory;
drop table monopoly.inventory;
go

-- t/dt inventory_deeds data
delete from monopoly.inventory_deeds;
drop table monopoly.inventory_deeds;
go

-- dv monopoly.users
drop view monopoly.users;
go

-- d db monopoly
drop database monopoly;
go


-- d log file
drop file monopoly;
go

-- del conversion
delete from alerts.conversions where KeyField in ('OwnerUID-1');
go

-- expire events
delete from alerts.status where Class in ( 1500, 1501, 1502 );
go

