-- 1 c roles ...
CREATE ROLE 'monopolyCardRandomiser' ID 20 COMMENT 'for the monopolyCardRandomiser.pl helper script';
CREATE ROLE 'monopolyUser' ID 21 COMMENT 'for players of monopoly, logged in via the web';
go

-- 2 c groups
CREATE GROUP 'monopolyCardRandomiser' ID 20 COMMENT 'for the monopolyCardRandomiser.pl helper script';
CREATE GROUP 'monopolyUser' ID 21 COMMENT 'for players of monopoly, logged in via the web';
go

-- 3 g roles to groups
grant role 'monopolyCardRandomiser' to group 'monopolyCardRandomiser';
grant role 'monopolyUser' to group 'monopolyUser';
go

-- 4 g
grant ISQL to role 'monopolyCardRandomiser', 'monopolyUser';
grant ISQL Write to role 'monopolyCardRandomiser', 'monopolyUser';
go


-- 5 c players accounts
create user 'peter'  id 101 full name 'Peter'  password 'monopoly' PAM false;
create user 'helen'  id 102 full name 'Helen'  password 'monopoly' PAM false;
create user 'leo'    id 103 full name 'Leo'    password 'monopoly' PAM false;
create user 'trixi'  id 104 full name 'Trixi'  password 'monopoly' PAM false;
create user 'ciran'  id 105 full name 'Ciran'  password 'monopoly' PAM false;
create user 'robert' id 106 full name 'Robert' password 'monopoly' PAM false;
go

-- 5 i conversions
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID101','OwnerUID',101,'Peter' );
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID102','OwnerUID',102,'Helen' );
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID103','OwnerUID',103,'Leo'   );
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID104','OwnerUID',104,'Trixi' );
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID105','OwnerUID',105,'Ciran' );
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID106','OwnerUID',106,'Robert');
go

-- 7 add users to group
alter group 'monopolyUser' assign MEMBERS 'peter', 'helen', 'leo', 'trixi', 'ciran', 'robert';
go

-- 8 c monopolyCardRandomiser user
create user 'monopolyCardRandomiser' id 20 full name 'monopolyCardRandomiser' password 'monopolyCardRandomiser' PAM false;
go

-- 9 i conversion
insert into alerts.conversions (KeyField,Colname,Value,Conversion) values ('OwnerUID20','OwnerUID',20,'monopolyCardRandomiser');
go

-- 10 add user to group
alter group 'monopolyCardRandomiser' assign MEMBERS 'monopolyCardRandomiser';
go

-- 11 grants

grant select on table alerts.status to role 'monopolyUser';
grant select on table alerts.conversions to role 'monopolyUser';
