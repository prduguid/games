-- monopoly_event(component, gameId, moveSerial, userId, deed, severity, txt, deduplicate, doToast)

-- toast about a game, clear
call monopoly_event('toaster test game 1',10,0,0,'',0,'toast about a game, clear',false,true);
-- toast about a game, indeternimate
call monopoly_event('toaster test game 2',10,0,0,'',1,'toast about a game, indeternimate',false,true);
-- toast about a game, warning
call monopoly_event('toaster test game 3',10,0,0,'',2,'toast about a game, warning',false,true);
-- toast about a game, minor
call monopoly_event('toaster test game 4',10,0,0,'',3,'toast about a game, minor',false,true);
-- toast about a game, major
call monopoly_event('toaster test game 5',10,0,0,'',4,'toast about a game, major',false,true);
-- toast about a game, critical
call monopoly_event('toaster test game 6',10,0,0,'',5,'toast about a game, critical',false,true);


-- toast about a place, warning
call monopoly_event('toaster test place 1',10,0,0,'the sun',2,'toast about a game, clear',false,true);
-- toast about a game, indeternimate
call monopoly_event('toaster test place 2',10,0,0,'Pall Mall',1,'toast about a game, indeternimate',false,true);


-- toast about a player, minor
call monopoly_event('toaster test 3',10,3,101,'Trafalgar Square',4,'toast about a player, minor',true,true);
-- toast about a game, minor
call monopoly_event('toaster test 4',10,0,0,'',3,'toast about a game, minor',true,true);
-- toast about a game, major
call monopoly_event('toaster test 5',10,0,0,'',4,'toast about a game, major',true,true);
-- toast about a game, critical
call monopoly_event('toaster test 6',10,0,0,'',5,'toast about a game, critical',true,true);