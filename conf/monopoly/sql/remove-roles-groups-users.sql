-- 1 d roles
drop role 'monopolyCardRandomiser';
go
drop role 'monopolyUser';
go

-- 2 d groups
drop group 'monopolyCardRandomiser';
go
drop group 'monopolyUser';
go


-- 3 d users
drop user 'monopolyCardRandomiser';
go
drop user 'peter';
go
drop user 'helen';
go
drop user 'leo';
go
drop user 'trixi';
go
drop user 'cleo';
go
drop user 'robert';
go

-- 4 d conversion
delete from alerts.conversions where KeyField in ( 'OwnerUID20' );
go
delete from alerts.conversions where KeyField in ( 'OwnerUID101','OwnerUID102','OwnerUID103','OwnerUID104','OwnerUID105','OwnerUID106' );
go
