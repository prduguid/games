var authConfiguration = {
   defaultRedirect: "results.html",
   authHashKey: "omnibusAuthHash",
   toastTitle: "QA",
   expireMS: 1000 * 60 * 60 * 24 * 7 * 2 // 2 weeks in milliseconds 
}

function setAuthHash(username, password) {
   localStorage.setItem(
      authConfiguration.authHashKey,
      JSON.stringify({
         "username": username,
         "hash": "Basic " + btoa(username + ":" + password),
         /* Base64 Encoding -> btoa */
         "epoch": new Date().getTime()
      })
   );
}

function removeAuthSession() {
   localStorage.removeItem(authConfiguration.authHashKey);
}

function isAuthSessionPresentAndUnexpired() {
   // is there a valid and unexpired auth hash object stored for this session?
   if (localStorage.getItem(authConfiguration.authHashKey) != null) {
      var authHash = JSON.parse(localStorage.getItem(authConfiguration.authHashKey));
      return authHash.hash && authHash.epoch && authHash.epoch > 0 && authHash.epoch <= new Date().getTime() && authHash.epoch >= (new Date().getTime() - authConfiguration.expireMS);
   };
   return false;
}

function _getAuthObject() {
   var tag = tagTag("_getAuthObject");
   // is there a valid and unexpired auth hash object stored for this session?
   if (localStorage.getItem(authConfiguration.authHashKey) != null) {
      console.debug(tag, "authHashKey exists, verifying ...")
      var authHash = JSON.parse(localStorage.getItem(authConfiguration.authHashKey));
      if (authHash.hash && authHash.epoch && authHash.epoch > 0 && authHash.epoch <= new Date().getTime() && authHash.epoch >= (new Date().getTime() - authConfiguration.expireMS)) {
         console.debug(tag, "authHashKey exists and is validated", authHash);
         return authHash;
      } else {
         console.warn(tag, "authHashKey exists and is expired or corrupt", authHash);
         // expired or corrupt 
         doLogout("session expired or invalid", true);
      }
   } else {
      console.warn(tag, "authHashKey does not exist", authHash);
      // no session, redirect to the login screen
      doLogout("no prior session", true);
   }
   return null;
}

function getAuthHash() { return _getAuthObject().hash; }

function getAuthUserName() { return _getAuthObject().username; }



function doLogout(reason, offerRedirect) {
   var tag = tagTag("doLogout");
   removeAuthSession();
   var currentURL = new URL(window.location);
   var newURL = new URL(currentURL.origin);

   if (reason) {
      newURL.searchParams.set('reason', reason)
   }
   if (offerRedirect) {
      newURL.searchParams.set('redirect', currentURL.href)
   }

   console.log(tag, "current url: ", currentURL);
   console.log(tag, "new url: ", newURL);
   window.location.replace(newURL.href);

}

function validateAuth(redirect) {
   var tag = tagTag("validateAuth");
   var userName = getAuthUserName();
   console.info(tag, "Attempting to test authentication (userName: '" + userName + "', redirect: '" + redirect + "').");

   $.when(ajaxQueryByFilter("qa_users", "UserName in ('" + userName + "')", "")).done(function(data) {
      console.groupCollapsed(tag, "data collected ...");
      console.debug(tag, '[qa_users]', data);
      console.debug(tag, '[qa_users] rows returned: ' + data.rowset.affectedRows);
      console.debug(tag, '[qa_users] rows', data.rowset.rows);
      console.groupEnd();
      if (data.rowset.affectedRows == 1) {
         // auth test & query returned exactly one row; good!
         console.info(tag, "Successfully fetched one valid user record for username (UserID: " + data.rowset.rows[0].UserID + ", UserName: '" + data.rowset.rows[0].UserName + "', FullName: '" + data.rowset.rows[0].FullName + "')");
         if (redirect) {
            console.warn(tag, "redirecting to '" + redirect + "'");
            window.location.replace(redirect);
         }
      } else {
         // invalidate session, logout and re-direct to the login page with a reason
         doLogout("failed to verify user account", false);
      }
   });
}

function attemptAuth(username, password, redirect) {
   var tag = tagTag("attemptAuth");
   console.info(tag, "username: '" + username + "', password: '" + password.replaceAll(/./g, "*") + "', redirect: '" + redirect + "'");
   setAuthHash(username, password);
   validateAuth(redirect);
}

function checkAuth(redirect) {
   var tag = tagTag("checkAuth");
   if (isAuthSessionPresentAndUnexpired()) {
      validateAuth(redirect);
   } else {
      // no stored/valid session, enable login screen
      console.info(tag, "no prior session");
      return false;
   }
}