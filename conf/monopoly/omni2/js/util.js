var toast;

function initToast(){
   toast = new bootstrap.Toast( $('#burntToast') );
}

function burnToast( header, message ){
   $('#burntToast .toast-header strong').text(header);
   $('#burntToast .toast-body').text(message);
   toast.show();
   $('button#fetchEvent span.spinner-border').hide();
   $('button#fetchEvent visually-hidden').text("");
}

function hideToast(){
   toast.hide();
}

function tagTag(str){
   str = str.substring(0,29);
   return '[' + str + Array(29-str.length).join(' ') + '] ';
}

function GetURLParameter(sParam) {
   // stolen from http://www.jquerybyexample.net/2012/06/get-url-parameters-using-jquery.html
   var sPageURL = window.location.search.substring(1);
   var sURLVariables = sPageURL.split('&');
   for (var i = 0; i < sURLVariables.length; i++) {
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == sParam) {
         return decodeURI(sParameterName[1]);
      };
   };
};



function getRandomNumber(min, max) {
   min = Math.ceil(min);
   max = Math.floor(max);
   return Math.floor(Math.random() * (max - min + 1)) + min;
}
   

function getRandom(arr, n) {
   var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
   if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
   while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
   }
   return result;
}

function ucfirst(string) { return string.charAt(0).toUpperCase() + string.slice(1); }


function setCookie(cname, cvalue, exdays) {
   var d = new Date();
   d.setTime(d.getTime() + (exdays*24*60*60*1000));
   var expires = "expires="+ d.toUTCString();
   document.cookie = cname + "=" + cvalue + "; SameSite=Strict; " + expires + ";path=/";
}

function getCookie(cname) {
   var name = cname + "=";
   var decodedCookie = decodeURIComponent(document.cookie);
   var ca = decodedCookie.split(';');
   for(var i = 0; i <ca.length; i++) {
     var c = ca[i];
     while (c.charAt(0) == ' ') {
       c = c.substring(1);
     }
     if (c.indexOf(name) == 0) {
       return c.substring(name.length, c.length);
     }
   }
   console.warn(tagTag("getCookie"), "cookie '"+cname+"' not found");
   return null;
}

function isTruth(x) {
  // thanks to https://stackoverflow.com/questions/5219105/javascript-parsing-a-string-boolean-value
  if( typeof x ==='string' && x.length>0 ) {
     x = JSON.parse(  x.toLowerCase() );
  }
  return !!x;
}


