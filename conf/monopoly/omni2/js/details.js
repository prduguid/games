//    insert into tools.menu_items (KeyField,MenuID,MenuItemID,Title,Description,Enabled,InvokeType,InvokeID,Position,Accelerator) values ('0:114',0,114,'View Details','0:112',1,0,37,15,'')
//    select MAX(ActionID) from tools.actions
//    insert into tools.actions (ActionID,Name,Owner,Enabled,Description1,Description2,Description3,Description4,HasInternal,InternalEffect1,InternalEffect2,InternalEffect3,InternalEffect4,InternalForEach,HasExternal,ExternalEffect1,ExternalEffect2,ExternalEffect3,ExternalEffect4,ExternalForEach,RedirectOut,RedirectErr,Platform,JournalText1,JournalText2,JournalText3,JournalText4,JournalForEach,HasForcedJournal) 
//                       values (38,'vd',-1,1,'','','','',0,'','','','',0,1,'rundll32.exe url.dll,FileProtocolHandler "http://pd-omnibus-core1.fyre.ibm.com:8080/details.html?serial=@Serial"','','','',1,0,1,'NT','','','','',0,0)
//    insert into tools.action_access (ActionID,GID,ClassID,ActionAccessID) values (38,-1,-1,15)
//    
var cache = {};
var eventData = {};
var internalFields = ["Identifier", "Serial", "StateChange", "InternalLast", "ExpireTime", "ProcessReq", "ServerName", "ServerSerial", "ExtendedAttr", "OldRow", "ProbeSubSecondId", "CollectionFirst", "AggregationFirst", "DisplayFirst", "ServerName", "ServerSerial"];

$(document).ready(function() {
   var tag = tagTag('$(document-ready(){});');
   console.debug(tag, "");

   // init the toast
   initToast();

   // are we logged in?
   if (getAuthHash()) {
      // configure the action of the #navbar-settings-button & #navbar-logout-button buttons
      $("button#navbar-logout-button").click(function() {
         // ensure that session values have been removed
         console.info("logout button clicked.");
         doLogout();
      });
      // set the logged username, as taken from localStorage via auth.js 
      $("button#navbar-username-indicator-button").text(getAuthUserName());

      // configure the fetch button onClick action
      $('button#fetchEvent').click(function() { getEventData(); return false; });

      // configure the options onClick actions
      $("input#viewOptionsSuppressInternal").prop("checked", "true");
      $("input#viewOptionsSuppressBlank").prop("checked", "true");
      $('input#viewOptionsSuppressInternal').click(function() { populate("status"); return true; });
      $('input#viewOptionsSuppressBlank').click(function() { populate("status"); return true; });

      // configure the select option onChange action
      $("select#viewOptionsView").change(function() { populate("status"); return true; })

      // cache conversions & column visuals
      $.when(ajaxQueryByFilter("conversions", "", ""), ajaxQueryByFilter("col_visuals", "Colname != Title", ""), $.ajax({ type: 'GET', dataType: 'json', url: '/js/views.json' })).then(function(conversionsResponse, colVisualsResponse, viewsResponse) {
         console.groupCollapsed(tag, "conversions data collected ...");
         console.debug(tag, "[conversions             ] state: " + conversionsResponse[1] + ", rows returned: " + conversionsResponse[0].rowset.affectedRows + ") data: ", conversionsResponse[0]);
         console.debug(tag, "[col_visuals             ] state: " + colVisualsResponse[1] + ", rows returned: " + colVisualsResponse[0].rowset.affectedRows + ") data: ", colVisualsResponse[0]);
         console.debug(tag, "[views                   ] state: " + viewsResponse[1] + ", rows returned: " + viewsResponse[0].length + ") data: ", viewsResponse[0]);
         cache.conversions = conversionsResponse[0].rowset.rows;
         cache.colVisuals = colVisualsResponse[0].rowset.rows;
         cache.views = viewsResponse[0];
         console.groupEnd();

         // populate any views into the view select control
         cache.views.forEach(v => {
            $("select#viewOptionsView").append(
               $('<option>').val(v.name).text(v.name)
            );
         })

         // if there is a view query parameter in the URL query, verify and pre-select it

         var queryParameters = {};
         ["view", "ServerName", "ServerSerial"].forEach(q => {
            var qp = GetURLParameter(q);
            if (qp) {
               queryParameters[q] = qp;
            }
         });
         console.debug(tag, "queryParameters: ", queryParameters);

         if (queryParameters.view && cache.views.map(v => v.name).indexOf(queryParameters.view) >= 0) {
            $('#viewOptionsView').val(queryParameters.view);
         };

         if (queryParameters.ServerName && queryParameters.ServerSerial && queryParameters.ServerSerial.match(/^\d+$/)) {
            $('input#eventServerName').val(queryParameters.ServerName);
            $('input#eventServerSerial').val(queryParameters.ServerSerial);
            getEventData();
         }

      });
   }
});

function getEventData() {
   var tag = tagTag('getEventData()');
   var serverName = $('input#eventServerName').val();
   var serverSerial = $('input#eventServerSerial').val();
   console.info(tag, "serverName: '" + serverName + "', serverSerial: " + serverSerial);

   hideToast();
   if (serverName && serverSerial && serverSerial.match(/^\d+$/)) {
      // appear valid

      console.info(tag, "resetting page");

      // set the spinner, disable the button
      $('button#fetchEvent').empty().prop('disabled', true).append(
         $('<span>').attr({ class: 'spinner-border spinner-border-sm', role: 'status' }).append($('<span>').attr({ class: 'visually-hidden' }).text('Loading...'))
      );
      // disable the input fields
      $('input#eventServerName').prop('disabled', true);
      $('input#eventServerSerial').prop('disabled', true);

      // select the first tab
      new bootstrap.Tab($("#tab-status")).show();

      // purge any existing data
      ["status", "journal", "details", "extended"].forEach(tab => {
         $('table#' + tab + '-table>tbody').empty();
         $('table#' + tab + '-table>caption').empty().hide();
         $('table#' + tab + '-table').hide();
      });

      console.info(tag, "Fetching data for an omnibus event");

      // get the alerts.status record
      $.when(ajaxQueryByFilter("status", "ServerName in ('" + serverName + "') and ServerSerial in (" + serverSerial + ")", ""))
         .then(function(statusResponse) {
            console.debug(tag, '[statusResponse       ] ' + statusResponse + " (rows returned: " + statusResponse.rowset.affectedRows + ") data: ", statusResponse);
            if (statusResponse.rowset.affectedRows == 1 && statusResponse.rowset.rows[0]["Identifier"] && statusResponse.rowset.rows[0]["Serial"]) {
               // get the details (via identifier) and journal (via serial)
               $.when(ajaxQueryByFilter("journal", "Serial in (" + statusResponse.rowset.rows[0]["Serial"] + ")", "Chrono"), ajaxQueryByFilter("details", "Identifier in ('" + statusResponse.rowset.rows[0]["Identifier"] + "')", "Sequence")).then(function(journalRowsResponse, detailsRowsResponse) {
                  console.groupCollapsed(tag, "data collected ...");
                  console.debug(tag, '[journalRowsResponse     ] ' + journalRowsResponse[1] + " (rows returned: " + journalRowsResponse[0].rowset.affectedRows + ") data: ", journalRowsResponse[0]);
                  console.debug(tag, '[detailsRowsResponse     ] ' + detailsRowsResponse[1] + " (rows returned: " + detailsRowsResponse[0].rowset.affectedRows + ") data: ", detailsRowsResponse[0]);

                  if (statusResponse.rowset.affectedRows == 1) {
                     console.info(tag, "parsing status ...");
                     eventData.raw = statusResponse;
                     eventData.status = statusResponse.rowset.coldesc
                        // exclude ExtendedAttr
                        .filter(col => col.name != "ExtendedAttr")
                        // for each [other] column in the table
                        .map(col => { return { "Colname": col.name, "type": col.type } })
                        // fetch its value
                        .map(col => { col.Value = eventData.raw.rowset.rows[0][col.Colname]; return col; })
                        // evaluate if its a null or internal value
                        .map(col => {
                           if (col.Colname == "Severity" || (col.type == "string" && col.Value.length > 0) || (col.type != "string" && col.Value > 0)) {
                              col.nullValue = false;
                           } else {
                              col.nullValue = true;
                           };
                           col.internalField = internalFields.indexOf(col.Colname) >= 0;
                           return col;
                        })
                        // convert utc to dt objects
                        .map(col => {
                           if (!col.nullValue && col.type == "utc") {
                              col.dt = new Date(col.Value * 1000);
                           }
                           return col;
                        })
                        // enrich the value from conversions (ignore nullValue and strings)
                        .map(col => {
                           if (!col.nullValue && col.type != "string") {
                              var conversion = cache.conversions.filter(c => c.Colname == col.Colname && c.Value == col.Value)[0];
                              if (conversion && conversion.Conversion) { col.Conversion = conversion.Conversion; }
                           }
                           return col;
                        })
                        // enrich the Column title from column visuals data
                        .map(col => {
                           var colVisual = cache.colVisuals.filter(cv => cv.Colname == col.Colname)[0];
                           if (colVisual && colVisual.Title) { col.ColTitle = colVisual.Title; }
                           return col;
                        });

                     console.debug(tag, "eventData.status", eventData.status)
                     eventData.statusValues = eventData.status.length;
                     eventData.statusInternalFields = eventData.status.filter(col => col.internalField).length;
                     eventData.statusNullValues = eventData.status.filter(col => col.nullValue).length;
                     console.debug(tag, "statusValues: " + eventData.statusValues + ", statusInternalFields: " + eventData.statusInternalFields + ", statusNullValues" + eventData.statusNullValues);

                     console.info(tag, "parsing extended attributes ...");
                     var extendedAttr = eventData.raw.rowset.rows[0]["ExtendedAttr"].split(';').map(pair => pair.split('=').map(x => x.replace(/\"/gi, "")));
                     eventData.extendedAttr = extendedAttr.map(p => {
                           if (p[0].match(/^\d{1,3} OID/)) {
                              var n = p[0].split(" ")[0];
                              var key = "SNMP varbind " + n;
                              var oid = p[1];
                              var valueFilter = n + " Value";
                              var value = extendedAttr.filter(pv => pv[0] == valueFilter)[0][1].replace(/\"/gi, "");
                              p[0] = key;
                              p[1] = oid;
                              p[2] = value;
                              console.debug(tag, "extendedAttr.map", "n: " + n + ", key: " + key + ", oid: " + oid + ", valueFilter: " + valueFilter + ", value: " + value);
                           }
                           return p;
                        })
                        .filter(p => !p[0].match(/^\d{1,3} Value/));
                     console.debug(tag, "eventData.extendedAttr", eventData.extendedAttr)

                     console.info(tag, "parsing journals and details ...");
                     eventData.journals = journalRowsResponse[0].rowset.rows.map(journal => {
                        var out = {};
                        out.dt = new Date(journal.Chrono * 1000);
                        out.userName = cache.conversions.filter(c => c.Colname == "OwnerUID" && c.Value == journal.UID)[0].Conversion;
                        out.text = journal.Text1 + journal.Text2 + journal.Text3 + journal.Text4 + journal.Text5 + journal.Text6 + journal.Text7 + journal.Text8 + journal.Text9 + journal.Text10 + journal.Text11 + journal.Text12 + journal.Text13 + journal.Text14 + journal.Text15 + journal.Text16;
                        return out;
                     })

                     console.debug(tag, "eventData.journals", eventData.journals)
                     eventData.details = detailsRowsResponse[0].rowset.rows;
                     console.debug(tag, "eventData.details", eventData.details)

                     console.groupEnd();
                     console.info(tag, "populating data ...");

                     populate("status");
                     populate("journals");
                     populate("details");
                     populate("extendedAttr");

                  } else {
                     console.warn(tag, "no event data returned");
                  }

               });
            } else {
               console.error(tag, "incomplete event?")
            }
         })
         .always(function() {
            // reset and reenable controls
            $('button#fetchEvent').empty().prop('disabled', false).append(
               $('<span>').attr({ class: 'bi bi-arrow-clockwise', 'aria-label': 'click to reload event data' })
            );
            // disable the input fields
            $('input#eventServerName').prop('disabled', false);
            $('input#eventServerSerial').prop('disabled', false);
         })

   } else {
      // doesn't look like a serial number
      burnToast("Error", "The supplied event serial does not look valid")
   }
};

function populateStatus(dataSet, tableBody, tableCaption) {
   var tag = tagTag('populateStatus()');

   console.info(tag, "view selected : " + $("select#viewOptionsView").val());
   var status = [];
   if ($("select#viewOptionsView").val() == "default") {
      status = eventData[dataSet];
   } else if (cache.views.filter(v => v.name == $("select#viewOptionsView").val())[0]) {
      status = cache.views
         .filter(v => v.name == $("select#viewOptionsView").val())[0]
         .columns
         .map(v => {
            var field = eventData.status.filter(s => s.Colname == v.Colname)[0];
            console.debug(v.Colname, field);
            field.ColTitle = v.ColTitle;
            return field;
         });

   } else {
      console.error(tag, "select view not found");
      burnToast("Error", "the select view not found");
   }

   var suppressed = 0;

   status.forEach(row => {
      var tr = $('<tr>');

      console.debug(tag, "status row: ", row);
      // title
      if (row.ColTitle) {
         tr.append($('<td>').html($('<abbr>').prop("title", row.Colname).text(row.ColTitle)));
      } else {
         tr.append($('<td>').text(row.Colname));
      }

      // value
      if (row.type == "utc" && row.Value > 0) {
         tr.append($('<td>').append($('<code>').text(row.dt.toLocaleString())));
      } else if (row.Conversion) {
         //tr.append( $('<td>').html( $('<abbr>').prop("title",row.Value).html( $('<code>').text( row.Conversion ) ) ) );
         tr.append(
            $('<td>').append(
               $('<code>').addClass("conversionText").text(row.Conversion),
               $('<code>').addClass("conversionValue").text(row.Value)
            )
         );

      } else {
         tr.append($('<td>').html($('<code>').text(row.Value)));
      }

      // suppress internal status fields or null status values if options are set
      if ((!row.nullValue || !$("input#viewOptionsSuppressBlank").prop("checked")) && (!row.internalField || !$("input#viewOptionsSuppressInternal").prop("checked"))) {
         tableBody.append(tr);
      } else {
         suppressed++;
      }
   });

   if (dataSet == "status") {
      if (suppressed > 0) {
         tableCaption.text(suppressed + " field(s) have been suppressed, either system fields or containing blank data , change the options to show them.").show();
      } else if (eventData.statusInternalFields + eventData.statusNullValues > 0) {
         tableCaption.text("showing all fields, change the options to hide system fields or those containing blank data.").show();
      }
   }


}

function populate(dataSet) {
   var tag = tagTag('populate()');
   console.groupCollapsed(tag, "dataSet: " + dataSet);
   var table = $('table#' + dataSet + '-table');
   var tableBody = $('table#' + dataSet + '-table>tbody').empty();
   var tableCaption = $('table#' + dataSet + '-table>caption').empty().hide();
   if (eventData[dataSet].length > 0) {
      if (dataSet == "status") {
         populateStatus(dataSet, tableBody, tableCaption);
      } else {
         eventData[dataSet].forEach(row => {
            switch (dataSet) {
               case "journals":
                  // array of objects
                  tableBody.append(
                     $('<tr>').append(
                        $('<td>').text(row.userName),
                        $('<td>').append($('<code>').text(row.dt.toLocaleString())),
                        $('<td>').append($('<code>').text(row.Text))
                     )
                  );
                  break;

               case "details":
                  // array of objects
                  tableBody.append(
                     $('<tr>').append(
                        $('<td>').text(row.Name),
                        $('<td>').append($('<code>').text(row.Detail))
                     )
                  );
                  break;

               case "extendedAttr":
                  var tr = $('<tr>');
                  // array of arrays
                  tr.append($('<td>').text(row[0]));
                  if (row.length == 3) {
                     // SNMP style varbind, display over 3 columns
                     tr.append($('<td>').append($('<code>').text(row[1])), $('<td>').append($('<code>').text(row[2])));
                  } else {
                     // regular key/value pair, display over 2 columns
                     tr.append($('<td>').prop("colspan", "2").append($('<code>').text(row[1])));
                  }
                  tableBody.append(tr);
                  break;
            };
         });
      }
      table.show();
      console.log(tag, "Populated");
   } else {
      console.log(tag, "No " + dataSet + " data was found for this event");
      tableCaption.text("No " + dataSet + " data was found for this event").show();
   }
   console.groupEnd();
}