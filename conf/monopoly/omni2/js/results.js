var results = {
   cache: {},
   ui: {
      tabs: {
         dashboard: { name: 'dashboard', id: "tab-dashboard", selector: "#tab-dashboard", refreshable: true, conditional: false, tabId: "tab-dashboard" },
         results: { name: 'results', id: "tab-results", selector: "#tab-results", refreshable: true, conditional: true, tabId: "tab-results" },
         systems: { name: 'systems', id: "tab-systems", selector: "#tab-systems", refreshable: true, conditional: true, tabId: "tab-systems" },
      },
      spinner: $('<div>').attr({ class: 'spinner-border', role: 'status' }).append($('<span>').addClass('visually-hidden').text('Loading ...'))
   },
};

const reload_bs_tab = new Event('reload.bs.tab');

$(document).ready(function() {
   var tag = tagTag('$(document-ready(){});');
   console.debug(tag, "");

   // init the toast
   initToast()

   // are we logged in?
   if (getAuthHash()) {
      // configure the action of the #navbar-settings-button & #navbar-logout-button buttons
      $("button#navbar-logout-button").click(function() {
         // ensure that session values have been removed
         console.info("logout button clicked.");
         doLogout();
      });
      // set the logged username, as taken from localStorage via auth.js 
      $("button#navbar-username-indicator-button").text(getAuthUserName());

      // cache conversions & column visuals
      $.when(ajaxQueryByFilter("conversions", "", ""), ajaxQueryByFilter("col_visuals", "Colname != Title", ""), $.ajax({ type: 'GET', dataType: 'json', url: '/js/views.json' })).then(function(conversionsResponse, colVisualsResponse, viewsResponse) {
         console.groupCollapsed(tag, "conversions data collected ...");
         console.debug(tag, "[conversions             ] state: " + conversionsResponse[1] + ", rows returned: " + conversionsResponse[0].rowset.affectedRows + ") data: ", conversionsResponse[0]);
         console.debug(tag, "[col_visuals             ] state: " + colVisualsResponse[1] + ", rows returned: " + colVisualsResponse[0].rowset.affectedRows + ") data: ", colVisualsResponse[0]);
         console.debug(tag, "[views                   ] state: " + viewsResponse[1] + ", rows returned: " + viewsResponse[0].length + ") data: ", viewsResponse[0]);
         results.cache.conversions = conversionsResponse[0].rowset.rows;
         results.cache.colVisuals = colVisualsResponse[0].rowset.rows;
         results.cache.views = viewsResponse[0];
         console.groupEnd();

         initTabs();
      });
   }
});


function initTabs() {
   var tag = tagTag("initTabs()");
   console.info(tag, "initializing bootstrap tabs ...");
   Object.values(results.ui.tabs).map(tab => {
      console.debug(tag, "initializing tab: " + tab.name + ", id: " + tab.id + ", conditional: " + tab.conditional + ", refreshable: " + tab.refreshable);
      tab.object = document.getElementById(tab.id);
      if (tab.object) {
         tab.tabObj = new bootstrap.Tab(tab.object);
         if (!tab.tabObj) {
            console.error(tag, "bootstrap tab object not initialised: '" + tab.name + "'");
         }
      } else {
         console.error(tag, "tab element not found: '" + tab.name + "'");
      }
   });

   results.ui.tabs.systems.object.addEventListener('show.bs.tab', function(event) {
      var tag = tagTag('show.bs.tab');
      var tabConf = Object.values(results.ui.tabs).filter(tab => tab.tabId == event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '" + event.target.id + '", tabConf: ', tabConf);
      // trigger the content population
      results.ui.tabs.systems.object.dispatchEvent(reload_bs_tab);
   });

   results.ui.tabs.systems.object.addEventListener('reload.bs.tab', function(event) {
      var tag = tagTag('reload.bs.tab');
      var tabConf = Object.values(results.ui.tabs).filter(tab => tab.tabId == event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '" + event.target.id + '", tabConf: ', tabConf);
      // trigger the content population

      console.log(tag, "refreshing the systems list");
      // spinner
      $('#content-systems caption').empty().append(results.ui.spinner);

      $.when(ajaxQueryByFilter("fyre_systems", "(( Class = 3973 ) AND ( Agent not in ( 'OMNIbus SelfMonitoring' ) ))", "AlertGroup asc, LastOccurrence desc, Node asc")).then(function(statusResponse) {
         console.debug(tag, '[statusResponse   ] (' + statusResponse.rowset.affectedRows + ') rows: ', statusResponse.rowset.rows);
         var table = $('#content-systems tbody').empty();

         // each system will exist as two rows in the table, a summary and a details row
         statusResponse.rowset.rows.forEach(row => {
            var trSummaryId = row.ServerName + "-" + row.ServerSerial + "-system-summary-row";
            var trDetailsId = row.ServerName + "-" + row.ServerSerial + "-system-details-row";
            var trColouringClass = row.AlertGroup == 'fyrePlusCluster' ? "table-info" : "table-secondary";
            var trSummary = $('<tr>').attr({ class: "system-summary-row " + trColouringClass, id: trSummaryId }).append(
               // collapse button
               $('<td>').append($('<button>').attr({ class: 'btn btn-sm row-collapse-button collapsed', type: 'button', 'data-bs-toggle': 'collapse', 'data-bs-target': '#' + trDetailsId, 'aria-expanded': false, 'aria-controls': trDetailsId }).html($('<i>').addClass("bi"))),
               // fyre > AlertGroup
               $('<td>').text(row.AlertGroup),
               // stack
               $('<td>').text(row.Node),
               // description
               $('<td>').text(row.Summary),
               // Status > AlertKey
               $('<td>').text(row.AlertKey),
               // Last seen > LastOccurrence
               $('<td>').text(new Date(row.LastOccurrence * 1000).toLocaleString()),
            );

            var tableDetails = $('<table>').addClass('table table-striped table-borderless table-light m-0');
            if (row.AlertGroup == 'fyrePlusCluster') {
               // OpenShift Container Platform Cluster

               var ocUsername = row.X733CorrNotif.split(':')[0];
               var ocPassword = row.X733CorrNotif.split(':')[1];
               var apiURL = "https://api." + row.Node + ".cp.fyre.ibm.com:6443";
               var oc = "oc login " + apiURL + " --insecure-skip-tls-verify=true --username=\"" + ocUsername + "\" --password=\"" + ocPassword + "\"";
               var sut = $('<code>').append(
                  "## sut.properties",
                  "<br />dc-1.openshift.url=" + apiURL,
                  "<br />dc-1.openshift.password=" + ocPassword,
                  "<br />dc-1.openshift.username=" + ocUsername,
                  "<br />"
               );
               var appUrl = row.Summary.indexOf("katamari") >= 0 ?
                  "https://cpd-katamari.apps." + row.Node + ".cp.fyre.ibm.com" :
                  "https://netcool.noi.apps." + row.Node + ".cp.fyre.ibm.com";

               tableDetails.append(
                  $('<tbody>').append(
                     $('<tr>').append($('<th>').text('Console'), $('<td>').append($('<a>').attr({ href: row.URL }).html($('<code>').text(row.URL)))),
                     $('<tr>').append($('<th>').text('Application'), $('<td>').append($('<a>').attr({ href: appUrl }).html($('<code>').text(appUrl)))),
                     $('<tr>').append($('<th>').text('CLI login'), $('<td>').append($('<code>').text(oc))),
                     $('<tr>').append($('<th>').text('sut.properties'), $('<td>').html(sut)),
                     $('<tr>').append($('<th>').text('First seen'), $('<td>').text(new Date(row.FirstOccurrence * 1000).toLocaleString()))
                  )
               )
            } else {
               // on-prem system

               var sut = $('<code>').append(
                  "## sut.properties",
                  "<br />dc-1.topology.service.host=" + row.Node + "-svt.fyre.ibm.com",
                  "<br />"
               );
               var appUrl = "https://" + row.Node + "-svt.fyre.ibm.com"

               tableDetails.append(
                  $('<tbody>').append(
                     $('<tr>').append($('<th>').text('Application'), $('<td>').append($('<a>').attr({ href: appUrl }).html($('<code>').text(appUrl))), ),
                     $('<tr>').append($('<th>').text('sut.properties'), $('<td>').html(sut)),
                     $('<tr>').append($('<th>').text('First seen'), $('<td>').text(new Date(row.FirstOccurrence * 1000).toLocaleString()))
                  )
               );
            };

            var trDetails = $('<tr>').attr({ class: "collapse system-details-row " + trColouringClass, id: trDetailsId }).append(
               $('<td>').addClass('table-light').text(''),
               $('<td>').attr({ colspan: 5, class: 'p-0' }).append(tableDetails),
            );

            table.append(trSummary, trDetails);
         })
         $('#content-systems caption').empty().append($('<cite>').text('NB: Whilst ASM and/or SUT values are generated for all systems, not all systems may actually have an ASM install.  Last refreshed at ' + new Date().toLocaleTimeString()));
      });
   });

   // has a tab been specified as a url parameter
   var url = new URL(window.location);
   if (url.searchParams.has("tab")) {
      Object.values(results.ui.tabs).filter(tab => tab.name == url.searchParams.get("tab")).forEach(tab => {
         tab.tabObj.show();
      })
   }

}

function getCurrentTabName() { return $('#navbarSupportedContent .nav-tabs li button.active').attr('id'); }


function doUpdate() {
   var tag = tagTag("doUpdate()");
   Object.values(results.ui.tabs).filter(tab => tab.tabId == getCurrentTabName()).forEach(tab => {
      console.debug(tag, "dispatching event 'reload_bs_tab' to tab '" + tab.name + "'");
      tab.object.dispatchEvent(reload_bs_tab);
   })
}