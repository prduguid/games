var schema = {
   "conversions": {
      "db": "alerts",
      "url": "/omnibus/objectserver/restapi/alerts/conversions",
      "collist": ["Colname", "Value", "Conversion"],
   },
   "col_visuals": {
      "db": "alerts",
      "url": "/omnibus/objectserver/restapi/alerts/col_visuals",
      "collist": ["Colname", "Title"],
   },
   "status": {
      "db": "alerts",
      "url": "/omnibus/objectserver/restapi/alerts/status",
      "collist": ["*"]
   },
   "journal": {
      "db": "alerts",
      "url": "/omnibus/objectserver/restapi/alerts/journal",
      "collist": ["UID", "Chrono", "Text1", "Text2", "Text3", "Text4", "Text5", "Text6", "Text7", "Text8", "Text9", "Text10", "Text11", "Text12", "Text13", "Text14", "Text15", "Text16"]
   },
   "details": {
      "db": "alerts",
      "url": "/omnibus/objectserver/restapi/alerts/details",
      "collist": ["Name", "Detail"]
   },
   "users": {
      "db": "security",
      "url": "/omnibus/objectserver/restapi/security/users",
      "collist": ["UserID", "UserName", "FullName"]
   },
   "qa_users": {
      // the subset of omnibus users who are in the resultsUsers group, provided by a view
      "db": "qa",
      "url": "/omnibus/objectserver/restapi/qa/users",
      "collist": ["UserID", "UserName", "FullName"]
   },
   "fyre_systems": {
      "db": "alerts",
      "url": "/omnibus/objectserver/restapi/alerts/status",
      "collist": ["AlertGroup", "Node", "Summary", "AlertKey", "Tally", "LastOccurrence", "FirstOccurrence", "PhysicalCard", "Customer", "Location", "X733CorrNotif", "URL", "ServerName", "ServerSerial"]
   }
}

function ajaxQueryByKey(dataSet, keyField) {
   var tag = tagTag("ajaxQueryByKey()");
   return $.ajax({ type: "GET", dataType: 'json', headers: { 'Authorization': getAuthHash() }, url: schema[dataSet].url + "/kf/" + keyField, data: { "collist": schema[dataSet].collist.join(',') } })
      .fail(ajaxQueryFailHandler, tag)
}

function ajaxQueryByFilter(dataSet, filterString, orderByString) {
   var tag = tagTag("ajaxQueryByFilter()");
   return $.ajax({ type: 'GET', dataType: 'json', headers: { 'Authorization': getAuthHash() }, url: schema[dataSet].url, data: { 'collist': schema[dataSet].collist.join(','), 'filter': filterString, 'orderby': orderByString } })
      .fail(ajaxQueryFailHandler, tag)
}

function __ajaxSqlQuery(tag, sqlData) {
   console.debug(tag, "sqlData: '", sqlData);
   $.ajax({ type: "POST", dataType: 'json', headers: { 'Authorization': getAuthHash() }, contentType: "application/json; charset=utf-8", url: "/omnibus/objectserver/restapi/sql/factory", data: JSON.stringify(sqlData) })
      .fail(ajaxQueryFailHandler, tag)
}

function ajaxQueryFailHandler(xhr, status, errorThrown, tag) {
   console.error(tag, "API Query failed (Error: '" + errorThrown + "', Status: '" + status + "'.");
   console.debug(tag, "xhr", xhr);
   // process http status code
   console.info(tag, "Status code: " + xhr.status + ", text: '" + xhr.text + "'");

   switch (xhr.status) {
      case 401:
         // Authorization Required (401)
         console.error(tag, "Authorization attempt failed (401)");
         // invalidate session, logout and re-direct to the login page with a reason
         doLogout("Invalid credentials or disabled account, you have been logged out", true);
         break;

      case 502:
         // Bad Gateway (502) database unavailable
         console.error(tag, "Authorization (back end database unavailable?");

         // state failure
         burnToast(authConfiguration.toastTitle, "database unavailable, please try later or contact the administrator");

         break;

      default:
         // unknown failure code
         console.error(tag, "Unknown/Unexpected failure");

         // state failure
         burnToast(authConfiguration.toastTitle, "Unknown/Unexpected failure, please try later or contact the administrator/n(" + xhr.status + ") " + xhr.text);

   }
}


function setOmnibusPassword(password) {
   var tag = tagTag("setOmnibusPassword()");
   __ajaxSqlQuery(tag, { sqlcmd: "alter user '" + getAuthUserName() + "' set password '" + password + "';" });
}