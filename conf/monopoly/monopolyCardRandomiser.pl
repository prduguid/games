#!/usr/bin/perl
use strict;
use PRD::Standard  qw( getTimeStamp getTime );
use PRD::Util qw( :COMMON );
use Getopt::Long;
use LWP::UserAgent;
use JSON;
use Encode qw(encode_utf8);
use Net::Syslog;
use List::Util qw(shuffle);


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #   Set up variables
# # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
my %config = (
   syslog => {
      Facility    => 'local4',                           ## kernel, user, mail, system, security, internal, printer, news, uucp, clock, security2, FTP, NTP, audit, alert, clock2, local0, local1, local2, local3, local4, local5, local6, local7
      destinations => [
         #{ host  => 'syslog001.stjamesgate.local', port  => 514, },
         #{ host  => 'syslog002.stjamesgate.local', port  => 514, },
         { host  => 'localhost', port  => 514, },
      ],
   },
   input => {
      messageLevel   => 3,
      port            => 8100,
      hostname        => 'localhost',
   },
   db       => 'monopoly',
   table    => 'games',
   cards => [
      {
         set      => 'Chance',
         column   => 'cardsChance',
         values   => [ 'CH01', 'CH03', 'CH04', 'CH05', 'CH06', 'CH07', 'CH02', 'CH08', 'CH09', 'CH10', 'CH11', 'CH12', 'CH13', 'CH14', 'CH15', 'CH16', ],
      }, {
         set      => 'Community Chest',
         column   => 'cardsCChest',
         values   => [ 'CC01', 'CC02', 'CC03', 'CC04', 'CC05', 'CC06', 'CC07', 'CC08', 'CC09', 'CC10', 'CC11', 'CC12', 'CC13', 'CC14', 'CC15', 'CC16', ],
      },
   ]
);

my $standard = PRD::Standard->new( level => $config{input}{messageLevel}, coloured => 1, dir => $config{input}{logDir}, filelevel=>2, );

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #   Subroutines
# # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
sub syslogCreateSessionObjects (){
   my $failCounter = 0;
   ## Configure the Net::Syslog sessions, one for each destination
   for ( @{ $config{syslog}{destinations} } ){
      next if $_->{object};
      $standard->messageLogger(1,sprintf("Establishing a Net::Syslog session to %s:%i", $_->{host}, $_->{port}));
      unless( $_->{object} = new Net::Syslog( Name => $standard->getScriptName(), Facility => $config{syslog}{Facility}, rfc3164 => 1, SyslogHost  => $_->{host}, SyslogPort  => $_->{port} ) ){
         $standard->messageLogger(4, "Failed to create a syslog object");
         $failCounter++;
      }
   };
   return 1 unless $failCounter;
   return 0;
};
sub sendSyslog {
   my $message    = shift;
   my $priority   = shift || 'informational' ;    ## emergency, alert, critical, error, warning, notice, informational, debug
   my $failureCounter = 0;
   for ( @{ $config{syslog}{destinations} } ){
      if( $_->{object}->send( $message, Priority=>$priority ) ){
         $standard->messageLogger(2, "SYSLOG Sent");
      } else {
         $standard->messageLogger(3, "SYSLOG NOT Sent");
         $failureCounter++;
      };
   };
   return 1 if $failureCounter == 0;
};

sub genEvent ($$$$$$$$){
   my $uaRef         = shift;
   my $component     = shift;
   my $gameId        = shift;
   my $moveSerial    = shift;
   my $userId        = shift;
   my $deed          = shift;
   my $severity      = shift;
   my $txt           = shift;
   #my $deduplicate   = shift;
   #my $deduplicate   = shift ? JSON::true : JSON::false;


   #[netcool@netcool ~]$ curl -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Authorization: Basic YXNtOmFzbQ==' --url 'http://localhost:8080/objectserver/restapi/sql/factory' --request POST --verbose --data '{"sqlcmd" : "call procedure monopoly_event(\'
   #call procedure monopoly_event( 'monopoly_newGameRow', new.gameId, -1, 0, '', 3, 'new game created, title deeds, chance and community chest cards populated for game.', true );
   #create or replace procedure monopoly_event(in component Char(32), in gameId Integer, in moveSerial Integer, in userId Integer, in deed Char(32), in severity Integer, in txt Char(256), in deduplicate Boolean) 

   my %eventPayload = (
      sqlcmd => "call procedure monopoly_event('${component}', ${gameId}, ${moveSerial}, ${userId}, '${deed}', ${severity}, '${txt}', false, false);",
   );

   $standard->dumperLogger( "eventPayload (perl)", \%eventPayload );
   my $jsonRef =   encode_json ( \%eventPayload );
   $standard->dumperLogger( "eventPayload (json)", $jsonRef );

   my $url = "http://$config{input}{hostname}:$config{input}{port}/objectserver/restapi/sql/factory";

   my $req = HTTP::Request->new(POST => $url );
      $req->content_type('application/json');
      $req->content(encode_json ( \%eventPayload ));
   # Pass request to the user agent and get a response back
   my $res = $$uaRef->request($req);
   
   # Check the outcome of the response
   if ($res->is_success) {
      $standard->messageLogger(1,sprintf("Request                            : Successful (HTTP %i)", $res->code ));
   } else {
      $standard->messageLogger(3,"Request                            : Failed (".$res->status_line.")");
   }

}

sub _utf8Encode_DecodeJSON($) {
   ##
   ## Attempts to cleanly convert a string of JSON to a perl data structure (returned via reference)
   ## First UTF8 Encodes the string to encode any non-utf-8 characters
   ## Then decodes the json string into a Perl data structure
   my $stringRef = shift;
   $standard->dumperLogger("raw json string", $$stringRef );

   eval {
      return JSON->new->utf8->decode( Encode::encode_utf8( $$stringRef ) );
   } or do {
      my $e = $@;
      $standard->messageLogger(3, "Failed to decode as JSON the refereneced data ($e)");
      return 0;
   };

};


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #   Main
# # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
syslogCreateSessionObjects() or $standard->messageLogger(4, "Failed to create the Net::Syslog sessions");

$config{input}{get} = GetOptions (
   "level=i"                  => sub { $standard->pushLevel($_[1]) },
   "debug"                    => sub { $standard->pushLevel(1)     },
   "info"                     => sub { $standard->pushLevel(2)     },
   "warn"                     => sub { $standard->pushLevel(3)     },
   "error"                    => sub { $standard->pushLevel(4)     },
   "hostname=s"      => \$config{input}{hostname},
   "port=i"          => \$config{input}{port},
   "username=s"      => \$config{input}{username},
   "password=s"      => \$config{input}{password},
   "gameId=i"        => \$config{input}{gameId},
);

$standard->dumperLogger("input", $config{input});

if( length($config{input}{hostname}) > 0 && $config{input}{port} > 0 && length($config{input}{username}) > 0 && length($config{input}{password} ) > 0 && $config{input}{gameId} >= 0  ) {


   # Create a LWP user agent object
   my $ua            = LWP::UserAgent->new;
      $ua->agent("perl/0.1 ");
      $ua->timeout(10);
      $ua->credentials( "$config{input}{hostname}:$config{input}{port}", 'omnibus', $config{input}{username}, $config{input}{password} );
      $ua->default_header('Accept' => "application/json");

   my $url = "http://$config{input}{hostname}:$config{input}{port}/objectserver/restapi/$config{db}/$config{table}/kf/$config{input}{gameId}";
   $standard->messageLogger(1,"setting cards url            : $url" );

   # Generate a GET request
   my $reqGet   = HTTP::Request->new(GET => $url );
   # Pass request to the user agent and get a response back
   my $resGet        = $ua->request($reqGet);

   # Check the outcome of the response
   if ($resGet->is_success) {
      my $dlBytes    = do {use bytes; length( $resGet->content )};
      $standard->messageLogger(2,sprintf("Request                            : Successful (HTTP %i), returned %s bytes of data", $resGet->code, utilCommifyNumber($dlBytes) ));

      my $decodedJsonRef = _utf8Encode_DecodeJSON( \$resGet->content );
      $standard->messageLogger(1,"rows returned                   : ".${ $decodedJsonRef }{rowset}{affectedRows} );
      $standard->dumperLogger("returned content (perl)", ${ $decodedJsonRef }{rowset}{rows}[0]);

      for my $cardsSetRef ( @{$config{cards}} ) {
         $standard->messageLogger(1, "processing ${ $cardsSetRef }{column}" );
         #$standard->dumperLogger("\$cardsSetRef", $cardsSetRef);

         if( ${ $decodedJsonRef }{rowset}{rows}[0]{   ${ $cardsSetRef }{column}   } eq ""  ) {
            my $shuffledCardsString = join( '', shuffle( @{ $cardsSetRef->{values} } ));
            $standard->messageLogger(2, "populating shuffled cards into '${ $cardsSetRef }{column}' ('${shuffledCardsString}')" );
            #$standard->dumperLogger("shuffledCardsString", $shuffledCardsString);

            ## create the json to patch the row
            my %patch = (
               rowset => {
                  coldesc => [ { name => ${ $cardsSetRef }{column}, type => "string", }, ],
                  rows => [ { "${ $cardsSetRef }{column}" => $shuffledCardsString, }, ]
               }
            );
            $standard->dumperLogger("patch payload", \%patch );

            my $req = HTTP::Request->new(PATCH => $url );
               $req->content_type('application/json');
               $req->content(encode_json ( \%patch ));
            # Pass request to the user agent and get a response back
            my $res = $ua->request($req);
            
            # Check the outcome of the response
            if ($res->is_success) {
               $standard->messageLogger(1,sprintf("Request                            : Successful (HTTP %i)", $res->code ));
               genEvent( \$ua, 'monopolyCardRandomiser.pl', $config{input}{gameId}, -1, -1, ${ $cardsSetRef }{set}, 1, "successfully deployed a shuffled deck of ${ $cardsSetRef }{set} cards");
            } else {
               $standard->messageLogger(3,"Request                            : Failed (".$res->status_line.")");
               genEvent( \$ua, 'monopolyCardRandomiser.pl', $config{input}{gameId}, -1, -1, ${ $cardsSetRef }{set}, 4, "failed to deploy a shuffled deck of ${ $cardsSetRef }{set} cards (".$res->status_line.")");
            }

         } else {
            $standard->messageLogger(3, "cards have already been populated into ${ $cardsSetRef }{column}" );
            genEvent( \$ua, 'monopolyCardRandomiser.pl', $config{input}{gameId}, -1, -1, ${ $cardsSetRef }{set}, 3, "failed to deploy a shuffled deck of ${ $cardsSetRef }{set} cards (already deployed?)");
         }
      } 
   } else {
      $standard->messageLogger(3,"Request                            : Failed (".$resGet->status_line.")");
      $standard->dumperLogger("LWP HTTP::Request Object", $resGet);
   };

} else {
   $standard->messageLogger(3,"valid object server credential and hostname/port required");
 #  sendSyslog( "valid object server credential and hostname/port required", 'critical' );
};

__END__



