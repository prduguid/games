function getRandom(arr, n) {
   var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
   if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
   while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
   }
   return result;
}

function capitaliseFirstLetter(string) { return string.charAt(0).toUpperCase() + string.slice(1); }

function toggleGameButtons() {
   $("button#table-buttons-endGame")
      .text("Reveal Next Card")

   if ($(".card-next").length == 0) {
      if ($('.card-faceDown').length > 0) {
         $("button#table-buttons-endGame")
            .text("Reveal All Cards")
            .prop('disabled', false);

      } else {
         $("button#table-buttons-endGame").prop('disabled', true);
      }
      $("button#table-buttons-newGame").prop('disabled', false);

   } else {

      if ($('.card-faceDown').length == 0) {
         // all face up / non face down
         // game over, no more cards to reveal
         $("button#table-buttons-endGame").prop('disabled', true);
         $("button#table-buttons-newGame").prop('disabled', false);
      } else if ($('.card-faceUp').length <= 1) {
         // game not yet started
         $("button#table-buttons-endGame").prop('disabled', false);
         $("button#table-buttons-newGame").prop('disabled', true);
      } else {
         // mid-way in game
         $("button#table-buttons-endGame").prop('disabled', false);
         $("button#table-buttons-newGame").prop('disabled', false);
      }
   }
}

function revealNextCard() {

   // next card will have the .card-next class tag set
   var prevCardValue = 0;
   if (!$(".card-next").hasClass('card-0')) {
      // value of the last card turned over
      prevCardValue = $("#acesAre").val() == "high" ? $(".card-faceUp:last").data("__DATA__")["valueAceHigh"] : $(".card-faceUp:last").data("__DATA__")["valueAceLow"];
   }

   // value of the card about to be turned over
   var thisCardValue = $("#acesAre").val() == "high" ? $(".card-next").data("__DATA__")["valueAceHigh"] : $(".card-next").data("__DATA__")["valueAceLow"];
   var thisCardRank = $(".card-next").data("__DATA__")["rank"];
   var thisCardPhrase = $(".card-next").data("__DATA__")["phrase"];
   var thisCardPrefix = thisCardRank == "ace" ? "an" : "a";

   console.debug("[revealNextCard]", "prevCardValue:", prevCardValue, "thisCardValue:", thisCardValue)


   // regardless of values, always reveal the next card
   if ($(".card-next").hasClass('card-faceDown')) {

      // reveal the card
      $(".card-next")
         .html("<img src=\"" + $(".card-next").data("__DATA__")["image"] + "\" alt=\"Playing card :" + $(".card-next").data("__DATA__")["phrase"] + "\">")
         .removeClass('card-faceDown')
         .addClass('card-faceUp');

      if (!$(".card-next").hasClass('card-0')) {
         // skip card-0


         if (thisCardValue == prevCardValue) {
            console.log("[revealNextCard] card lost, game lost (pair)");
            $(".score").addClass("game-lost").text(0);
            $("#caption").text("You don't get anything for a pair, not in this game!");
            $(".card-next").addClass("card-lost");
            $(".cards-on-table-wrapper").addClass("game-lost");

         } else if ($(".card-next").hasClass("card-higher") && thisCardValue < prevCardValue) {
            console.log("[revealNextCard] card lost, game lost (not higher)");
            $(".score").addClass("game-lost").text(0);
            $("#caption").text(capitaliseFirstLetter(thisCardPrefix) + " " + capitaliseFirstLetter(thisCardPhrase) + ", Ohh No LOWER!");
            $(".card-next").addClass("card-lost");
            $(".cards-on-table-wrapper").addClass("game-lost");

         } else if ($(".card-next").hasClass("card-lower") && thisCardValue > prevCardValue) {
            console.log("[revealNextCard] card lost, game lost (not lower)");
            $(".score").addClass("game-lost").text(0);
            $("#caption").text(capitaliseFirstLetter(thisCardPrefix) + " " + capitaliseFirstLetter(thisCardPhrase) + ", Ohh No HIGHER!");
            $(".card-next").addClass("card-lost");
            $(".cards-on-table-wrapper").addClass("game-lost");

         } else if ($(".card-next").hasClass("card-lower") || $(".card-next").hasClass("card-higher")) {
            console.debug("[revealNextCard] card won");
            $(".card-next").addClass("card-win");

            $(".score").text($(".card-win").length);

            if ($(".card-next").hasClass("card-last")) {
               $(".score").addClass("game-won");
               // won game
               $("#caption").text(capitaliseFirstLetter(thisCardPrefix) + " " + capitaliseFirstLetter(thisCardPhrase) + ". Good Game, Good Game!");

            } else {
               // win card
               $("#caption").text(capitaliseFirstLetter(thisCardPrefix) + " " + capitaliseFirstLetter(thisCardPhrase) + ". Higher or Lower than a " + capitaliseFirstLetter(thisCardRank) + "?");

            }
         } else {
            // presumably the reveal all cards (end game) has been clicked
            console.log("[revealNextCard] card terminated");
            $(".score").addClass("game-lost").text(0);
            $("#caption").text("Game Over");
            $(".card-next").addClass("card-lost");
            $(".cards-on-table-wrapper").addClass("game-lost");
         }
      }


      // increment the card-next class tag
      $(".card-next").removeClass('card-next');
      $(".card-faceDown:first").addClass('card-next');

   }

}

function newGame() {
   // reset the caption text
   $("#caption").text("");

   // reset the score
   $(".score").text(0).removeClass("game-won").removeClass("game-lost");

   // reset the game-lost affect on .cards-on-table-wrapper
   $(".cards-on-table-wrapper").removeClass("game-lost");

   // reset all cards
   $("div.cards-on-table").empty();

   // get the number of cards to deal from the select input
   var cardQuantity = parseInt($("select#cardQuantity").val());
   console.debug("[newGame]", "cardQuantity", cardQuantity, "type:", typeof(cardQuantity));
   // randomly select the cards
   var randomDrawnCards = getRandom(cardDeck, cardQuantity);
   console.log("[newGame]", "randomDrawnCards", randomDrawnCards);

   for (c = 0; c < cardQuantity; c++) {
      // create a face down card image
      var img = $("<img>").attr("alt", "Playing card face down").attr("src", cardBackPath);
      console.debug("[newGame]", c, "img", img);

      // create a card-placeholder div, and append the image, class tags and the randomly select card data
      var divPlaceholder = $("<div>")
         .addClass("card-placeholder")
         .addClass("card-" + c)
         .addClass("card-faceDown").append(img)
         .data("__DATA__", cards[randomDrawnCards[c]]);
      console.debug("[newGame]", c, "divPlaceholder", divPlaceholder);

      // append the card-placeholder into the table div
      $("div.cards-on-table").append(divPlaceholder);
   }

   // update the card-last and card-next class tags
   $("div.card-placeholder:last").addClass("card-last");
   $("div.card-placeholder:first").addClass("card-next");

}

$(document).ready(function() {
   // Configure the new game button
   $("button#table-buttons-newGame").click(function() {
      // start a new game (deal cards)
      newGame();

      // reveal the first card
      revealNextCard();
   });

   // Configure the end game button
   $("button#table-buttons-endGame").click(function() {
      // reveal all cards
      $(".card-faceDown").each(revealNextCard());
   })

   $("button#game-buttons__higher").click(function() {
      if ($(".card-lost").length == 0) {
         $(".card-next").addClass("card-higher");
         revealNextCard();
      }
   })

   $("button#game-buttons__lower").click(function() {
      if ($(".card-lost").length == 0) {
         $(".card-next").addClass("card-lower");
         revealNextCard();
      }
   })

   $("button#game-buttons__freeze").click(function() {})


   $("button#table-buttons-newGame").click();
});