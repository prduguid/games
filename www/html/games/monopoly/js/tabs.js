
function initTabs(){
   var tag                    = tagTag( "initTabs()" );
   console.info(tag, "initializing bootstrap tabs ...");
   Object.values(monopoly.ui.tabs).map(tab=>{
      tab.object = document.getElementById(tab.id);
      console.debug(tag, "initializing tab: " + tab.name + ", id: " + tab.id + ", conditional: " + tab.conditional + ", refreshable: " + tab.refreshable );
      tab.tabObj = new bootstrap.Tab( tab.object );
   });


   monopoly.ui.tabs.game.object.addEventListener('show.bs.tab', function (event) {
      var tag = tagTag('show.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      populateTabGame();
   });
   monopoly.ui.tabs.game.object.addEventListener('reload.monopoly.bs.tab', function (event) {
      var tag = tagTag('reload.monopoly.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      populateTabGame();
   });


   monopoly.ui.tabs.bank.object.addEventListener('show.bs.tab', function (event) {
      var tag = tagTag('show.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      populateTabBank();
   });
   monopoly.ui.tabs.bank.object.addEventListener('reload.monopoly.bs.tab', function (event) {
      var tag = tagTag('reload.monopoly.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      populateTabBank();
   });


   monopoly.ui.tabs.players.object.addEventListener('show.bs.tab', function (event) {
      var tag = tagTag('show.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      populateTabPlayer();
   });
   monopoly.ui.tabs.players.object.addEventListener('reload.monopoly.bs.tab', function (event) {
      var tag = tagTag('reload.monopoly.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      populateTabPlayer();
   });


   monopoly.ui.tabs.board.object.addEventListener('show.bs.tab', function (event) {
      var tag = tagTag('show.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      if( ! ( sessionStorage.getItem("monopolyGameId") && event.target.hasAttribute('data-monopoly-game-id') && event.target.getAttribute('data-monopoly-game-id') == sessionStorage.getItem("monopolyGameId")  ) ) {
         // tab has never been populated or was populated for a different game; construct the board
         $( '.board-content' ).empty().append( __generateBoard() );
         event.target.setAttribute('data-monopoly-game-id', sessionStorage.getItem("monopolyGameId"));
         event.target.setAttribute('data-monopoly-game-touch', 0);
      }
      // trigger the content population
      monopoly.ui.tabs.board.object.dispatchEvent(reload_monopoly_bs_tab);
   });

   monopoly.ui.tabs.board.object.addEventListener('reload.monopoly.bs.tab', function (event) {
      var tag = tagTag('reload.monopoly.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      if( sessionStorage.getItem("monopolyGameId") && event.target.hasAttribute('data-monopoly-game-id') && event.target.getAttribute('data-monopoly-game-id') == sessionStorage.getItem("monopolyGameId") ) {

         $.when( ajaxQueryByKey(schema.games,  sessionStorage.getItem("monopolyGameId")) ).then( function(gamesResponse){
            if( gamesResponse.rowset.rows[0].touch > event.target.getAttribute('data-monopoly-game-touch') ) {
               var data = { game: gamesResponse.rowset.rows[0] };
               $(".dice .dice-inner-bottommsg").empty().append(monopoly.ui.spinner);

               $.when( ajaxQueryByFilter(schema.players, "gameId=" + data.game.gameId,""), ajaxQueryByFilter(schema.deeds,   "gameId=" + data.game.gameId,""), ajaxQueryByFilter(schema.journal, "Class in(1501) and Severity in(3,4) and Grade in("+data.game.gameId+")", "") ).then( function(playersResponse, deedsResponse, journalResponse ){
                  console.debug(tag, '[gameData                ]', gamesResponse.rowset.affectedRows,      gamesResponse.rowset.rows      );
                  console.debug(tag, '[playersData             ]', playersResponse[0].rowset.affectedRows, playersResponse[0].rowset.rows );
                  console.debug(tag, '[deedsData               ]', deedsResponse[0].rowset.affectedRows,   deedsResponse[0].rowset.rows   );
                  console.debug(tag, '[journalData             ]', journalResponse[0].rowset.affectedRows, journalResponse[0].rowset.rows );
                  console.debug(tag, '[data                    ]',data);

                  data.players                     = playersResponse[0].rowset.rows;
                  data.deeds                       = deedsResponse[0].rowset.rows;
                  data.journal                     = journalResponse[0].rowset.rows;
                  // enrich data
                  enrichDeedSetData(data.deeds, data.game);
                  data.player = {};
                  data.players.forEach(p=>{
                     p.FullName = monopoly.cache.users.filter(u=>u.UserID==p.uid)[0].FullName;
                     p.UserName = monopoly.cache.users.filter(u=>u.UserID==p.uid)[0].UserName;
                     p.journal  = data.journal.filter(j=>j.OwnerUID==p.uid);
                     data.player[p.uid] = p;
                  });
                  data.player.active         =  data.player[ data.game.activePlayer ];
                  data.player.session        =  data.player[ getAuthUserID() ];
                  data.sessionPlayer         = data.players.filter(p=>p.uid==getAuthUserID()).length>0;
                  data.sessionPlayerIsActive = data.sessionPlayer 
                     ? data.player.session.uid==data.player.active.uid
                     : false;

                  console.groupCollapsed(tag,"updating board");
                  console.info(tag,"clearing / resetting ...");
                  // Remove all the avatars
                  $(".square img.avatar").remove();
                  // Remove all .inner .status span elements
                  $(".square.inner div.status span").remove();
                  // Remove all .inner .icon span elements that are not marked as static
                  $(".square.inner div.icon span:not(.static)").remove();
                  // Remove all .outer .cost span elements
                  $(".square.outer div.cost span").remove();
                  // Clear the dice messages
                  $(".dice .dice-inner-topmsg").empty();
                  $(".dice .dice-inner-bottommsg").empty();
                  // Reset the chance and community chest card decks
                  $("#tabs-board .board-content .board.center .cards").empty();
                  $("#tabs-board .board-content .board.center .cards.communityChest").append( $('<span>').addClass("icon").addClass( monopoly.ui.iconClasses.communityChest) );
                  $("#tabs-board .board-content .board.center .cards.chance").append( $('<span>').addClass("icon").addClass(monopoly.ui.iconClasses.chance) );

                  for( var s = 0; s < monopoly.cache.board.length; s++ ){
                     console.groupCollapsed(tag, "Square: " + s + ", title: '"+monopoly.cache.board[s].title+"'");
                     // 1b) add the avatars back in their current squares
                     var avatars = [];
                     data.players.filter( player => player.position == s ).forEach( player => {
                        console.debug(tag, "-> player "+player.uid);
                        var avatar = $("<img>").addClass('avatar').prop("src","images/token-"+player.avatar+".png").prop("alt",player.FullName)
                        if( data.game.activePlayer == player.uid ) {
                           avatar.addClass("active");
                        }
                        avatars.push( avatar );
                     });
               
                     switch(monopoly.cache.board[s].title){
                        case "Jail":
                           switch( avatars.length ){
                              case 1:
                                 $(".square.s40 .cell.cell1").append(avatars);
                                 break;
                              case 2:
                                 $(".square.s40 .cell.cell0").append(avatars[0]);
                                 $(".square.s40 .cell.cell2").append(avatars[1]);
                                 break;
                              case 3:
                                 $(".square.s40 .cell.cell0").append(avatars[0]);
                                 $(".square.s40 .cell.cell1").append(avatars[1]);
                                 $(".square.s40 .cell.cell2").append(avatars[2]);
                                 break;
                              case 4:
                                 $(".square.s40 .cell.cell0").append(avatars[0]);
                                 $(".square.s40 .cell.cell1").append(avatars[1], avatars[3]);
                                 $(".square.s40 .cell.cell2").append(avatars[2]);
                                 break;
                              case 5:
                                 $(".square.s40 .cell.cell0").append(avatars[0], avatars[3]);
                                 $(".square.s40 .cell.cell1").append(avatars[1]);
                                 $(".square.s40 .cell.cell2").append(avatars[2], avatars[4]);
                                 break;
                              case 6:
                                 $(".square.s40 .cell.cell0").append(avatars[0], avatars[3]);
                                 $(".square.s40 .cell.cell1").append(avatars[1], avatars[4]);
                                 $(".square.s40 .cell.cell2").append(avatars[2], avatars[5]);
                                 break;
                              }
                           break;
                        default:
                           $(".square.s"+s+" .avatars").append(avatars);
                     }
               
                     data.deeds.filter( deed => deed.title == monopoly.cache.board[s].title ).forEach( deed => {
                        // 2b) re-populate the .inner .status span elements
                        $(".square.inner.s"+s+" div.status").append(
                           deed.checks.owned 
                           ? $('<span>').append("Owner: " + getFullNameForUserID(deed.owner) + ",", $('<br>'), "Rent: £" + ( deed.checks.isMortgaged ? 0 : deed.checks.rent ))
                           : $('<span>').text("For Sale")
                        );

                        // 3b) re-populate the .inner .icon span elements
                        var iconDiv = $(".square.inner.s"+s+" div.icon");
                        switch( deed.state ){
                           case 5:
                              iconDiv.append($('<span>').addClass( monopoly.cache.stock.filter(s=>s.item=='hotel')[0]['icon'] ));
                              console.debug(tag, "-> hotel");
                              break;
                           case 4:
                              iconDiv.append($('<span>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                           case 3:
                              iconDiv.append($('<span>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                           case 2:
                              iconDiv.append($('<span>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                           case 1:
                              iconDiv.append($('<span>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                              console.debug(tag, "-> "+deed.state+" house(s)");
                              break;
               
                           case 0:
                           case -1:
                              if( deed.checks.owned ) {
                                 if( deed.checks.uniqueOwnership ) {
                                    iconDiv.append($('<span>').addClass("owner").addClass(monopoly.ui.iconClasses.personAdd));
                                    console.debug(tag, "-> owned user+");
                                 } else {
                                    iconDiv.append($('<span>').addClass("owner").addClass(monopoly.ui.iconClasses.person));
                                    console.debug(tag, "-> owned user");
                                 }
                              }
                              break;
               
                        }
               
                        // 4b) re-populate the .outer .cost span elements
                        var costDiv = $(".square.outer.s"+s+" div.cost");
                        $('.square.outer.s'+s+' .cost').removeClass("mortgaged").empty();
                        if( deed.checks.owned ) {
                           if( deed.checks.isMortgaged ){
                              // owned but mortgaged
                              costDiv.append( $('<span>').addClass("mortgaged").text("Mortgaged") );
                              console.debug(tag, "--> costDiv -> owned but mortgaged");
                           }
                        } else {
                           // not owned, set the purchase price
                           costDiv.append( $('<span>').text("£"+monopoly.cache.deedsInventory.filter(d=>d.title==deed.title)[0].purchase) );
                           console.debug(tag, "--> costDiv -> not owned setting purchase price");
                        }
               
                        // 4c) re-attach the deedData to the title div for deeds, stations and utilities
                        switch( monopoly.cache.board[s].rule ) {
                           case "deed":
                           case "stations":
                           case "utilities":
                              // add the config to launch the manage sets modal
                              $(".square.outer.s"+s+" div.title").data('deedData',deed).attr({'data-bs-toggle': 'modal', 'data-bs-target': '#modalManageDeedSet', 'data-bs-context': 'deed', 'data-bs-deed': monopoly.cache.board[s].title });
                              break;
                        }
                     });

                     console.groupEnd();
                  };

                  // Set the dice
                  setDice( data.game.activeMoveDice0, data.game.activeMoveDice0);

                  // the following game.actions will cause the game to become blocked for a user action:
                  // active player:
                  //                chance         : pick up a Chance card
                  //                communityChest : pick up a Community Chest card
                  //                forSale        : 
                  // Any player:
                  //                rentDemanded      
                  //                paymentDemanded
                  // All players:
                  //                auction

                  if( data.game.state == 1 || data.game.state == 2 ) {
                     // game is active / blocked

                     $(".dice .dice-inner-topmsg").empty().append(
                        $("<img>").addClass('avatar').prop("src","images/token-"+data.player.active.avatar+".png").prop("alt",data.player.active.FullName),
                        $("<span>").text( data.game.moves + ": " + data.player.active.FullName + "'s turn" )
                     );
   
                     var diceMessageDiv = $(".dice .dice-inner-bottommsg").empty();

                     if(      ( data.game.action == "rentDemanded" || data.game.action == "paymentDemanded" ) && data.player.session.journal.filter( j=>j.Severity==4 ).length > 0 ){
                        // outstanding rent/fines demands, switch to the journal tab
                        diceMessageDiv.append( $('<button>').text("Outstanding demands").attr({class:'btn btn-warning', type:'button' }).click(function(){monopoly.ui.tabs.journal.tabObj.show();}) );
      
                     } else if( data.game.action == "chance"            && data.sessionPlayerIsActive ){
                        var card = _getCardFromGameData( data.game );
                        console.debug(tag,"Chance card: ", card );
                        if( card ==  null ) {
                           diceMessageDiv.append(
                              $('<button>').attr({class:'btn btn-light', type:'button'}).text("Draw a Chance card").click(function(){ _drawCard(sessionStorage.getItem("monopolyGameId").gameId)})
                           );
                        } else {
                           diceMessageDiv.append("See Chance cards");
                           if( card.cardId == "CH08" || card.cardId == "CH09") {
                              // CH08: Make general repairs on all of your houses. For each house pay £25. For each hotel pay £100
                              // CH09: You are assessed for street repairs: £40 per house, £115 per hotel
                              var fineData = { CH08 : { houses: 25, hotels: 100 }, CH09 : { houses: 40, hotels: 115 }, count: { houses: 0, hotels: 0 }, fine : {}, note : {} };
                              data.deeds.filter( deed => deed.owner == data.game.activePlayer && deed.state > 0 && deed.set != "utilities"  && deed.set != "stations" ).forEach( deed => {
                                    switch( deed.state) {
                                       case 5: fineData.count.hotels++; break;
                                       case 4: fineData.count.houses++;
                                       case 3: fineData.count.houses++;
                                       case 2: fineData.count.houses++;
                                       case 1: fineData.count.houses++;
                                    }
                              });
                              [ "houses", "hotels"].forEach( h => {
                                 fineData.fine[h] = fineData[card.cardId][h] * fineData.count[h];
                                 fineData.note[h] = fineData.count[h] + " " + h + ": £"+ fineData.fine[h];
                              });
                              console.info(tag, "fineData: ", fineData );
                              
                              $("#tabs-board .board-content .board.center .cards.chance").empty().append( 
                                 $('<div>').addClass("main").append( card.title ),
                                 $('<div>').addClass("sub").append( card.text, $('<br>'), "(" + fineData.note.houses + ", " + fineData.note.hotels + ")" ),
                                 $('<div>').addClass("action").append( 
                                    $('<button>').attr({class:'btn btn-light', type:'button'}).text("Pay costs of £" + (fineData.fine.houses + fineData.fine.hotels)).click( function(){ _enactCard(sessionStorage.getItem("monopolyGameId"),false) })
                                 ),
                              );
                              
                           } else {
                              $("#tabs-board .board-content .board.center .cards.chance").empty().append( 
                                    $('<div>').addClass("main").append( card.title ),
                                    $('<div>').addClass("sub").append( card.text ),
                                    $('<div>').addClass("action").append(  $('<button>').attr({class:'btn btn-light', type:'button'}).text(card.actionText).click( function(){ _enactCard(sessionStorage.getItem("monopolyGameId"),false) }) ),
                              );
                           }
                        }

                     } else if( data.game.action == "communityChest"    && data.sessionPlayerIsActive ){
                        var card = _getCardFromGameData( data.game );
                        console.debug(tag,"Community Chest card: ", card );
                        if( card == null ) {
                           diceMessageDiv.append(
                              $('<button>').attr({class:'btn btn-light', type:'button'}).text("Draw a Community Chest card").click(function(){ _drawCard(sessionStorage.getItem("monopolyGameId"))})
                           );
                        } else {
                           diceMessageDiv.append("See Community Chest Cards");
                           if( card.cardId == "CC15" ) {
                              // Pay a £10 fine	or take a "Chance"	
                              $("#tabs-board .board-content .board.center .cards.communityChest").empty().append( 
                                 $('<div>').addClass("main").append( card.title ),
                                 $('<div>').addClass("sub").append( card.text ),
                                 $('<div>').addClass("action").append( 
                                    $('<button>').attr({class:'btn btn-light', type:'button'}).text("Pay a £10 fine").click( function(){ _enactCard(sessionStorage.getItem("monopolyGameId"),false) }),
                                    $('<span>').text("or"),
                                    $('<button>').attr({class:'btn btn-light', type:'button'}).text("Take a Chance").click( function(){ _enactCard(sessionStorage.getItem("monopolyGameId"),true) })
                                 ),
                              );
                           } else {
                              $("#tabs-board .board-content .board.center .cards.communityChest").empty().append( 
                                 $('<div>').addClass("main").append( card.title ),
                                 $('<div>').addClass("sub").append( card.text ),
                                 $('<div>').addClass("action").append( 
                                    $('<button>').attr({class:'btn btn-light', type:'button'}).text(card.actionText).click( function(){ _enactCard(sessionStorage.getItem("monopolyGameId"),false) })
                                 ),
                              );
                           }
                        }

                     } else if( data.game.action == "forSale"           && data.sessionPlayerIsActive ){
                        diceMessageDiv.append( $('<button>').text("Purchase or Auction" ).attr({class:'btn btn-light', type:'button', 'data-bs-toggle': 'modal', 'data-bs-target': '#modalPurchaseAuctionDeed' }).data({gameData:data.game, deedsData:data.deeds, playersData:data.players}) );

                     } else if( data.game.action == "auction" ){
                        diceMessageDiv.append( $('<button>').text("Auction in progress" ).attr({class:'btn btn-light', type:'button', 'data-bs-toggle': 'modal', 'data-bs-target': '#modalPurchaseAuctionDeed' }).data({gameData:data.game, deedsData:data.deeds, playersData:data.players}) );

                     } else if( data.sessionPlayerIsActive && data.player.session.jailTime > 0 ){
                        // incarcerated
                        diceMessageDiv.append( $('<button>').text("In Jail ...").attr({class:'btn btn-light', type:'button', 'data-bs-toggle': 'modal', 'data-bs-target': '#modalPlayerIncarcerated' }) );

                     } else if( data.sessionPlayerIsActive ){
                        // active not blocked
                        diceMessageDiv.append( $('<button>').text("Throw Dice").attr({class:'btn btn-light', type:'button'}).click(function() { throwDice(data.game.gameId, data.game.activePlayer); } ) );

                        
                     } else {
                        // other players turn

                     }


                  } else {
                     // game not started or completed
                     $(".dice .dice-inner-topmsg").empty();
                     $(".dice .dice-inner-bottommsg").empty()
                  }

                  console.groupEnd();
                  event.target.setAttribute('data-monopoly-game-touch', data.game.touch);
               })
            }
         });
      }
   });


   monopoly.ui.tabs.journal.object.addEventListener('show.bs.tab', function (event) {
      var tag = tagTag('show.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      // trigger the content population
      monopoly.ui.tabs.journal.object.dispatchEvent(reload_monopoly_bs_tab);
   });
   monopoly.ui.tabs.journal.object.addEventListener('reload.monopoly.bs.tab', function (event) {
      var tag = tagTag('reload.monopoly.bs.tab');
      var tabConf = Object.values(monopoly.ui.tabs).filter(tab=>tab.tabId==event.target.getAttribute("aria-controls"))
      console.log(tag, "event triggered on tab '"+event.target.id + '", tabConf: ', tabConf);
      console.log(tag,"refreshing the game journal for gameId: " + sessionStorage.getItem("monopolyGameId") );
      // spinner
      $('#tabs-journal caption').empty().append( $('<div>').attr({class:"spinner-border spinner-border-sm",role:"status"}).append( $('<div>').addClass('visually-hidden').text('Loading ...') ) );
      // fetch data and populate table
      //
      // "Grade", "Node", "Class", "AlertGroup", "AlertKey", "Severity", "Summary", "Poll", "OwnerUID", "LastOccurrence", "ProbeSubSecondId", "INT02", "INT02", "INT03", "PhysicalPort"
      //  | Date/Time | Move | Player | Square / Deed / Card | Summary (x3) |
      $.when( ajaxQueryByFilter(schema.journal,  "Class in (1500, 1501) and Grade in (" + sessionStorage.getItem("monopolyGameId") +")", "ProbeSubSecondId desc") ).then( function( statusResponse ){
         console.debug(tag, '[statusResponse   ] ('+statusResponse.rowset.affectedRows+') rows: ', statusResponse.rowset.rows );
         $('#tabs-journal tbody').empty().append( 
            statusResponse.rowset.rows.map(row=>{
               var tr = $('<tr>').addClass( monopoly.mappings.severity.filter(s=>s.omnibus==row.Severity)[0]['bs-table-class'] ).append(
                  // Date/Time
                  $('<td>').text( new Date( row.LastOccurrence * 1000).toLocaleString() ),
                  // Move
                  $('<td>').text( row.Poll > 0 ? row.Poll : '-' ),   
                  // Player
                  $('<td>').text( row.OwnerUID > 0 ? getFullNameForUserID( row.OwnerUID ) : '-'),
                  // Square / Deed / card
                  $('<td>').text( row.AlertKey ),
                  // Summary
                  //$('<td>').text( row.Summary )
               );
               if( row.Class == 1500 ){
                  // Summary
                  tr.append( $('<td>').attr({colspan:3}).text( row.Summary ))
               } else {
                  tr.append( $('<td>').text( row.Summary ) );

                  switch( row.Severity){
                     case 5:
                        tr.append( $('<td>').text( 'Bankrupted' ));
                        break;
                     case 4:
                        tr.append( $('<td>').text( 'Demanded' ));
                        break;
                     case 3:
                        tr.append( $('<td>').text( 'Due' ));
                        break;
                     case 2:
                        tr.append( $('<td>').text( 'Rent Missed' ));
                        break;
                     case 1:
                        tr.append( $('<td>').text( 'Paid' ));
                        break;
                  }                  

                  // rent/fine
                  var td = $('<td>');
                  // row.OwnerUID is the tenant!
                  if( row.AlertGroup == 'rent' && row.OwnerUID == getAuthUserID() && row.INT03 > 0 && row.Severity >= 3 ) {
                     // insert a Pay Rent button
                     console.info(tag, 'logged in user is a tenant of property ' + row.boardPlace +' and owes rent');
                     td.append(
                        $('<button>')
                        .attr({class:'btn btn-primary btn-sm', type:'button', 'data-monopoly-row-sn': row.ServerName, 'data-monopoly-row-ss': row.ServerSerial, 'data-monopoly-row-tenant': row.OwnerUID, 'data-monopoly-row-rent': row.INT02 })
                        .text( "Pay rent" )
                        .click( function(event){
                           var sn      = event.target.getAttribute('data-monopoly-row-sn');
                           var ss      = event.target.getAttribute('data-monopoly-row-ss');
                           var tenant  = event.target.getAttribute('data-monopoly-row-tenant');
                           var rent    = event.target.getAttribute('data-monopoly-row-rent');

                           $.when( ajaxQueryByKey(schema.players, sessionStorage.getItem("monopolyGameId") + '-' + tenant) ).then(function(playerResponse){
                              /// enrich the collected data and populate into event.target
                              console.debug(tag, '[data collected ] playerResponse ('+playerResponse.rowset.affectedRows+'): ', playerResponse.rowset.rows );
                              if( playerResponse.rowset.rows[0].cash > rent ){
                                 __callProcedurePayRent(sn,ss);
                              } else {
                                 doSimpleToast({ severity: 5, epoch: 0, autoHide: false, header: "Insufficient funds to pay rent", body: "rent owed £" + rent + ", bank balance £" +playerResponse.rowset.rows[0].cash, component: '' });
                              }
                           });
                        })
                     );

                  } else if ( row.AlertGroup == 'payment demand' && row.OwnerUID == getAuthUserID() && row.Severity == 4 ){
                     // insert a pay demand button
                     console.info(tag, 'logged in user has an outstanding payment demand');
                     td.append(
                        $('<button>')
                        .attr({class:'btn btn-primary btn-sm', type:'button', 'data-monopoly-row-sn': row.ServerName, 'data-monopoly-row-ss': row.ServerSerial, 'data-monopoly-row-tenant': row.OwnerUID, 'data-monopoly-row-rent': row.INT02 })
                        .text( "Pay" )
                        .click( function(event){
                           var sn      = event.target.getAttribute('data-monopoly-row-sn');
                           var ss      = event.target.getAttribute('data-monopoly-row-ss');
                           var tenant  = event.target.getAttribute('data-monopoly-row-tenant');
                           var rent    = event.target.getAttribute('data-monopoly-row-rent');

                           $.when( ajaxQueryByKey(schema.players, sessionStorage.getItem("monopolyGameId") + '-' + tenant) ).then(function(playerResponse){
                              /// enrich the collected data and populate into event.target
                              console.debug(tag, '[data collected ] playerResponse ('+playerResponse.rowset.affectedRows+'): ', playerResponse.rowset.rows );
                              if( playerResponse.rowset.rows[0].cash > rent ){
                                 __callProcedurePayDemand(sn,ss);
                              } else {
                                 doSimpleToast({ severity: 5, epoch: 0, autoHide: false, header: "Insufficient funds to pay demand", body: "demand owed £" + rent + ", bank balance £" +playerResponse.rowset.rows[0].cash, component: '' });
                              }
                           });
                        })
                     );

                  } else if( row.AlertGroup == 'rent' && row.INT01 == getAuthUserID() && row.INT03 > 0 && row.Severity == 3 ) {
                     // insert a Demand rent button
                     console.info(tag, 'logged in user is the owner of property ' + row.boardPlace +' and is due rent');
                     td.append(
                        $('<button>').attr({class:'btn btn-primary btn-sm', type:'button', 'data-monopoly-row-sn': row.ServerName, 'data-monopoly-row-ss': row.ServerSerial}).text( "Demand rent" ).click( function(event){
                              __callProcedureDemandRent(event.target.getAttribute('data-monopoly-row-sn'),event.target.getAttribute('data-monopoly-row-ss'));}
                        )
                     );
                  };
                  tr.append(td);

               }

               return tr;
            })
         );
         $('#tabs-journal caption').empty().append($('<cite>').text('Last refreshed at ' + new Date().toLocaleTimeString()));
      });      
   });

}


function populateTabGame () {
   var tag                    = tagTag( "populateTabGame()" );
   var tab                    = 'tabs-game';
   var tabSelector            = '#' + tab;
   var tabCardGroupSelector   = tabSelector.concat( ' ', tabSelector, '-cards');
   
   console.groupCollapsed(tag, "Populating the GAME tab", "(tab conf)");
   console.debug(tag,'tab', tab);
   console.debug(tag,'tabSelector', tabSelector);
   console.debug(tag,'tabCardGroupSelector', tabCardGroupSelector);
   console.groupEnd();
   var tabCardGroup = $( tabCardGroupSelector );
   

   ajaxQueryByFilter( schema.games, "state<3", "gameId" ).done(function( gamesData ) {
      console.info(tag, "Fetched game data");
      console.debug(  tag, '[gamesData  ]', gamesData.rowset.rows );
      var gameIds = [];
      gamesData.rowset.rows.forEach( gameData => { gameIds.push(gameData.gameId); });
      if( sessionStorage.getItem("monopolyGameId") && ( gameIds.length == 0 || gameIds.indexOf( parseInt(sessionStorage.getItem("monopolyGameId")) ) == -1 )){
         console.warn(tag,"selected game ("+sessionStorage.getItem("monopolyGameId")+") does not exist, unselecting game (current games:",gameIds,")");
         sessionStorage.removeItem("monopolyGameId");
      }
      if( gameIds.length > 0 ) {

         console.debug(tag,"getting player data for gameIds: "+gameIds.join(',')+" ...");

         // fetch the players for all the selected games
         ajaxQueryByFilter( schema.players, "gameId in (" +gameIds.join(',') + ")", "sequence,uid" ).done(function( playersData ) {
            console.debug(tag, "Fetched player data (for all fetched games).", playersData.rowset.rows );

            tabCardGroup.empty();

            gamesData.rowset.rows.forEach( gameData => {
               console.groupCollapsed(tag, "Populating gameId:", gameData.gameId);
               console.debug(tag,"gameData",gameData);
               var start = new Date( gameData.start *1000 );
               var cardId = "game-"+gameData.gameId;
               console.debug(tag,'cardId', cardId);

               var selectedGame = gameData.state > 0 && gameData.gameId == sessionStorage.getItem("monopolyGameId");

               var playerCountForGame = playersData.rowset.rows.filter( allPlayers => allPlayers.gameId == gameData.gameId ).length;
               console.debug(tag,'playerCountForGame',playerCountForGame );

               var footerContent;
               switch( gameData.state ){
                  case 0:
                     footerContent = $('<small>').append(
                        playerCountForGame < 2 
                        ? "Add at least two players to a game"
                        : $('<a>').text("Start game").attr('href', '#').click(function() { __callProcedureSetGameState(gameData.gameId, "activate"); } )
                     );
                     break;
                  case 1:
                     footerContent = $('<small>').append(
                        gameData.start > 0
                        ? "Game started on " + monopoly.mappings.weekday[start.getDay()].substr(0,3) + ' ' + start.getDate() + ' ' + monopoly.mappings.month[start.getMonth()].substr(0,3) + ' ' + start.getFullYear() + " (" + gameData.moves + " moves)"
                        : "Game ready to start"
                     )
                     break;
                  case 2:
                     footerContent = $('<small>').text("Game is active, but waiting for a decision by a player");
                     break;

                  default:
                     footerContent = $('<small>').text("Game is in an unexpected state");

               };

               // populate the players for the game
               // presented in a fixed grid, 5 columns, six rows

               var playerListItems = [];
               var players = 0;
               var playerAvatarAlternatives = monopoly.cache.avatar.filter( av => 
                  playersData.rowset.rows.filter( p => p.gameId == gameData.gameId ).map( p=>p.avatar ).indexOf( av.avatar ) == -1
               );

               // LOOP around each player for this game and populate into the grid (via playerListItems)
               playersData.rowset.rows
               .filter( allPlayers => allPlayers.gameId == gameData.gameId )
               // sort first on active players
               .sort((x, y) => { return (x.isActive === y.isActive)? 0 : x.isActive? -1 : 1; } )
               .forEach((playerData, playerIndex) => {
            
                  var playerClass = 'player-'+playerIndex;
                  var playerActivityClass = playerData.isActive ? "" : "inactive";
                  console.debug(tag,"playerIndex: "+playerIndex+", (playerClass: "+playerClass+", playerActivityClass: "+playerActivityClass+") playerData",playerData);

                  ///  set the players avatar
                  var playerAvatarDiv = $('<div>').addClass( playerClass ).addClass( playerActivityClass ).addClass( 'player-avatar' );
                  if( playerAvatarAlternatives.length > 0 ){ 
                     ///   if there are un-set avatars, include the control to allow the avatar to be changed via a dropdown
                     playerAvatarDiv
                        .addClass("dropdown dropend")
                        .append(
                           $('<div>')
                              .attr({type:'button', id:"playerAvatarDiv-"+playerData.identifer, 'class':'dropdown-toggle','aria-label':"player avatar icon: "+playerData.avatar, 'data-bs-toggle':'dropdown', 'aria-expanded':'false'})
                              .append( $('<img>').prop("src", "images/token-"+playerData.avatar+".png") ),

                           $('<ul>').attr({"class":"dropdown-menu", "aria-labelledby": "playerAvatarDiv-"+playerData.identifer }).css('min-width', '18rem').append(
                              playerAvatarAlternatives.map(av => {
                                 return $('<li>')
                                    .attr({type:'button', "class":"dropdown-item", "aria-label": av.title })
                                    .click( function(){ __callProcedurePlayerSetAvatar( playerData.gameId, playerData.uid, av.avatar) })
                                    .append( $('<img>').prop("src", av.src), $('<span>').text(av.title) )
                              })
                           )
                        );
                  } else {
                     playerAvatarDiv .append( $('<div>').attr("aria-label","player avatar icon: "+playerData.avatar).append( $('<img>').prop("src", "images/token-"+playerData.avatar+".png") ) );
                  }

                  var playerStateDiv = $('<div>').addClass( playerClass ).addClass( playerActivityClass ).addClass( 'player-state' ).attr("aria-label","players state (e.g. in jail, bankrupt etc");
                  if( playerData.isActive ){
                     if( gameData.activePlayer == playerData.uid && gameData.activeMoveDice0 + gameData.activeMoveDice1 > 0 ){
                        playerStateDiv.append( $('<i>').addClass("bi bi-dice-"+gameData.activeMoveDice0), $('<i>').addClass("bi bi-dice-"+gameData.activeMoveDice1) )
                     }
                     if( playerData.jailTime > 0 ) {
                        playerStateDiv.append( $('<div>').addClass("injail").text("In jail"));
                     }
                     
                  } else {
                     playerStateDiv.addClass("inactive").text("inactive");
                  };

                  var playerBankerDiv = $('<div>').addClass( playerClass ).addClass( playerActivityClass ).addClass( 'player-banker' ).attr("aria-label","player banker control");
                  if( playerData.isActive ) {
                     if( playerData.uid == gameData.banker ) {
                        playerBankerDiv.prop("title", "this player is set as the banker for this game" ).append( $('<i>').addClass( "bi bi-piggy-bank-fill"  ) );
                     } else {
                        playerBankerDiv.prop("title","click to set this player to be the banker for the game")
                        .append( 
                           $('<i>').addClass( "bi bi-piggy-bank").attr('type','button').click( function(){ __callProcedureSetBanker( gameData.gameId, playerData.uid); })
                        );
                     };
                  };

                  playerListItems.push( 
                     playerAvatarDiv,
                     $('<div>').addClass( playerClass ).addClass( playerActivityClass ).addClass( 'player-name' ).attr("aria-label","player user name").text( getFullNameForUserID(playerData.uid) ),
                     $('<div>').addClass( playerClass ).addClass( playerActivityClass ).addClass( 'player-cash' ).attr("aria-label","players bank balance").append( $('<span>').addClass("cash").text(playerData.cash) ),
                     playerStateDiv,
                     playerBankerDiv,
                     $('<div>').addClass( playerClass ).addClass( playerActivityClass ).addClass( 'player-remove' ).attr({"aria-label":"player remove control", 'type':'button'}).append(
                        $('<i>').addClass("bi bi-person-x").attr("aria-label", "click to remove this player from the game, any assets they own will be returned to the bank.").click(
                           function() { confirm("Are you sure you wish to remove player '"+getFullNameForUserID(playerData.uid)+"?\nAny assets they own will be returned to the bank.") && __callProcedureRemovePlayer(gameData.gameId, playerData.uid); }
                        )
                     )
                  );
                  players++;
               });

               // if there are less than maxPlayers and the game is inactive, append a add player button
               // pad out the remainder of the table cells
               if( gameData.state == 0 && players < monopoly.maxPlayers ) {
                  console.debug(tag,"game "+ gameData.gameId +" is inactive and has less than 6 players ("+players+"), inserting the add player button");
                  playerListItems.push(
                     $('<div>').attr({ "class": 'player-add', "aria-label":"player add control" }).append(
                        $('<i>').attr({ "class": 'bi bi-person-plus-fill', type:'button', 'data-bs-toggle': 'modal', 'data-bs-target': '#modalAddUserToGame', 'data-bs-gameId': gameData.gameId })
                     )
                  );
               }

               // populate card header, body and footer into the card and then populate the card into the card-group
               tabCardGroup.append(
                  $('<div>').addClass("col").append( 
                     $('<div>').attr({ class: 'card h-100', id: cardId, 'data-monopoly-gameId': gameData.gameId}).addClass( selectedGame ? "border-primary" : "" ).append(
                        //header (with close button)
                        $('<div>').addClass('card-header d-flex').append(
                           // game selector radio button
                           $("<input>").attr({ type: "radio", name:"gameSelector", id:"gameSelector-"+gameData.gameId, value: gameData.gameId, disabled: gameData.state == 0, checked: selectedGame, "class": "gameIdSelector"} ).change(
                              function(){ if( this.checked ){ sessionStorage.setItem( "monopolyGameId", this.value); doUpdate(); } }
                           ),
                           // label for radio button
                           $('<label>').attr({ "class":'flex-grow-1 ps-2', for: "gameSelector-"+gameData.gameId }).text( "Game " + gameData.gameId ),
                           // complete game "close" button
                           $('<button>').attr({ type: "button", "class": "btn-close", "aria-label": "Close" }).click(
                              function() { confirm("Set this game as complete?\nThe game will be archived.") && __callProcedureSetGameState(gameData.gameId, "complete"); }
                           )
                        ),
      
                        // body -- player list
                        $('<div>').addClass('card-body py-0 px-0').addClass('playerList').append( playerListItems ),
      
                        // footer
                        $('<div>').addClass("card-footer px-1").append( footerContent )
                     )
                  )
               );
               console.info(tag, "card added");
               console.groupEnd();

            });
         });

      } else {
         console.info(tag, "no active games");
      }
   });
};

function populateTabBank() {
   var tag = tagTag("populateTabBankBoard()");
   var gameId = sessionStorage.getItem("monopolyGameId");
   console.log(tag,"gameId: " + gameId);
   var bankGridContainer = $( '.bank-grid' );
   if( bankGridContainer.length ){
      $.when(
         ajaxQueryByKey(   schema.games,  gameId ),
         ajaxQueryByFilter(schema.players,"gameId=" + gameId,""),
         ajaxQueryByFilter(schema.deeds,  "gameId=" + gameId,""),
      ).then(
         function(gamesResponse, playersResponse, deedsResponse ){
            console.groupCollapsed(tag, "data collected ...");
            console.debug(tag, '[gamesResponse           ]', gamesResponse[1], gamesResponse );
            console.debug(tag, '[playersResponse         ]', playersResponse[1], playersResponse );
            console.debug(tag, '[deedsResponse           ]', deedsResponse[1], deedsResponse );

            console.debug(tag, '[gameData                ]', gamesResponse[0].rowset.affectedRows,   gamesResponse[0].rowset.rows     );
            console.debug(tag, '[playersData             ]', playersResponse[0].rowset.affectedRows, playersResponse[0].rowset.rows   );
            console.debug(tag, '[deedsData               ]', deedsResponse[0].rowset.affectedRows,   deedsResponse[0].rowset.rows     );         

            enrichDeedSetData(deedsResponse[0].rowset.rows, gamesResponse[0].rowset.rows[0])
            console.groupEnd();


            console.groupCollapsed(tag, "Populating the bank grid container (selector: '.bank-grid'");
            console.debug(tag, "removing any '.content' elements");
            $( '.bank-grid .content' ).remove();

            // CASH
            // monopoly.cache.cashInventory gives the banks initial distribution of cash
            console.info(tag, "Populate bank and total player cash reserves");
            var totalCash     = monopoly.cache.cash.map(ci=>ci.denomination*ci.baseQuantity).reduce((pv, cv) => pv + cv, 0);
            console.debug(tag,"totalCash : £"+ totalCash);
            var totalPlayerCash = playersResponse[0].rowset.rows.map(p=>p.cash).reduce((pv, cv) => pv + cv, 0);
            console.debug(tag,"totalPlayerCash : £"+ totalPlayerCash );
            bankGridContainer.append(
               $('<div>').addClass("content cash-content").css("grid-area","cashc").append(
                  $('<div>').append( $('<span>').text('Bank'),     $('<span>'), $('<span>').text( (totalCash - totalPlayerCash).toLocaleString() )  ),
                  $('<div>').append( $('<span>').text('Players'),  $('<span>'), $('<span>').text( totalPlayerCash.toLocaleString() )                ),
                  $('<div>').append( $('<span>').text('Total'),    $('<span>'), $('<span>').text( totalCash.toLocaleString() )                      )
               )
            );

            // DEEDS
            console.groupCollapsed(tag, "populating deed cards");
            bankGridContainer.append( 
               monopoly.cache.board.filter(b=>b.sets).map(b=>
                  __generateDeedCardDiv('mini', deedsResponse[0].rowset.rows.filter( d => d.title == b.title )[0])
                  // add the config to launch the manage sets modal
                  .attr({'data-bs-toggle': 'modal', 'data-bs-target': '#modalManageDeedSet', 'data-bs-context': 'deed', 'data-bs-deed': b.title })
               )
            );
            console.groupEnd();

            // HOUSE & HOTEL STOCK
            console.groupCollapsed(tag, "Populating the house and hotel stock");
            // init unsoldStock from monopoly.cache.stock
            // 0: house, 1:hotel
            var unsoldStock = monopoly.cache.stock.map(s=>s.quantity);
            // decrement the unsold counters based on the buildings built
            deedsResponse[0].rowset.rows.forEach( deed=> {
               switch( deed.state ){
                  case 5: unsoldStock[1]--;
                  break;
                  case 4: unsoldStock[0]--;
                  case 3: unsoldStock[0]--;
                  case 2: unsoldStock[0]--;
                  case 1: unsoldStock[0]--;
               }
            })
            console.info(tag, "unsold stock levels; house: " + unsoldStock[0] + ", hotel: " + unsoldStock[1]);
            var stockDiv = $('<div>').addClass("content building-stock-content").css("grid-area","stockc");
            // create and populate the sub elements into the stockDiv 
            monopoly.cache.stock.forEach((s,i)=>{
               var iconDiv = $('<div>').addClass('icons').addClass(s.item);
               for( b = 0; b < unsoldStock[i]; b ++ ) {
                  iconDiv.append($('<i>').addClass(s.icon));
               }
               for( b = unsoldStock[i]; b < s.quantity; b ++ ) {
                  iconDiv.append($('<i>').addClass(s.icon).addClass('sold'));
               }
               stockDiv.append( iconDiv, $('<div>').addClass('summary').addClass(s.item).text("("+unsoldStock[i]+"/"+s.quantity+")") );
            })
            // push into the DOM
            bankGridContainer.append(stockDiv);
            console.groupEnd();


            // GOOFJC
            console.groupCollapsed(tag, "Populate Get Out of Jail Free Cards");
            monopoly.ui.goojfc.spec.forEach( g => {
               var userId = gamesResponse[0].rowset.rows[0][g.key];
               console.debug(tag,"the '", g.title, "' card is owned by ", userId);
               var detail = $('<div>').addClass('detail');
               if( userId > -1 ){
                  // note the owner
                  var detailIcon  = $('<span>').addClass("detail-icon").addClass(monopoly.ui.iconClasses.person);
                  var detailText  = $('<span>').addClass("detail-text").text( getFullNameForUserID( userId ) );
                  detail.append(detailIcon).append(detailText);

               } else {
                  var detailIcon  = $('<span>').addClass("detail-icon");
                  var detailText  = $('<span>').addClass("detail-text").text( 'available' );
                  detail.append(detailIcon).append(detailText);
               }

               bankGridContainer.append(
                  $('<div>')
                     .addClass('monopoly-card content card mini goojf ' + g.class)
                     .css("grid-area","goojfc-"+g.class)
                     .append( $('<div>').addClass('title').append( $('<span>').addClass('title-icon').addClass(g.icon), $('<span>').addClass('title-text').text(g.title) ) )
                     .append(detail)
                     .data("goojfData", gamesResponse[0].rowset.rows[0])
               );
            });
            console.groupEnd();

            console.groupEnd();
         }
      );
   } else {
      console.error(tag,"can't find the bank container ('.bank-content')")
   }
};

function populateTabPlayer() {
   var tag = tagTag("populateTabPlayer()");
   var gameId = sessionStorage.getItem("monopolyGameId");
   console.log(tag,"Populating the ADMIN PLAYER tab for gameId: " + gameId );
   $( '#tabs-players>.container').empty();

   $.when(
      ajaxQueryByKey(   schema.games,  gameId ),
      ajaxQueryByFilter(schema.players,"gameId=" + gameId,""),
      ajaxQueryByFilter(schema.deeds,  "gameId=" + gameId,"")
   ).then(
      function(gamesResponse, playersResponse, deedsResponse ){
         console.groupCollapsed(tag, "data collected ...");
         console.debug(tag, '[gamesResponse   ]', gamesResponse[1], gamesResponse );
         console.debug(tag, '[playersResponse ]', playersResponse[1], playersResponse );
         console.debug(tag, '[deedsResponse   ]', deedsResponse[1], deedsResponse );

         console.debug(tag, '[gameData        ]', gamesResponse[0].rowset.affectedRows,   gamesResponse[0].rowset.rows     );
         console.debug(tag, '[playersData     ]', playersResponse[0].rowset.affectedRows, playersResponse[0].rowset.rows   );
         console.debug(tag, '[deedsData       ]', deedsResponse[0].rowset.affectedRows,   deedsResponse[0].rowset.rows     );         
         console.groupEnd();

         
         playersResponse[0].rowset.rows.forEach( playerData => {
            console.groupCollapsed(tag, 'Adding player uid: ' + playerData.uid + ', name: "'+ getFullNameForUserID(playerData.uid) + "'.");
            console.debug(tag, '[players] ',"playerData",playerData);

            var divId = 'players-playerCard-'+playerData.uid;
            var netWorth = playerData.cash;
            var avatarDiv  = $("<div></div>").addClass("token").html('<img src="images/token-'+playerData.avatar+'.png"></img>');
            var nameDiv    = $("<div></div>").addClass("name").text(getFullNameForUserID(playerData.uid));
            var cashDiv    = $("<div></div>").addClass("cash").text(playerData.cash);
            var deedsUl    = $('<ul></ul>').addClass('deeds').addClass('cards');
            var goojfUl    = $('<ul></ul>').addClass('goojf').addClass('cards');
            var cardCount = 0;
            console.debug(tag,"running net worth: £" + netWorth + " (cash in bank)");

            monopoly.ui.goojfc.spec.filter( goojf => gamesResponse[0].rowset.rows[goojf.k] == playerData.uid ).forEach( goojf => {
               var title  = $('<div>').addClass("title").text( goojf.t );
               var detail = $('<div>').addClass("detail").text( "Get out of jail free ..." );
               var li = $('<li>').addClass('card').addClass(goojf.c).append(title).append(detail).data("goojfData",gamesResponse[0].rowset.rows).prop("title", monopoly.ui.goojfc.title );
               goojfUl.append(li);
               cardCount++;
            });

            monopoly.cache.deedsInventory.forEach( deedi => {
               console.debug(tag, '[monopoly.cache.deedsInventory] ', "checking for square ownership; title : ", deedi.title, ", set: ", deedi.sets, deedi);
               deedsResponse[0].rowset.rows
               .filter( deedo => parseInt(deedo.owner) == parseInt(playerData.uid) )
               .filter( deedo => deedo.title == deedi.title )
               .forEach( deedo => {
                  console.debug(tag,"Adding Title Deed: ", deedo.title);

                  var title   = $('<div></div>').addClass('title').text(deedi.title);
                  var detail  = $('<div></div>').addClass("detail");

                  switch( deedo.isMortgaged ){
                     case true:
                        netWorth = netWorth + ( deedi.purchase / 2 );
                        detail.addClass("status-morgage").text("MORGAGED");
                        break;
                     default:
                        netWorth = netWorth + deedi.purchase;
                        console.debug(tag,"running net worth: £" + netWorth );

                        switch( deedi.sets ) {
                           case "stations":
                              detail.addClass("status-cash").text(deedi.purchase);
                              break;
                           case "utilities":
                              detail.addClass("status-cash").text(deedi.purchase);
                              break;
                           default:
                              netWorth = netWorth + ( deedi.cost_building * parseInt(deedo.state) );
                              switch( deedo.state ) {
                                 case 5:
                                    detail.addClass("status-hotel").html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='hotel')[0]['icon']));
                                    break;
                                 case 4:
                                    detail.addClass("status-houses").html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                                    break;
                                 case 3:
                                    detail.addClass("status-houses").html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                                    break;
                                 case 2:
                                    detail.addClass("status-houses").html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                                    break;
                                 case 1:
                                    detail.addClass("status-houses").html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                                    break;
                                 default:
                                    detail.addClass("status-cash").text(deedi.purchase);
                                    break;
                              }
                        }
                        break;
                  }

                  console.debug(tag,"running net worth: £" + netWorth );

                  var li = $('<li></li>')
                           .addClass('card')
                           .addClass('deed')
                           .addClass('set-'+deedi.sets )
                           .append(title)
                           .append(detail)
                           .data("deedData",deedo);
                  if( deedo.isMortgaged == true ) {
                     li.addClass("mortgaged");
                  }
                  deedsUl.append(li);
                  cardCount++;
               });
            });

            var netWorthDiv = $("<div></div>").addClass("netWorth").text( 'Net worth: £ ' + netWorth );

            var div        = $('<div></div>')
                              .prop('id',divId)
                              .data('playerData',playerData)
                              .addClass('player')
                              .addClass("wrapper")
                              .append(avatarDiv)
                              .append(nameDiv)
                              .append(cashDiv)
                              .append(netWorthDiv)
                              .append(goojfUl)
                              .append(deedsUl);
            if( playerData.isActive == false ) {
               div.addClass("inactive");
            }
            if( playerData.jailTime > 0  ) {
               div.addClass("injail");
            }
            $( '#tabs-players>.container').append(div);
            console.info(tag,"Added " + cardCount + " cards (goojf and deeds) for user " + getFullNameForUserID( playerData.uid ) +" ("+ playerData.uid +")" );
            console.groupEnd();
         })



      }
   )
};

function __generateDeedCardDiv(context, deedData){
   var tag = tagTag("__generateDeedCardDiv()");
   var rootClassList = [ "monopoly-card", "content", "card", "deed" ];
   console.groupCollapsed(tag,"context: '"+context+"'");
   switch(context){
      case "mini":
      case "tabs-bank": 
      case "modalManageDeedSet-tab-buildings-cards":
         context = 'mini';
         break;
      case "full":
      case "modalManageDeedSet-tab-deed-card":
         context = 'full';
         break;
   }
   rootClassList.push(context);

   console.log(tag,"deedData      : ", deedData);
   var deedInventory = monopoly.cache.deedsInventory.filter(d=>d.title==deedData.title)[0]
   console.log(tag,"deedInventory : ", deedInventory);
   console.groupEnd();

   if( deedData.title.length > 0 ) {
      console.log(tag,"deed: " + deedData.title);
      // append classNames for the set and position in the board/grid
      rootClassList.push('set-'+ deedData.sets);
      rootClassList.push('s'+ monopoly.cache.board.filter(b=>b.title==deedData.title)[0].place)


      if( context=='full' && deedData.owner > 0 && deedData.state == -1 ) {
         // mortgaged full card
         rootClassList.push("owned");
         rootClassList.push("mortgaged")

         return $('<div>')
            .addClass(rootClassList)
            .css("grid-area","deedsc-"+monopoly.cache.board.filter(b=>b.title==deedData.title)[0].place)
            .data("deedData", deedData)
            .append(
               $('<div>').addClass('mortgaged-top').text("MORTGAGED"),
               $('<div>').addClass('mortgaged-middle').append(
                  $('<div>').addClass('mortgaged-middle-title').text(deedData.title),
                  $('<div>').addClass('mortgaged-middle-value').append( $('<span>').text('MORTGAGED FOR '),   $('<span>').addClass('cash').text( deedInventory.mortgage ) )
               ),
               $('<div>').addClass('mortgaged-bottom').text("Card must be turned this side up if property mortgaged.")
            );

      } else {

         var title      = $('<div>').addClass('title');
         switch( deedData.title ) {
            case "Electricty Company":
               rootClassList.push('non-colour');
               title.append(
                  $('<span>').addClass('title-icon').addClass(deedData.icon),
                  $('<span>').addClass('title-text').text(deedData.title)
               );
               
               break;

            case "Water Works":
               rootClassList.push('non-colour');
               title.append(
                  $('<span>').addClass('title-icon').addClass(deedData.icon),
                  $('<span>').addClass('title-text').text(deedData.title)
               );
               break;

            case "Kings Cross Station":
            case "Marylebone Station":
            case "Fenchurch Street Station":
            case "Liverpool Street Station":
               rootClassList.push('non-colour');
               title.append(
                  $('<span>').addClass('title-icon').addClass(deedData.icon),
                  $('<span>').addClass('title-text').text(deedData.title),
                  $('<span>').addClass('title-subtitle').text('BRITISH RAILWAYS')
               );
               break;

            default:
               rootClassList.push('colour');
               title.append(
                  $('<span>').addClass('title-subtitle').text('TITLE DEED'),
                  $('<span>').addClass('title-text').text(deedData.title)
               )
         }

         var detail     = $('<div>').addClass('detail');
         var status     = $('<div>').addClass('status');

         if( context=='mini' ){
            detail.append(
               $('<span>').addClass("detail-icon").addClass( deedData.owner > 0 ? monopoly.ui.iconClasses.person : monopoly.ui.iconClasses.currency ),
               $('<span>').addClass("detail-text").text( (deedData.owner > 0) ? getFullNameForUserID(deedData.owner) : monopoly.cache.deedsInventory.filter( d => d.title==deedData.title)[0].purchase )
            );

            if( deedData.owner > 0 ){
               rootClassList.push("owned");
               switch( deedData.state ){
                  case -1:
                     rootClassList.push("mortgaged")
                     status.text("MORTGAGED");
                     break;
                  case 5:
                     status.html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='hotel')[0]['icon']));
                     break;
                  case 4:
                     status.html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                     break;
                  case 3:
                     status.html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                     break;
                  case 2:
                     status.html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']),$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                     break;
                  case 1:
                     status.html($('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']));
                     break;
               } 
            }

         } else {
            // 'full'
            switch( deedData.title ) {
               case "Electricty Company":
               case "Water Works":
                  detail.append(
                     $('<div>').addClass('detail-rent p-3').css({'text-indent':'2em', 'font-size': '1.4em'} ).text('If one "Utility" is owned rent is 4 times amount shown on dice.'),
                     $('<div>').addClass('detail-rent p-3').css({'text-indent':'2em', 'font-size': '1.4em'} ).text('If both "Utilities" are owned, rent is 10 times amount shown on dice.'),
                     $('<div>').addClass('detail-mortgagevalue text-center mx-3 my-2').append( $('<span>').text('MORTGAGE Value '),   $('<span>').addClass('cash').text( deedInventory.mortgage ) )
                  );

               break;
      
               case "Kings Cross Station":
               case "Marylebone Station":
               case "Fenchurch Street Station":
               case "Liverpool Street Station":
                  detail.append(
                     $('<table>').addClass('detail-rent mx-3 my-2').append(
                        $('<tbody>').append(
                           $('<tr>').append( $('<td>').addClass('text-start pt-3').attr('colspan',4).text('Rent'),                                                                                                                                                  $('<td>').addClass('pt-3 text-end').append($('<span>').addClass('cash').text( deedInventory.rent_0   ))),
                           $('<tr>').append( $('<td>').addClass('text-start pt-3').text('If 2'), $('<td>').addClass('text-center pt-3').text('Stations'), $('<td>').addClass('text-center pt-3').text('are'), $('<td>').addClass('text-center pt-3').text('owned'), $('<td>').addClass('pt-3 text-end').append($('<span>').addClass('cash').text( deedInventory.rent_1   ))),
                           $('<tr>').append( $('<td>').addClass('text-start pt-3').text('If 3'), $('<td>').addClass('text-center pt-3').text('"'),        $('<td>').addClass('text-center pt-3').text('"'),   $('<td>').addClass('text-center pt-3').text('"'),     $('<td>').addClass('pt-3 text-end').append($('<span>').addClass('cash').text( deedInventory.rent_2   ))),
                           $('<tr>').append( $('<td>').addClass('text-start pt-3').text('If 4'), $('<td>').addClass('text-center pt-3').text('"'),        $('<td>').addClass('text-center pt-3').text('"'),   $('<td>').addClass('text-center pt-3').text('"'),     $('<td>').addClass('pt-3 text-end').append($('<span>').addClass('cash').text( deedInventory.rent_3   ))),
                        )
                     ),
                     $('<div>').addClass('detail-mortgagevalue text-center mx-3 my-2').append( $('<span>').text('MORTGAGE Value '),   $('<span>').addClass('cash').text( deedInventory.mortgage ) )
                  );
               break;
      
               default:
                  detail.append(
                     $('<table>').addClass('detail-rent mx-3 my-2').append(
                        $('<tbody>').append(
                           $('<tr>').append( $('<td>').addClass('text-center').text('RENT—'),   $('<td>').attr('colspan',2).text('Site only'),                                $('<td>').append($('<span>').addClass('cash text-end').text( deedInventory.rent_0) ) ),
                           $('<tr>').append( $('<td>').addClass('text-center').text('"'),       $('<td>').addClass('text-center').text('With'), $('<td>').text('1 House'),    $('<td>').append($('<span>').addClass('cash text-end').text( deedInventory.rent_1) ) ),
                           $('<tr>').append( $('<td>').addClass('text-center').text('"'),       $('<td>').addClass('text-center').text('"'),    $('<td>').text('2 Houses'),   $('<td>').append($('<span>').addClass('cash text-end').text( deedInventory.rent_2) ) ),
                           $('<tr>').append( $('<td>').addClass('text-center').text('"'),       $('<td>').addClass('text-center').text('"'),    $('<td>').text('3 Houses'),   $('<td>').append($('<span>').addClass('cash text-end').text( deedInventory.rent_3) ) ),
                           $('<tr>').append( $('<td>').addClass('text-center').text('"'),       $('<td>').addClass('text-center').text('"'),    $('<td>').text('4 Houses'),   $('<td>').append($('<span>').addClass('cash text-end').text( deedInventory.rent_4) ) ),
                           $('<tr>').append( $('<td>').addClass('text-center').text('"'),       $('<td>').addClass('text-center').text('"'),    $('<td>').text('HOTEL'),      $('<td>').append($('<span>').addClass('cash text-end').text( deedInventory.rent_5) ) )
                        )
                     ),
                     $('<div>').addClass('detail-rent mx-3 my-2').css({'text-indent':'2em', 'font-size': '0.7em'} ).text('If a player owns ALL the Sites of any Colour-Group, the rent is Doubled on Unimproved Sites in that group.'),
                     $('<table>').addClass('detail-buildings mx-3 my-2').append(
                        $('<tbody>').append(
                           $('<tr>').append( $('<td>').addClass('text-center').text('COST'),    $('<td>').addClass('text-center').text('of'),   $('<td>').text('Houses, '),   $('<td>').append($('<span>').addClass('cash').text( deedInventory.cost_building + ' each'         ))),
                           $('<tr>').append( $('<td>').addClass('text-center').text('"'),       $('<td>').addClass('text-center').text('"'),    $('<td>').text('Hotels, '),   $('<td>').append($('<span>').addClass('cash').text( deedInventory.cost_building + ' plus 4 houses')))
                        )
                     ),

                     $('<div>').addClass('detail-mortgagevalue text-center mx-3 my-2').append( $('<span>').text('MORTGAGE Value of Site, '),   $('<span>').addClass('cash').text( deedInventory.mortgage ) )
                  );
            }
         }

         return $('<div>')
            .addClass(rootClassList)
            .css("grid-area","deedsc-"+monopoly.cache.board.filter(b=>b.title==deedData.title)[0].place)
            .data("deedData", deedData)
            .append(title)
            .append(detail)
            .append(status);
      }
   } else{
      console.error(tag,"unable to parse deedData :" + deedData );
   }
};

function __generateBoard(){
   var tag = tagTag("__generateBoard()");
   var divs = [];
   console.groupCollapsed(tag);

   divs.push( 
      $('<div>').addClass("board").addClass("center").append(
         $('<div>').addClass("title").css("grid-area","title").text("MONOPOLY"),
         $('<div>').addClass("cards").addClass("chance").text("Chance"),
         $('<div>').addClass("cards").addClass("communityChest").text("Community Chest"),
         $('<div>').addClass("dice").append( 
            $('<div>').addClass("dice-inner").append( 
               $('<div>').addClass("dice-inner-topmsg"),
               $('<div>').addClass("dice-inner-bottommsg"),
               $('<div>').addClass("dice-inner-die1"),
               $('<div>').addClass("dice-inner-die2"),
               $('<div>').addClass("dice-inner-total"),
            )
         )
      )
   );

   for( var s = 0; s < monopoly.cache.board.length; s++ ){
      console.groupCollapsed(tag,"Square: " + s + ", title: '"+monopoly.cache.board[s].title+"'");
      console.debug(tag,"monopoly.cache.board["+s+"]:", monopoly.cache.board[s]);
      console.debug(tag, "set:",monopoly.cache.board[s].set,"icon:",monopoly.cache.board[s].icon,"subtitle:",monopoly.cache.board[s].subtitle);

      var squareDivs = [];

      var iconDiv    =  $('<div>').addClass('icon');
      if( monopoly.cache.board[s].icon != null && monopoly.cache.board[s].icon.length > 0 ) {
         iconDiv.append(
            $('<span>').addClass('static').addClass(monopoly.cache.board[s].icon)
         );
      }

      var titleDiv   =  $('<div>').addClass('title').text( monopoly.cache.board[s].title );
      var costDiv    =  $('<div>').addClass('cost');

      if( 
         monopoly.cache.deedsInventory.filter(d=>d.title==monopoly.cache.board[s].title).length > 0
         && monopoly.cache.deedsInventory.filter(d=>d.title==monopoly.cache.board[s].title)[0].title != null
         && monopoly.cache.deedsInventory.filter(d=>d.title==monopoly.cache.board[s].title)[0].purchase > 0  ){
         costDiv.append( $('<span>').text("£"+monopoly.cache.deedsInventory.filter(d=>d.title==monopoly.cache.board[s].title)[0].purchase) );
      }

      var avatarDiv    =  $('<div>').addClass('avatars');
      
      var classes = ["square", "s"+s ];
      if( s == 40 ) {
         classes.push("corner")
         classes.push("corner1" );
      } else if( s % 10 == 0 ) {
         classes.push("corner")
         classes.push("corner" + Math.floor(s/10) );
      } else {
         classes.push("side");
         classes.push("side" + Math.floor(s/10) );
      }
      if( monopoly.cache.board[s].sets != null && monopoly.cache.board[s].sets.length > 0 ){
         classes.push("set-"+monopoly.cache.board[s].sets);
      }

      console.debug(tag, "titleDiv:    ", titleDiv);
      console.debug(tag, "iconDiv:     ", iconDiv);
      console.debug(tag, "costDiv:     ", costDiv);
      console.debug(tag, "avatarDiv:   ", avatarDiv);
      console.debug(tag, "classes:     ", classes);

      switch(monopoly.cache.board[s].title){
         case "Go": 
            squareDivs[0] = $('<div>').addClass(classes).css("grid-area", "s"+s).append( 
               $('<div>').addClass('title').append( $('<span>').text(monopoly.cache.board[s].subtitle), $('<div>').text(monopoly.cache.board[s].title) ),
               avatarDiv, 
               $('<div>').addClass('icon').append( $('<div>').addClass('arrow').append( $('<span>').addClass('left'), $('<span>').addClass('spacer'), $('<span>').addClass('left'), $('<span>').addClass('left'), $('<div>').addClass('bar') ) )
            );
            break;

         case "Just Visiting": 
            squareDivs[0] = $('<div>').addClass(classes).addClass("s10a").css("grid-area", "s"+s+"a").append( $('<div>').addClass('title').text(monopoly.cache.board[s].title) );
            squareDivs[2] = $('<div>').addClass(classes).addClass("s10c").css("grid-area", "s"+s+"c").append(  avatarDiv );
            break;

         case "Free Parking": 
         case "Go To Jail": 
            // corner squares
            squareDivs[0] = $('<div>').addClass(classes).css("grid-area", "s"+s).append( titleDiv, iconDiv, avatarDiv );
            break;
   
         case "Jail":
            squareDivs[0] = $('<div>').addClass(classes).css("grid-area", "jail").append( 
               $('<div>').append(
                  $('<div>').addClass('jailcell').append(
                     $('<div>').addClass('title').text("IN").css("grid-area", "t1"),
                     $('<div>').addClass('cell').addClass('cell0').css("grid-area", "c1"), $('<div>').addClass('cell').addClass('cell1').css("grid-area", "c2"), $('<div>').addClass('cell').addClass('cell2').css("grid-area", "c3"),
                     $('<div>').addClass('title').text("JAIL").css("grid-area", "t2")
                  )
               )
            );

            break;
         case "Chance": 
         case "Community Chest": 
            squareDivs[0] = $('<div>').addClass(classes).addClass("inner").css("grid-area", "s"+s+"i").append( iconDiv );
            squareDivs[1] = $('<div>').addClass(classes).addClass("outer").css("grid-area", "s"+s+"o").append( titleDiv, costDiv, avatarDiv  );
            break;
         case "Income Tax": 
            squareDivs[0] = $('<div>').addClass(classes).addClass("inner").css("grid-area", "s"+s+"i");
            squareDivs[1] = $('<div>').addClass(classes).addClass("outer").css("grid-area", "s"+s+"o").append(
               titleDiv,
               $('<div>').addClass('cost').text(monopoly.cache.board[s].subtitle),
               avatarDiv
            );
            break;

         case "Super Tax": 
            squareDivs[0] = $('<div>').addClass(classes).addClass("inner").css("grid-area", "s"+s+"i");
            squareDivs[1] = $('<div>').addClass(classes).addClass("outer").css("grid-area", "s"+s+"o").append(
               titleDiv,
               $('<div>').addClass('cost').text(monopoly.cache.board[s].subtitle),
               avatarDiv
            );
            break;

         case "Electricty Company":
         case "Water Works":
         case "Kings Cross Station":
         case "Marylebone Station":
         case "Fenchurch Street Station":
         case "Liverpool Street Station":
         default:
            squareDivs[0] = $('<div>').addClass(classes).addClass("inner").css("grid-area", "s"+s+"i").append(
               iconDiv.addClass("hoverOff"),
               $('<div>').addClass('status').addClass("hoverOn")
            );
            squareDivs[1] = $('<div>').addClass(classes).addClass("outer").css("grid-area", "s"+s+"o").append( titleDiv, costDiv, avatarDiv );
            break;
      }

      console.debug(tag, "squareDivs:", squareDivs);
      squareDivs.forEach( d => { divs.push(d); });
      console.groupEnd();
   };
   console.groupEnd();
   return divs;
};

function generateDiceFace( number ){
   var tag = tagTag("generateDiceFace()");
   // console.info(tag, "number: ", number);
   var index  = [1,2,3,4,5,6].indexOf( parseInt(number) );
   // console.info(tag, "index:  ", index);
   switch( index ) {
      case 0: return $('<div>').addClass("diceFace").addClass("d1").append( $('<span>').addClass("pip").css("grid-area","a") );
      case 1: return $('<div>').addClass("diceFace").addClass("d2").append( $('<span>').addClass("pip").css("grid-area","a"), $('<span>').addClass("pip").css("grid-area","b") );
      case 2: return $('<div>').addClass("diceFace").addClass("d3").append( $('<span>').addClass("pip").css("grid-area","a"), $('<span>').addClass("pip").css("grid-area","b"), $('<span>').addClass("pip").css("grid-area","c") );
      case 3: return $('<div>').addClass("diceFace").addClass("d4").append( $('<span>').addClass("pip").css("grid-area","a"), $('<span>').addClass("pip").css("grid-area","b"), $('<span>').addClass("pip").css("grid-area","c"), $('<span>').addClass("pip").css("grid-area","d") );
      case 4: return $('<div>').addClass("diceFace").addClass("d5").append( $('<span>').addClass("pip").css("grid-area","a"), $('<span>').addClass("pip").css("grid-area","b"), $('<span>').addClass("pip").css("grid-area","c"), $('<span>').addClass("pip").css("grid-area","d"), $('<span>').addClass("pip").css("grid-area","e") );
      case 5: return $('<div>').addClass("diceFace").addClass("d6").append( $('<span>').addClass("pip").css("grid-area","a"), $('<span>').addClass("pip").css("grid-area","b"), $('<span>').addClass("pip").css("grid-area","c"), $('<span>').addClass("pip").css("grid-area","d"), $('<span>').addClass("pip").css("grid-area","e"), $('<span>').addClass("pip").css("grid-area","f") );
      default:
         console.error(tag, "not a valid number");
         return null;
   }
};

function throwDice( gameId, userId ){
   var tag = tagTag("throwDice()");
   var period     = getRandomNumber(2, 5) * 1000;
   var interval   = getRandomNumber(50, 125);

   console.groupCollapsed(tag, "rolling dice (gameId: "+gameId+", userId: "+userId+", period: " + period + " ms, interval: " + interval + " ms).");
   var roll = setInterval(
      function() {
         setDice(-1,-1);
      },
      interval
   )

   setTimeout(
      function(){
         console.groupEnd();
         clearInterval(roll);
         var diceValues = [ $('.dice-inner-die1').data("value"), $('.dice-inner-die2').data("value") ];
         console.info(tag, "diceValues: ", diceValues );
         __callProcedurePlayerMove( gameId, userId, diceValues[0], diceValues[1]);
      },
      period
   )
   
};
function setDice(dice0,dice1){
   var tag = tagTag("setDice()");
   if(dice0==-1){
      dice0=getRandomNumber(1, 6);
      console.info(tag,"setting a random value for dice0: " + dice0);
   };
   if(dice1==-1){
      dice1=getRandomNumber(1, 6);
      console.info(tag,"setting a random value for dice1: " + dice1);
   };
   if( dice0 > 0 && dice0 <= 6 && dice1 > 0 && dice1 <= 6 ) {
      console.debug(tag,"dice0: " + dice0 + ", dice1: " + dice1 + ", total: " + ( dice0 + dice1 ) );
      $('.dice-inner-die1').empty();
      $('.dice-inner-die1').append( generateDiceFace(dice0) ).data("value", dice0 );
      $('.dice-inner-die2').empty();
      $('.dice-inner-die2').append( generateDiceFace(dice1) ).data("value", dice1 );
      $('.dice-inner-total').empty();
      $('.dice-inner-total').append( dice0 + dice1 );
   } else {
      console.error(tag,"invalid dice value(s) provided: ice0: " + dice0 + ", dice1: " + dice1 );
   }
}


