var schema = {
   "games" : {
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/games",
      "collist" : [ "gameId", "start", "state", "action", "moves", "activePlayer", "activeMoveDice0", "activeMoveDice1", "banker", "cardsChance", "cardsCChest", "cardsChancePointer", "cardsCChestPointer", "cardsChance4Move", "cardsCChest4Move", "cardsChanceGoojfOwner", "cardsCChestGoojfOwner", "touch" ],
   },
   "touch" : {
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/games",
      "collist" : [ "touch" ],
   },
   "players" : {
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/players", 
      "collist" : ["identifer", "gameId", "uid", "avatar", "sequence", "cash", "isActive", "jailTime", "position", "doubleRun"]
   },
   "deeds" : {
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/deeds", 
      "collist" : ["identifer", "gameId", "title", "owner", "state", "sets"]
   },
   "board" : {
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/board", 
      "collist" : ["place", "title", "subtitle", "rule", "sets", "icon"]
   },
   "inventory_deeds" : {
      "db"        : "monopoly",
      "url"       : "/omnibus/objectserver/restapi/monopoly/inventory_deeds", 
      "collist"   : ["title", "sets", "colourSet", "setOrder", "boardPlace", "htmlColour", "icon", "purchase", "mortgage", "unmortgage", "rent_0", "rent_1", "rent_2", "rent_3", "rent_4", "rent_5", "cost_building", "refund_building"],
      "sort"      :"setOrder, boardPlace"
   },
   "inventory" : {
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/inventory", 
      "collist" : ["item", "type", "qty", "json"],
      "sort":     "denomination"
   },
   "users": {
      "db" :      "security",
      "url" :     "/omnibus/objectserver/restapi/security/users",
      "collist" : [ "UserID", "UserName", "FullName" ]
   },
   "monopoly_users": {
      // the subset of omnibus users who are in the monopoly user group
      // create view monopoly.users persistent as 
      //    select UserID, UserName, FullName from security.users where UserID in ( 
      //       select UserID from security.group_members where GroupID in ( 
      //          select GroupID from security.groups where GroupName in ( 'monopolyUser') 
      //    ) 
      // );
      "db" :      "monopoly",
      "url" :     "/omnibus/objectserver/restapi/monopoly/users",
      "collist" : [ "UserID", "UserName", "FullName" ]
   },
   "toast": {
      "db": "alerts",
      "url" :     "/omnibus/objectserver/restapi/alerts/status",
      "collist" : [ "TEXT01",  "TEXT02", "AlertKey", "Severity", "LastOccurrence", "ProbeSubSecondId", "Class", "AlertGroup" ],
      "sort":     "ProbeSubSecondId"
   },
   "journal": {
      "db": "alerts",
      "url" :     "/omnibus/objectserver/restapi/alerts/status",
      "collist" : [ "Grade", "Node", "Class", "AlertGroup", "AlertKey", "Severity", "Summary", "Poll", "OwnerUID", "LastOccurrence", "ProbeSubSecondId", "INT01", "INT02", "INT03", "PhysicalPort", "ServerName", "ServerSerial" ],
      "sort":     "LastOccurrence"
   },
   "bids": {
      "db": "alerts",
      "url" :     "/omnibus/objectserver/restapi/alerts/status",
      "collist" : [ "Severity", "OwnerUID", "LastOccurrence", "ProbeSubSecondId", "INT02", "BSM_Identity" ],
      "sort":     "ProbeSubSecondId"
   },
   // "status": {
   //    "db": "alerts",
   //    "url" :     "/omnibus/objectserver/restapi/alerts/status",
   //  //"collist" : [ "Grade", "Node", "Manager", "Agent", "AlertGroup", "AlertKey", "Severity", "Summary", "Poll", "Class", "OwnerUID", "SuppressEscl", "LastOccurrence", "ServerSerial", "ProbeSubSecondId" ],
   //    "collist" : [ "Grade", "Node",  "AlertGroup", "AlertKey", "Severity", "Summary", "Poll", "OwnerUID", "LastOccurrence", "ProbeSubSecondId", "Class" ],
   //    "sort":     "LastOccurrence"

   // },
   
}


