



function initModals(){

   var tag                    = tagTag( "initModals()" );
   console.info(tag, "initializing bootstrap modals ...");
   Object.values(monopoly.ui.modals).map(modal=>{
      modal.object = document.getElementById(modal.id);
      console.debug(tag, "initializing modal: " + modal.name + ", id: " + modal.id + ", conditional: " + modal.conditional + ", refreshable: " + modal.refreshable );
      modal.modalObj = bootstrap.Modal.getOrCreateInstance( modal.object );
   });


   /** modalAddUserToGame */
   monopoly.ui.modals.modalAddUserToGame.object.addEventListener('show.bs.modal', function (event) {
      var tag = tagTag(event.type);
     console.log(tag, "event triggered on modal '"+event.target.id+"', newGameId: " + event.target.modalAddUserToGameNewGameId.value);
      // Button that triggered the modal
      var button = event.relatedTarget
      // Extract info from data-bs-* attributes
      var newGameId = button.getAttribute('data-bs-gameId')
      // Update the modal's content --- title / label
      $('#modalAddUserToGameLabel').text( 'Add players to Game ' + newGameId );
      // Update the modal's content --- hidden game input - this holds the id of the game that users are to be added to, not necessarily the current game id!
      $('#modalAddUserToGameNewGameId').val(newGameId);
      // call the reload event to populate the modal
      event.target.dispatchEvent(reload_monopoly_bs_modal);
   });
   monopoly.ui.modals.modalAddUserToGame.object.addEventListener('hidden.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.log(tag, "event triggered on modal '"+event.target.id+"', clearing contents and reloading current tab");
      console.debug(tag,event);
      $( event.target.modalAddUserToGameSelectName ).empty();
      $('#modalAddUserToGameAvatarDiv', event.target ).empty();
      event.target.modalAddUserToGameSubmit.disabled=true;
      doUpdate();
   });
   monopoly.ui.modals.modalAddUserToGame.object.addEventListener('reload.monopoly.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.debug(tag,event);
      console.log(tag, "event triggered on modal '"+event.target.id+"', newGameId: " + event.target.modalAddUserToGameNewGameId.value);

      if( event.target.modalAddUserToGameNewGameId && event.target.modalAddUserToGameNewGameId.value ){
         // ripple an update to the game tab
         monopoly.ui.tabs.game.object.dispatchEvent(reload_monopoly_bs_tab);

         // -- fetch and populate unassigned users and unassigned avatars
         console.info(tag,"Fetching the player data for game " + event.target.modalAddUserToGameNewGameId.value + " ...");
         $.when( ajaxQueryByFilter(schema.players,"gameId in ( "+event.target.modalAddUserToGameNewGameId.value+" )","") ).then(function( newGameData ){
            console.groupCollapsed(tag,"populating the unassigned players and avatars")
            console.debug(tag, "Player data for new game "+event.target.modalAddUserToGameNewGameId.value+" (" + newGameData.rowset.affectedRows + " records) : ",  newGameData.rowset.rows);
   
            console.info(tag,"populating the players not yet added")
            $( event.target.modalAddUserToGameSelectName ).empty().append(
               monopoly.cache.users.filter(u => !newGameData.rowset.rows.map(g => g.uid).includes(u.UserID) ).map( unAssignedUser =>  $('<option>').attr({value: unAssignedUser.UserID}).text(unAssignedUser.FullName) )
            );
   
            console.info(tag,"populating the un-assigned avatars")
            $('#modalAddUserToGameAvatarDiv', event.target ).empty().append(
               monopoly.cache.avatar.filter( a => !newGameData.rowset.rows.map(g => g.avatar).includes(a.avatar) ).map( unAssignedAvatar => 
                  $('<div>').addClass('form-check').append(
                     $('<input>').attr({ class:'form-check-input', type:'radio', name:'avatarRadioControl', id:'avatar-'+unAssignedAvatar.avatar, value:unAssignedAvatar.avatar }),
                     $('<label>').attr({ class:'form-check-label', for:'avatar-'+ unAssignedAvatar.avatar, alt:unAssignedAvatar.title, 'aria-label': unAssignedAvatar.title+' avatar'  }).css('background-image','url('+unAssignedAvatar.src+')')
                  )
               )
            );
            // preselect the first avatar
            $( '#modalAddUserToGameAvatarDiv input[name="avatarRadioControl"]', event.target )[0].checked=true
            // enable the submit button
            event.target.modalAddUserToGameSubmit.disabled=false;
            console.groupEnd();
         });
      } else {
         console.error(tag,"failed to determine the new game id");
      }
   });
   // Update the modal's content --- config form submit action - leave disabled
   $('form#modalAddUserToGame').submit( function(event){
      var tag = tagTag('modalAddUserToGame submit');
      event.preventDefault();
      event.target.modalAddUserToGameSubmit.setAttribute('disabled', true);
      console.debug(tag,"submit event: ", event);
      console.log(tag, "newGameId: "+event.target.modalAddUserToGameNewGameId.value+", new uid: " + event.target.modalAddUserToGameSelectName.value + ", with avatar: " + event.target.avatarRadioControl.value );
      __callProcedureAddPlayer(event.target.modalAddUserToGameNewGameId.value, event.target.modalAddUserToGameSelectName.value, event.target.avatarRadioControl.value);
   })




   /** modalManageDeedSet */
   monopoly.ui.modals.modalManageDeedSet.object.addEventListener('show.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.debug(tag,event);
      console.log(tag, "event triggered on modal '"+event.target.id+"'");

      // Extract info from the element that was clicked upon
      var context    = event.relatedTarget.getAttribute('data-bs-context');
      var deedData   = $(event.relatedTarget).data('deedData')
      console.debug(tag, 'context: '+context+', deedData: ', deedData);
      var activeTab;
      // Clear and populate the tab bar
      $('#modalManageDeedSet ul.navbar-nav').empty().append(
         monopoly.cache.setInventory.map(set=>{
            return $('<li>').addClass('nav-item dropdown').addClass(set.set).append(
               $('<a>').attr({ 'class':"nav-link dropdown-toggle" + ( deedData.sets==set.set ? " active" : ""), 'data-bs-toggle': "dropdown", 'href':"#", 'role':"button", 'aria-expanded':"false" }).text(set.name),
               $('<ul>').addClass('dropdown-menu').append(
                  monopoly.cache.board.filter(b=>b.sets==set.set).sort((a,b) => a.place-b.place).map(b=>{
                     var id = 'modalManageDeedSet-tab-deed-tab-'+b.place;
                     console.info(tag,"populating deed tab " + b.title +" (id: '"+id+"')");
                     var a = $('<a>')
                        .attr({ 'class': 'nav-link', 'data-bs-toggle': 'tab', 'role': 'tab', 'aria-controls': 'modalManageDeedSet-tab-deed', 'aria-disabled': 'false', 'data-bs-target': '#modalManageDeedSet-tab-deed', 'href': '#', 'id':id })
                        .text( b.title )
                        /* 'shown.bs.tab' is run after the tab has been switched to, then populateModalManageDeedSet() finds the active tab in order to populate the tab content */
                        .on('shown.bs.tab', function(){ populateModalManageDeedSet(); } )
                     if( context == 'deed' && b.title == deedData.title ){
                       activeTab = new bootstrap.Tab(a); 
                     } else {
                        new bootstrap.Tab(a);
                     }
                     return $('<li>').append(a)
                  })
               )
            )
         })
      );
      // finally init the selected tab
      activeTab.show();
   });
   monopoly.ui.modals.modalManageDeedSet.object.addEventListener('hidden.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.debug(tag,event);
      console.log(tag, "event triggered on modal '"+event.target.id+"'");
      doUpdate();
   });
   monopoly.ui.modals.modalManageDeedSet.object.addEventListener('reload.monopoly.bs.modal', function (event) { populateModalManageDeedSet() });
   //modalManageDeedSet-settingsDropdown.addEventListener




   /** modalPurchaseAuctionDeed */
   monopoly.ui.modals.modalPurchaseAuctionDeed.object.addEventListener('show.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.groupCollapsed(tag, "event triggered on modal '"+event.target.id+"'");
      console.debug(tag,event);
      var modal = $( event.target );

      // reset the modal
      modal.attr('data-monopoly-prev', '');
      modal.attr('data-monopoly-square', 0);
      modal.find('.modal-header .modal-title').first().text('Purchase or Auction Title Deed');
      modal.find('.modal-body .col.deed-actions').first().empty().append( monopoly.ui.spinner );
      var deedCard = modal.find('.modal-body .col.deed-card').first().empty().append( monopoly.ui.spinner );

      // log data provided via the relatedTarget
      ['gameData','playersData','deedsData'].forEach(d=>console.debug(tag, tagTag(d),$(event.relatedTarget).data(d)));

      // determine the board position of the current active player in the current game
      var activePlayerData  = $(event.relatedTarget).data('playersData').filter(player=>player.gameId==sessionStorage.getItem("monopolyGameId") && player.uid==$(event.relatedTarget).data('gameData').activePlayer)[0];
      console.debug(tag, 'activePlayerData            : ', activePlayerData);
      if( monopoly.cache.deedsInventory.filter(di=>di.boardPlace==activePlayerData.position).length == 1 ){
         modal.attr('data-monopoly-square', activePlayerData.position);
         var currentDeedData =  $(event.relatedTarget).data('deedsData').filter(dd=>dd.board.place==activePlayerData.position)[0];
         console.debug(tag, 'currentDeedData             : ', currentDeedData);

         // load in the deed card of the given property, this will be static whilst this modal is displayed
         deedCard.empty().append( __generateDeedCardDiv('full', currentDeedData) );

         console.groupEnd();
         // construct and send an event to trigger the reload action for this modal
         event.target.dispatchEvent(reload_monopoly_bs_modal);
   
      } else {
         console.groupEnd();
         console.error(tag, "square "+square+" is not a known property");
         monopoly.ui.modals[event.target.id].modalObj.hide();
      }
   });
   monopoly.ui.modals.modalPurchaseAuctionDeed.object.addEventListener('reload.monopoly.bs.modal', function (event) {
      var tag = tagTag(event.type);
      var gameId = sessionStorage.getItem("monopolyGameId");
      var modal = $( event.target );
      var square = parseInt(modal.attr('data-monopoly-square'));

      console.info(tag, "event triggered on modal '"+event.target.id+"', gameId="+gameId+ ", square="+square+". Fetching game data...");
      if( monopoly.cache.deedsInventory.filter(di=>di.boardPlace==square).length == 1 ){
         $.when(ajaxQueryByKey(schema.games,gameId),ajaxQueryByFilter(schema.players,"gameId="+gameId,""),ajaxQueryByFilter(schema.deeds,"gameId="+gameId,"")).then(function(r0,r1,r2){
            console.groupCollapsed(tag, "data collected ...");
            console.debug(tag, tagTag('gameData'),    r0[0].rowset.affectedRows, r0[0].rowset.rows );
            console.debug(tag, tagTag('playersData'), r1[0].rowset.affectedRows, r1[0].rowset.rows );
            console.debug(tag, tagTag('deedsData'),   r2[0].rowset.affectedRows, r2[0].rowset.rows );
            enrichDeedSetData(r2[0].rowset.rows, r0[0].rowset.rows[0]);
            var gameData      = r0[0].rowset.rows[0];
            var playersData   = r1[0].rowset.rows;
            var deedsData     = r2[0].rowset.rows;
            console.groupEnd();
         
            var loggedInUserIsActivePlayer   =  gameData.activePlayer==getAuthUserID();
            var loggedInUserIsBanker         =  gameData.banker==getAuthUserID();
            var activePlayerData             =  playersData.filter(player=>player.gameId==sessionStorage.getItem("monopolyGameId") && player.uid==gameData.activePlayer)[0];
            var currentDeedData              =  deedsData.filter(dd=>dd.board.place==activePlayerData.position)[0];
            console.debug(tag, 'loggedInUserIsActivePlayer  : ' + loggedInUserIsActivePlayer);
            console.debug(tag, 'loggedInUserIsBanker        : ' + loggedInUserIsBanker );
            console.debug(tag, 'activePlayerData            : ', activePlayerData);
            console.debug(tag, 'currentDeedData             : ', currentDeedData);
            console.debug(tag, 'game                        : state: '+gameData.state+', action: ' + gameData.action);
            
            // this modal has a number of different modes:
            // 1) the logged in user is presented with the option to purchase deed or auction it off
            // 2) the deed has been put up for auction, and the auction is in progress
            // 3) the deed is owned; i.e. 
            //       the logged in user has just purchased the deed (effectively owns the deed)
            //    or the deed has been put up for auction, and the auction has been completed
            // 5) the deed is not owned

            var modalTitle = modal.find('.modal-header .modal-title').first();
            var modalActions = modal.find('.modal-body .col.deed-actions').first();

            if( currentDeedData.owner < 0 && gameData.action == 'forSale' && loggedInUserIsActivePlayer ){
               //
               // PURCHASE TITLE DEED
               //
               modalTitle.text('Purchase Title Deed');
               modalActions.empty().append( 
                  // purchase group title   
                  $('<label>').attr({for:'modalPurchaseAuctionDeed-purchaseGroup', class: 'form-label'}).text('Title Deed ownership'),

                  // purchase title deed group
                  $('<div>').attr({class:'input-group input-group-sm mb-3', id:'modalPurchaseAuctionDeed-purchaseGroup'}).append(
                     $('<span>').attr({ class: 'form-control input-group-text fa-solid fa-sign-hanging', 'aria-label': 'title deed ownership details'}).css("max-width","2.5em"),
                     $('<span>').attr({ class: 'form-control input-group-text', id: 'modalPurchaseAuctionDeed-purchaseGroupLabel', 'aria-label': 'title deed purchase price'}).text( 'For sale (£'+currentDeedData.inventory.purchase+')' ),

                     // append button
                     activePlayerData.cash >= currentDeedData.inventory.purchase
                     ? /* active player has sufficient funds to purchase */ 
                        $('<button>').attr({style:'min-width: 110px;', class:'btn btn-outline-primary', type:'button', id:'modalPurchaseAuctionDeed-purchaseGroupButton', 'aria-describedby':'modalPurchaseAuctionDeed-purchaseGroupLabel', 'data-monopoly-deed-title':currentDeedData.title, 'data-monopoly-deed-purchase':currentDeedData.inventory.purchase, 'data-monopoly-deed-buyer':gameData.activePlayer }).text('Purchase').click(function(event){
                           $(event.currentTarget).attr('disabled', true);
                           __callProcedures_transferDeedToPlayer_resumeGame(sessionStorage.getItem("monopolyGameId"), event.currentTarget.getAttribute('data-monopoly-deed-buyer'), event.currentTarget.getAttribute('data-monopoly-deed-title'), event.currentTarget.getAttribute('data-monopoly-deed-purchase'));
                        })
                     : /* active player has insufficient funds */ 
                        $('<button>').attr({style:'min-width: 110px;', class:'btn btn-warning',type:'button',id:'modalPurchaseAuctionDeed-purchaseGroupButton','aria-describedby':'modalPurchaseAuctionDeed-purchaseGroupLabel',disabled:true}).append($('<i>').addClass('bi bi-exclamation-triangle-fill'),' Insufficient funds')
                  ),
               
                  // start auction of title deeds group
                  $('<div>').attr({ class: 'input-group input-group-sm mb-3', id: 'modalPurchaseAuctionDeed-auctionGroup'}).append(
                     $('<span>').attr({ class: 'form-control input-group-text', id: 'modalPurchaseAuctionDeed-auctionGroupLabel', 'aria-label': 'title deed auction'}).text( 'Auction this deed' ),
                     $('<button>').attr({style:'min-width: 110px;', class:'btn btn-outline-primary', type:'button', id:'modalPurchaseAuctionDeed-purchaseGroupButton', 'aria-describedby':'modalPurchaseAuctionDeed-auctionGroupLabel' }).text('Start Auction').click(function(event){ $(event.currentTarget).prop('disabled'); _auctionDeedStart(sessionStorage.getItem("monopolyGameId")); })
                  )
               );
               // set the prev value
               modal.attr('data-monopoly-prev', 'purchase');
         
            } else if( currentDeedData.owner < 0 && gameData.action == 'auction' ){
               //
               // TITLE DEED AUCTION
               //
               modalTitle.text('Title Deed Auction');

               if( modal.attr('data-monopoly-prev') != 'auction' ){
                  // re-create structure
                  modalActions.empty().append( 
                     // controls
                     // 
                     $('<div>').attr({class:'input-group input-group-sm mb-3', id:'modalPurchaseAuctionDeed-purchaseGroup'}).append(
                        $('<span>').attr({ class: 'form-control input-group-text fa-solid fa-sign-hanging', 'aria-label': 'title deed ownership details'}).css("max-width","2.5em"),
                        $('<span>').attr({ class: 'form-control input-group-text', id: 'modalPurchaseAuctionDeed-purchaseGroupLabel', 'aria-label': 'title deed purchase price'}).text( 'Purchase price £'+currentDeedData.inventory.purchase ),
                     ),

                     // enter bid group
                     $('<div>').attr({class:'input-group input-group-sm mb-3', id:'modalPurchaseAuctionDeed-bidGroup'}).append(
                        $('<span>').attr({ class: 'form-control input-group-text fa-solid fa-gavel'}).css("max-width","2.5em"),
                        $('<span>').attr({style:'display: inline-block; text-align:right;',  class: 'form-control input-group-text'}).text( '£' ),
                        $('<input>').attr({type:'number', style:'min-width: 6em; max-width: 6em;', class:'form-control', id:'modalPurchaseAuctionDeed-bidGroupBid', 'aria-label':'Enter bid', value: 0, min: 1, max:activePlayerData.cash, disabled:true }).on('input paste', function(event){
                           $('#modalPurchaseAuctionDeed-bidGroupSubmit').prop('disabled',( parseInt(event.target.value) > parseInt(event.target.max) || parseInt(event.target.value) < parseInt(event.target.min) ));
                        }),
                        $('<button>').attr({style:'min-width: 110px;', class:'btn btn-primary', type:'button', id:'modalPurchaseAuctionDeed-bidGroupSubmit' }).text('Submit bid').click(function(event){ $(event.currentTarget).prop('disabled', true); _auctionDeedBid(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), $('#modalPurchaseAuctionDeed-bidGroupBid').val()); })
                     ),

                     // bid table & header
                     $('<label>').attr({for:'modalPurchaseAuctionDeed-bidHistory', class: 'form-label'}).text('Bid history'),
                     $('<div>').attr({ class:'mb-3', style: 'overflow-y:auto; min-height: 32vh; max-height: 32vh;' }).append(
                        $('<table>').attr({class:'table table-sm caption-top', style:'font-size: 0.8em;', id:'modalPurchaseAuctionDeed-bidHistory'}).append(
                           $('<thead>').attr({style:'top: 0; z-index: 2; position: sticky; background-color: white;'}).append($('<tr>').append(
                              $('<th>').attr({colspan:2}).append('Player'),
                              $('<th>').attr({colspan:2}).append('Bid'),
                           )),
                           $('<tbody>').append( $('<tr>').append( $('<td>').attr({colspan:4}).append( monopoly.ui.spinner ) ) )
                        )
                     ),

                     // withdraw group
                     $('<div>').attr({class:'input-group input-group-sm', id:'modalPurchaseAuctionDeed-bidGroup'}).append(
                        $('<span>').attr({ class: 'form-control input-group-text fa-solid fa-gavel'}).css("max-width","2.5em"),
                        $('<span>').attr({ class: 'form-control input-group-text'}),
                        $('<button>').attr({style:'min-width: 110px;', class:'btn btn-secondary', type:'button', id:'modalPurchaseAuctionDeed-bidGroupWithdrawButton' }).text('Withdraw').click(function(event){ $(event.currentTarget).prop('disabled',true); _auctionDeedBid(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), -1); })
                     )

                  );
               } else {
                  $('table#modalPurchaseAuctionDeed-bidHistory tbody').empty().append( $('<tr>').append( $('<td>').attr({colspan:4}).append( monopoly.ui.spinner ) ) );
               }

               $.when( ajaxQueryByFilter(schema.bids,  "Class=1502 and Grade=" + sessionStorage.getItem("monopolyGameId") + " and Poll=" +gameData.moves, "") ).then( function(bidsResponse){
                  console.debug(tag, "bids data collected: ", bidsResponse);
                  // transform
                  // "Severity", "OwnerUID", "LastOccurrence", "ProbeSubSecondId", "INT02", "BSM_Identity"
                  var bidData    = [];
                  bidsResponse.rowset.rows.map(playerBids=>{
                     playerBids.BSM_Identity.split(";").filter(a=>a.length>0).map(p=>p.split(",").map(s=>parseInt(s))).forEach(a=>{
                        bidData.push( { epoch: a[0], bid: a[1], uid: playerBids.OwnerUID } );
                     })
                  });
                  console.debug(tag, "transformed bidData: ", bidData );
                  // re-populate into structure

                  // auction in progress
                  var suggestedValue =  bidData.length==0 ? currentDeedData.inventory.mortgage : ( Math.max(...bidData.map(bd=>bd.bid)) + 10 );
                  var minimumValue   =  bidData.length==0 ? 1 : Math.max(...bidData.map(bd=>bd.bid)) + 1;
                  var bidInput       =  $('input#modalPurchaseAuctionDeed-bidGroupBid').attr({ min: minimumValue, disabled: false });
                  if( bidInput.val() == 0 ) {
                     bidInput.val(suggestedValue);
                  }
                  $('button#modalPurchaseAuctionDeed-bidGroupSubmit').prop('disabled',( parseInt(bidInput.val()) > parseInt(activePlayerData.cash) || parseInt(bidInput.val()) < minimumValue ));
      
                  $('table#modalPurchaseAuctionDeed-bidHistory tbody').empty().append( bidData.sort((a,b) => b.epoch-a.epoch).map((bid,i)=> $('<tr>').append(
                     $('<td>').append($('<img>').attr({src:'images/token-'+ playersData.filter(player=>player.gameId==sessionStorage.getItem("monopolyGameId") && player.uid==bid.uid)[0].avatar+'.png', style:'height:1.5em;'})),
                     $('<td>').append(getFullNameForUserID(bid.uid)),
                     $('<td>').append( i==0 ? $('<strong>').text('£'+bid.bid) : $('<strike>').text('£'+bid.bid) ),
                     $('<td>').append( i==0 ? 'Highest bid' : '' )
                  )));

               });
               // set the prev value
               modal.attr('data-monopoly-prev', 'auction');

            } else if( currentDeedData.owner > -1 ){
               // owned
               modalActions.empty().append( 
                  // title
                  $('<label>').attr({for:'modalPurchaseAuctionDeed-purchaseGroup', class: 'form-label'}).text('Title Deed ownership'),
                  // ownership details
                  $('<div>').attr({id: 'modalPurchaseAuctionDeed-purchaseGroup', class: 'input-group input-group-sm mb-3'}).append(
                     $('<button>').attr({ class: 'btn btn-outline-secondary py-0 px-1',type:'button','aria-label':'title deed owners avatar',disabled:true}).append(
                        $("<img>").attr({class: 'avatar', alt: currentDeedData.ownerUsername, src: "images/token-"+activePlayerData.avatar+".png"}).css('height', '25px')
                     ),
                     $('<span>').attr({ class: 'form-control input-group-text', 'aria-label': 'title deed ownership details'}).text( 
                        currentDeedData.ownerUsername==getAuthUserID()
                        ? currentDeedData.ownerUsername + ' (bank balance £'+activePlayerData.cash+')' 
                        : currentDeedData.ownerUsername
                     )
                  )
               )
               if( modal.attr('data-monopoly-prev') == 'auction' ){
                  modalActions.append(
                     // bid table & header
                     $('<label>').attr({for:'modalPurchaseAuctionDeed-bidHistory', class: 'form-label'}).text('Bid history'),
                     $('<div>').attr({ class:'mb-3', style: 'overflow-y:auto; min-height: 32vh; max-height: 32vh;' }).append(
                        $('<table>').attr({class:'table table-sm caption-top', style:'font-size: 0.8em;', id:'modalPurchaseAuctionDeed-bidHistory'}).append(
                           $('<thead>').attr({style:'top: 0; z-index: 2; position: sticky; background-color: white;'}).append($('<tr>').append(
                              $('<th>').attr({colspan:2}).append('Player'),
                              $('<th>').attr({colspan:2}).append('Bid'),
                           )),
                           $('<tbody>').append( $('<tr>').append( $('<td>').attr({colspan:4}).append( monopoly.ui.spinner ) ) )
                        )
                     ),
                  );

                  $.when( ajaxQueryByFilter(schema.bids,  "Class=1502 and Grade=" + sessionStorage.getItem("monopolyGameId") + " and Poll=" +gameData.moves, "") ).then( function(bidsResponse){
                     console.debug(tag, "bids data collected: ", bidsResponse);
                     var bidData    = [];
                     bidsResponse.rowset.rows.map(playerBids=>{
                        playerBids.BSM_Identity.split(";").filter(a=>a.length>0).map(p=>p.split(",").map(s=>parseInt(s))).forEach(a=>{
                           bidData.push( { epoch: a[0], bid: a[1], uid: playerBids.OwnerUID } );
                        })
                     });
                     console.debug(tag, "transformed bidData: ", bidData );
                     // populate into table
                        $('table#modalPurchaseAuctionDeed-bidHistory tbody').empty().append( bidData.sort((a,b) => b.epoch-a.epoch).map((bid,i)=> $('<tr>').append(
                        $('<td>').append($('<img>').attr({src:'images/token-'+ playersData.filter(player=>player.gameId==sessionStorage.getItem("monopolyGameId") && player.uid==bid.uid)[0].avatar+'.png', style:'height:1.5em;'})),
                        $('<td>').append(getFullNameForUserID(bid.uid)),
                        $('<td>').append( i==0 ? $('<strong>').text('£'+bid.bid) : $('<strike>').text('£'+bid.bid) ),
                        $('<td>').append( i==0 ? 'Winning bid' : '' )
                     )));
                  });
               }
         
            } else {
               // not owned
               modalActions.empty().append( 
                  // title
                  $('<label>').attr({for:'modalPurchaseAuctionDeed-purchaseGroup', class: 'form-label'}).text('Title Deed ownership'),
                  // ownership details
                  $('<div>').attr({class:'input-group input-group-sm mb-3', id:'modalPurchaseAuctionDeed-purchaseGroup'}).append(
                     $('<span>').attr({ class: 'form-control input-group-text fa-solid fa-sign-hanging', 'aria-label': 'title deed ownership details'}).css("max-width","2.5em"),
                     $('<span>').attr({ class: 'form-control input-group-text', id: 'modalPurchaseAuctionDeed-purchaseGroupLabel', 'aria-label': 'title deed purchase price'}).text( 'For sale (£'+currentDeedData.inventory.purchase+')' )
                  )
               )
            }

            
         });
      } else {
         console.error(tag, "square "+square+" is not a known property");
      }
   });
   monopoly.ui.modals.modalPurchaseAuctionDeed.object.addEventListener('hidden.bs.modal', function (event) {
      var tag = tagTag(event.type);
      //console.debug(tag,event);
      console.log(tag, "event triggered on modal '"+event.target.id+"'");
      var modal = $( event.target );
      [ '.modal-header .modal-title', '.modal-body .col.deed-actions', '.modal-body .col.deed-card' ].forEach( selector=>modal.find(selector).first().empty() );
      modal.attr('data-monopoly-prev', '');
      doUpdate();
   });




   /** modalPlayerIncarcerated */
   // turn           | jailTime > 1 | serve time   |       40 | jailTime--
   // "              | jailTime = 1 | serve time   |       10 | jailTime-- and player fined £50
   // fine           | jailTime > 0 | release      |       10 | player has chosen to pay their £50 fine this turn
   // doubledice     | jailTime > 0 | release      |       10 |
   // chance         | jailTime > 0 | release      |       10 | chance goojf card returned
   // communityChest | jailTime > 0 | release      |       10 | community chest goojf card returned
   
   monopoly.ui.modals.modalPlayerIncarcerated.object.addEventListener('show.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.groupCollapsed(tag, "event triggered on modal '"+event.target.id+"'");
      var modal = $(event.target);
      var modalActions = modal.find('.modal-body .col.actions').first();

      // set the on-click action for all the buttons, since only one option can be used at a time, all buttons will be disabled when any one is clicked
      modalActions.find('#modalPlayerIncarcerated-serveTime button').first().click(function(){
         modalActions.find('button').attr('disabled', true);
         __callProcedurePlayerSetJailTime(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), 'turn');
      });
      modalActions.find('#modalPlayerIncarcerated-dice button').first().click(function(){
         modalActions.find('button').attr('disabled', true);
         __callProcedurePlayerSetJailTime(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), 'fine');
      });
      modalActions.find('#modalPlayerIncarcerated-payFine button').first().click(function(){
         modalActions.find('button').attr('disabled', true);
         //__callProcedurePlayerSetJailTime(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), 'turn');
      });
      modalActions.find('#modalPlayerIncarcerated-goojfc-chance button').first().click(function(){
         modalActions.find('button').attr('disabled', true);
         __callProcedurePlayerSetJailTime(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), 'chance');
      });
      modalActions.find('#modalPlayerIncarcerated-goojfc-communityChest button').first().click(function(){
         modalActions.find('button').attr('disabled', true);
         __callProcedurePlayerSetJailTime(sessionStorage.getItem("monopolyGameId"), getAuthUserID(), 'communityChest');
      });


      // construct and send an event to trigger the reload action for this modal
      event.target.dispatchEvent(reload_monopoly_bs_modal);
   });
   monopoly.ui.modals.modalPlayerIncarcerated.object.addEventListener('reload.monopoly.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.info(tag, "event triggered on modal '"+event.target.id+"'");
      console.debug(tag,event);
      var modal = $(event.target);
      var modalActions = modal.find('.modal-body .col.actions').first();
      modalActions.find('button').attr('disabled', true);
      modalActions.find('#modalPlayerIncarcerated-serveTime span').first().empty().append(monopoly.ui.spinner);

      $.when( ajaxQueryByKey(schema.games,sessionStorage.getItem("monopolyGameId")), ajaxQueryByKey(schema.players,sessionStorage.getItem("monopolyGameId")+"-"+getAuthUserID())).then(function(r0,r1){
         console.groupCollapsed(tag, "data collected ...");
         console.debug(tag, tagTag('gameData'),   r0[0].rowset.affectedRows, r0[0].rowset.rows[0] );
         console.debug(tag, tagTag('playerData'), r1[0].rowset.affectedRows, r1[0].rowset.rows[0] );
         var gameData      = r0[0].rowset.rows[0];
         var playerData    = r1[0].rowset.rows[0];
         console.groupEnd();

         if( gameData.activePlayer == playerData.uid && playerData.jailTime > 0 ){
            console.debug(tag,'player is in jail');
            modalActions.find('#modalPlayerIncarcerated-serveTime span').first().text( playerData.jailTime==1 ? '1 turn remaining' : playerData.jailTime +' turns remaining' );
            modalActions.find('#modalPlayerIncarcerated-serveTime button').first().attr('disabled',false);

            //modalActions.find('#modalPlayerIncarcerated-dice button').first().attr('disabled',false);

            modalActions.find('#modalPlayerIncarcerated-payFine button').first().attr('disabled',false);

            if( gameData.cardsCChestGoojfOwner > -1 && gameData.cardsCChestGoojfOwner == playerData.uid ){
               modalActions.find('#modalPlayerIncarcerated-goojfc-communityChest button').first().attr('disabled',false);
            }
            if( gameData.cardsChanceGoojfOwner > -1 && gameData.cardsChanceGoojfOwner == playerData.uid ){
               modalActions.find('#modalPlayerIncarcerated-goojfc-chance button').first().attr('disabled',false);
            }

         } else if( gameData.activePlayer == playerData.uid && playerData.jailTime > 0 ){
            console.debug(tag,'player is NOT in jail');

         } else {
            console.error(tag,'player is not the active player');

            monopoly.ui.modals[event.target.id].modalObj.hide();
         }
      });
   });
   monopoly.ui.modals.modalPlayerIncarcerated.object.addEventListener('hidden.bs.modal', function (event) {
      var tag = tagTag(event.type);
      console.info(tag, "event triggered on modal '"+event.target.id+"'");
      console.debug(tag,event);
      var modal = $(event.target);
   });

   
}







function populateModalManageDeedSet(){
   var tag              = tagTag("modalManageDeedSetLoadTab()");
   var gameId           = sessionStorage.getItem("monopolyGameId");
   // find out which tab is active, scrape the title text
   var activeDeedTitle = $('div#modalManageDeedSet a.active.nav-link[role=tab]').text();
  
   console.debug(tag,'gameId: '+gameId+', activeDeedTitle: "'+activeDeedTitle+'"' );

   $.when( ajaxQueryByKey(schema.games, gameId), ajaxQueryByFilter(schema.players,"gameId=" + gameId,""), ajaxQueryByFilter(schema.deeds,  "gameId=" + gameId,"") )
   .then( function(gamesResponse, playersResponse, deedsResponse ){
         console.groupCollapsed(tag, "data collected ...");
         console.debug(tag, '[gameData                ]', gamesResponse[0].rowset.affectedRows,   gamesResponse[0].rowset.rows     );
         console.debug(tag, '[playersData             ]', playersResponse[0].rowset.affectedRows, playersResponse[0].rowset.rows   );
         console.debug(tag, '[deedsData               ]', deedsResponse[0].rowset.affectedRows,   deedsResponse[0].rowset.rows     );
         enrichDeedSetData(deedsResponse[0].rowset.rows, gamesResponse[0].rowset.rows[0])
         console.groupEnd();

         var deedData = deedsResponse[0].rowset.rows.filter( d => d.title == activeDeedTitle )[0];
         console.debug(tag, '[deedData                ]', deedData );

         var loggedInUserIsOwner    =  deedData.owner==getAuthUserID();
         var loggedInUserIsBanker   =  gamesResponse[0].rowset.rows[0].banker==getAuthUserID();
         console.debug(tag,"loggedInUserIsOwner: " + loggedInUserIsOwner + ", loggedInUserIsBanker: " + loggedInUserIsBanker );
         var playerData       =  playersResponse[0].rowset.rows.filter(player=>player.uid==deedData.owner)[0];
         var ownerBalance     =  deedData.checks.owned ? playerData.cash : 0;

         var infoPopoverContent = $('<table>').css('font-size','xx-small').append(
            $('<tbody>').append(

               $('<tr>').append( $('<td>').attr('colspan','3').append($('<h6>').text('Deed')) ),
               $('<tr>').append( $('<td>').text('Set'),           $('<td>').text(deedData.sets),                        $('<td>').text('colourSet: ' + deedData.inventory.colourSet) ),
               $('<tr>').append( $('<td>').text('Place'),         $('<td>').text(deedData.board.place) ),
               $('<tr>').append( $('<td>').text('Owner'),         $('<td>').text(deedData.owner),                       $('<td>').text(deedData.ownerUsername) ),
               $('<tr>').append( $('<td>').text('State'),         $('<td>').text(deedData.state),                       $('<td>').text(monopoly.states.deed[deedData.inventory.colourSet][deedData.state])  ),
               $('<tr>').append( $('<td>').text('Current rent'),  $('<td>').text('£' + deedData.checks.rent) ),

               $('<tr>').append( $('<td>').attr('colspan','3').append($('<h6>').text('Set')) ),
               deedsResponse[0].rowset.rows.filter(otherSetDeeds => otherSetDeeds.sets == deedData.sets ).sort((a,b) => a.place-b.place).map( otherSetDeeds => $('<tr>').append( 
                  $('<td>').text( " - " + otherSetDeeds.title ), $('<td>').attr('colspan','2').append( otherSetDeeds.checks.owned ? 'owner: ' + otherSetDeeds.ownerUsername +' (' + monopoly.states.deed[otherSetDeeds.inventory.colourSet][otherSetDeeds.state] +')' : 'for sale' )
               )),
               $('<tr>').append( $('<td>').text('deeds'),                  $('<td>').text(deedData.set.setDeeds) ),
               $('<tr>').append( $('<td>').text('houses built'),           $('<td>').text(deedData.set.setBuildings.house) ),
               $('<tr>').append( $('<td>').text('hotels built'),           $('<td>').text(deedData.set.setBuildings.hotel) ),
               $('<tr>').append( $('<td>').text('cost of buildings'),      $('<td>').text("£ " + deedData.set.setBuildings.cost) ),
               $('<tr>').append( $('<td>').text('unit building cost'),     $('<td>').text("£ " + deedData.inventory.cost_building) ),
               $('<tr>').append( $('<td>').text('unit building refund'),   $('<td>').text("£ " + deedData.inventory.refund_building) ),

               $('<tr>').append( $('<td>').attr('colspan','3').append($('<h6>').text('checks')) ),
               Object.entries( deedData.checks ).map(check=> $('<tr>').append( $('<td>').text(check[0]), $('<td>').text(check[1]) ) ),

               $('<tr>').append( $('<td>').attr('colspan','3').append($('<h6>').text('Game:')) ),
               $('<tr>').append( $('<td>').text('building stock'),         $('<td>').attr('colspan','2').text(
                  monopoly.cache.stock.map(si=> si.item + 's: ' + si.remaining + '/' + si.quantity).join(', '), " (remaining/quantity)")
               )
            )
         )

         var infoPopoverButton = $('<sup>')
            .attr({tabindex:0, role:'button', class:'btn btn-sm outline-info bi bi-info-circle', 'data-bs-toggle':'popover', 'data-bs-trigger':'focus', 'data-bs-placement': 'bottom', title:'status / debug info for title deed'})
            .css({color: 'lightblue', 'padding-left': '0.3em'})
            .data(deedData);
         new bootstrap.Popover(infoPopoverButton,{html: true, trigger: 'focus', content:infoPopoverContent });


         var manageDeed = $('<div>');
         // ownership
         manageDeed.append( $('<label>').attr({for:'owner', class: 'form-label'}).text('Deed ownership'), infoPopoverButton)

         if( deedData.checks.owned ){
            manageDeed.append(
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<button>').attr({ class: 'btn btn-outline-secondary py-0 px-1',type:'button','aria-label':'title deed owners avatar',disabled:true}).append(
                     $("<img>").attr({class: 'avatar', alt: deedData.ownerUsername, src: "images/token-"+playerData.avatar+".png"}).css('height', '25px')
                  ),
                  $('<span>').attr({ class: 'form-control input-group-text', 'aria-label': 'title deed ownership details'}).text( deedData.ownerUsername + ' (bank balance £'+ownerBalance+')' )
               )
            )
   
         } else {
            manageDeed.append(
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<span>').attr({ class: 'form-control input-group-text', 'aria-label': 'title deed ownership details'}).text( 'For sale (£'+deedData.inventory.purchase+')' )
               )
            )
         }

         // Sell Deed - act of selling the deed from the bank to a user, availaible to the banker only and only if the deed is un-owned
         // TODO need to consider an auction
         // TODO probably need to factor in the players turn etc
         // TODO nice to have a god mode
         if( ! deedData.checks.owned && loggedInUserIsBanker ){
            manageDeed.append(
               $('<label>').attr({for:'sellTo-submit', class: 'form-label'}).text('Sell deed'),
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<span>').addClass('input-group-text').text( 'Sell deed to' ),
                  $('<select>').attr({class:'form-select', id:'sellTo-buyer', 'aria-label':'Select the user to transfer this deed to, users are listed with their current bank balance'}).append(
                     playersResponse[0].rowset.rows.map(player=> $('<option>').text( getFullNameForUserID(player.uid) + " (£"+player.cash+")").data(player) )
                  ).change(function(){
                     $('#modalManageDeedSet #sellTo-submit').prop('disabled', $('#modalManageDeedSet #sellTo-price').val() > $('#modalManageDeedSet #sellTo-buyer option:selected').data().cash);
                  }),
                  $('<span>').addClass('input-group-text').text('for £'),
                  $('<input>').attr({class:'form-control', type:'number', id:'sellTo-price', 'aria-label':'Enter a value for the transaction', value: deedData.inventory.purchase }).change(function(){
                     $('#modalManageDeedSet #sellTo-submit').prop('disabled', $('#modalManageDeedSet #sellTo-price').val() > $('#modalManageDeedSet #sellTo-buyer option:selected').data().cash);
                  }),
                  $('<input>').attr({type:'hidden',id:'sellTo-title',value:deedData.title}),
                  $('<button>')
                     .attr({class:'btn btn-outline-primary',type:'button',id:'sellTo-submit', 'aria-describedby':'sellTo-help'})
                     .text('Submit')
                     .click(function(event){
                        var buyerData      = $('#modalManageDeedSet #sellTo-buyer option:selected').data();
                        var purchaseValue  = $('#modalManageDeedSet #sellTo-price').val();
                        var title          = $('#modalManageDeedSet #sellTo-title').val();
                        if( purchaseValue > buyerData.cash ){
                           $('#modalManageDeedSet #sellTo-submit').prop('disabled');
                           doToast("Insuficient funds", getFullNameForUserID(buyerData.uid) + " cannot afford £"+purchaseValue);
                        } else {
                           __callProcedureTransferDeedToPlayer(sessionStorage.getItem("monopolyGameId"), buyerData.uid, title, purchaseValue);
                        }
                     })
               ),
               $('<div>').attr({id:'sellTo-help', class:'form-text'}).text('NB: this action will be preformed by the banker role.')
            )
         }

         // Transfer deed between players
         if( deedData.checks.canSell && ( loggedInUserIsOwner || loggedInUserIsBanker ) ) {
            manageDeed.append(
               $('<label>').attr({for:'transfer-submit', class: 'form-label'}).text('Sell deed'),
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<span>' ).attr({id:'transfer-from-label',class:'input-group-text'}).text('Transfer from ' + deedData.ownerUsername + ' to'),
                  $('<select>').attr({class:'form-select', id:'transfer-to', 'aria-label':'Select the user to sell/transfer this deed to, users are listed with their current bank balance','aria-describedby':'transfer-from-label'}).append(
                     playersResponse[0].rowset.rows.filter(player=>player.uid!=deedData.owner).map(player=> $('<option>').text( getFullNameForUserID(player.uid) + " (£"+player.cash+")").data(player) )
                  ).change(function(){
                     $('#modalManageDeedSet #transfer-submit').prop('disabled', $('#modalManageDeedSet #transfer-price').val() > $('#modalManageDeedSet #transfer-to option:selected').data().cash);
                  }),

                  $('<span>' ).attr({id:'transfer-price-label',class:'input-group-text'}).text('for £'),
                  $('<input>').attr({class:'form-control', type:'number', id:'transfer-price', 'aria-label':'Enter a value for the transaction', 'aria-describedby':'transfer-price-label', value: deedData.inventory.purchase }).change(function(){
                     $('#modalManageDeedSet #transfer-submit').prop('disabled', $('#modalManageDeedSet #transfer-price').val() > $('#modalManageDeedSet #transfer-to option:selected').data().cash);
                  }),

                  $('<input>').attr({type:'hidden',id:'transfer-title',value:deedData.title}),
                  $('<button>')
                     .attr({class:'btn btn-outline-primary',type:'button',id:'transfer-submit', 'aria-describedby':'transfer-help'})
                     .text('Submit')
                     .click(function(event){
                        var buyerData      = $('#modalManageDeedSet #transfer-to option:selected').data();
                        var purchaseValue  = $('#modalManageDeedSet #transfer-price').val();
                        var title          = $('#modalManageDeedSet #transfer-title').val();
                        if( purchaseValue > buyerData.cash ){
                           $('#modalManageDeedSet #transfer-submit').prop('disabled');
                           doToast("Insuficient funds", getFullNameForUserID(buyerData.uid) + " cannot afford £"+purchaseValue);
                        } else {
                           __callProcedureTransferDeedToPlayer(sessionStorage.getItem("monopolyGameId"), buyerData.uid, title, purchaseValue);
                        }
                     })
               ),
               $('<div>').attr({id:'transfer-help', class:'form-text'}).text( loggedInUserIsOwner ? '' : 'NB: this action will be preformed by the banker role.')
            )
         };

         // mortgage deed
         if( deedData.checks.canMortgage && ( loggedInUserIsBanker || loggedInUserIsOwner ) ){
            manageDeed.append(
               $('<label>').attr({for:'mortgage-submit', class: 'form-label'}).text('Mortgage deed'),
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<span>').addClass('form-control input-group-text').text('Mortgage this deed for £' + deedData.inventory.mortgage),
                  $('<input>').attr({type:'hidden',id:'mortgage-title',value:deedData.title}),
                  $('<button>')
                     .attr({class:'btn btn-outline-primary',type:'button',id:'mortgage-submit', 'aria-describedby':'mortgage-help', 'data-bs-monopoly-deed-title': deedData.title, 'data-bs-monopoly-action':'mortgage'})
                     .text('Submit')
                     .click( function(event){ __callProcedureManageOwnedDeedState(sessionStorage.getItem("monopolyGameId"), event.currentTarget.getAttribute('data-bs-monopoly-deed-title'), event.currentTarget.getAttribute('data-bs-monopoly-action') ) })
               ),
               $('<div>').attr({id:'mortgage-help', class:'form-text'}).text( loggedInUserIsOwner ? '' : 'NB: this action will be preformed by the banker role.')
            );
         }
         // unmortgage deed
         if( deedData.checks.isMortgaged && ( loggedInUserIsBanker || loggedInUserIsOwner )){
            manageDeed.append(
               $('<label>').attr({for:'unmortgage-submit', class: 'form-label'}).text('Unmortgage deed'),
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<span>').addClass('form-control input-group-text').text('Unmortgage this deed for £' + deedData.inventory.unmortgage),
                  $('<input>').attr({type:'hidden',id:'unmortgage-title',value:deedData.title}),
                  $('<button>')
                     .attr({class:'btn btn-outline-primary',type:'button',id:'unmortgage-submit', disabled: ownerBalance<deedData.inventory.unmortgage, 'aria-describedby':'unmortgage-help', 'data-bs-monopoly-deed-title': deedData.title, 'data-bs-monopoly-action':'unmortgage'})
                     .text('Submit')
                     .click( function(event){ __callProcedureManageOwnedDeedState(sessionStorage.getItem("monopolyGameId"), event.currentTarget.getAttribute('data-bs-monopoly-deed-title'), event.currentTarget.getAttribute('data-bs-monopoly-action') ) })
               ),
               $('<div>').attr({id:'unmortgage-help', class:'form-text'}).text( 
                  ownerBalance<deedData.inventory.unmortgage 
                     ? 'The owner has insufficient funds to unmortgage this deed'
                     : loggedInUserIsOwner
                        ? '' 
                        : 'NB: this action will be preformed by the banker role.'
               )
            );
         }

         // Building control
         if( deedData.inventory.colourSet ){

            manageDeed.append( $('<label>').attr({class: 'form-label'}).text('Buy/Sell Houses and Hotels') );


            monopoly.cache.setInventory.filter(set=>set.set==deedData.sets)[0].deeds.forEach(setDeed => {
               var deedGameData     =  deedsResponse[0].rowset.rows.filter( d => d.title == setDeed.title )[0];
               var id               =  'buildingControl-'+setDeed.sets+'-'+setDeed.boardPlace;
               var disableSell      =  !( deedGameData.checks.canSellBuilding && deedGameData.checks.buildingToSell && ( loggedInUserIsBanker || loggedInUserIsOwner ));
               var disableBuy       =  !( deedGameData.checks.canBuyBuilding  && deedGameData.checks.buildingToBuy  && ( loggedInUserIsBanker || loggedInUserIsOwner ) && ownerBalance >= setDeed.cost_building );
               manageDeed.append(
                  $('<div>').addClass('input-group input-group-sm mb-3').append(
                     $('<span>').addClass('form-control input-group-text').text(deedGameData.title),
                     $('<span>').attr({class:'input-group-text '+$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']), id: id+'-state-1'}).css( 'color', deedGameData.state >= 1 ? "": "lightgray"),
                     $('<span>').attr({class:'input-group-text '+$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']), id: id+'-state-2'}).css( 'color', deedGameData.state >= 2 ? "": "lightgray"),
                     $('<span>').attr({class:'input-group-text '+$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']), id: id+'-state-3'}).css( 'color', deedGameData.state >= 3 ? "": "lightgray"),
                     $('<span>').attr({class:'input-group-text '+$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='house')[0]['icon']), id: id+'-state-4'}).css( 'color', deedGameData.state >= 4 ? "": "lightgray"),
                     $('<span>').attr({class:'input-group-text '+$('<i>').addClass(monopoly.cache.stock.filter(s=>s.item=='hotel')[0]['icon']), id: id+'-state-5'}).css( 'color', deedGameData.state >= 5 ? "": "lightgray"),
                     $('<button>')
                        .attr({class:'btn btn-outline-secondary building-control building-control-sell', type:'button',id: id+'-sell', disabled: disableSell, 'aria-describedby': 'buildingControl-sell-help', 'data-bs-monopoly-deed-title': deedGameData.title, 'data-bs-monopoly-action':'sell'})
                        .append( $('<span>').addClass(monopoly.ui.iconClasses.minusCircle) )
                        .click( function(event){ 
                           $('#modalManageDeedSet-tab-deed-details button.building-control').prop('disabled', true);
                           __callProcedureManageOwnedDeedState(sessionStorage.getItem("monopolyGameId"), event.currentTarget.getAttribute('data-bs-monopoly-deed-title'), event.currentTarget.getAttribute('data-bs-monopoly-action'))
                        }),
                     $('<button>')
                        .attr({class:'btn btn-outline-secondary building-control building-control-buy',  type:'button',id: id+'-buy',  disabled: disableBuy,  'aria-describedby': 'buildingControl-buy-help', 'data-bs-monopoly-deed-title': deedGameData.title, 'data-bs-monopoly-action':'buy' })
                        .append( $('<span>').addClass(monopoly.ui.iconClasses.plusCircle) )
                        .click( function(event){ 
                           $('#modalManageDeedSet-tab-deed-details button.building-control').prop('disabled', true);
                           __callProcedureManageOwnedDeedState(sessionStorage.getItem("monopolyGameId"), event.currentTarget.getAttribute('data-bs-monopoly-deed-title'), event.currentTarget.getAttribute('data-bs-monopoly-action')) 
                        })
                  ),
               );
            });
            // helper text(s)
               
            // manageDeed.append( $('<div>').attr({id:'buildingControl-sell-help', class:'form-text'}).text( helperText ) );
            // manageDeed.append( $('<div>').attr({id:'buildingControl-buy-help', class:'form-text'}).text( helperText ) );

            var disableLiquidate =  !( deedData.checks.canSellBuilding && ( loggedInUserIsBanker || loggedInUserIsOwner ));
            console.debug(tag, "disableLiquidate: " + disableLiquidate + " (deedData.checks.canSellBuilding: "+deedData.checks.canSellBuilding+", loggedInUserIsBanker: "+loggedInUserIsBanker+", loggedInUserIsOwner: "+loggedInUserIsOwner+")") ;
            manageDeed.append(
               $('<div>').addClass('input-group input-group-sm mb-3').append(
                  $('<span>').addClass('form-control input-group-text').text('Liquidate all buildings in this set'),
                  $('<div>').addClass('input-group-text building-control building-control-liquidate-check').append($('<input>').attr({class:'form-check-input mt-0', type:'checkbox',value:null,'aria-label':'check to confirm that are you sure you wish to liquidate all buildings in this set.'})),
                  $('<button>')
                     .attr({class:'btn btn-outline-secondary building-control building-control-liquidate',  type:'button',  disabled: disableLiquidate,  'aria-describedby': 'buildingControl-liquidate-help', 'data-bs-monopoly-deed-set': deedData.sets, 'data-bs-monopoly-deed-title': deedData.title, 'data-bs-monopoly-action':'liquidate' })
                     .text('Liquidate')
                     .click( function(event){ 
                        if( $('#modalManageDeedSet-tab-deed-details button.building-control').prop('checked') || confirm("Liquidating will sell all hotels and/or houses on this title deed, are you sure?") ) {
                           $('#modalManageDeedSet-tab-deed-details button.building-control').prop('disabled', true);
                           __callProcedureLiquidateOwnedDeedSet(sessionStorage.getItem("monopolyGameId"), event.currentTarget.getAttribute('data-bs-monopoly-deed-set'));
                        }
                     })
               ),
            );

            // https://hasbro-new.custhelp.com/app/answers/detail/a_id/68/~/i-need-to-sell-a-hotel-in-order-to-pay-a-large-rent-in-the-monopoly-game-but
            // https://boardgames.stackexchange.com/questions/8049/housing-shortage-in-monopoly



            if( !loggedInUserIsOwner && loggedInUserIsBanker ) {
               manageDeed.append( $('<div>').attr({id:'buildingControl-banker-help', class:'form-text'}).text( 'NB: this action will be preformed by the banker role.' ) );
            }



         }



         $('#modalManageDeedSet-tab-deed-card').empty().append( __generateDeedCardDiv('full', deedData) );
         $('#modalManageDeedSet-tab-deed-details').empty().append(
            $('<h3>').text('Manage title deed'),
            $('<hr>'),
            manageDeed,
         );

   });

}