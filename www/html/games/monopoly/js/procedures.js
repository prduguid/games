function __callProcedureNewGame() {
   __callProcedure( tagTag("__callProcedureNewGame()"), { "sqlcmd" : "call procedure monopoly_newGame(0);" }, 'failed to create a new game');
};

function __callProcedureSetGameState(gameId, state) {
   // states
   // 0        inactive
   // 1        active
   // 2        blocked (manual intervention required)
   // 3        complete
   //
   // legal state changes 
   // activate    0 -> 1      (only if 2+ players have been added)
   // deactivate  1 -> 0
   // complete    x -> 3
   // block       1 -> 2
   // resume      2 -> 1

   var tag   = tagTag("__callProcedureSetGameState()");
   var validStateActions = ["activate", "deactivate", "complete", "block", "resume"];
   console.log(tag, "gameId: " + gameId + ", state: '"+state+"'");
   if( validStateActions.indexOf( state ) > -1 ) {
      $.ajax({
         type: "POST",
         contentType: "application/json; charset=utf-8",
         headers: {'Authorization': getAuthHash()}, 
         url: "/omnibus/objectserver/restapi/sql/factory",
         dataType: 'json',
         data: JSON.stringify( { "sqlcmd" : "call procedure monopoly_setGameState("+gameId+",'"+state+"');" } )
       })
      .fail(ajaxQueryFailHandler, tag)
      .done(function(){
         if( state="activate" ){
            sessionStorage.setItem("monopolyGameId", gameId);
         }
      })
      .always(function(){ doUpdate(); });

   } else {
      alert(tag + "\nInvalid state ('"+state+"'), try one of '"+ validStateActions.join("', '") + "'.");
   };
};

function __callProcedureAddPlayer( gameId, userId, avatar) {
   var tag   = tagTag("__callProcedureAddPlayer()");
   console.log(tag, "gameId: " + gameId + ", userId: " + userId, ", avatar: '" + avatar + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_newPlayer("+gameId+", "+userId+",'"+avatar+"');" }, 'Failed to add a new player to the game');
};

function __callProcedureTransferDeedToPlayer( gameId, buyerId, deed, cash) {
   var tag   = tagTag("__callProcedureTransferDeedToPlayer()");
   console.log(tag, "gameId: " + gameId + ", buyerId: " + buyerId, ", deed: '" + deed + "', cash: £" + cash);
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_sellDeedToPlayer("+gameId+","+buyerId+",'"+deed+"',"+cash+");" }, "Failed to transfer the '"+ deed +" deed");
};

function __callProcedures_transferDeedToPlayer_resumeGame( gameId, buyerId, deed, cash) {
   var tag   = tagTag("__callProcedures_transferDeedToPlayer_resumeGame()");
   console.log(tag, "gameId: " + gameId + ", buyerId: " + buyerId, ", deed: '" + deed + "', cash: £" + cash);

   var sqlDataList = [
      { "sqlcmd" : "call procedure monopoly_sellDeedToPlayer("+gameId+","+buyerId+",'"+deed+"',"+cash+");" },
      { "sqlcmd" : "call procedure monopoly_setGameState("+gameId+",'resume');" }
   ]
   __callProcedures( tag, sqlDataList, "Failed to transfer the '"+ deed +" deed");
};

function __callProcedureTransferGoojfc(gameId,buyerId,set,cash) {
   var tag   = tagTag("__callProcedureTransferGoojfc()");
   console.log(tag, "gameId: " + gameId + ", buyerId: " + buyerId, ", set: '"+set+"', cash: £" + cash);
   var validSets = ['chance', 'communityChest'];
   if( validSets.indexOf( set ) > -1 ) {
      __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_tradeGOOJFC("+gameId+","+buyerId+",'"+set+"',"+cash+");" }, 'Failed to transfer the "+set+" Get out of Jail Free Card');
   } else {
      alert(tag + "\nInvalid Get Out of Jail Free set ('"+set+"'), try one of '"+ validSets.join("', '") + "'.");
   }
};
function __callProcedureGoojfcPickup( gameId, playerId, set ) {
   var tag   = tagTag("__callProcedureGoojfcPickup()");
   console.log(tag, "gameId: " + gameId + ", playerId: " + playerId, ", set: '"+set +"'");
   var validSets = ['chance', 'communityChest'];
   if( validSets.indexOf( set ) > -1 ) {
      __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_tradeGOOJFC("+gameId+","+playerId+",'"+set+"',0);" }, 'Failed to transfer the '+set+' Get out of Jail Free Card');
   } else {
      alert(tag + "\nInvalid Get Out of Jail Free set ('"+set+"'), try one of '"+ validSets.join("', '") + "'.");
   }
};



function __callProcedureManageOwnedDeedState(gameId, deed, action) {
   var tag   = tagTag("__callProcedureManageOwnedDeedState()");
   var validActions = ["buy", "sell", "liquidate", "mortgage", "unmortgage", "auction"];
   console.log(tag, "gameId: " + gameId + ", deed: '" + deed, "', action: '" + action + "'");
   if( validActions.indexOf( action ) > -1 ) {
      __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_deedManageState("+gameId+",'"+deed+"','"+action+"');" }, "Failed to preform action, '" + action + "', on deed '"+deed+"'.");
   } else {
      alert(tag + "\nInvalid action ('"+action+"'), try one of '"+ validActions.join("', '") + "'.");
   }
};
function __callProcedureLiquidateOwnedDeedSet(gameId,set){
   var tag   = tagTag("__callProcedureLiquidateOwnedDeedSet()");
   console.log(tag, "gameId: " + gameId + ", set: '" + set, "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_deedSetLiquidate("+gameId+",'"+set+"');" }, "Failed to liquidate the buildings on the "+set+" colour group.");
}

/**
 * 
 * @param {*} gameId    Integer the id of the game
 * @param {*} userId    Integer the userId of the buyer
 * @param {*} action  
 *                action         | conditions   | outcome
 *                sentence       | jailTime = 0 | sent to jail, position = -1
 *                chance         | jailTime > 0 | released, chance goojf card returned, position = 10
 *                communityChest | jailTime > 0 | released, community chest goojf card returned, position = 10
 *                doubledice     | jailTime > 0 | released, position = 10
 *                fine           | jailTime > 0 | released, fine debited from ballance, position = 10
 *                turn           | jailTime > 1 | jailTime--
 *                "              | jailTime = 1 | jailTime--, fine debited from ballance, position = 10
 */
function __callProcedurePlayerSetJailTime( gameId, userId, action ) {
   var tag   = tagTag("__callProcedurePlayerSetJailTime()");
   var validActions = ["sentence", "chance", "communityChest", "doubledice", "fine", "turn" ];
   console.log(tag, "gameId: " + gameId + ", userId: " + userId, ", action: '" + action + "'");
   if( validActions.indexOf( action ) > -1 ) {
      __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_player_jailTime("+gameId+","+userId+",'"+action+"');" }, "Failed to set the players jailTime");
   } else {
      alert(tag + "\nInvalid action ('"+action+"'), try one of '"+ validActions.join("', '") + "'.");
   }
};

/**
 * 
 * @param {*} gameId    Integer the id of the game
 * @param {*} userId    Integer the userId of the buyer
 * @param {*} avatar   
 * @param {*} doRefresh 
 */
function __callProcedurePlayerSetAvatar( gameId, userId, avatar) {
   var tag   = tagTag("__callProcedurePlayerSetAvatar()");
   console.log(tag, "gameId: " + gameId + ", userId: " + userId, ", avatar: '" + avatar + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_player_setAvatar("+gameId+","+userId+",'"+avatar+"');" }, "Failed to set the players avatar");
};

/**
 * 
 * @param {*} gameId    Integer the id of the game
 * @param {*} userId    Integer the userId of the player to be set as the banker for the game
 * @param {*} doRefresh 
 */
 function __callProcedureSetBanker( gameId, userId ) {
   var tag   = tagTag("__callProcedureSetBanker()");
   console.log(tag, "gameId: " + gameId + ", userId: " + userId, "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_setBanker("+gameId+","+userId+");" }, "Failed to set the players avatar");
};

/**
 * 
 * @param {*} gameId    Integer the id of the game
 * @param {*} userId   Integer the userId of the buyer
 */
function __callProcedureRemovePlayer( gameId, userId) {
   var tag   = tagTag("__callProcedureRemovePlayer()");
   console.log(tag, "gameId: " + gameId + ", userId: " + userId, "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_removePlayer("+gameId+","+userId+");" }, "Failed to remove a player from a game.");
};

/**
 * 
 * @param {*} gameId    Integer the id of the game
 * @param {*} userId   Integer the userId of the buyer
 */
function __callProcedurePlayerMove( gameId, userId, dice0, dice1 ) {
   var tag   = tagTag("__callProcedurePlayerMove()");
   console.log(tag, "gameId: " + gameId + ", userId: " + userId + ", dice: " + dice0 + ", " + dice1 + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_playerMove("+gameId+","+userId+","+dice0+","+dice1+");" }, "Failed to move the player.");
};


function __callProcedureCreditPlayer( gameId, userId, amount ) {
   var tag   = tagTag("__callProcedureCreditPlayer()");
   console.log(tag, "gameId: " + gameId + ", userId: " + userId + ", amount: " + amount + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_creditToPlayer("+gameId+","+userId+","+amount+");" }, "Failed to fine the player.");
};


function _drawCard(gameId){
   var tag   = tagTag("_drawCard()");
   console.log(tag, "gameId: " + gameId + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_drawCard("+gameId+");" }, "Failed to draw a card.");
}
function _enactCard(gameId,alternativeAction){
   var tag   = tagTag("_enactCard()");
   console.log(tag, "gameId: " + gameId + "', alternativeAction: " + alternativeAction);
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_enactCard("+gameId+", "+alternativeAction+");" }, "Failed to enact on the card.");
}
function _cardAdvanceOrRetreat(gameId){
   var tag   = tagTag("_cardAdvanceOrRetreat()");
   console.log(tag, "gameId: " + gameId + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_playerAdvanceOrRetreat("+gameId+");" }, "Failed to Advance/Retreat the player.");
}

function __callProcedureDemandRent(ServerName, ServerSerial) {
   var tag   = tagTag("__callProcedureDemandRent()");
   console.log(tag, "ServerName: " + ServerName + "', ServerSerial: " + ServerSerial);
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_demandRent('"+ServerName+"', "+ServerSerial+");" }, "Failed to register the rent demand.");
};
function __callProcedurePayRent(ServerName, ServerSerial) {
   var tag   = tagTag("__callProcedurePayRent()");
   console.log(tag, "ServerName: " + ServerName + "', ServerSerial: " + ServerSerial);
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_payRent('"+ServerName+"', "+ServerSerial+");" }, "Failed to submit the rent payment request.");
};

function __callProcedurePayDemand(ServerName, ServerSerial) {
   var tag   = tagTag("__callProcedurePayDemand()");
   console.log(tag, "ServerName: " + ServerName + "', ServerSerial: " + ServerSerial);
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_payDemand('"+ServerName+"', "+ServerSerial+");" }, "Failed to submit the Demand payment request.");
};



function _auctionDeedStart(gameId){
   var tag   = tagTag("_auctionDeedStart()");
   console.log(tag, "gameId: " + gameId + "'");
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_auctionDeedStart("+gameId+");" }, "Failed to start the auction.");
}
function _auctionDeedBid(gameId, userId, bid){
   var tag   = tagTag("_auctionDeedBid()");
   console.log(tag, "gameId=" + gameId + "', userId="+userId+", bid="+bid);
   __callProcedure( tag, { "sqlcmd" : "call procedure monopoly_auctionDeedBid("+gameId+", "+userId+", "+bid+");" }, "Failed to submit a bid to the auction.");
}