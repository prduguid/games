
var monopoly = {
   settings: {
      updatePollerEnabled : true,
      doTabRefresh : true,
      doToastPoll  : true,
   }, 
   cache: { cash:[], stock:[], avatar:[], card:[] },  
   mappings: {
      severity:[
         {"omnibus":0, "omnibus-conversion": "clear",         "bs-table-class": "table-success", "bs-icon-classes": "bi bi-square text-success", },
         {"omnibus":1, "omnibus-conversion": "indeterminate", "bs-table-class": "",              "bs-icon-classes": "bi bi-square", },
         {"omnibus":2, "omnibus-conversion": "warning",       "bs-table-class": "table-info",    "bs-icon-classes": "bi bi-info-square text-primary", },
         {"omnibus":3, "omnibus-conversion": "minor",         "bs-table-class": "table-warning", "bs-icon-classes": "bi bi-exclamation-square text-warning", },
         {"omnibus":4, "omnibus-conversion": "major",         "bs-table-class": "table-danger",  "bs-icon-classes": "bi bi-exclamation-square-fill text-warning", },
         {"omnibus":5, "omnibus-conversion": "critical",      "bs-table-class": "table-dark",    "bs-icon-classes": "bi bi-exclamation-triangle-fill text-danger", },
      ],
      weekday: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
      month: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
   },
   ui: { 
      tabs: {
         game:                      { name: 'game',                     id: "tabs-game-tab",             selector: "#tabs-game-tab",            refreshable: true,   conditional: false,  tabId:"tabs-game"    },
         bank:                      { name: 'bank',                     id: "tabs-bank-tab",             selector: "#tabs-bank-tab",            refreshable: true,   conditional: true,   tabId:"tabs-bank"    },
         players:                   { name: 'players',                  id: "tabs-players-tab",          selector: "#tabs-players-tab",         refreshable: true,   conditional: true,   tabId:"tabs-players" },
         board:                     { name: 'board',                    id: "tabs-board-tab",            selector: "#tabs-board-tab",           refreshable: true,   conditional: true,   tabId:"tabs-board"   },
         journal:                   { name: 'journal',                  id: "tabs-journal-tab",          selector: "#tabs-journal-tab",         refreshable: true,   conditional: true,   tabId:"tabs-journal" }
      },
      modals:{
         modalAddUserToGame:        { name: 'modalAddUserToGame',       id: 'modalAddUserToGame',        selector: '#modalAddUserToGame',       refreshable: true,   conditional: false   },
         modalManageDeedSet:        { name: 'modalManageDeedSet',       id: 'modalManageDeedSet',        selector: '#modalManageDeedSet',       refreshable: true,   conditional: true    },
         modalPurchaseAuctionDeed:  { name: 'modalPurchaseAuctionDeed', id: 'modalPurchaseAuctionDeed',  selector: '#modalPurchaseAuctionDeed', refreshable: true,   conditional: true    },
         modalPlayerIncarcerated:   { name: 'modalPlayerIncarcerated',  id: 'modalPlayerIncarcerated',   selector: '#modalPlayerIncarcerated',  refreshable: false,  conditional: true    },
      },
      goojfc:{
         title: "Get out of jail free. This card may be kept until needed or sold",
         spec: [
            { "key": "cardsChanceGoojfOwner", "title": "Chance",          "class": "chance",         "icon": "bi bi-question" },
            { "key": "cardsCChestGoojfOwner", "title": "Community Chest", "class": "communityChest", "icon": "bi bi-archive"  }
         ]
      },
      spinner: $('<div>').attr({class:'spinner-border', role:'status'}).append($('<span>').addClass('visually-hidden').text('Loading ...')),
      iconClasses:{
         communityChest:   'bi bi-archive-fill',
         chance:           'bi bi-question-diamond-fill',
         minusCircle:      'bi bi-dash-circle-fill',
         plusCircle:       'bi bi-plus-circle-fill',
         personAdd:        'bi bi-person-fill-add',
         person:           'bi bi-person-fill',
         currency:         'bi bi-currency-pound'
      }
   },
   states:{
      game : { 0 : 'inactive',     1 : 'active',  2 : 'blocked',  3 : 'completed' },
      deed : {
         true  : { '-1' : 'mortgaged', 0 : 'un-developed', 1 : '1 house', 2 : '2 houses', 3 : '3 houses', 4 : '4 houses', 5 : '1 hotel' },
         false : { '-1' : 'mortgaged', 0 : 'unmortgaged'  }
      }
   },
   maxPlayers: 6,
   updatePoller: {
      enabled: true,
      lastTouch: 0,
      pollsSinceUpdate : 0,
      intervals: [ 2011 /* prime number near 2000 */ ],
      toastEnabled: true,
      toastLastSerial: 0, /* @ProbeSubSecondId */
      toastLimit: 5,
      toastMaxAge: 600,
   }
};

const reload_monopoly_bs_modal = new Event('reload.monopoly.bs.modal');
const reload_monopoly_bs_tab   = new Event('reload.monopoly.bs.tab');

$(document).ready(function() {
   var tag = tagTag('$(document-ready(){});');
   console.debug(tag, "");

   // are we logged in?
   if( getAuthHash() ){

      // configure the action of the #navbar-settings-button & #navbar-logout-button buttons
      $("button#navbar-settings-button").click( console.info("button#navbar-settings-button button clicked.") );
      $("button#navbar-logout-button"  ).click(function() {
         // ensure that session values have been removed
         console.info("logout button clicked.");
         ["monopolyGameId"].forEach( item => { sessionStorage.removeItem( item ) });
         doLogout();
      });
      // initially set the logged user id, as taken from sessionStorage 
      $("button#navbar-username-indicator-button").text( getAuthUserName() );

      // cache static/inventory data 
      // then activate the games tab
      $.when( ajaxQueryByFilter(schema.inventory,"",""), ajaxQueryByFilter(schema.monopoly_users,"",""), ajaxQueryByFilter(schema.board,"",""), ajaxQueryByFilter(schema.inventory_deeds,"","") ).then( function(inventoryResponse, userResponse, boardResponse, deedsInventoryResponse ){
         console.groupCollapsed(tag, "data collected ...");

         // inventory
         console.debug(tag, '[inventoryResponse       ]', inventoryResponse[1],   inventoryResponse   );
         Object.keys(monopoly.cache).forEach( cacheType => {
            if( inventoryResponse[0].rowset.rows.filter(r=>r.type==cacheType).length > 0 ){
               monopoly.cache[cacheType] = inventoryResponse[0].rowset.rows.filter(r=>r.type==cacheType).map(r=> JSON.parse(r.json))
            }
         })

         // users
         console.debug(tag, '[userResponse            ]', userResponse[1], userResponse );
         monopoly.cache.users = userResponse[0].rowset.rows;
         // not all users will have a FullName set, in which case attempt to provide a substitute
         monopoly.cache.users.forEach( u => {
            if( u.FullName.length == 0 ) {
               if( u.UserName.length > 0 ) {
                  u.FullName = u.UserName;
               } else {
                  u.FullName = u.uid;
               }
            }
         })


         // board
         console.debug(tag, '[boardResponse           ]', boardResponse[1], boardResponse );
         monopoly.cache.board = boardResponse[0].rowset.rows;

         // deeds
         console.debug(tag, '[deedsInventoryResponse  ]', deedsInventoryResponse[1],  deedsInventoryResponse  );
         monopoly.cache.deedsInventory  = deedsInventoryResponse[0].rowset.rows;

         // populate an ordered setInventory by processing deedsInventory
         monopoly.cache.setInventory = [];
         monopoly.cache.deedsInventory.sort((a,b) => a.setOrder-b.setOrder ).forEach(deedOuter=>{
            if( monopoly.cache.setInventory.filter(set=>set.set==deedOuter.sets).length == 0 ) {
               var deedsInSet = monopoly.cache.deedsInventory.filter(deedInner=>deedInner.sets==deedOuter.sets).sort((a,b) => a.boardPlace-b.boardPlace);
               monopoly.cache.setInventory.push(
                  {
                     set:              deedOuter.sets,
                     name:             ucfirst(deedOuter.sets),
                     setOrder:         deedsInSet[0].setOrder,
                     colourSet:        deedsInSet[0].colourSet,
                     htmlColour:       deedsInSet[0].htmlColour,
                     cost_building:    deedsInSet[0].cost_building,
                     refund_building:  deedsInSet[0].refund_building,
                     deeds:            deedsInSet
                  }
               );
            }
         })

         console.debug(tag, '[monopoly.cache          ]', monopoly.cache );
         console.groupEnd();

         // update the the logged user id with full username
         $("button#navbar-username-indicator-button").text( getFullNameForUserID( getAuthUserName() ));

         // init the settings switch controls

         $('#navbar-settings--updatePoller-enabled').attr({checked: monopoly.updatePoller.enabled }).click(function(event){ 
            console.debug("click on '#navbar-settings--updatePoller-enabled', updatedCheckedState: "+event.target.checked);
            monopoly.updatePoller.enabled = event.target.checked;
            $('#navbar-settings--updatePoller-toastEnabled').attr({disabled: !monopoly.updatePoller.enabled});
         });


         $('#navbar-settings--updatePoller-toastEnabled').attr({checked: monopoly.updatePoller.toastEnabled, disabled: !monopoly.updatePoller.enabled }).click(function(event){ 
            console.debug("click on '#navbar-settings--updatePoller-toastEnabled', updatedCheckedState: "+event.target.checked);
            monopoly.updatePoller.toastEnabled = event.target.checked;
         });

         initModals();
         initTabs();

         monopoly.ui.tabs.game.tabObj.show();
         monopoly.ui.tabs.game.object.dispatchEvent(reload_monopoly_bs_tab);
         updatePoller();
         // set an interval timer for the update Poller
         monopoly.updatePoller.timer = window.setInterval( updatePoller, monopoly.updatePoller.intervals[0] );
      });
   };
});




function getCurrentTabName () { return $('#monopoly-tabs li button.active').attr('aria-controls'); }
function getCurrentModalId(){ return $('.modal.show').attr('id'); }

function getFullNameForUserID( UserID ){
   var users = monopoly.cache.users.filter( u => u.UserID == UserID );
   if( users.length == 1 ){
      return users[0].FullName
   }
   return UserID;
}

function updatePoller(){
   // the monopoly.games table has an integer column "touch".  Any updates should cause this number to  increment.  
   // this function polls that number and if it has incremented will trigger the UI to refresh
   var tag = tagTag("updatePoller()");
   var gameId = sessionStorage.getItem("monopolyGameId");
   if( monopoly.updatePoller.enabled && gameId > 0 ){
      $.when( ajaxQueryByKey(schema.touch, gameId ) ).then( function( touchResponse ){
         if( touchResponse.rowset.rows[0].touch > monopoly.updatePoller.lastTouch ){
            console.debug(tag,"touch has been incremented (last : " + monopoly.updatePoller.lastTouch +", updated: " + touchResponse.rowset.rows[0].touch + " (gameId: " + gameId + ")." );
            monopoly.updatePoller.lastTouch        =  touchResponse.rowset.rows[0].touch;
            monopoly.updatePoller.pollsSinceUpdate =  0;

            // trigger updates to either the current modal (where applicable) or tab
            doUpdate();

            // check for toast if enabled
            if(monopoly.updatePoller.toastEnabled){
               var filterString = 'Class in (1500, 1501) and SuppressEscl in (1) and Grade in (' + gameId + ') and LastOccurrence > getdate() - '+ monopoly.updatePoller.toastMaxAge +' and ProbeSubSecondId > ' + monopoly.updatePoller.toastLastSerial;
               // fetch data and populate table
               // ProbeSubSecondId contains an increasing integer value per event
               $.when( ajaxQueryByFilter(schema.toast, filterString, "ProbeSubSecondId") ).then( function( statusResponse ){
                  if( statusResponse.rowset.affectedRows > 0 ) {
                     console.debug(tag, 'polled for toast-able updates, found '+statusResponse.rowset.affectedRows+', rows: ', statusResponse.rowset.rows );
                     statusResponse.rowset.rows.map(row=>{
                        if( row.ProbeSubSecondId > monopoly.updatePoller.toastLastSerial ){
                           monopoly.updatePoller.toastLastSerial = row.ProbeSubSecondId
                        };
                        // butter the slice
                        var slice = { severity: row.Severity, epoch: row.LastOccurrence, autoHide: true, body: row.TEXT02, header: row.TEXT01, component: row.AlertGroup };
                        // add jam / dice
                        var dicePattern = /^(Dice thrown\: )(\d) and (\d)(.*)$/; //       set toastHeader = 'Dice thrown: ' + to_char(dice0) + ' and ' + to_char(dice1);

                        if( slice.AlertGroup = "monopoly_playerMove" && dicePattern.test(slice.header) ){
                           var p = dicePattern.exec(slice.header);
                           slice.header  = $('<span>').append( p[1], $('<i>').addClass("bi bi-dice-"+parseInt(p[2])), " ", $('<i>').addClass("bi bi-dice-"+parseInt(p[3])), p[4] );
                        }

                        doToast( slice );
                     })
                  }
               });
            }
         } else {
            monopoly.updatePoller.pollsSinceUpdate++;
            // TODO implement a back off algorithm here
         }
      });
   }
}

function doUpdate(){
   var tag = tagTag("doUpdate()");
   // 1 re-evaluate the conditional tabs and/or modals, conditional=true requires there to be a gameId set
   if( sessionStorage.getItem("monopolyGameId") == null) {
      $("button#navbar-gameId-indicator-button").text("-");

      // close any conditional modal that may be open if there is no gameId set
      Object.values(monopoly.ui.modals).filter(modal=>modal.conditional).filter(modal=>modal.object.getAttribute('class').indexOf('show')>=0).forEach(modal=>{
         console.warn(tag,"monopolyGameId not set; closing conditional modal '"+modal.name+"'");
         bootstrap.Modal.getOrCreateInstance( modal.object ).hide();
      });
      // disable any conditional tab that may be enabled if there is no gameId set
      Object.values(monopoly.ui.tabs).filter(tab=>tab.conditional).filter(tab=>tab.object.getAttribute('class').indexOf('disabled')==-1).forEach(tab=>{
         console.warn(tag,"monopolyGameId not set; disabling conditional tab '"+tab.name+"'");
         $( tab.object ).addClass("disabled").attr("aria-disabled", true);
      })
   } else {
      $("button#navbar-gameId-indicator-button").text(sessionStorage.getItem("monopolyGameId"));
      // ensure that conditional tabs are enabled
      Object.values(monopoly.ui.tabs).filter(tab=>tab.conditional).forEach(tab=>{
         $( tab.object ).removeClass("disabled").attr("aria-disabled", false);
      })
   };

   // is there a modal active, in which case should the modal be reload instead?
   var potentialModalId = getCurrentModalId();
   console.debug(tag,"potentialModalId: '"+potentialModalId+"'");
   if( potentialModalId!=null && monopoly.ui.modals[potentialModalId] ){
      if( monopoly.ui.modals[potentialModalId].refreshable ){
         // reload modal
         console.warn(tag,"reloading refreshable modal '"+monopoly.ui.modals[potentialModalId].name+"'");
         monopoly.ui.modals[potentialModalId].object.dispatchEvent(reload_monopoly_bs_modal);
      }
   } else {
      Object.values(monopoly.ui.tabs).filter(tab=> tab.tabId == getCurrentTabName()).forEach(tab=>{
         console.debug(tag,"dispatching event 'reload_monopoly_bs_tab' to tab '"+tab.name+"'");
         tab.object.dispatchEvent(reload_monopoly_bs_tab);
      })
   }

   // finally is there any slices of toast to ..
   unRackToast();
}



function _getCardFromGameData( gameData ){
   var tag   = tagTag("_getCardFromGameData()");
   var cardId;
   console.log(tag, "gameData: ", gameData );
   switch(gameData.action){
      case 'chance':
         if( gameData.moves == gameData.cardsChance4Move) {
            var cardId = gameData.cardsChance.substr( 4*gameData.cardsChancePointer, 4 );
         } else {
            console.warn(tag, "Chance card not drawn yet.");
         }
         break;
      case 'communityChest':
         if( gameData.moves == gameData.cardsCChest4Move) {
            var cardId = gameData.cardsCChest.substr( 4*gameData.cardsCChestPointer, 4 );
         } else {
            console.warn(tag, "Community Chest card not drawn yet.");
         }
         break;
      default:
         console.error(tag, "Invalid cardSet ('"+gameData.action+"'), try 'chance' or 'communityChest'.");
   }
   console.debug(tag, "cardId: ", cardId );
   var card = monopoly.cache.card.filter( card => card.cardId == cardId );
   console.debug(tag, "card: ", card[0] );
   return card[0];
}



function enrichDeedSetData( deedData, gameData ){
   var tag = "[enrichDeedSetData()]";
   console.groupCollapsed(tag);
   console.debug(tag, "deedData", deedData);
   console.debug(tag, "gameData", gameData);

   // across all deeds, calculate the number of buildings built
   monopoly.cache.stock.forEach( stock => {
      // default
      stock.built = 0;
      // split the stateList string into an array of integers (if not all ready done)
      if( ! Array.isArray( stock.stateList) ){
         stock.stateList = stock.stateList.split(',').map( s => parseInt(s) );
      }
      // count the houses/hotels on each deed (state 1= one house, 2= two houses ... 5 = one hotel)
      deedData.forEach( deed => {
         if( stock.stateList.indexOf( deed.state ) > -1 ){
            stock.built = stock.built + stock.stateList.indexOf( deed.state ) + 1;
         }
      });
      // calc the remaining stock levels
      stock.remaining = stock.quantity - stock.built;
      console.debug(tag,"updated monopoly.cache.stock - " + stock.item + ": " + stock.built + " / " + stock.quantity + " built (remaining: " + stock.remaining + ").");
   })

   deedData.forEach( deed => {
      console.groupCollapsed(tag,"deed: '" + deed.title + "' (set: '"+deed.sets+"').", deed );
      deed.checks = {};

      // pull in the deeds_inventory data (monopoly.cache.deedsInventory)
      // title, sets, htmlColour, purchase, mortgage, rent_0, rent_1, rent_2, rent_3, rent_4, rent_5, cost_building
      // updated
      //       title, sets, colourSet, setOrder, boardPlace, htmlColour, icon, purchase, mortgage, unmortgage, rent_0, rent_1, rent_2, rent_3, rent_4, rent_5, cost_building, refund_building
      deed.inventory = monopoly.cache.deedsInventory.filter( di => di.title == deed.title)[0];
      console.debug( tag, "filtered inventory data: ", deed.inventory);

      // pull in the board data to get the place/position (monopoly.cache.board)
      // place, title, rule, sets
      deed.board = monopoly.cache.board.filter( b => b.title == deed.title)[0];
      console.debug( tag, "filtered board data: ", deed.board);

      deed.set = {
         "owners"          : [], 
         "states"          : [], 
         "setBuildings"    : { "house": 0, "hotel" : 0, "cost": 0 },
         "setDeeds"        : 0
      };

      deedData.filter( otherDeed => otherDeed.sets == deed.sets ).forEach( otherDeed => {
         // get unique values across the set
         if( deed.set.owners.indexOf(otherDeed.owner) == -1 ) deed.set.owners.push( otherDeed.owner );
         if( deed.set.states.indexOf(otherDeed.state) == -1 ) deed.set.states.push( otherDeed.state );
         if( deed.inventory.colourSet ){

            switch( otherDeed.state ){
               case 5: deed.set.setBuildings.hotel++;
               break;
               case 4: deed.set.setBuildings.house++;
               case 3: deed.set.setBuildings.house++;
               case 2: deed.set.setBuildings.house++;
               case 1: deed.set.setBuildings.house++;
               break;
            };

            // total cost of buildings on this set
            if( otherDeed.state > 0 ){
               deed.set.setBuildings.cost = deed.set.setBuildings.cost + deed.inventory.cost_building*deed.state;
            }

         }
         deed.set.setDeeds++;
      });

      console.debug(tag,"collected data across this set: ", deed.set);

      deed.checks.uniqueOwnership   = ( deed.set.owners.length == 1 && deed.set.owners[0] > -1 );
      deed.checks.noMortgages       = Math.min( ...deed.set.states) > -1;
      deed.checks.owned             = deed.owner > -1;
      if( deed.checks.owned ){
         deed.ownerUsername = getFullNameForUserID(deed.owner)
      }

      // default values
      deed.checks.propertySpread    = deed.checks.owned;
      deed.checks.canBuyBuilding    = false;
      deed.checks.canSellBuilding   = false;
         // a mortgaged deed can be sold, however can't sell deed if there are buildings
      deed.checks.canSell           = deed.checks.owned && deed.state <= 0
      deed.checks.isMortgaged       = deed.state==-1;
         // can only mortgage if owned and there are no buildings
      deed.checks.canMortgage       = deed.checks.owned && deed.state == 0;
      deed.checks.deedsAlsoOwned    = deed.checks.owned ? deedData.filter( d => d.sets == deed.sets && d.owner == deed.owner ).length : 0;

      if( deed.sets == "stations"  ) {
         deed.checks.rent              = deed.checks.owned  ? deed.inventory["rent_"+( deed.checks.deedsAlsoOwned -1 ) ] : 0;

      } else if( deed.sets == "utilities" ) {
         deed.checks.rentFactor        = deed.checks.owned 
            ? deed.checks.uniqueOwnership 
               ? 10 
               : 4 
            : 0;
         // multiply the dice (activeMoveDice0 + activeMoveDice1) by the rent 

         if( gameData.state > 0 && deed.checks.owned && gameData.activeMoveDice0 > 0 && gameData.activeMoveDice1 > 0 ) {
            deed.checks.rent              = deed.checks.rentFactor * ( gameData.activeMoveDice0 + gameData.activeMoveDice1 );
         } else {
            deed.checks.rent              = 0;
         }

      } else {
         deed.checks.propertySpread    =  deed.checks.owned && Math.min( ...deed.set.states) >= 0 && ( Math.max( ...deed.set.states) - Math.min( ...deed.set.states) <= 1 );
         deed.checks.canBuyBuilding    =  deed.checks.owned && deed.checks.uniqueOwnership && deed.checks.noMortgages && deed.checks.propertySpread && ( Math.min( ...deed.set.states) == deed.state ) && deed.state < 5;
         deed.checks.canSellBuilding   =  deed.checks.owned && deed.checks.uniqueOwnership && deed.checks.noMortgages && deed.checks.propertySpread && ( Math.max( ...deed.set.states) == deed.state ) && deed.state > 0;

         deed.checks.buildingToBuy     =  deed.state <= 4 && monopoly.cache.stock.filter(i=>i.item=='house')[0].remaining >= 1 || deed.state > 4 && monopoly.cache.stock.filter(i=>i.item=='hotel')[0].remaining >= 1;
         // can't downgrade a hotel if there are no houses to substitute 
         deed.checks.buildingToSell    =  deed.state <= 4 || deed.state > 4 && monopoly.cache.stock.filter(i=>i.item=='house')[0].remaining >= 4;

         deed.checks.rent              = deed.checks.owned ? deed.inventory["rent_"+deed.state ] : 0;
         // if all of the set is owned, then the base rent is doubled
         if( deed.state == 0 && deed.checks.uniqueOwnership == true ){
            deed.checks.rent = deed.checks.rent + deed.checks.rent;
         }
      }


      console.log(tag, "housing stock           : ", monopoly.cache.stock.map(si=> si.item + 's: ' + si.remaining + '/' + si.quantity).join(', '), " (remaining/quantity)");
      console.log(tag, "board position/place    : ", deed.board.place);                   // the place on the board (go=0)
      console.log(tag, "set uniqueOwnership     : ", deed.checks.uniqueOwnership );       // are all deeds in this set owned by one person
      console.log(tag, "set noMortgages         : ", deed.checks.noMortgages );           // are all deeds in this set mortgage free
      console.log(tag, "set propertySpread      : ", deed.checks.propertySpread );        // are the buildings built evenly across this set
      console.log(tag, "set setBuildings.house  : ", deed.set.setBuildings.house );       // total number of houses built on this set
      console.log(tag, "set setBuildings.hotel  : ", deed.set.setBuildings.hotel );       // total number of hotels built on this set
      console.log(tag, "set setBuildings.cost   : ", deed.set.setBuildings.cost );        // total purchase cost of all buildings currently built on this set
      
      console.log(tag, "set building cost       : ", deed.inventory.cost_building);       // the purchase cost of a single house or hotel on this set
      console.log(tag, "set building refund     : ", deed.inventory.refund_building);     // the refund value of a single house or hotel on this set (when returned to the bank)
      
      console.log(tag, "set setDeeds            : ", deed.set.setDeeds);                  // the number of deeds in this set
      console.log(tag, "title owned             : ", deed.checks.owned );                 // is this title deed owned
      console.log(tag, "title canSell           : ", deed.checks.canSell );               // can this deed be sold
      console.log(tag, "title isMortgaged       : ", deed.checks.isMortgaged );           // is this deed mortgaged
      console.log(tag, "title canMortgage       : ", deed.checks.canMortgage );           // can this deed be mortgaged
      console.log(tag, "title deedsAlsoOwned    : ", deed.checks.deedsAlsoOwned );        // how many deeds of this set are owned by the same player
      console.log(tag, "title rent              : ", deed.checks.rent );                  // the current rent if another player lands on this deed
      console.log(tag, "title rentFactor        : ", deed.checks.rentFactor );            // the current rent factor if another player lands on this deed (for utilities)

      console.log(tag, "title canBuyBuilding    : ", deed.checks.canBuyBuilding );        // can a building be built on this deed
      console.log(tag, "title canSellBuilding   : ", deed.checks.canSellBuilding );       // can a building be sold 
      console.log(tag, "title buildingToBuy     : ", deed.checks.buildingToBuy );         // for the next purchase on this deed, is there a building in stock
      console.log(tag, "title buildingToSell    : ", deed.checks.buildingToSell );        // if disposing of a hotel, on this deed, are there four houses to substitute in stock

      console.groupEnd();
   })
   console.debug(tag,"enriched deedData: ",deedData);
   console.groupEnd();
}
