var letters = [
   { "letter": "a", "class": "vowel", "distributionStd": "9", "distributionSuper": "16" },
   { "letter": "b", "class": "consonant", "distributionStd": "2", "distributionSuper": "4" },
   { "letter": "c", "class": "consonant", "distributionStd": "2", "distributionSuper": "6" },
   { "letter": "d", "class": "consonant", "distributionStd": "4", "distributionSuper": "8" },
   { "letter": "e", "class": "vowel", "distributionStd": "12", "distributionSuper": "24" },
   { "letter": "f", "class": "consonant", "distributionStd": "2", "distributionSuper": "4" },
   { "letter": "g", "class": "consonant", "distributionStd": "3", "distributionSuper": "5" },
   { "letter": "h", "class": "consonant", "distributionStd": "2", "distributionSuper": "5" },
   { "letter": "i", "class": "vowel", "distributionStd": "9", "distributionSuper": "13" },
   { "letter": "j", "class": "consonant", "distributionStd": "1", "distributionSuper": "2" },
   { "letter": "k", "class": "consonant", "distributionStd": "1", "distributionSuper": "2" },
   { "letter": "l", "class": "consonant", "distributionStd": "4", "distributionSuper": "7" },
   { "letter": "m", "class": "consonant", "distributionStd": "2", "distributionSuper": "6" },
   { "letter": "n", "class": "consonant", "distributionStd": "6", "distributionSuper": "13" },
   { "letter": "o", "class": "vowel", "distributionStd": "8", "distributionSuper": "15" },
   { "letter": "p", "class": "consonant", "distributionStd": "2", "distributionSuper": "4" },
   { "letter": "q", "class": "consonant", "distributionStd": "1", "distributionSuper": "2" },
   { "letter": "r", "class": "consonant", "distributionStd": "6", "distributionSuper": "13" },
   { "letter": "s", "class": "consonant", "distributionStd": "4", "distributionSuper": "10" },
   { "letter": "t", "class": "consonant", "distributionStd": "6", "distributionSuper": "15" },
   { "letter": "u", "class": "vowel", "distributionStd": "4", "distributionSuper": "7" },
   { "letter": "v", "class": "consonant", "distributionStd": "2", "distributionSuper": "3" },
   { "letter": "w", "class": "consonant", "distributionStd": "2", "distributionSuper": "4" },
   { "letter": "x", "class": "consonant", "distributionStd": "1", "distributionSuper": "2" },
   { "letter": "y", "class": "consonant", "distributionStd": "2", "distributionSuper": "4" },
   { "letter": "z", "class": "consonant", "distributionStd": "1", "distributionSuper": "2" }
];

function generateUnShuffledCardDecks() {
   var cards = {
      "vowel": [],
      "consonant": []
   }

   letters.forEach((letter) => {
      if (letter.class === "vowel") {
         for (var i = 0; i < letter.distributionSuper; i++) {
            cards.vowel.push(letter.letter)
         }
      } else {
         for (var i = 0; i < letter.distributionSuper; i++) {
            cards.consonant.push(letter.letter)
         }

      }
   })
   return cards;
}

var cardDecks = generateUnShuffledCardDecks();
console.debug("card deck", "vowels", cardDecks.vowel);
console.debug("card deck", "consonant", cardDecks.consonant);

function selectRandomCardFromGivenDeck(cardClass) {
   var randomCardIndex = Math.floor(Math.random() * cardDecks[cardClass].length);
   var letter = cardDecks[cardClass][randomCardIndex];
   cardDecks[cardClass].splice(randomCardIndex, 1);
   console.debug("[dealCard]", "cardClass:", cardClass, "randomCardIndex:", randomCardIndex, "letter:", letter, "remaining:", cardDecks[cardClass].length)
   return letter.toUpperCase();
}