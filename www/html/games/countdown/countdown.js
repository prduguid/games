function dragLetter(ev) {
   ev.dataTransfer.setData("text", ev.target.id);
}

function allowLetterDrop(ev) {
   ev.preventDefault();
}

function getLetterClass(classList) {
   if (classList.contains("vowel") && !classList.contains("consonant")) {
      return "vowel";
   } else if (classList.contains("consonant") && !classList.contains("vowel")) {
      return "consonant";
   }
   return "";
}

function dropLetter(ev) {
   ev.preventDefault();
   var draggedElementId = ev.dataTransfer.getData("text");
   var draggedElement = document.getElementById(draggedElementId);
   var draggedLetter = draggedElement.innerText;
   var draggedLetterClass = getLetterClass(draggedElement.classList);
   console.debug("[dropLetter]", "draggedElementId:", draggedElementId, "draggedLetter:", draggedLetter, "draggedLetterClass:", draggedLetterClass);

   var destinationElementId = ev.target.id;
   var destinationElement = document.getElementById(destinationElementId);
   var destinationLetter = destinationElement.innerText;
   var destinationLetterClass = getLetterClass(destinationElement.classList);
   console.debug("[dropLetter]", "destinationElementId:", destinationElementId, "destinationLetter:", destinationLetter, "destinationLetterClass:", destinationLetterClass);

   // blank the dragged  and destination squares
   draggedElement.innerText = "";
   draggedElement.classList.remove("vowel", "consonant")
   destinationElement.innerText = "";
   destinationElement.classList.remove("vowel", "consonant")

   if (draggedLetter != "") {
      destinationElement.innerText = draggedLetter;
      destinationElement.classList.add(draggedLetterClass)
   }
   if (destinationLetter != "") {
      draggedElement.innerText = destinationLetter;
      draggedElement.classList.add(destinationLetterClass)
   }

   // check to see if any of the tiles have been dragged to the bottom rack ...
   if( getBottomRackLetters().length > 0 ) {
      // ... in which case enable the check word button
      $("button#button_check").prop('disabled', false);
   }
}

function reset() {
   $("div.check").empty;
   $("div.rack div")
      .empty()
      .removeClass("vowel")
      .removeClass("consonant");
   $("button#button_vowel").prop('disabled', false);
   $("button#button_consonants").prop('disabled', false);
   $("button#button_reset").prop('disabled', true);
   $("button#button_check").prop('disabled', true);
   $("button#button_best").prop('disabled', true);

   $("#seconds").removeClass("animate");
   $("#countdownTheme")[0].load();

}

function selectLetter(letterClass) {
      // must contain at least 3 vowels and 4 consonants, i.e. max vowels=5 and max consonants=6

   if (letterClass === "vowel" || letterClass === "consonant") {
      $("div.rack div:not(.vowel):not(.consonant):first")
         .text(selectRandomCardFromGivenDeck(letterClass))
         .addClass(letterClass);

      var selectedConsonants = $("div.rack div.consonant").length;
      var selectedVowels = $("div.rack div.vowel").length;
      if (selectedVowels >= 5 || selectedVowels + selectedConsonants >= 9) {
         $("button#button_vowel").prop('disabled', true);
      }
      if (selectedConsonants >= 6 || selectedVowels + selectedConsonants >= 9) {
         $("button#button_consonants").prop('disabled', true);
      }

      // [re-]enable the reset button
      $("#button_reset").prop('disabled', false);
   } else {
      console.error("[selectLetter] letterClass was neither 'vowel' or 'consonant', was: ", letterClass);
   }
}


function getLettersFromRack( rackSelector ) {
   var letters = "";
   $(rackSelector).each( function(){
      letters = letters + $(this).text()
   })
   return letters;
}

function getTopRackLetters() {
   return getLettersFromRack("div.rack>div.top-rack");
}
function getBottomRackLetters() {
   return getLettersFromRack("div.rack>div.bottom-rack");
}
function getAllRackLetters() {
   return getLettersFromRack("div.rack>div");
}


$(document).ready(function() {

   // define an event to run after the clock has run
   $("#seconds").on("animationend", function() {
      console.log("times up");
      // set a 15 second timer to reset the clock
      setTimeout(function() {
         $("#seconds").removeClass("animate");
         $("#countdownTheme")[0].load();
      }, 15000);

      // enable drag-n-drop
      $("div.rack div").attr("draggable", true);
      // enable the best word button
      $("button#button_best").prop('disabled', false);

   })

   $("#clock").click(function() {
      if( getTopRackLetters().length == 9 ) {
         // if stopped/reset, play
         // if playing, pause
         // if paused, resume
         if ($("#seconds").hasClass("animate")) {
            if ($("#seconds").hasClass("pause")) {
               // is paused; unpause
               $("#seconds").removeClass("pause");
               $("#countdownTheme")[0].play();

            } else {
               // is playing; pause
               $("#seconds").addClass("pause");
               $("#countdownTheme")[0].pause();

            }
         } else {
            // stopped; play
            $("#seconds").addClass("animate");
            $("#countdownTheme")[0].play();
         }
      }
   })

   $("#clock").dblclick(function() {
      // if stopped/reset, do nothing
      // if playing, reset
      // if paused, reset
      if ($("#seconds").hasClass("animate")) {
         $("#seconds").removeClass("animate");
         $("#countdownTheme")[0].load();
      }
   });

   // Define button click actions

   $("button#button_consonants").click(function() { selectLetter("consonant") });
   $("button#button_vowel").click(function() { selectLetter("vowel") });
   $("button#button_reset").click(function(){ reset() });

   // api call to a scrabble dictionary to verify
   $("button#button_check").click(function(){ 
      var bottomRackLetters = getBottomRackLetters();
      console.debug("bottomRackLetters", bottomRackLetters, ", length:", bottomRackLetters.length);
      if( bottomRackLetters.length >= 2 ) {
         var apiUrl = "/cgi-bin/scrabble.bash?"+bottomRackLetters.toLowerCase();
         console.debug("apiUrl", apiUrl);
         $.get( apiUrl )
            .done(function( data ) {
               console.debug("returned data: ", data);
               $("div.check").html( data );
               if( $("div.check a").text().length > 0 ) {
                  $("div.check a").parent().html("<span class=\"see\">valid, see " + $("div.check a").text() + "<span>")
               }
            });
      }

   });

   // Opens a new page with best word suggestions
   $("button#button_best").click( function(){
      var allLetters = getAllRackLetters();
      console.debug("allLetters", allLetters, ", length:", allLetters.length);
      var url = "https://www.anagrammer.com/word-unscrambler/"+allLetters.toLowerCase()+"?prefix=&combination=or&suffix=&dictionaryName=SOWPODS&gameName=scrabble_uk&wordLengths%5B5%5D=on&wordLengths%5B6%5D=on&wordLengths%5B7%5D=on&wordLengths%5B8%5D=on&wordLengths%5B9%5D=on&resultLimit=500&page=0&sort=length"
      console.debug("url", url);
      window.open(url);
   })


   // click to re-deal a letter
   $("div.rack>div").click(function() {
      // clicking on a letter card will cause it to be re-dealt
      // disable this feature once the clock has started
      if ( $("div.rack div").attr("draggable") == "false" || $("#seconds").hasClass("animate") ) {
         if ($(this).hasClass("vowel")) {
            $(this).text(selectRandomCardFromGivenDeck("vowel"))

         } else if ($(this).hasClass("consonant")) {
            $(this).text(selectRandomCardFromGivenDeck("consonant"))

         }
      }
   })

   // double-click to remove a letter
   $("div.rack>div").dblclick(function() {
      // double clicking on a letter card will cause a card to be removed
      // disable this feature once the clock has started
      if ( $("div.rack div").attr("draggable") == "false" || $("#seconds").hasClass("animate") ) {
         $(this)
            .empty()
            .removeClass("vowel")
            .removeClass("consonant");
      }
   })

   reset();

});