#!/bin/bash

USER_AGENT="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0"
APIBASEURL="https://unikove.com/projects/scrabble_widget/scrabble_api.php"
URL="${APIBASEURL}?word=${QUERY_STRING}"

echo -e "Content-type: text/html\n\n"
echo -e "<!-- QUERY_STRING : ${QUERY_STRING} --> "
echo -e "<!-- USER_AGENT   : ${USER_AGENT} -->"
echo -e "<!-- APIBASEURL   : ${APIBASEURL} -->"
echo -e "<!-- URL          : ${URL} -->"
RESPONSE=$( curl --silent --user-agent "${USER_AGENT}" --url "${URL}" )
#echo -e "<!-- RESPONSE     : ${RESPONSE} -->"
if [[ "0" = $RESPONSE ]]; then
   # word not found
   echo -e "<div class=\"entry_container\"><div class=\"entry\"><span class=\"inline\"><h1 class=\"hwd\">${QUERY_STRING}</h1></span><div class=\"hom\"><span class=\"gramGrp\"><span class=\"pos\">Sorry, word not found</span></span></div></div></div>"
else 
   echo "${RESPONSE//[^[:ascii:]]/}"
fi
