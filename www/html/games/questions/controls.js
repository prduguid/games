
function loadQuestionSet(set) {
   console.debug("loadQuestionSet", set);
   if (set >= 0 && set < questionSets.length) {
      // remove any question number/text/scores/choices/answers/cite
      [ ".question-text", ".scoring", ".answer", ".cite" ].forEach( function(selector){ $(selector).addClass("hidden").empty(); })
      $(".question-number").addClass("hidden");
      $(".number").text("0");
      $(".choices").addClass("hidden");
      $(".choices>ol").empty();

      // load question set theme and number of questions
      $(".question-set").text(questionSets[set].theme)
      $("span#questions").text(questionSets[set].questions.length);

      // set / reset any set specified template
      var template = ( questionSets[set].template === undefined || questionSets[set].template === null ) ? "" : questionSets[set].template;
      console.info("loadQuestionSet", "template: " + template );
      $(".grid-wrapper").attr("template", template);

      // if snow-true then let it snow
      if( questionSets[set].snow == true ){
         letItSnow();
      } else {
         removeFlakes();
      }
   } else {
      console.error("loadQuestionSet", "out of bounds value of set", set);
   }
}

function loadQuestion(q) {
   var set = $('#questionSet').val();

   console.debug("[loadQuestion]", "set", set, "questionSets[" + set + "].length", questionSets[set].length, "q", q);

   if (q >= 1 && questionSets[set].questions.length >= q) {
      console.debug("[loadQuestion]", "q", q, questionSets[set].questions[(q - 1)]);

      $(".question-number")
         .removeClass("hidden");

      $(".number")
         .text(q);

      $(".question-text")
         .removeClass("hidden")
         .html(questionSets[set].questions[(q - 1)]["q"]);

      $(".scoring")
         .removeClass("hidden")
         .text(questionSets[set].questions[(q - 1)]["scoring"]);
      
      $(".choices")
         .addClass("hidden")
         .removeClass("revealed");

      $(".choices ol").empty();
      
      $(".answer")
         .addClass("hidden")
         .html(questionSets[set].questions[(q - 1)]["a"]);

      if( questionSets[set].questions[(q - 1)]["cite"] === null || questionSets[set].questions[(q - 1)]["cite"] === undefined ) {
         $(".cite")
            .addClass("hidden")
            .empty();
      } else {
         $(".cite")
            .addClass("hidden")
            .html(questionSets[set].questions[(q - 1)]["cite"]);
      }

      if ( questionSets[set].questions[q - 1].choices != null && questionSets[set].questions[q - 1].choices.length > 0 ) {
         var choices = questionSets[set].questions[(q - 1)].choices.sort();
         for (c = 0; c < choices.length; c++) {
            var li = $("<li>")
            li.html(choices[c]);
            if (choices[c] == questionSets[set].questions[q - 1].a) {
               li.addClass("correct-answer")
            } else {
               li.addClass("wrong-answer")
            }
            $(".choices ol").append(li);
         }
         $(".choices").removeClass("hidden");
      }
   } else {
      console.warn("out of questions");
   }
}

function revealAnswer() {
   var set = $('#questionSet').val();
   var q = parseInt($(".number:first").text());
   console.debug("reveal", "set", set, "q", q );
   if( q > 0 ){
      if ($(".correct-answer").length > 0) {
         console.debug("reveal", "set", set, "q", q, "via choices");
         $(".choices").addClass("revealed");

      } else if($(".answer").text().length > 0){
         console.debug("reveal", "set", set, "q", q, "via direct answer");
         $(".answer").removeClass("hidden");
      } else {
         console.warn("reveal", "set", set, "q", q, "no answer");
      }

      if( $(".cite").text().length > 0  ) {
         $(".cite").removeClass("hidden");
      }
   }

}

$(document).ready(function() {
   // load the list of question sets, in reverse order (prepend)
   for (set = 0; set < questionSets.length; set++) {
      $('#questionSet').prepend($('<option>', {
         value: set,
         text: questionSets[set].date + " - " + questionSets[set].theme
      }));
   }

   // set to the most recent set of questions (assume last in array)
   $('#questionSet').val(questionSets.length - 1);
   loadQuestionSet(questionSets.length - 1);

   $('#questionSet').change(function() {
      loadQuestionSet($("#questionSet").val())
   })

   loadQuestionSet($("#questionSet").val());

   $("button#questionFirst").click(function() {
      loadQuestion(1);
   })
   $("button#questionPrevious").click(function() {
      loadQuestion(parseInt($(".number:first").text()) - 1);
   })
   $("button#questionNext").click(function() {
      loadQuestion(parseInt($(".number:first").text()) + 1);
   })
   $("button#reveal").click(function() {
      revealAnswer();
   })

   $("body").keypress(function(args) {
      console.debug("keypress event", args.keyCode, args)
      switch (args.keyCode) {
         case 112: // p - previous
         case 80:  // P
         case 98:  // b
         case 66:  // B
         case 45:  // -
         case 44:  // ,
         case 60:  // <
            loadQuestion(parseInt($(".number:first").text()) - 1);
            break;

         case 110: // n - next
         case 78:  // N
         case 43:  // +
         case 46:  // .
         case 62:  // >
            loadQuestion(parseInt($(".number:first").text()) + 1);
            break;

         case 114: // r - reveal
         case 42: // * the answer to life universe and everything
            revealAnswer();
            break;

         case 102: // f - first
            loadQuestion(1);
            break;

         case 49: // 1
            loadQuestion(1);
            break;

         case 50: // 2
            loadQuestion(2);
            break;

         case 51: // 3 
            loadQuestion(3);
            break;

         case 52: // 4
            loadQuestion(4);
            break;

         case 53: // 5
            loadQuestion(5);
            break;

         case 54: // 6
            loadQuestion(6);
            break;

         case 55: // 7
            loadQuestion(7);
            break;

         case 56: // 8
            loadQuestion(8);
            break;

         case 57: // 9
            loadQuestion(9);
            break;

         case 48: // 0
            loadQuestion(10);
            break;

      }
   })


   // $('body').on('keypress', 'button#questionNext', function(args) {
   //    if (args.keyCode == 110) {
   //       $('#MyButtonId').click();
   //       return false;
   //    }
   // });

});