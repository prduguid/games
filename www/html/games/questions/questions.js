var questionSets = [
   {
      "snow": false,
      "template":"answer-45",
      "date": "2020-02-28",
      "theme": "Northern Ireland",
      "questions": [
         {
            "q": "The RMS Titanic, was constructed in the Harland & Wolfe shipyards of Belfast, and set sail on its maiden voyage on the 10th April, but which year?",
            "scoring": "1 point",
            "a": "1912"
         },
         {
            "q": "On the maiden voyage of Titanic from Southhampton to New York it made two additional ports of call</br >For an additional point, name them",
            "scoring": "½ point each",
            "a": "<li>Cherborg, France</li><li>Queenstown (Cobh), Ireland</li>"
         },
         {
            "q": "The Crown is a National Trust property in the centre of Belfast, but what is it?",
            "scoring": "1 point",
            "a": "A pub"
         },
         {
            "q": "The DeLorean DMC-12, made famous in the Back to the Future trilogy of films, was manufactured near Belfast, Northern Ireland.</br>To the nearest 100, approximatly how many were produced?",
            "scoring": "1 point",
            "a": "9,000"
         },
         {
            "q": "There are 32 counties in the land of Ireland, of which 6 form Northern Ireland.</br>Name three of them.",
            "scoring": "1 point each, maximum of three points",
            "a": "<li>County Antrim</li><li>County Armagh</li><li>County Down</li><li>County Fermanagh</li><li>County Londonderry</li><li>County Tyrone</li>"
         },
         {
            "q": "One end of the 910 mile (1460 km) E1 Euro Route starts at the Harbour town of Larne in Northern Ireland and passes through several countries, but where does the other end terminate?",
            "scoring": "1 point for the right country, 1 point for a country it passes through",
            "a": "<ol><li>Larne, Northern Ireland</li><li>Republic of Ireland</li><li>Spain</li><li>Portugal</li><li>Seville, Spain</li></ol>"
         },
         {
            "q": "The Lion, the Witch and the Wardrobe was writen by Belfast born author C S Lewis, and is one book from the The Chronicals of Narnia.</br>Name one other book from the cronicals",
            "scoring": "1 point",
            "a": "<ul class='fa-ul'><li><span class='fa-li'><i class='fas fa-book'></i></span>The Lion, the Witch and the Wardrobe (1950)</li><li><span class='fa-li'><i class='fas fa-book'></i></span>Prince Caspian (1951)</li><li><span class='fa-li'><i class='fas fa-book'></i></span>The Voyage of the Dawn Treader (1952)</li><li><span class='fa-li'><i class='fas fa-book'></i></span>The Silver Chair (1953)</li><li><span class='fa-li'><i class='fas fa-book'></i></span>The Horse and His Boy (1954)</li><li><span class='fa-li'><i class='fas fa-book'></i></span>The Magician's Nephew (1955)</li><li><span class='fa-li'><i class='fas fa-book'></i></span>The Last Battle (1956)</li></ul>"
         },
      ]
   },{
      "snow": false,
      "template":"answer-50",
      "date": "2020-04-19",
      "theme": "Board Games",
      "questions": [
         {
            "q": "Name all six suspects in the game of Cludeo?",
            "scoring": "½ point for each, total of 3 points, and I'm going need full answers (UK standard edition).",
            "a": "<ul class='fa-ul'><li><span class='fa-li'><i class='fas fa-user' style='color: red;'></i></span>Miss Scarlet</li><li><span class='fa-li'><i class='fas fa-user' style='color: SkyBlue;'></i></span>Mrs Peacock</li><li><span class='fa-li'><i class='fas fa-user' style='color: white;'></i></span>Mrs White</li><li><span class='fa-li'><i class='fas fa-user' style='color: indigo;'></i></span>Professor Plum</li><li><span class='fa-li'><i class='fas fa-user' style='color: DarkOliveGreen;'></i></span>Reverend Green</li><li><span class='fa-li'><i class='fas fa-user' style='color: gold;'></i></span>Colonel Mustard</li></ul>"
         },
         {
            "q": "Name the murdered victim in the game of Cluedo?",
            "scoring": "1 point (UK standard edition)",
            "a": "<ul class='fa-ul'><li><span class='fa-li'><i class='far fa-user'></i></span>Dr Black</li></ul>"

         },
         {
            "q": "The Cluedo board has two secret passages, one starts from the Kitchen, where does that passage come out?",
            "choices": ["Hall", "Lounge", "Study", "Conservatory"],
            "scoring": "1 point (UK standard edition)",
            "a": "Study"
         },
         {
            "q": "Name all 4 train stations in the London Monoploy board game?",
            "scoring": "½ point for each, total of 2 points (UK standard, London edition)",
            "a": "<li>Kings Cross station</li><li>Marylebone station</li><li>Fenchurch Street Statio</li><li>Liverpool Street station</li>"
         },
         {
            "q": "In the standard Monopoly game how much money is given to each player at the start of the game?",
            "scoring": "1 point (UK standard, London edition)",
            "choices": ["£ 1,000", "£ 1,500", "£ 2,000", "£ 2,500"],
            "a": "£ 1,500"
         },
         {
            "q": "In the standard London Monopoly game how many of the chance and community chest cards given an 'Advance to' instruction?",
            "scoring": "1 point (UK standard, London edition)",
            "choices": ["3", "5", "6", "8"],
            "a": "5",
            "cite": "<ol><li>Advance to 'Go' (Community Chest)</li><li>Advance to 'Go' (Chance)</li><li>Advance to Pall Mall. If you pass 'Go' collection £200 (Chance)</li><li>Advance to Trafalgar Square. If you pass 'Go' collect £200 (Chance)</li><li>Advance to Mayfair (Chance)</li></ol>"
         },
         {
            "q": "What are the two utilities on the standard London Monolopy board",
            "scoring": "1 point (UK standard, London edition)",
            "choices": [
               "Gas & Electricity",
               "Electricity & Telegram",
               "Water & Sewerage",
               "Water and Electricity"
            ],
            "a": "Water and Electricity",
            "cite": "<ul class='fa-ul'><li><span class='fa-li'><i class='fas fa-faucet'></i></span>Water Works</li><li><span class='fa-li'><i class='far fa-lightbulb'></i></span>Electric Company</li</ul>"
         }
      ]
   },{
      "snow": false,
      "template":"answer-40",
      "date": "2020-05-03",
      "theme": "Inventions",
      "questions": [
         {
            "q": "The Mechanical Calculator was invented in 1642 by Blaise Pascal.<p></p>Was the Pendulum Clock (invented by Christian Huygens) invented before or after?",
            "scoring": "1 point",
            "choices": ["before", "afterwards"],
            "a": "afterwards",
            "cite": "The Pendulum Clock was invented in 1653"
         },{
            "q": "From 1730 John Harrison entered a series of chronometers & watches into the £20,000 Longitude contest.  He was finally successful after 42 years when King George III personally tested a later version and declared it accurate.<br>How many attempts did John make?",
            "scoring": "1 point",
            "a": "<ol class='fa-ul'><li><span class='fa-li'><i class='far fa-clock'></i></span>1730 - H1</li><li><span class='fa-li'><i class='far fa-clock'></i></span>1741 - H2</li><li><span class='fa-li'><i class='far fa-clock'></i></span>1758 - H3</li><li><span class='fa-li'><i class='far fa-clock'></i></span>1772 - H4</li></ol>",
            "cite": "<a href='https://en.wikipedia.org/wiki/John_Harrison'>https://en.wikipedia.org/wiki/John_Harrison</a>"
         },{
            "q": "In 1798 Edward Jenner develops the first successful vaccine.<br>What disease did this target?",
            "scoring": "1 point",
            "a": "The smallpox vaccine",
            "cite":"<a href='https://en.wikipedia.org/wiki/Smallpox_vaccine'>https://en.wikipedia.org/wiki/Smallpox_vaccine</a>",
         },{
            "q": "In 1867 Lucien B. Smith invented barbed wire, later improved on by Joseph F. Glidden.<br>This invention is credited with the demise of which traditional US career?",
            "scoring": "1 point",
            "a": "The cowboy",
            "cite":"<a href='https://en.wikipedia.org/wiki/Barbed_wire'>https://en.wikipedia.org/wiki/Barbed_wire</a>",
         },{
            "q": "<div class='note'>In the 1880's the following were invented, put them into chronological order.</div><ul><li>Ballpoint pen</li><li>Petrol engine powered car</li><li>Recoil-operated gun (aka semi / fully automatic firearm)</li></ul>",
            "scoring": "1 point for 2 items in the right order, 2 points for all items in the right order",
            "a": "<ol><li>1884 recoil-operated gun<li>1886 Petrol engine powered car<li>1888 ballpoint pen</ol>"
         },{
            "q": "In 1945, the Manhattan Project invents which weapon?",
            "scoring": "1 point",
            "a": "The Atomic Bomb",
            "cite":"<a href='https://en.wikipedia.org/wiki/Manhattan_Project'>https://en.wikipedia.org/wiki/Manhattan_Project</a>"
         },{
            "q": "In the next month the Atomic Bomb was dropped on which two cities?",
            "scoring": "1 point for each city",
            "a": "<li>Hiroshima</li><li>Nagasaki</li>",
            "cite":"<a href='https://en.wikipedia.org/wiki/Atomic_bombings_of_Hiroshima_and_Nagasaki'>https://en.wikipedia.org/wiki/Atomic_bombings_of_Hiroshima_and_Nagasaki</a>"
         },{
            "q": "In 1947 what revolutionary electronic component was invented by John Bardeen, Walter Brattain & Willam Shockley?",
            "scoring": "1 point",
            "a": "The first transistor",
            "cite":"<a href='https://en.wikipedia.org/wiki/Transistor'>https://en.wikipedia.org/wiki/Transistor</a>"
         }
      ]
   },{
      "snow": false,
      "date": "2020-05-24",
      "theme": "Coca-Cola logos from around the world",
      "questions": [
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/poland.png'></p>", "scoring": "½ point", "choices": ["Poland", "Brazil", "China", "Bulgaria"], "a": "Poland" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/korea.png'></p>", "scoring": "½ point", "choices": ["Korea", "France", "Poland", "Egypt"], "a": "Korea" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/israel.png'></p>", "scoring": "½ point", "choices": ["Israel", "Taiwan", "Egypt", "Bulgaria"], "a": "Israel" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/somalia.png'></p>", "scoring": "½ point", "choices": ["Somalia", "China", "Egypt", "Bulgaria"], "a": "Somalia" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/spain+brazil.png'></p>", "scoring": "½ point", "choices": ["Brazil", "Poland", "Bulgaria", "Egypt"], "a": "Brazil" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/ethiopia.png'></p>", "scoring": "½ point", "choices": ["Ethiopia", "Finland", "Korea", "Bangladesh"], "a": "Ethiopia" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/srilanka.png'></p>", "scoring": "½ point", "choices": ["Sri Lanka", "Bangladesh", "Taiwan", "Spain"], "a": "Sri Lanka" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/france.png'></p>", "scoring": "½ point", "choices": ["France", "Finland", "Brazil", "Israel"], "a": "France" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/thailand-b.png'></p>", "scoring": "½ point", "choices": ["Thailand", "Germany", "Poland", "Bangladesh"], "a": "Thailand" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/mexico.png'></p>", "scoring": "½ point", "choices": ["Mexico", "Brazil", "Bangladesh", "China"], "a": "Mexico" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/germany.png'></p>", "scoring": "½ point", "choices": ["Germany", "Egypt", "China", "Sri Lanka"], "a": "Germany" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/morocco.png'></p>", "scoring": "½ point", "choices": ["Morocco", "Brazil", "Egypt", "Bulgaria"], "a": "Morocco" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/bulgaria.png'></p>", "scoring": "½ point", "choices": ["Bulgaria", "Thailand", "Pakistan", "Morocco"], "a": "Bulgaria" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/pakistan.png'></p>", "scoring": "½ point", "choices": ["Pakistan", "Mexico", "Brazil", "Spain"], "a": "Pakistan" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/taiwan.png'></p>", "scoring": "½ point", "choices": ["Taiwan", "Sri Lanka", "Israel", "France"], "a": "Taiwan" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/china.png'></p>", "scoring": "½ point", "choices": ["China", "Korea", "Egypt", "Poland"], "a": "China" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/bangladesh.png'></p>", "scoring": "½ point", "choices": ["Bangladesh", "Morocco", "Brazil", "Finland"], "a": "Bangladesh" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/russia.png'></p>", "scoring": "½ point", "choices": ["Russia", "Poland", "Taiwan", "Korea"], "a": "Russia" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/egypt.png'></p>", "scoring": "½ point", "choices": ["Egypt", "France", "Pakistan", "Poland"], "a": "Egypt" },
         { "q": "Which country is this Coca-Cola logo from?<p><img src='images/coca-cola/finland.png'></p>", "scoring": "½ point", "choices": ["Finland", "Bulgaria", "Germany", "Mexico"], "a": "Finland" }
      ]
   },{
      "snow": false,
      "template":"answer-20",
      "date": "2020-05-31",
      "theme": "Guess these world-famous landmarks from their close-ups",
      "questions": [
         { 
            "q": "These are the windows of?<br><img src='images/guess-landmarks-closeups/empire-state-closeup.jfif'><div class='note'>A landmark 102-story skyscraper was the world's tallest building for almost four decades, before it was surpassed by the World Trade Center's North Tower in 1970.</div>",
            "scoring": "1 point",
            "a": "Empire State Building, New York City, New York, USA" 
         },{
            "q": "This curved roof belongs to?<br><img src='images/guess-landmarks-closeups/sydney-opera-house-closeup.jfif'><div class='note'>A multi-venue performing-arts center, this iconic building is located by one of the most famous harbors in the world.</div>",
            "scoring": "1 point",
            "a": "Sydney Opera House, Sydney, Australia" 
         },{ 
            "q": "This letterbox belongs to?<br><img src='images/guess-landmarks-closeups/10-downing-street-closeup.jfif'><div class='note'>This letterbox is part of an iconic door, belonging to a building finished in 1684.</div>",
            "scoring": "1 point",
            "a": "10 Downing Street, London, UK" 
         },{ 
            "q": "This is the wall of?<br><img src='images/guess-landmarks-closeups/hover-dam-closeup.jfif'><div class='note'>This world-famous wall is part of an engineering masterpiece that was finished in 1936. Millions of people tour this site every year.</div>",
            "scoring": "1 point",
            "a": "Hoover Dam, Nevada, USA" 
         },{ 
            "q": "This ceiling is located in?<br><img src='images/guess-landmarks-closeups/park-güell-closeup.jfif'><div class='note'>It's located in a park created by a notable Catalonian architect.</div>",
            "scoring": "1 point",
            "a": "Park Güell, Barcelona, Spain" 
         },{ 
            "q": "These are the curves of?<br><img src='images/guess-landmarks-closeups/guggenheim-museum-closeup.jfif'><div class='note'>A landmark building designed by one of the 20th century's most important architects, Frank Lloyd Wright and opened in 1959.</div>",
            "scoring": "1 point",
            "a": "Solomon R. Guggenheim Museum, New York City, USA" 
         },{ 
            "q": "This is the bottom up view of?<br><img src='images/guess-landmarks-closeups/eiffel-tower-closeup.jfif'><div class='note'>Constructed as the entrance to the 1889 World’s Fair, this iconic tower was supposed to be demolished after the exposition but instead it’s become one an iconic structure.</div>",
            "scoring": "1 point",
            "a": "Eiffel Tower, Paris, France" 
         },{ 
            "q": "These arches belong to?<br><img src='images/guess-landmarks-closeups/sagrada-família-closeup.jfif'><div class='note'>A famous architect's lifetime project, renowned for the unique combination of Gothic and Art Nouveau styles.</div>",
            "scoring": "1 point",
            "a": "Sagrada Família, Barcelona, Spain" 
         },{ 
            "q": "These inscribed tiles are part of?<br><img src='images/guess-landmarks-closeups/taj-mahal-closeup.jfif'><div class='note'>This ivory-white mausoleum attracts millions of visitors each year and is the backdrop of many iconic photographs.</div>",
            "scoring": "1 point",
            "a": "Taj Mahal, Agra, India" 
         },{ 
            "q": "This is the ceiling of?<br><img src='images/guess-landmarks-closeups/arc-de-triomphe-de-i’etoile-closeup.jfif'><div class='note'>Beneath this landmark there is a vault where in lies the Tomb of the Unknown Soldier from the First World War.</div>",
            "scoring": "1 point",
            "a": "Arc de Triomphe, Paris, France" 
         }

      ]
   },{
      "snow": false,
      "template":"single",
      "date": "2020-06-07",
      "theme": "Work out the film titles with the vowels removed",
      "questions": [
         // {
         //    "q": "<ol type='a'><li><code>mlnrg</code></li><li><code>bc kt thft r</code></li><li><code>l ffp</code></li><li><code>thsh wsh nkrdm ptn</code></li><li><code>thhntf rrdc tbr</code></li></ol><div class='note'>(I may have misplaced the spaces too)</div>",
         //    "scoring": "1 point each",
         //    "a": "<ol type='a'><li>Moulin Rouge</li><li>Back to The Future</li><li>Life of Pi</li><li>The Shawshank Redemption</li><li>The Hunt for Red October</li></ol>"
         // },
         // {
         //    "q": "<ol type='a' start='6'><li><code>dsbt</code></li><li><code>mr cnbty</code></li><li><code>lwrncfrb</code></li><li><code>nd njnsn dt hlstc rsd</code></li><li><code>fr rsb llr sd yff</code></li></ol><div class='note'>(I may have misplaced the spaces too)</div>",
         //    "scoring": "1 point each",
         //    "a": "<ol type='a' start='6'><li>Das Boot</li><li>American Beauty</li><li>Lawrence of Arabia</li><li>Indiana Jones and the Last Crusade</li><li>Ferris Bueller's Day Off</li></ol>"
         // },


         {
            "q": "<ol><li><code>mlnrg</code></li><li><code>bc kt thft r</code></li><li><code>l ffp</code></li><li><code>thsh wsh nkrdm ptn</code></li><li><code>thhntf rrdc tbr</code></li><li><code>dsbt</code></li><li><code>mr cnbty</code></li><li><code>lwrncfrb</code></li><li><code>nd njnsn dt hlstc rsd</code></li><li><code>fr rsb llr sd yff</code></li></ol><div class='note'>(I may have misplaced the spaces too)</div>",
            "scoring": "1 point each",
            "a": "<ol type='a'><li>Moulin Rouge</li><li>Back to The Future</li><li>Life of Pi</li><li>The Shawshank Redemption</li><li>The Hunt for Red October</li><li>Das Boot</li><li>American Beauty</li><li>Lawrence of Arabia</li><li>Indiana Jones and the Last Crusade</li><li>Ferris Bueller's Day Off</li></ol>"
         },



      ]
   },{
      "snow": false,
      "date": "2020-06-14",
      "theme": "Objects may appear closer...",
      "questions": [
         { "q": "<img src='images/yourcloseup/close-up--carrot.jpg'>", "scoring": "1 point", "a": "A carrot slice" },
         { "q": "<img src='images/yourcloseup/close-up--cornflake.png'>", "scoring": "1 point", "a": "Cornflake (frosted)" },
         { "q": "<img src='images/yourcloseup/close-up--guitar.png'>", "scoring": "1 point", "a": "Guitar String & Tensioner" },
         { "q": "<img src='images/yourcloseup/close-up--paintbrush.jpg'>", "scoring": "1 point", "a": "Paintbrush" },
         { "q": "<img src='images/yourcloseup/close-up--spaghetti.jpg'>", "scoring": "1 point", "a": "Spaghetti" },
         { "q": "<img src='images/yourcloseup/close-up--sponge.jpg'>", "scoring": "1 point", "a": "Washing Up Sponge (pink)" },
         { "q": "<img src='images/yourcloseup/close-up--honey.jpg'>", "scoring": "1 point", "a": "Honey" },
         { "q": "<img src='images/yourcloseup/close-up--coke-bottle-cap.jpg'>", "scoring": "1 point", "a": "Coke Bottle Cap" },
         { "q": "<img src='images/yourcloseup/close-up--cork.png'>", "scoring": "1 point", "a": "Wine Cork" },
         { "q": "<img src='images/yourcloseup/close-up-flame.png'>", "scoring": "1 point", "a": "Flame from a matchstick" }
      ]
   },{
      "snow": false,
      "date": "2020-06-21",
      "theme": "Bob's Your Uncle!",
      "questions": [
         {
            "q": "A Bobby or a Peeler is a slang name for a police officer, the origin of these names being a tribute to whom?<br><img src='images/bobs-your-uncle/met-police-sergeant-1865.jpg'>", "scoring": "1 point", "a": "Robert Peel"
         },{
            "q": "In pre-decimal British currency a Bob is a nickname for what coin?<br><img src='images/bobs-your-uncle/predeccoins.png'>", "scoring": "1 point", "choices": ["1 Farthing", "1 Penny", "1 Shilling", "1 Florin"], "a": "1 Shilling"
         },{ 
            "q": "The Bob Club web site* allows people called Bob to submit their name.  How many Bobs (as of 20th June 2020) have submitted their name<br><img src='images/bobs-your-uncle/the-bob-club-logo.jpg'><div class='note'> * <a href='http://thebobclub.com'>http://thebobclub.com</a> - The Big Bob List Of People, Places, And Things Named Bob ~ The Bob Club ~ A Club For Guys Named Bob And The People Who Know Them</div>",
            "scoring": "1 point",
            "choices": ["412", "4,121", "41,216", "412,167"],
            "a": "4,121"
         },{
            "q": "Robert Underdunk Terwilliger Jr., PhD is better known as which recurring character in The Simpson and voiced by Kelsey Grammer?<br><img src='images/bobs-your-uncle/the-simpsons-characters.jpg'>", "scoring": "1 point", "a": "Sideshow Bob"
         },{
            "q": "Bob Carolgees is a comedy entertainer who appeared on the Saturday morning TV series Tiswas. He is best known for appearing with a canine puppet, what was the name of the puppet?", "scoring": "1 point", "a": "Spit the Dog"
         },{
            "q": "Bobby Ball is a UK comedian, actor, and singer, best known as part of a double act with which other UK comedian and singer?", "scoring": "1 point", "a": "Tommy Cannon"
         },{
            "q": "In the original stop motion animation series of Bob the builder, which UK actor provided Bob’s voice?<br><img src='images/bobs-your-uncle/bob-the-builder.jpg'>", "scoring": "1 point", "a": "Neil Morrissey"
         },{
            "q": "Robert Allen Zimmerman, born May 24, 1941, is an American singer-songwriter, author, and visual artist.  He is better known as?", "scoring": "1 point", "a": "Bob Dylan"
         },{
            "q": "The BBC revived the 1960 series Randall and Hopkirk (Deceased) in 2000.  The characters of Randall and Hopkirk were played by a popular comedic double act.  Which Bob played the character Randall?", "scoring": "1 point", "a": "Bob Mortimer"
         },{
            "q": "In Dallas (both the original TV series, the spin off films and the 2012 revival) which actor played the part of Bobby Ewing?<br><img src='images/bobs-your-uncle/dallas.jpg'>", "scoring": "1 point", "a": "Patrick Duffy"
         },{
            "q": "Bonus Question<br><img src='images/bobs-your-uncle/bob_the_street_cat_high-fives_james_bowen.jpg'><div class='note'>The street cat named Bob sadly died recently aged 14.  Bob was taken in by a recovering drug addict, James, and credited with helping him come clean.  Bob accompanied James as he busked and sold copies of the Big Issue.  In which UK City did James and Bob live and work?</div>",
            "scoring": "1 point",
            "choices": ["Birmingham", "Bristol", "Southampton", "London"],
            "a": "London"
         },
      ]
   },{
      "snow": false,
      "template":"answer-45",
      "date": "2020-09-13",
      "theme": "Are you social media savy?",
      "questions": [
         {
            "q": "<i class='fab fa-facebook'></i>Facebook was created by Mark Zuckerberg (and others) and lauched 4th Febuary 2004, initally the social network was availaible only to students of which USA university?",
            "scoring": "1 point",
            "choices": ["UCLA", "Stanford", "Harvard", "Princetown"],
            "a": "Harvard"
         },
         {
            "q": "<i class='fab fa-facebook'></i>Facebook subsequenly opened up to other universitys, including the first few UK universities by October 2005 and then to some private companies employees by April 2006.  When did facebook membership become generally open to all?",
            "scoring": "1 point",
            "choices": ["26th September 2006", "9th November 2006", "25th December 2006", "1st January 2007"],
            "a": "26th September 2006"
         },
         {
            "q": "<i class='fab fa-facebook'></i>Facebook has bought a number of competitors, but in which order did they buy these key companies?",
            "scoring": "1 point",
            "choices": ["Instagram, Oculus VR, WhatsApp", "Instagram, WhatsApp, Oculus VR", "WhatsApp, Instagram, Oculus VR", ],
            "a": "Instagram, WhatsApp, Oculus VR",
            "cite": "<ul class='fa-ul'><li><span class='fa-li'><i class='fab fa-instagram'></i></span>Instagram (April 2012 for $1bn)</li><li><span class='fa-li'><i class='fab fa-whatsapp'></i></span>WhatsApp (Feb 2014 $19bn)</li><li><span class='fa-li'><i class='fas fa-vr-cardboard'></i></span>Oculus VR (March 2014 $2bn)</li></ol>"
         },
         {
            "q": "Which Social Network was launched first?",
            "scoring": "1 point",
            "choices": ["Bebo", "LinkedIn", "MySpace", "Twitter"],
            "a": "LinkedIn",
            "cite": "<ul class='fa-ul'><li><span class='fa-li'><i class='fab fa-linkedin'></i></span>LinkedIn (5th May 2003)</li><li><span class='fa-li'><i class='fas fa-users'></i></span>MySpace (1st August 2003)</li><li><span class='fa-li'><i class='fas fa-users'></i></span>Bebo (July 2005 - August 2013)</li><li><span class='fa-li'><i class='fab fa-twitter-square'></i></span>Twitter (21st March 2006)</li></ol>"
         },
         {
            "q": "On the 9th October 2008; 501 messages choosen by Bebo users where sent into space towards the planet Gliese 581c.  Which year are they due to arrive?<div class='note'>Gliese is 20.37 light-years from Earth (192 trillion km)</div>",
            "scoring": "1 point",
            "choices": ["2028", "2099", "3600"],
            "a": "2028"
         },
         {
            "q": "<i class='fab fa-twitter-square'></i>Checking Twitter I can see that Paul Cooper has tweeted 118 times, Tim 'Redders' Redmayne 837 times and apparently I've managed 3009 tweets.  Excluding re-tweets who has tweeted most recently?",
            "scoring": "1 point",
            "choices": ["@PaulCooper15", "@prduguid", "@TimRedmayne"],
            "a": "@TimRedmayne",
            "cite": "Tim: 22nd June 2020 (Peter: 9th December 2019, Paul: 12th May 2019.)<blockquote class='twitter-tweet'><p lang='und' dir='ltr'><a href='https://twitter.com/hashtag/IStandWithBubba?src=hash&amp;ref_src=twsrc%5Etfw'>#IStandWithBubba</a></p>&mdash; Tim Redmayne (@TimRedmayne) <a href='https://twitter.com/TimRedmayne/status/1275134328796782594?ref_src=twsrc%5Etfw'>June 22, 2020</a></blockquote> <script async src='https://platform.twitter.com/widgets.js' charset='utf-8'></script>"
         },
         {
            "q": "As of July 2020, Which [of these] social media networks is most popular?",
            "scoring": "1 point",
            "choices": ["Snapchat", "WeChat", "YouTube"],
            "a": "YouTube",
            "cite": "<ul class='fa-ul'><li><span class='fa-li'><i class='fab fa-youtube'></i></span>YouTube (2,000,000,000 active users)</li><li><span class='fa-li'><i class='fab fa-weixin'></i></span>WeChat (1,203,000,000 active users)</li><li><span class='fa-li'><i class='fab fa-snapchat-square'></i></span>Snapchat (397,000,000 active users)</li></ol>"
         },
         {
            "q": "<i class='fab fa-google-plus-square'></i>Google launched a social network called <strong>Google+</strong> 28th June 2011, which wasn't particulary popular, it eventually closed when?",
            "scoring": "1 point",
            "choices": ["2nd April 2012", "31st December 2018", "2nd April 2019"],
            "a": "2nd April 2019"
         },
         {
            "q": "<i class='fab fa-twitter-square'></i>Who has the most followers on Twitter?",
            "scoring": "1 point",
            "choices": ["@BarackObama", "@BorisJohnson", "@jeremycorbyn", "@KimKardashian", "@realDonaldTrump"],
            "a": "@BarackObama",
            "cite": "<ul><li>@BarackObama (122.5 Million)</li><li>@realDonaldTrump (85.9M)</li><li>@KimKardashian (66.7M)</li><li>@BorisJohnson (2.9M)</li><li>@jeremycorbyn (2.4M)</li>"
         },
         {
            "q": "One bonus point goes to the first person to send (via messenger) a screenshot showing that they've logged into their MySpace account, GO!",
            "scoring": "1 point"
         }
      ]
   },{
      "snow": false,
      "template":"answer-60",
      "date": "2020-10-11",
      "theme": "Medical Milestones",
      "questions": [{
            "q": "<div class='note'>Antibiotics can truly be considered the epitome of the 20th century's &quot;wonder drugs.&quot;<br/>In 1929 Alexander Fleming, reported his observation that the culture medium on which a penicillium mould had grown attacked certain bacteria.</div>In which hospital did he discover this?",
            "scoring": "1 point",
            "choices": ["Great Ormond Street Hospital", "Oxford University Hospital", "St Mary's Hospital Medical School in London", "University College Hospital, London"],
            "a": "St Mary's Hospital Medical School in London"
         },{
            "q": "On 8 November 1895 the German physicist, Wilhelm Conrad Röntgen of Würzburg discovered which medical imaging technique?",
            "scoring": "1 point",
            "choices": ["CAT scan", "MRI", "Ultrasound", "X-Ray"],
            "a": "X-Ray"
         },{
            "q": "Queen Victora had eight children, during the birth of the last two the Queen was successfully given which an Anaesthesia, which effecticely ended the debate on the safety of anaesthesia during childbirth?",
            "scoring": "1 point",
            "choices": ["alcohol", "chloroform", "ether", "opiates"],
            "a": "chloroform"
         },{
            "q": "Vaccines have saved <strike>tens</strike> hundreds of millions of lives. All modern innovations can be traced back to Louis Pasteur's 1885 breakthrough, but which disease did his vaccine combat?",
            "scoring": "1 point",
            "choices": ["cowpox", "flu", "measles", "rabies"],
            "a": "rabies"
         },{
            "q": "The actual first vaccine was discovered before Louis Pasteur's 1885 breakthrough, in 1796 when Edward Jenner realised that milkmaids who contracted cowpox were imnue to which other and more serious disease?",
            "scoring": "1 point",
            "choices": ["diphtheria", "measles", "smallpox", "tuberculosis"],
            "a": "smallpox"
         },{
            "q": "<div class='note'>During the rise of the industrial reveolution in Europe (1820s - 1860s), large numbers of people relocated into the citys, this lead to a drop in life expectency due to worsing sanatary conditions. Finally John Snow's theory that disease was getting into water supply was proven when he disabled a water pump.</div>Which desease was this?",
            "scoring": "1 point",
            "choices": ["botulism", "cholera", "dysentery", "typhoid fever"],
            "a": "cholera"
         },{
            "q": "<div class='note'>In February 1953, Watson and Crick completed the first correct model of the double-helix of DNA.</div>On 28 February 1953 Crick interrupted patrons' lunchtime at which pub in Cambridge to announce that he and Watson had &quot;discovered the secret of life&quot;",
            "scoring": "1 point",
            "choices": ["The Anchor", "The Eagle", "The Mill", "Pickerell Inn"],
            "a": "The Eagle"
         },{
            "q": "According to wikipedia, how many hospitals are there in London (NHS and private)?",
            "scoring": "1 point",
            "choices": ["65", "72", "85", "98"],
            "a": "85"
         },{
            "q": "<div class='note'>The UK National Health Service was born on the 5th July 1948, with &quot;the motivation to provide a good, strong and reliable healthcare to all.&quot;</div>In which city was the first hospital?",
            "scoring": "1 point",
            "choices": ["Bristol", "Coventry", "Leeds", "Manchester"],
            "a": "Manchester"
         },{
            "q": "<div class='note'>[as of 2018] The NHS is one of the top 10 largest employer in the world.</div>Which is the correct ranking for these [selected] top 10 global organisations (in decensing order):",
            "scoring": "1 point",
            "choices": [
               "Indian Armed Forces, McDonald's, NHS, People's Liberation Army (China), Walmart",
               "People's Liberation Army (China), Walmart, McDonald's, NHS, Indian Armed Forces",
               "Walmart, Indian Armed Forces, NHS, People's Liberation Army (China), McDonald's",
               "People's Liberation Army (China), Indian Armed Forces, NHS, Walmart, McDonald's",
            ],
            "a": "People's Liberation Army (China), Walmart, McDonald's, NHS, Indian Armed Forces",
            "cite": "<table style='font-size:18px;'><thead><tr><th>Rank</th><th>Employer</th><th>Employees (Millions)</th></tr></thead><tbody><tr><td>1</td><td>United States Department of Defense</td><td>3.2</td></tr><tr><td>2</td><td>People's Liberation Army (China)</td><td>2.3</td></tr><tr><td>3</td><td>Walmart</td><td>2.1</td></tr><tr><td>4</td><td>McDonald's </td><td>1.7</td></tr><tr><td>5</td><td>China National Petroleum Corporation</td><td>1.7</td></tr><tr><td>6</td><td>State Grid Corporation of China</td><td>1.6</td></tr><tr><td>7</td><td>China National Petroleum</td><td>1.5</td></tr><tr><td>8</td><td>National Health Service (United Kingdom)</td><td>1.4</td></tr><tr><td>9</td><td>Indian Railways</td><td>1.4</td></tr><tr><td>10</td><td>Indian Armed Forces</td><td>1.3</td></tr></tbody></table>"
         }
      ]
   },{
      "snow": false,
      "date": "2020-11-08",
      "theme": "Alton Towers -- One of my favourite places",
      "questions": [{
            "q": "What <strike>county</strike> shire in England is Alton Towers situated?",
            "scoring": "1 point",
            "choices": ["Staffordshire", "Leicestershire", "Derbyshire", "Warickshire"],
            "a": "Staffordshire"
         },
         {
            "q": "Alton Towers opened as a theme park on 4th April 1980. Orginally it was a private estate, but the grounds were opened to the public when?",
            "scoring": "1 point",
            "choices": ["13 April 1820", "13 April 1860", "13 April 1900", "13 April 1940"],
            "a": "13 April 1860"
         },
         {
            "q": "In 1987 who helped to open the Alton Towers monorail?",
            "scoring": "1 point",
            "choices": ["William Shatner", "Andi Peters", "Timmy Mallet", "Carrie Fisher"],
            "a": "William Shatner"
         },
         {
            "q": "The Alton towers Monorail trains are second hand, where were they orginally from?",
            "scoring": "1 point",
            "choices": ["The Philadelphia Zoo", "Blackpool Pleasure Beach", "World Expo 88 (Brisbane, Queensland)", "Expo 86 (Vancouver BC)"],
            "a": "Expo 86 (Vancouver BC)"
         },
         {
            "q": "Oblivion's trains can host 16 people, at full capacity how many riders can Oblivion send into eternal darkness each hour?",
            "scoring": "1 point",
            "choices": ["400", "950", "1900", "2400"],
            "a": "1900"
         },
         {
            "q": "Alton Towers introduced the worlds first vertical drop roller coaster in 1998. What is the name of this ride?",
            "scoring": "1 point",
            "choices": ["Air", "Black Hole", "Enterprise", "Oblivion"],
            "a": "Oblivion"
         },
         {
            "q": "Alton Towers introduces the worlds first vertical freefall drop roller coaster in 2010.  What is the name of this ride?",
            "scoring": "1 point",
            "choices": ["Nemisis", "Rita", "Spinball Wizzer", "TH13TEEN"],
            "a": "TH13TEEN"
         },
         {
            "q": "Who opened the Congo River Rapids ride in 1988?",
            "scoring": "1 point",
            "choices": ["Timmy Mallet", "David Hasselhoff", "Noel Edmonds", "William Shatner"],
            "a": "Noel Edmonds"
         },
         {
            "q": "Which Alton Towers roller coaster is my [current] favouite?",
            "scoring": "1 point",
            "choices": ["TH13TEEN", "Nemisis", "Rita", "Smiller"],
            "a": "Smiller"
         },
         {
            "q": "Which mainstream fastfood chain didn't have a restaurent at Alton Towers?",
            "scoring": "1 point",
            "choices": ["McDonalds", "Burger King", "KFC", "Wimpy"],
            "a": "Wimpy"
         },
      ]
   },{
      "snow": true,
      "date": "2020-12-13",
      "theme": "My Christmas quiz",
      "questions": [
         {  "q": "Which two of Father Christmas's reindeers are named after meteorological terms?",                                                   "scoring": "1 point",   "a": "Donner & Blitzen"       },
         {  "q": "There are 365 days in a year. What number day is Christmas Day?",                                                                   "scoring": "1 point",   "a": "359"                    },
         {  "q": "Popular in Victorian England at Christmas and also appearing in the Dickens' story A Christmas Carol, what is 'Smoking Bishop'?",   "scoring": "1 point",   "a": "A type of mulled wine"  },
         {  "q": "Bob Geldof and Midge Ur wrote the song 'Do They Know It's Christmas?' in which year?",                                              "scoring": "1 point",   "a": "1984"                   },
         {  "q": "What fruit is usually placed in stockings?",                                                                                        "scoring": "1 point",   "a": "Tangerines"             },
         {  "q": "If you're born on Christmas Day, what's your star sign?",                                                                           "scoring": "1 point",   "a": "Capricorn"              },
         {  "q": "How many Drummers Drummed?",                                                                                                        "scoring": "1 point",   "a": "12"                     },
         {  "q": "Which plant, beginning with P is associated with Christmas?",                                                                       "scoring": "1 point",   "a": "Poinsettia"             },
         {  "q": "What are the most two popular phrases on Christmas gift cards?",                                                                    "scoring": "1 point",   "a": "to and from"            },
         {  "q": "In the novel A Christmas Carol, which is the first ghost to visit Scrooge?",                                                        "scoring": "1 point",   "a": "Jacob Marley"           }
      ]
   },{
      "snow": false,
      "date": "2021-01-24",
      "theme": "My Nephews' maths homework",
      "questions": [
         {  
            "q": "Which formulae correcly descirbes this linear series?<br><blockquote><code>13, 10, 7, 4, 1</code></blockquote>",
            "choices": ["<code>T<sub>n</sub> = -3n+16</code>","<code>T<sub>n</sub> = -3n+10</code>","<code>T<sub>n</sub> = 3n+11</code>","<code>T<sub>n</sub> = 3n-15</code>"],
            "scoring": "2 points",
            "a": "<code>T<sub>n</sub> = -3n+16</code>"
         },
         {  
            "q": "What is <code>v</code>?<br><blockquote><code>4(3v - 9) = 96</code></blockquote>",
            "choices": ["<code>11</code>","<code>-11</code>","<code>7</code>","<code>9</code>"],
            "scoring": "2 points",
            "a": "<code>11</code>"
         },
         {  
            "q": "What is <code>x</code>?<br><blockquote><code>2(x + 5) + 3(6x + 6) = 8</code></blockquote>",
            "choices": ["<code>-3</code>","<code>-2</code>","<code>-1</code>","<code>1</code>"],
            "scoring": "2 points",
            "a": "<code>-1</code>"
         },
         {  
            "q": "What is the height of this table?<br><img src='images/mathshomework/table-problem.png'>",
            "choices": ["<code>3 cats</code>","<code>150cm</code>","<code>3 cats + 2 turtles</code>","<code>160cm</code>"],
            "scoring": "2 points",
            "a": "<code>150cm</code>"
         },
         {  
            "q": "Can you solve this?<br><table><tr><td rowspan='2'><code>9 - 3 ÷&nbsp;</code></td><td style='border-bottom: 4px solid white;'><code>1</code></td><td rowspan='2'><code>&nbsp;+ 1 =</code></td></tr><tr><td><code>3</code></td></tr></table>",
            "choices": ["<code>1</code>","<code>-1</code>","<code>3</code>","<code>9</code>"],
            "scoring": "2 points",
            "a": "<code>1</code>"
         }
      ]
   },{
      "snow": false,
      "template":"answer-50",
      "date": "2021-02-28",
      "theme": "80's Chocolate bar puzzle ",
      "questions": [
         { "q": "<img src='images/80s-chocolate-bars/cadburys-star-bar.redacted.png'>",      "scoring": "1 point", "a": "Cadbury's Star Bar           <br /><img src='images/80s-chocolate-bars/cadburys-star-bar.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/rowntrees-lion-bar.redacted.png'>",     "scoring": "1 point", "a": "Rowntree's Lion Bar          <br /><img src='images/80s-chocolate-bars/rowntrees-lion-bar.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/mackintoshs-rolo.redacted.png'>",       "scoring": "1 point", "a": "Mackintosh's Rolo            <br /><img src='images/80s-chocolate-bars/mackintoshs-rolo.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/nestle-milky-bar.redacted.png'>",       "scoring": "1 point", "a": "Nestle Milky Bar             <br /><img src='images/80s-chocolate-bars/nestle-milky-bar.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/terrys-pyramint.redacted.png'>",        "scoring": "1 point", "a": "Terry's Pyramint             <br /><img src='images/80s-chocolate-bars/terrys-pyramint.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/cadburys-curly-wurly.redacted.png'>",   "scoring": "1 point", "a": "Cadbury's Curly Wurly        <br /><img src='images/80s-chocolate-bars/cadburys-curly-wurly.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/polo.redacted.png'>",                   "scoring": "1 point", "a": "Polo                         <br /><img src='images/80s-chocolate-bars/polo.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/mars-bounty-red.redacted.png'>",        "scoring": "1 point", "a": "Mars Bounty (dark chocolate) <br /><img src='images/80s-chocolate-bars/mars-bounty-red.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/cadburys-crunchie.redacted.png'>",      "scoring": "1 point", "a": "Cadbury's Crunchie           <br /><img src='images/80s-chocolate-bars/cadburys-crunchie.png'>" },
         { "q": "<img src='images/80s-chocolate-bars/cadburys-double-decker.redacted.png'>", "scoring": "1 point", "a": "Cadbury's Double Decker      <br /><img src='images/80s-chocolate-bars/cadburys-double-decker.png'>", "cite":"<a href='images/80s-chocolate-bars/puzzle.jpg'>see the full puzzle</a></p>" }
      ]
   },{
      "snow": false,
      "template":"answer-20",
      "date": "2021-03-28",
      "theme": "Six degrees of Bacon",
      "questions": [
         { 
            "q": "<a href='https://en.wikipedia.org/wiki/Jonny_Lee_Miller'>Jonny Lee Miller</a> and <a href='https://en.wikipedia.org/wiki/Angelina_Jolie'>Angelina Jolie</a> have a <cite>Bacon Number</cite> of one, as they have appeared together in the same film.<br />Name the first film they appeared in together?",
            "scoring": "1 point",
            "a": "<a href='https://en.wikipedia.org/wiki/Hackers_(film)'>Hackers</a>"
         },{ 
            "q": "Another actor that <a href='https://en.wikipedia.org/wiki/Angelina_Jolie'>Angelina Jolie</a> has been married to is <a href='https://en.wikipedia.org/wiki/Brad_Pitt'>Brad Pitt</a>.<br />Name the first film they have appeared in together?",
            "scoring": "1 point",
            "a": "<a href='https://en.wikipedia.org/wiki/Mr._%26_Mrs._Smith_(2005_film)'>Mr &amp; Mrs Smith</a>",
         },{
            "q": "Which sequal heist comedy film links <a href='https://en.wikipedia.org/wiki/Brad_Pitt'>Brad Pitt</a> and <a href='https://en.wikipedia.org/wiki/Catherine_Zeta-Jones'>Catherine Zeta-Jones</a>",
            "scoring": "1 point",
            "a": "<a href='https://en.wikipedia.org/wiki/Ocean%27s_Twelve'>Ocean's Twelve</a>"
         },{
            "q": "<a href='https://en.wikipedia.org/wiki/Catherine_Zeta-Jones'>Catherine Zeta-Jones</a> co-starred in <a href='https://en.wikipedia.org/wiki/Entrapment_(film)'>Entrapment</a> along side which recently deceased Actor?",
            "scoring": "1 point",
            "a": "<a href='https://en.wikipedia.org/wiki/Sean_Connery'>Sean Connery</a>"
         },{
            "q": "The same Scottish actor starred in the first instalment of the Jack Ryan film series, alongside <a href='https://en.wikipedia.org/wiki/Tim_Curry'>Tim Curry</a>.<br/>Name the film?",
            "a": "<a href='https://en.wikipedia.org/wiki/The_Hunt_for_Red_October_(film)'>The Hunt for Red October</a>",
            "scoring": "1 point"
         },{
            "q": "In 1985 <a href='https://en.wikipedia.org/wiki/Tim_Curry'>Tim Curry</a> played Wadsworth in which black comedy murder-mystery film?",
            "a": "<a href='https://en.wikipedia.org/wiki/Clue_(film)'>Clue</a>",
            "scoring": "1 point"
         },{
            "q": "In the same film the character Proffessor Plum was played by an actor who also appeared in a series of <cite>time adventure</cite> films.  Name the actor?",
            "a": "<a href='https://en.wikipedia.org/wiki/Christopher_Lloyd'>Christopher Lloyd</a>",
            "scoring": "1 point"
         },{
            "q": "In that film series, the characters spouse in the final film is played by <a href='https://en.wikipedia.org/wiki/Mary_Steenburgen'>Mary Steenburgen</a>, but what Actor is she married to?",
            "a": "<a href='https://en.wikipedia.org/wiki/Ted_Danson'>Ted Danson</a>",
            "scoring": "1 point"
         },{
            "q": "<a href='https://en.wikipedia.org/wiki/John_Ratzenberger'>John Ratzenberger</a> (who started in a long running tv series with that actor) provides the voice for <a href='https://en.wikipedia.org/wiki/List_of_Toy_Story_characters#Hamm'>Hamm</a> in which film series?",
            "a": "<a href='https://en.wikipedia.org/wiki/Toy_Story_(franchise)'>Toy Story</a>",
            "scoring": "1 point",
            "cite":"Ted Danson and John Ratzenberger both starred in the TV comedy Cheers (as Sam the bartender and Cliff the mail carrier & barstool regular, respectively)."
         },{
            "q": "<a href='https://en.wikipedia.org/wiki/Tom_Hanks'>Tom Hanks</a> and <a href='https://en.wikipedia.org/wiki/Bill_Paxton'>Bill Paxton</a> appeared at the flight crew in <a href='https://en.wikipedia.org/wiki/Apollo_13_(film)'>Apollo 13</a>.  Which actor played the third member of the crew?",
            "a": "<a href='https://en.wikipedia.org/wiki/Kevin_Bacon'>Kevin Bacon</a>",
            "scoring": "1 point",
            "cite":"Tom Hanks also provided the voice for a character, Woody, in the Toy Story films.  The <cite>Six degrees of Bacon</cite> or <cite>Bacon Number</cite>, is named after Kevin Bacon who has appeared along side a large number of actors."
         }
      ]
   },{
      "snow": false,
      "template":"answer-40",
      "date": "2021-05-02",
      "theme": "London Transport",
      "questions": [
         /*
         {
            "q": "<table style='font-size:0.75em;'><tr><td><img src='images/fence/fenceparts.png'></td><td>Connect the named items below to the numbered points on the fence<ul><li>Gravel board</li><li>Copping</li><li>Arris / cant rail</li><li>Cap</li><li>Post</li><li>Feather edge</li></ul></td></tr></table>",
            "scoring": "1/2 point each; total 3 points",
            "a": "<table style='width:100%;'><tr><td><ol><li>Post</li><li>Cap</li><li>Copping</li></ol></td><td><ol><li value='4'>Feather edge</li><li>Arris / cant rail</li><li>Gravel board</li></ol></td></tr></table>"
         },
         */
         {
            "q":"Name the four 'sub-surface lines' of the London Undergroud network",
            "scoring": "1 point",
            "choices": ["District, Metropolitain, Circle, Hammersmith &amp; City", "Northern, Circle, Central, Waterloo &amp; City", "Jubilee, Victoria, Central, Bakerloo", "Hammersmith &amp City, Great Northern, Northern, Metropolitan"],
            "a": "District, Metropolitain, Circle, Hammersmith &amp; City"
         },{
            "q":"Which tube station is the odd one out and Why?<ul><li>Aldwych</li><li>Down Street</li><li>Latimer Road</li><li>York Road</li><ul>",
            "scoring": "1 point for the odd one out and 1 point for the reason",
            "a": "Latimer Road is the odd one out, as all the other stations were abandoned and closed many years ago!"
         },{
            "q":"There are two underground station names which contain all of the vowels.  Name one of them?",
            "scoring": "1 point",
            "a": "<ul><li>Mansion House</li><li>South Ealing</li>"
         },{
            "q":"Which London Undergroud station has the deepest platform below ground level?",
            "scoring": "1 point",
            "choices": ["Angel", "Hampsted", "London Bridge", "Waterloo" ],
            "a": "Hampsted",
            "cite": "Hampsted's southbound platform is 58.5m below ground level.  Waterloo has the deepest platform below sea level."
         },{
            "q":"Which line has the most stations?",
            "choices": ["District", "Central", "Northern", "Picadilly", "Waterloo &amp; City" ],
            "scoring": "1 point",
            "a":"District",
            "cite":"District: 60, Picadilly: 53, Northern: 50, Central Line: 49 &amp; W&amp;C: 2 stations."
         },{
            "q":"The Elizabeth line (crossrail 1) is penciled in to partly open later this year, but when was the central section orginally due to open?",
            "scoring": "1 point",
            "choices": ["December 2018", "April 2019", "Setpember 2018", "August 2019" ],
            "a":"December 2018"
         },{
            "q":"What is the estimated number of mice who live in the underground system?",
            "scoring": "1 point",
            "choices": ["100,000", "250,000", "500,000", "750,000"],
            "a":"500,000"
         },{
            "q":"At what station does the Underground <cite>run over</cite> the Overground?",
            "scoring": "1 point",
            "choices": ["Canada Water", "Whitechapel", "Highbury &amp; Islington", "Shepherd’s Bush" ],
            "a":"Whitechapel"
         },{
            "q":"What percent of the underground network is actually underground?",
            "scoring": "1 point",
            "choices":["45%", "55%", "65%", "75%"],
            "a":"45%"
         }
      ]
   },{
      "snow": false,
      "template":"answer-35",
      "date": "2021-05-02",
      "theme": "Fencing",
      "questions": [
         {
            "q": "<table style='font-size:0.75em;'><tr><td><img src='images/fence/fenceparts.png'></td><td>Connect the named items below to the numbered points on the fence<ul><li>Gravel board</li><li>Copping</li><li>Arris / cant rail</li><li>Cap</li><li>Post</li><li>Feather edge</li></ul></td></tr></table>",
            "scoring": "1/2 point each; total 3 points",
            "a": "<table style='width:100%;'><tr><td><ol><li>Post</li><li>Cap</li><li>Copping</li></ol></td><td><ol><li value='4'>Feather edge</li><li>Arris / cant rail</li><li>Gravel board</li></ol></td></tr></table>"
         },
      ]
   },{
      "snow": false,
      "template":"answer-15",
      "date": "2021-04-07",
      "theme": "Famous fine art",
      "questions": [
         { "q": "<img src='images/fineart/Claude Monet - The Japanese Bridge.jpg'>",                                    "scoring":"Name the Artist (½ point) and either the Painting or the Series it is from (½ point)",  "a": "Claude Monet - The Japanese Bridge, from the Water Lily paintings" },
         { "q": "<img src='images/fineart/Edvard Munch - The Scream.jpg'>",                                             "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Edvard Munch - The Scream" },
      // { "q": "<img src='images/fineart/Frans Hals - Laughing Cavalier.jpg'>",                                        "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Frans Hals - Laughing Cavalier" },
         { "q": "<img src='images/fineart/Georges Seurat - A Sunday Afternoon on the Island of La Grande Jatte.jpg'>",  "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Georges Seurat - A Sunday Afternoon on the Island of La Grande Jatte" },
      // { "q": "<img src='images/fineart/Grant Wood - American Gothic.jpg'>",                                          "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Grant Wood - American Gothic" },
         { "q": "<img src='images/fineart/Gustav Klimt - The Kiss.jpg'>",                                               "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Gustav Klimt - The Kiss" },
         { "q": "<img src='images/fineart/Johannes Vermeer - The Girl With A Pearl Earring.jpg'>",                      "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Johannes Vermeer - The Girl With A Pearl Earring" },
         { "q": "<img src='images/fineart/John Constable - The Hay Wain.jpg'>",                                         "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "John Constable - The Hay Wain" },
         { "q": "<img src='images/fineart/L. S. Lowry - Going to Work.jpeg'>",                                          "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "L. S. Lowry  - Going to Work" },
         { "q": "<img src='images/fineart/M.C.Escher - Relativity.jpg'>",                                               "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "M.C.Escher - Relativity" },
         { "q": "<img src='images/fineart/Rembrandt - The Night Watch.jpg'>",                                           "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Rembrandt - The Night Watch" },
      // { "q": "<img src='images/fineart/Salvador Dali - The Persistence of Memory.jpg'>",                             "scoring":"Name the Artist (½ point) and the Painting (½ point)",                                  "a": "Salvador Dali - The Persistence of Memory" },
         { "q": "<img src='images/fineart/Van Gogh - Sunflowers series.jpg'>",                                          "scoring":"Name the Artist (½ point) and the Series it is from (½ point)",                         "a": "Van Gogh - Sunflowers series" }
      ]
   },{
      "snow": true,
      "template":"single",
      "date": "2021-11-28",
      "theme": "Internet scavenger hunt (open book exam)",
      "questions": [
         {
            "q": "<ol><li>Who is/was the longest serving monarch?</li><li>Which country has more kilometres of roads, India or China?</li><li>When was the last recorded sighting of a Dodo?</li><li>What date was the oldest building in the UK built?</li><li>Of the colours in the visible light spectrum (Red, Orange, Yellow etc) which covers the widest spectrum?</li><li>Which global company is the most profitable?</li><li>In which UK city is the Channel 4 game show countdown filmed?</li><li>The London Eye can claim title to specifically the world’s tallest what?</li><li>In 2019/2020 which was National trust property was the most visited?</li><li>What title was Roald Dahl's first published children’s book?</li></ol><div class='note'>You have five minutes, your time started when you read the first question!</div>",
            "scoring": "1 point each",
            "a": "<ol type='a'><li>Louis XIV (72 Years 110 days)</li><li>India (6,215,797 Km) China (5,198,000)</li><li>1662</li><li>3700 bce &quot;Knap of Howar&quot;</li><li>Red</li><li>Microsoft</li><li>Salford, Manchester </li><li>The world's tallest cantilevered observation wheel</li><li>Giant's Causeway</li><li>The Gremlins</li></ol>"
         },
      ]
   },{
      "snow": true,
      "date": "2023-12-30",
      "theme": "My end of year Christmas quiz",
      "questions": [
         {
            "q": "Name the traditional Christmas carol with this verse<blockquote><cite><small>E'en so here below, below<br />Let steeple bells be swungen<br />And i-o, i-o, i-o<br />By priest and people be sungen<br />Gloria, Hosanna in excelsis</small></blockquote>",
            "scoring": "1 point",
            "a": "<a href='https://en.wikipedia.org/wiki/Ding_Dong_Merrily_on_High#Text_and_melody'>Ding Dong Merrily on High</a>"
         },{
            "q": "Name the traditional Christmas song with this verse<blockquote><cite><small>A day or two ago<br />I thought I'd take a ride<br />And soon, Miss Fanny Bright<br />Was seated by my side,<br />The horse was lean and lank<br />Misfortune seemed his lot<br />He got into a drifted bank<br />And then we got upsot</small></blockquote>",
            "scoring": "1 point",
            "a": "<a href='https://en.wikipedia.org/wiki/Jingle_Bells#Lyrics'>Jingle Bells</a>"
         },
         {  "q": "What do Japanese people traditionally eat on chrismas day?",                                                                   "scoring": "1 point",   "a": "KFC"                         },
         {  "q": "What has been the worlds heavest Christmas present?<details><summary>Clue</summary>France gave it to the US</details>",    "scoring": "1 point",   "a": "The Statue of Liberty"       },
         {  "q": "Which country created Eggnog?",                                                                                                "scoring": "1 point",   "a": "England"                     },
         {  "q": "Guess the Christmas songs or carols<ol type='a'><li>🎉➡️🌍<li>🔔🔔🤘<li>🚗🏠🎄</ul>",                                      "scoring": "1 point each",   "a": "<ol type='a'><li>Joy to the world<li>Jingle bell rock<li>Driving home for christmas</ol>"             },
         {  "q": "Which animal will represent the Chinese New Year 2024?",                                                                        "scoring": "1 point",   "a": "Dragon"       },
         {  "q": "Paris will host the 2024 summer Olympic games. In which year did Paris last host the games?",                                   "scoring": "1 point",   "a": "1924"       },
      ]
   },{
      "snow": true,
      "date": "2025-01-02",
      "theme": "My end of year 2024 quiz",
      "questions": [
         {
            "q":        "How man days did Roald Amundsen trek in 1911 to become the first person to reach the South pole?",
            "scoring":  "Closest team wins 1 point",
            "a":        "<code>58</code>"
         },{
            "q":        "<strong>Complete the Christmas cracker joke</strong><p><cite>What happened to the burglar who robbed an advent calendar factory?</cite></p>",
            "scoring":  "1 point",
            "a":        "He got 24 days!"
         },{
            "q":        "How many people watched the King's first Christmas message on BBC One in 2022?",
            "scoring":  "Closest team wins 1 point",
            "a":        "<code>9.46</code> million"
         },{
            "q":        "<strong>Complete the Christmas cracker joke</strong><p><cite>What do you get if you cross a turkey and a centipede</cite></p>",
            "scoring":  "1 point",
            "a":        "Drumsticks for everyone!"
         },{
            "q":        "According to an Office for National Statistics approximation, UK supermarkets sell how many individual Brussels sprouts at Christmas time?",
            "scoring":  "Closest team wins 1 point",
            "a":        "<code>750</code> million"
         },{
            "q":        "<strong>Complete the Christmas cracker joke</strong><p><cite>What was the snowman doing in the vegatable patch</cite></p>",
            "scoring":  "1 point",
            "a":        "He was picking his nose!"
         },{
            "q":        "According to the RSPB, how many swans are there in the UK?",
            "scoring":  "Closest team wins 1 point",
            "a":        "<code>76,350</code>"
         },{
            "q":        "<strong>Complete the Christmas cracker joke</strong><p><cite>What athlete is warmest during winter?</cite></p>",
            "scoring":  "1 point",
            "a":        "A long jumper!"
         },{
            "q":        "In total, UK consumers use how many miles / km of wrapping paper each year?<br />(to the nearest 1000)",
            "scoring":  "Closest team wins 1 point",
            "a":        "<br /><code>227,000</code> miles<br /><code>365,000</code> km"
         },{
            "q":        "<strong>Complete the Christmas cracker joke</strong><p><cite>How does Good King Wenceslas like his pizza?</cite></p>",
            "scoring":  "1 point",
            "a":        "Deep pan, crisp and even!"
         }
      ]
   }
];
console.debug("questionSets", questionSets);
